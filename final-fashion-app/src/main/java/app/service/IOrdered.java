package app.service;

import app.dto.OrderDTO;
import app.response.Response;

public interface IOrdered {
	Response<OrderDTO> addProduct(OrderDTO orderDTO);

	Response<OrderDTO> removeProductFromCart(OrderDTO orderDTO);
}
