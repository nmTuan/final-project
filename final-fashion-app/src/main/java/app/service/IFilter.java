package app.service;

import app.dto.PaymentDTO;
import app.entity.AgeEntity;
import app.entity.CategoryEntity;
import app.entity.GenderEntity;
import app.response.ResponseList;

public interface IFilter {
	ResponseList<CategoryEntity> getListCategory();
	
	ResponseList<AgeEntity> getListAge();
	
	ResponseList<GenderEntity> getListGender();
	
	ResponseList<PaymentDTO> getListPayment();
}
