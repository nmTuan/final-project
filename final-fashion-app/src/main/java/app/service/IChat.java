package app.service;

import app.dto.ChatContentDTO;
import app.dto.CustomerDTO;
import app.dto.RoomDTO;
import app.dto.ShopDTO;
import app.response.Response;
import app.response.ResponseList;

public interface IChat {

	Response<CustomerDTO> getListRoomChatByCust(Long custId, int page, int size);
	
	Response<String> updateStatusRoomCust(Long roomId, int status);

	Response<ShopDTO> getListRoomChatByShop(Long shopId, int page, int size);
	
	Response<String> updateStatusRoomShop(Long roomId, int status);

	ResponseList<ChatContentDTO> getListContentByRoom(Long roomId, int page, int size);
	
	Response<RoomDTO> getRoomByCustShop(Long custId, Long shopId);
	
	Response<Integer> countChatByStatusShop(Long shopId, int status);
	
	Response<Integer> countChatByStatusCust(Long custId, int status);

}
