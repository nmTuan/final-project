package app.service;

import app.dto.RevenueYearDTO;
import app.entity.RevenueYearEntity;
import app.response.Response;
import app.response.ResponseList;

public interface IRevenue {
	void createMonthRevenue(RevenueYearEntity revenueYearEntity);
	
	void updateRevenue(String month, String year, Long shopId, int revenue);
	
	ResponseList<RevenueYearDTO> getListYearByShopId(Long shopId);
	
	Response<RevenueYearDTO> getListMonthByYear(Long yearId);
}
