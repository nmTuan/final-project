package app.service;

import app.dto.AccountDTO;
import app.dto.CustomerDTO;
import app.dto.OrderDTO;
import app.response.Response;
import app.response.ResponseList;

public interface ICustomer {
	Response<CustomerDTO> saveInforCust(CustomerDTO customerDTO);
	
	Response<CustomerDTO> checkPhoneNumber(CustomerDTO customerDTO); 

	Response<CustomerDTO> findCust(AccountDTO accountDTO);

	Response<CustomerDTO> updateUserInfo(CustomerDTO cutomerDto);
	
	Response<CustomerDTO> updatePhoneNumber(CustomerDTO customerDto);
	
	ResponseList<OrderDTO> getListProductCart(OrderDTO orderDTO);
}
