package app.service;

import app.dto.PaymentDTO;
import app.dto.product.ImageProductDTO;
import app.dto.product.ProductDTO;
import app.dto.product.ProductDetailDTO;
import app.dto.product.SizeColorDTO;
import app.response.Response;
import app.response.ResponseList;

public interface IProduct {
	
	//Product
	Response<ProductDetailDTO> save(ProductDetailDTO productDTO);

	ResponseList<ProductDTO> findProductByName(ProductDTO productDTO, int page, int size);

	Response<ProductDetailDTO> getProductDetail(ProductDTO productDTO);

	Response<ProductDetailDTO> updateProduct(ProductDetailDTO productDetail);
	
	Response<ProductDTO> deleteProduct(ProductDTO productDTO);
	
	// Image Product
	Response<ProductDetailDTO> addNewListImage(ProductDetailDTO productDTO);
	
	Response<ProductDetailDTO> updateImageProduct(ImageProductDTO imageDTO);
	
	Response<ProductDetailDTO> deleteImage(ImageProductDTO imageDTO);
	
	//Size color product
	Response<ProductDetailDTO> addNewListSizeColor(ProductDetailDTO productDTO);
	
	Response<ProductDetailDTO> updateSizeColorProduct(SizeColorDTO sizeColorDTO);
	
	Response<ProductDetailDTO> deleteSizeColor(SizeColorDTO sizeColorDTO);
	
	ResponseList<PaymentDTO> getListPaymentBySizeProduct(Long id);
}
