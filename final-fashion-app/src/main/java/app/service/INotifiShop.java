package app.service;

import app.dto.NotificationShopDTO;
import app.dto.ShopDTO;
import app.entity.ShopEntity;
import app.response.Response;

public interface INotifiShop {
	void addNewNotification(NotificationShopDTO notifiShopDTO, ShopEntity shopEntity);

	Response<ShopDTO> getListNotifiByShopId(Long shopId, int page, int size);

	Response<String> updateStatusNotifiShop(NotificationShopDTO notificationShopDTO);
	
	Response<Integer> countNotification(Long shopId, int status);
}
