package app.service;

import app.dto.BillDTO;
import app.dto.ProductInBillDTO;
import app.response.Response;
import app.response.ResponseList;

public interface IBIll {
	Response<BillDTO> save(BillDTO billDTO);

	ResponseList<BillDTO> findBillByCustId(BillDTO billDTO);
	
	ResponseList<ProductInBillDTO> findProductByStatusCust(ProductInBillDTO productInBillDTO);
	
	Response<ProductInBillDTO> getBillDetail(ProductInBillDTO productInBillDTO);
	
	Response<ProductInBillDTO> updateStatusProductCust(ProductInBillDTO productInBillDTO);
	
	Response<ProductInBillDTO> updateStatusProductShop(ProductInBillDTO productInBillDTO);
	
	ResponseList<ProductInBillDTO> findProductByStatusShop(ProductInBillDTO productInBillDTO);
}
