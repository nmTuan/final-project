package app.service;

import app.dto.ShopDTO;
import app.dto.product.ProductDTO;
import app.entity.FeedbackEntity;
import app.response.Response;
import app.response.ResponseList;

public interface IShop {
	Response<ShopDTO> save(ShopDTO shopDTO);

	Response<ShopDTO> getShopInfoById(Long id);

	ResponseList<ShopDTO> getShopInfoByShopname(String name, int page, int size);

	Response<ShopDTO> updateShopInfo(ShopDTO shopDTO);
	
	Response<ShopDTO> getShopInfoByAccId(Long accId);
	
	Response<String> checkPhoneNumber(ShopDTO shopDTO);
	
	ResponseList<ProductDTO> getListProduct(ProductDTO productDTO, int page, int size);
	
	void updateVote(FeedbackEntity feedbackEntity);
}
