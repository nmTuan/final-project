package app.service;

import app.dto.BankCustDTO;
import app.dto.BankShopDTO;
import app.entity.BankEntity;
import app.response.Response;
import app.response.ResponseList;

public interface IBank {
	
	ResponseList<BankEntity> findBankByName(String name);
	
	Response<BankShopDTO> saveBankShop(BankShopDTO bankShopDTO);
	
	ResponseList<BankShopDTO> getListBankByShopId(Long shopId);
	
	Response<String> deleteBankShop(int bankId);
	
	Response<BankCustDTO> saveCustShop(BankCustDTO bankCustDTO);
	
	ResponseList<BankCustDTO> getListBankByCustId(Long custId);
	
	Response<String> deleteBankCust(int bankId);
}
