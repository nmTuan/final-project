package app.service;

import app.dto.AddressDTO;
import app.response.ResponseList;

public interface IAddress {
	ResponseList<AddressDTO> findAllCity(String name, int page, int size);

	ResponseList<AddressDTO> findDistrictByCityCode(int code, String name, int page, int size);

	ResponseList<AddressDTO> findVillageByDistrictCode(int code, String name, int page, int size);
}
