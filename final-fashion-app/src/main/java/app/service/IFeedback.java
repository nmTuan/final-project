package app.service;

import app.dto.FeedbackDTO;
import app.response.Response;
import app.response.ResponseList;

public interface IFeedback {
	Response<String> createNewFeedback(FeedbackDTO feedbackDTO);

	Response<String> updateFeedback(FeedbackDTO feedbackDTO);

	ResponseList<FeedbackDTO> getListFeedbackByDetail(Long detailId, String vote, int page, int size);
}
