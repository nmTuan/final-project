package app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.dto.PaymentDTO;
import app.entity.AgeEntity;
import app.entity.CategoryEntity;
import app.entity.GenderEntity;
import app.entity.PaymentEntity;
import app.repository.AgeRepository;
import app.repository.CategoryRepository;
import app.repository.GenderRepository;
import app.repository.PaymentRepository;
import app.response.Responesive;
import app.response.ResponseList;
import app.service.IFilter;

@Service
public class Filter implements IFilter{
	
	@Autowired
	CategoryRepository categoryRepository;
	
	@Autowired
	AgeRepository ageReposity;
	
	@Autowired
	GenderRepository genderRepository;
	
	@Autowired
	PaymentRepository paymentRepository;
	
	@Autowired
	Responesive<CategoryEntity> resCategory;
	
	@Autowired
	Responesive<AgeEntity> resAge;
	
	@Autowired
	Responesive<GenderEntity> resGender;
	
	@Autowired
	Responesive<PaymentDTO> resPayment;
	
	@Override
	public ResponseList<CategoryEntity> getListCategory() {
		List<CategoryEntity> categoryEntities = categoryRepository.findAll();
		
		return resCategory.successResList("success", categoryEntities);
	}
	
	@Override
	public ResponseList<AgeEntity> getListAge() {
		List<AgeEntity> ageEntities = ageReposity.findAll();
		
		return resAge.successResList("success", ageEntities);
	}
	
	@Override
	public ResponseList<GenderEntity> getListGender() {
		List<GenderEntity> genderEntities = genderRepository.findAll();
		
		return resGender.successResList("success", genderEntities);
	}
	
	@Override
	public ResponseList<PaymentDTO> getListPayment() {
		List<PaymentEntity> paymentEntities = paymentRepository.findAll();
		List<PaymentDTO> paymentDTOs = new ArrayList<PaymentDTO>();
		for (PaymentEntity paymentEntity : paymentEntities) {
			PaymentDTO paymentDTO = new PaymentDTO();
			paymentDTO.setId(paymentEntity.getId());
			paymentDTO.setPayment(paymentEntity.getPayment());
			paymentDTOs.add(paymentDTO);
		}
		return resPayment.successResList("success", paymentDTOs);
	}
}
