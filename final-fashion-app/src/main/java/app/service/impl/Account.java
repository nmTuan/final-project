package app.service.impl;

import java.util.Collections;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import app.common.Constant;
import app.converter.AccountConverter;
import app.dto.AccountDTO;
import app.entity.AccountEntity;
import app.entity.RoleEntity;
import app.exception.AppException;
import app.repository.AccountRepository;
import app.repository.CustomerRepository;
import app.repository.RoleRepository;
import app.response.JwtAuthenticationReponse;
import app.response.Responesive;
import app.response.Response;
import app.security.JwtTokenProvider;
import app.security.UserPrincipal;
import app.service.IAccount;

@Service
public class Account implements IAccount, UserDetailsService {

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	AccountConverter accountConverter;

	@Autowired
	Responesive<AccountDTO> resAcc;

	@Autowired
	Responesive<?> res;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	JwtTokenProvider tokenProvider;

	@Override
	public Response<AccountDTO> saveAccount(AccountDTO accountDTO) {
		AccountEntity accountEntity = accountRepository.findOneByUsername(accountDTO.getUsername());
		if (accountEntity != null) {
			return new Response<AccountDTO>(202, "Tài khoản đã tồn tại");
		} else {
			RoleEntity roleEntity = roleRepository.findOneByRoleName(Constant.CUSTOMER)
					.orElseThrow(() -> new AppException("user role not set"));
			accountEntity = accountConverter.toAccountEntity(accountDTO);
			accountEntity.setPassword(passwordEncoder.encode(accountDTO.getPassword()));
			accountEntity.setIsShop(false);
			accountEntity.setRoles(Collections.singleton(roleEntity));
			accountEntity = accountRepository.save(accountEntity);

			AccountDTO resAccDTO = accountConverter.toAccountDTO(accountEntity);
			return resAcc.successRes("success", resAccDTO);
		}
	}

	@Override
	public ResponseEntity<?> login(AccountDTO accountDTO) {
		UserPrincipal userDetail = loadUserByUsername(accountDTO.getUsername());
		AccountEntity accountEntity = accountRepository.findOneByUsername(accountDTO.getUsername());
		if (userDetail != null) {
			if (userDetail.getPassword().equals(accountEntity.getPassword())) {
				try {
					Authentication authentication = authenticationManager
							.authenticate(new UsernamePasswordAuthenticationToken(accountDTO.getUsername(),
									accountDTO.getPassword()));
					SecurityContextHolder.getContext().setAuthentication(authentication);
					String jwt = tokenProvider.generateToken(authentication);
					return ResponseEntity.ok(new JwtAuthenticationReponse(jwt, userDetail.getId()));
				} catch (Exception e) {
					System.out.println("err " + e);
				}
			}
		}
		return ResponseEntity.ok(new Response<String>(400, "Username hoặc mật khẩu sai "));
	}

	@Override
	public Response<AccountDTO> updatePassword(AccountDTO accountDTO) {
		AccountEntity accountEntity = accountRepository.findOneById(accountDTO.getId());
		if (accountEntity == null) {
			return resAcc.successRes("User doesn't existed");
		} else {
			try {
				Authentication auth = authenticationManager.authenticate(
						new UsernamePasswordAuthenticationToken(accountEntity.getUsername(), accountDTO.getOldPassword()));
				SecurityContextHolder.getContext().setAuthentication(auth);
				String password = passwordEncoder.encode(accountDTO.getPassword());
				accountRepository.updatePassword(accountDTO.getId(), password);
				return resAcc.successRes("update success");
			} catch(Exception e) {
				System.out.println(e);
			}
			return resAcc.successRes("Mật khẩu hiện tại sai");
		}
	}

	@Override
	@Transactional
	public UserPrincipal loadUserByUsername(String username) throws UsernameNotFoundException {
		AccountEntity accountEntity = accountRepository.findOneByUsername(username);
		if (accountEntity == null) {
			return null;
		}
		return UserPrincipal.createPrincipal(accountEntity);
	}

	@Transactional
	public UserDetails loadUserById(Long id) throws UsernameNotFoundException {
		AccountEntity accountEntity = accountRepository.findOneById(id);
		if (accountEntity == null) {
			new UsernameNotFoundException("User not found");
		}

		return UserPrincipal.createPrincipal(accountEntity);
	}
}
