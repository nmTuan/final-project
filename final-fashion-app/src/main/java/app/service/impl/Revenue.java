package app.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.common.Common;
import app.converter.RevenueConverter;
import app.dto.RevenueMonthDTO;
import app.dto.RevenueYearDTO;
import app.entity.RevenueMonthEntity;
import app.entity.RevenueYearEntity;
import app.entity.ShopEntity;
import app.repository.RevenueMonthRepository;
import app.repository.RevenueYearRepository;
import app.repository.ShopRepository;
import app.response.Responesive;
import app.response.Response;
import app.response.ResponseList;
import app.service.IRevenue;

@Service
public class Revenue implements IRevenue {

	@Autowired
	RevenueMonthRepository revenueMonthRepository;

	@Autowired
	RevenueYearRepository revenueYearRepository;

	@Autowired
	ShopRepository shopRepository;

	@Autowired
	Responesive<RevenueYearDTO> responseYear;

	@Autowired
	Common common;

	@Autowired
	RevenueConverter revenueConverter;

	@Override
	public void createMonthRevenue(RevenueYearEntity revenueYearEntity) {
		for (int i = 1; i <= 12; i++) {
			RevenueMonthEntity revenueMonthEntity = new RevenueMonthEntity();
			String month = "Tháng " + i;
			revenueMonthEntity.setMonth(month);
			revenueMonthEntity.setRevenue(0);
			revenueMonthEntity.setRevenueYearEntity(revenueYearEntity);
			revenueMonthRepository.save(revenueMonthEntity);
		}
	}

	@Override
	public void updateRevenue(String month, String year, Long shopId, int revenue) {
//		System.out.println(shopId);
		RevenueMonthEntity revenueMonthEntity = revenueMonthRepository.findOneByMonth(month, year, shopId);
		int newRevenue = revenueMonthEntity.getRevenue() + revenue;
		revenueMonthRepository.updateRevenue(month, year, shopId, newRevenue);
	}

	@Override
	public ResponseList<RevenueYearDTO> getListYearByShopId(Long shopId) {
		ShopEntity shopEntity = shopRepository.findOneById(shopId);
		if (shopEntity != null) {
			Pageable pages = PageRequest.of(0, 5);
			String year = common.dateToString(new Date(), "y");
			getRevenueYearEntity(year, shopEntity);
			List<RevenueYearEntity> revenueYearEntities = new ArrayList<RevenueYearEntity>();
			List<RevenueYearDTO> revenueYearDTOs = new ArrayList<RevenueYearDTO>();
			revenueYearEntities = revenueYearRepository.findByShopIdPage(shopId, pages);
			for (RevenueYearEntity revenueYearEntity : revenueYearEntities) {
				RevenueYearDTO revenueYearDTO = revenueConverter.toRevenueYearDTO(revenueYearEntity);
				revenueYearDTOs.add(revenueYearDTO);
			}
			return responseYear.successResList("success", revenueYearDTOs);
		}
		return responseYear.successResList("Shop does not found");
	}

	@Override
	public Response<RevenueYearDTO> getListMonthByYear(Long yearId) {
		RevenueYearEntity revenueYearEntity = revenueYearRepository.findOneById(yearId);
		if(revenueYearEntity != null) {
			List<RevenueMonthDTO> revenueMonthDTOs = new ArrayList<RevenueMonthDTO>();
			for(RevenueMonthEntity revenueMonthEntity : revenueYearEntity.getRevenueMonthEntities()) {
				RevenueMonthDTO revenueMonthDTO = revenueConverter.toRevenueMonthDTO(revenueMonthEntity);
				revenueMonthDTOs.add(revenueMonthDTO);
			}
			RevenueYearDTO revenueYearDTO = revenueConverter.toRevenueYearDTO(revenueYearEntity);
			revenueYearDTO.setRevenueMonthDTOs(revenueMonthDTOs);
			return responseYear.successRes("success", revenueYearDTO);
		}
		return responseYear.successRes("ERROR");
	}

	public RevenueYearEntity getRevenueYearEntity(String year, ShopEntity shopEntity) {
		List<RevenueYearEntity> revenueYearEntities = new ArrayList<RevenueYearEntity>();
		revenueYearEntities = revenueYearRepository.findByShopId(shopEntity.getId());
		if (revenueYearEntities.size() > 0) {
			for (RevenueYearEntity revenueYearEntity : revenueYearEntities) {
				if (year.equals(revenueYearEntity.getYear())) {
					return revenueYearEntity;
				}
			}
		}
		RevenueYearEntity revenueYearEntity = new RevenueYearEntity();
		revenueYearEntity.setYear(year);
		revenueYearEntity.setRevenueShop(shopEntity);
		revenueYearEntity = revenueYearRepository.save(revenueYearEntity);
		createMonthRevenue(revenueYearEntity);
		return revenueYearEntity;
	}
}
