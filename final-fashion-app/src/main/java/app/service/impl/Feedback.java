package app.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.common.Common;
import app.converter.FeedbackConverter;
import app.dto.FeedbackDTO;
import app.dto.NotificationShopDTO;
import app.dto.product.ProductDetailDTO;
import app.entity.CustomerEntity;
import app.entity.FeedbackEntity;
import app.entity.ProductDetailEntity;
import app.entity.ProductInBillEntity;
import app.entity.ShopEntity;
import app.entity.SizeColorEntity;
import app.repository.CustomerRepository;
import app.repository.FeedbackRepository;
import app.repository.ProductBillRepository;
import app.repository.ProductDetailRepository;
import app.repository.SizeColorRepository;
import app.response.Responesive;
import app.response.Response;
import app.response.ResponseList;
import app.service.IFeedback;

@Service
public class Feedback implements IFeedback {

	@Autowired
	ProductBillRepository productBillRepository;

	@Autowired
	ProductDetailRepository productDetailRepository;

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	SizeColorRepository sizeColorRepository;

	@Autowired
	FeedbackRepository feedbackRepository;

	@Autowired
	FeedbackConverter feedbackConverter;

	@Autowired
	NotifiShop notifiShop;

	@Autowired
	Responesive<String> res;

	@Autowired
	Responesive<ProductDetailDTO> resProduct;
	
	@Autowired
	Responesive<FeedbackDTO> resFeedback;

	@Autowired
	Common common;

	@Autowired
	Shop shop;

	@Override
	public Response<String> createNewFeedback(FeedbackDTO feedbackDTO) {
		ProductInBillEntity productInBillEntity = productBillRepository.findOneById(feedbackDTO.getProductBillId());
		CustomerEntity customerEntity = customerRepository.findOneById(feedbackDTO.getCustId());
		if (productInBillEntity != null) {
			if (productInBillEntity.getBill().getCustomerBill().getId() == customerEntity.getId()
					&& productInBillEntity.getStatus() == 3) {
				SizeColorEntity sizeColorEntity = sizeColorRepository.findOneById(productInBillEntity.getSizeId());
				Boolean checkFeedback = checkFeedback(feedbackDTO);
				if (checkFeedback) {
					FeedbackEntity feedbackEntity = feedbackConverter.toFeedbackEntity(feedbackDTO);
					Timestamp createdDate = common.dateToTimestamp(new Date());
					// create feedback
					feedbackEntity.setCreatedDate(createdDate);
					feedbackEntity.setCustomerFeedback(customerEntity);
					feedbackEntity.setProductFeedback(sizeColorEntity.getDetail());
					feedbackEntity = feedbackRepository.save(feedbackEntity);

					// Update vote
					shop.updateVote(feedbackEntity);

					// Notification
					Long postId = feedbackEntity.getId();
					String thumbnail = customerEntity.getAvatar();
					String title = "Đánh giá";
					String content = "Khách hàng " + customerEntity.getFullName() + " đánh giá";
					ShopEntity shopEntity = sizeColorEntity.getDetail().getProduct().getShopProduct();
					NotificationShopDTO notificationShopDTO = new NotificationShopDTO(postId, thumbnail, title, content,
							createdDate, 0);
					notifiShop.addNewNotification(notificationShopDTO, shopEntity);
					return res.successRes("Success");
				}
				return res.successRes("The user is already feedback");
			}
			return res.successRes("Can not send feedback");
		}
		return res.successRes("Not found");
	}

	@Override
	public Response<String> updateFeedback(FeedbackDTO feedbackDTO) {
		FeedbackEntity feedbackEntity = feedbackRepository.findOneById(feedbackDTO.getId());
		if (feedbackEntity != null) {
			if (feedbackEntity.getCustomerFeedback().getId() == feedbackDTO.getCustId()) {
				if (feedbackDTO.getContent().length() > 0) {
					Timestamp modifiedDate = common.dateToTimestamp(new Date());
					feedbackRepository.updateFeedback(feedbackDTO.getId(), feedbackDTO.getContent(), modifiedDate);
					return res.successRes("Success");
				}
				return res.successRes("The content must not empty");
			}
			return res.successRes("You can not edit");
		}
		return res.successRes("Not found");
	}

	@Override
	public ResponseList<FeedbackDTO> getListFeedbackByDetail(Long detailId, String vote, int page, int size) {
		ProductDetailEntity productDetailEntity = productDetailRepository.findOneById(detailId);
		if (productDetailEntity != null) {
			Pageable pages = PageRequest.of(page, size);
			List<FeedbackEntity> feedbackEntities = feedbackRepository.findByDetailId(detailId, vote, pages);
			int countFeedback = feedbackRepository.countFeedback(detailId);
			List<FeedbackDTO> feedbackDTOs = new ArrayList<FeedbackDTO>();
			for (FeedbackEntity feedbackEntity : feedbackEntities) {
				FeedbackDTO feedbackDTO = feedbackConverter.toFeedbackDTO(feedbackEntity);
				feedbackDTOs.add(feedbackDTO);
			}
			return resFeedback.successResList("success", countFeedback, feedbackDTOs);
		}
		return resFeedback.successResList("Not found");
	}

	public Boolean checkFeedback(FeedbackDTO feedbackDTO) {
		FeedbackEntity feedbackEntity = feedbackRepository.findOneByBillId(feedbackDTO.getProductBillId());
		if (feedbackEntity != null) {
			return false;
		}
		return true;
	}
}
