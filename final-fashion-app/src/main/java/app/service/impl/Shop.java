package app.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.common.Common;
import app.common.Constant;
import app.config.OtpConfig;
import app.converter.ProductConverter;
import app.converter.ShopConverter;
import app.dto.ShopDTO;
import app.dto.product.ProductDTO;
import app.entity.AccountEntity;
import app.entity.FeedbackEntity;
import app.entity.ItemProductEntity;
import app.entity.RoleEntity;
import app.entity.ShopEntity;
import app.exception.AppException;
import app.repository.AccountRepository;
import app.repository.ProductRepository;
import app.repository.RoleRepository;
import app.repository.ShopRepository;
import app.response.Responesive;
import app.response.Response;
import app.response.ResponseList;
import app.service.IShop;

@Service
public class Shop implements IShop {

	@Autowired
	ShopRepository shopRepository;

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	ProductRepository productRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	ShopConverter shopConverter;

	@Autowired
	ProductConverter productConverter;

	@Autowired
	Responesive<ShopDTO> res;

	@Autowired
	Responesive<ProductDTO> resProduct;

	@Autowired
	Common common;
	
	@Autowired
	OtpConfig otpConfig;

	@Override
	public Response<ShopDTO> save(ShopDTO shopDTO) {
		AccountEntity accountEntity = accountRepository.findOneById(shopDTO.getAccId());
		if (accountEntity != null) {
			ShopEntity shopEntityCheck = shopRepository.findOneByPhoneNumber(shopDTO.getPhoneNumber());
			if (accountEntity.getIsShop()) {
				return res.successRes("This user already register shop");
			} else if (shopDTO.getAddress().isEmpty() || shopDTO.getDescription().isEmpty()
					|| shopDTO.getPhoneNumber().isEmpty() || shopDTO.getShopName().isEmpty()) {
				return res.successRes("Invalidate");
			} else if (shopEntityCheck != null) {
				return res.successRes("this phone number is already registed");
			} else {
				ShopEntity shopEntity = new ShopEntity();
				RoleEntity roleEntity = roleRepository.findOneByRoleName(Constant.SHOP)
						.orElseThrow(() -> new AppException("user role not set"));
				shopDTO.setVote(0);
				shopEntity = shopConverter.toShopEntity(shopDTO);
				shopEntity.setShopCode("FA" + new Date().getTime());
				shopEntity.setAccShop(accountEntity);
				
				//
				accountEntity.getRoles().add(roleEntity);
				accountEntity.setRoles(accountEntity.getRoles());
				shopEntity = shopRepository.save(shopEntity);
				accountRepository.updateStatusShop(shopDTO.getAccId(), true);
				
				return res.successRes("Success", shopDTO);
			}
		}
		return res.successRes("User doesn't existed");
	}

	@Override
	public Response<ShopDTO> getShopInfoById(Long id) {
		ShopEntity shopEntity = shopRepository.findOneById(id);
		if (shopEntity == null) {
			return res.successRes("Shop doesn't existed");
		}
		ShopDTO shopDto = shopConverter.toShopDto(shopEntity);
		shopDto.setSocketId(shopEntity.getSocketId());
		return res.successRes("succes", shopDto);
	}
	
	@Override
	public Response<ShopDTO> getShopInfoByAccId(Long accId) {
		ShopEntity shopEntity = shopRepository.findOneByAccId(accId);
		if (shopEntity == null) {
			return new Response<ShopDTO>(404, "không tìm thấy shop");
		}
		ShopDTO shopDTO = shopConverter.toShopDto(shopEntity);
		shopDTO.setAccId(accId);
		return res.successRes("success", shopDTO);
	}

	@Override
	public ResponseList<ShopDTO> getShopInfoByShopname(String name, int page, int size) {
		List<ShopEntity> shopEntities = new ArrayList<ShopEntity>();
		List<ShopDTO> shopDtos = new ArrayList<>();
		Pageable pages = PageRequest.of(page, size);
		int sizeAll = shopRepository.countShopByName(name);
		shopEntities = shopRepository.findByShopName(name, pages);
		for (ShopEntity shop : shopEntities) {
			ShopDTO shopDTO = shopConverter.toShopDto(shop);
			shopDtos.add(shopDTO);
		}
		return res.successResList("succes", sizeAll, shopDtos);
	}

	@Override
	public Response<ShopDTO> updateShopInfo(ShopDTO shopDTO) {
		ShopEntity shopEntity = shopRepository.findOneById(shopDTO.getId());
		ShopEntity shopEntity2 = shopRepository.findOneByPhoneNumber(shopDTO.getPhoneNumber());
		if (shopEntity == null) {
			return res.successRes("Shop is not existed");
		} else if (shopEntity2 != null) {
			return res.successRes("The phone number is already registed");
		} else {
			String address = shopDTO.getAddress();
			String avatar = shopDTO.getAvatar();
			String coverava = shopDTO.getCoverave();
			String phonenumber = shopDTO.getPhoneNumber();
			String shopname = shopDTO.getShopName();
			String description = shopDTO.getDescription();
			Long id = shopDTO.getId();
			shopRepository.updateUserInfo(address, avatar, coverava, shopname, phonenumber, description, id);
			return res.successRes("update success");
		}
	}
	
	@Override
	public Response<String> checkPhoneNumber(ShopDTO shopDto) {
		ShopEntity shopEntity = shopRepository.findOneByPhoneNumber(shopDto.getPhoneNumber());
		if (shopEntity == null) {
			if(!shopDto.getOtp().isEmpty()) {
				int otp = otpConfig.getOtp(shopDto.getPhoneNumber());
				if (otp > 0) {
					if (otp == Integer.parseInt(shopDto.getOtp())) {
						return new Response<String>(200, "Success");
					}
					return new Response<String>(400, "Otp is wrong");
				} 
				return new Response<String>(400, "otp is not validate");
			}
			return new Response<String>(400, "Otp can not be null");
		}
		return new Response<String>(202, "Số điện thoại đã tồn tại");
	}

	@Override
	public ResponseList<ProductDTO> getListProduct(ProductDTO productDTO, int page, int size) {
		Pageable pages = PageRequest.of(page, size);
		List<ItemProductEntity> listProductEntity = productRepository.findByShopId(productDTO.getShopId(), pages);
		List<ProductDTO> listProduct = new ArrayList<ProductDTO>();
		int sizeAll = productRepository.countProduct(productDTO.getShopId());
		for (ItemProductEntity productEntity : listProductEntity) {
			ProductDTO product = productConverter.toProductDTO(productEntity);
			product.setShopId(productEntity.getShopProduct().getId());
			product.setDetailId(productEntity.getDetail().getId());
			product.setId(productEntity.getId());
			listProduct.add(product);
		}
		return resProduct.successResList("success", sizeAll, listProduct);
	}

	@Override
	public void updateVote(FeedbackEntity feedbackEntity) {
		double vote = feedbackEntity.getVote();
		ItemProductEntity productEntity = feedbackEntity.getProductFeedback().getProduct();
		double productVote = 0;
		double sumShopVote = 0;
		double sumProductFeedback = 1; // Tong so product duoc feedback
		// Vote product
		if (productEntity.getDetail().getFeedbacks().size() > 0) {
			int sizeFeedback = productEntity.getDetail().getFeedbacks().size() - 1;
			productVote = (productEntity.getVote() * sizeFeedback + vote) / (sizeFeedback + 1);

		} else {
			productVote = vote;
		}
		productRepository.updateVote(productVote, productEntity.getId());

		// Vote shop
		ShopEntity shopEntity = productEntity.getShopProduct();
		for (ItemProductEntity productEntity1 : shopEntity.getProducts()) {
			if (productEntity1.getDetail().getFeedbacks().size() > 0
					&& productEntity1.getDetail().getId() != productEntity.getDetail().getId()) {
				sumShopVote += productEntity1.getVote();
				sumProductFeedback++;
			}
		}
		double shopVote = (sumShopVote + productVote) / sumProductFeedback;
		shopRepository.updateVoteShop(shopVote, shopEntity.getId());
	}
}
