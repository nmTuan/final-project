package app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import app.common.SortCommon;
import app.converter.ProductConverter;
import app.dto.PaymentDTO;
import app.dto.product.ImageProductDTO;
import app.dto.product.ProductDTO;
import app.dto.product.ProductDetailDTO;
import app.dto.product.SizeColorDTO;
import app.entity.ImagesProductEntity;
import app.entity.ItemProductEntity;
import app.entity.PaymentEntity;
import app.entity.ProductDetailEntity;
import app.entity.ShopEntity;
import app.entity.SizeColorEntity;
import app.repository.ImageProductRepository;
import app.repository.PaymentRepository;
import app.repository.ProductDetailRepository;
import app.repository.ProductRepository;
import app.repository.ShopRepository;
import app.repository.SizeColorRepository;
import app.response.Responesive;
import app.response.Response;
import app.response.ResponseList;
import app.service.IProduct;

@Service
public class Product implements IProduct {

	@Autowired
	ShopRepository shopRepsitory;

	@Autowired
	ProductRepository productRepository;

	@Autowired
	ProductDetailRepository detailRepository;

	@Autowired
	ImageProductRepository imageRepository;

	@Autowired
	SizeColorRepository sizeColorRepository;
	
	@Autowired
	PaymentRepository paymentRepository;

	@Autowired
	SortCommon sortCommon;

	@Autowired
	ProductConverter productConverter;

	@Autowired
	Responesive<ProductDetailDTO> res;

	@Autowired
	Responesive<ProductDTO> resProductBase;
	
	@Autowired
	Responesive<PaymentDTO> resPay;

	public ProductDetailEntity saveItemProduct(ProductDetailDTO productDTO, ShopEntity shopEntity) {
		Boolean checkRent = checkIsrent(productDTO.getIsRent(), productDTO.getSizeColors());
//		List<PaymentEntity> paymentEntities = new ArrayList<PaymentEntity>();
 		if (!checkRent)
			return null;
		ItemProductEntity itemProduct = productConverter.toItemProductEntity(productDTO);
		itemProduct.setVote(0);
		itemProduct.setShopProduct(shopEntity);
		itemProduct.setQuantity(0);
		itemProduct.setSold(0);
		itemProduct = productRepository.save(itemProduct);
		ProductDetailEntity detailEntity = productConverter.toDetailEntity(productDTO);
		detailEntity.setProduct(itemProduct);
//		for (PaymentDTO paymentDTO : productDTO.getPayments()) {
//			PaymentEntity paymentEntity = paymentRepository.findOneById(paymentDTO.getId());
//			paymentEntities.add(paymentEntity);
////			detailEntity.setPaymentEntities(Collections.singleton(paymentEntity));
//		}
//		detailEntity.setSold(0);
//		detailEntity.setPaymentEntities(paymentEntities);
//		detailEntity.setFactory(productDTO.getFactory());
		detailEntity = detailRepository.save(detailEntity);
		return detailEntity;
	}

	@Override
	public Response<ProductDetailDTO> save(ProductDetailDTO productDTO) {
		ShopEntity shopEntity = shopRepsitory.findOneById(productDTO.getShopId());
		if (shopEntity == null) {
			return res.successRes("The shop is not existed");
		}
		ProductDetailEntity detailEntity = saveItemProduct(productDTO, shopEntity);
		if (detailEntity == null) {
			return res.successRes("The rent price, rent due id, deposit must be greater than 0");
		}
		addListImage(productDTO.getImages(), detailEntity);
		int quantity = addListSizeColor(productDTO.getSizeColors(), detailEntity);
		productDTO.setId(detailEntity.getId());
		productDTO.setQuantity(quantity);
		sumQuantity(detailEntity.getId(), quantity);
		return res.successRes("success", productDTO);
	}

	@Override
	public ResponseList<ProductDTO> findProductByName(ProductDTO productDTO, int page, int size) {
		String productName = productDTO.getProductName();
		Long categoryId = productDTO.getCategory_id();
		Long genderId = productDTO.getGender_id();
		Long ageId = productDTO.getAge_id();
		double vote = productDTO.getVote();
		boolean isRent = productDTO.getIsRent() == null ? false : productDTO.getIsRent();
		Sort sort = sortCommon.sortOrder(productDTO.getListSort());
		Pageable pages = PageRequest.of(page, size, sort);
		List<ItemProductEntity> listPorductEntity = new ArrayList<ItemProductEntity>();
		listPorductEntity = productRepository.findProduct(productName, categoryId, genderId, ageId, vote, isRent, pages);
		int sizeAll = productRepository.countProductByName(productName, categoryId, genderId, ageId, vote, isRent);
		List<ProductDTO> listProduct = new ArrayList<ProductDTO>();
		if (listPorductEntity != null) {
			for (ItemProductEntity productEntity : listPorductEntity) {
				ProductDTO product = productConverter.toProductDTO(productEntity);
				product.setId(productEntity.getId());
				product.setShopId(productEntity.getShopProduct().getId());
				product.setDetailId(productEntity.getDetail().getId());
				listProduct.add(product);
			}
		}
		return resProductBase.successResList("success", sizeAll, listProduct);
	}

	@Override
	public Response<ProductDetailDTO> getProductDetail(ProductDTO productDTO) {
		ProductDetailEntity detailEntity = detailRepository.findOneByProductId(productDTO.getId());
		if (detailEntity == null) {
			return res.successRes("Product not found");
		} else {
			ProductDetailDTO detailDto = productConverter.toDetailDTO(detailEntity);
			detailDto.setId(detailEntity.getId());
			detailDto.setShopId(detailEntity.getProduct().getShopProduct().getId());
			detailDto.setGender_id(detailEntity.getProduct().getGender_id());
			detailDto.setAge_id(detailEntity.getProduct().getAge_id());
			detailDto.setCategory_id(detailEntity.getProduct().getCategory_id());
			return res.successRes("success", detailDto);
		}
	}

	@Override
	public Response<ProductDetailDTO> updateProduct(ProductDetailDTO productDetail) {
		ProductDetailEntity detailEntity = detailRepository.findOneById(productDetail.getId());
		if (detailEntity == null) {
			return res.successRes("not found");
		}
//		if (productDetail.getPayments().size() > 0) {
//			detailRepository.deletePayment(productDetail.getId());
//			for (PaymentDTO paymentDTO : productDetail.getPayments()) {
//				detailRepository.insertValuePaymentProduct(productDetail.getId(), paymentDTO.getId());
//			}
//		}
		detailRepository.updateProduct(productDetail.getId(), productDetail.getThumbnail(),
				productDetail.getProductName(), productDetail.getShortDescription(), productDetail.getMinPrice(),
				productDetail.getMaxPrice(), productDetail.getOldPrice(), productDetail.getQuantity(), 
				productDetail.getDescription(), productDetail.getBrand(),
				productDetail.getType(), productDetail.getMaterial(), productDetail.getIsRent(), productDetail.getAge_id(),
				productDetail.getGender_id(), productDetail.getCategory_id(), productDetail.getBankId(), productDetail.getPriceDelivery());
		return res.successRes("success");
	}

	@Override
	public Response<ProductDTO> deleteProduct(ProductDTO productDTO) {
		ProductDetailEntity productDetailEntity = detailRepository.findOneById(productDTO.getId());
		if (productDetailEntity == null) {
			return resProductBase.successRes("Product is not found");
		}
		productRepository.deteleProduct(productDTO.getId());
		return resProductBase.successRes("success");
	}

	// Image Service
	@Override
	public Response<ProductDetailDTO> addNewListImage(ProductDetailDTO productDTO) {
		ProductDetailEntity detailEntity = detailRepository.findOneById(productDTO.getId());
		if (detailEntity == null) {
			return res.successRes("Product Not found");
		}
		if (productDTO.getImages().size() <= 0) {
			return res.successRes("The list image size must be greater than 0");
		}
		addListImage(productDTO.getImages(), detailEntity);
		return res.successRes("success");
	}

	@Override
	public Response<ProductDetailDTO> updateImageProduct(ImageProductDTO imageDTO) {
		ImagesProductEntity imageEntity = imageRepository.findOneById(imageDTO.getId());
		if (imageEntity == null) {
			return res.successRes("Not found");
		}
		imageRepository.updateImageProduct(imageDTO.getId(), imageDTO.getImage(), imageDTO.getName());
		return res.successRes("Success");
	}

	@Override
	public Response<ProductDetailDTO> deleteImage(ImageProductDTO imageDTO) {
		ImagesProductEntity imageEntity = imageRepository.findOneById(imageDTO.getId());
		if (imageEntity == null) {
			return res.successRes("Not found");
		}
		imageRepository.deleteById(imageDTO.getId());
		return res.successRes("success");
	}

	// Size color Service
	@Override
	public Response<ProductDetailDTO> addNewListSizeColor(ProductDetailDTO productDTO) {
		ProductDetailEntity detailEntity = detailRepository.findOneById(productDTO.getId());
		if (detailEntity == null) {
			return res.successRes("Product Not found");
		}
		if (productDTO.getSizeColors().size() <= 0) {
			return res.successRes("The list image size must be greater than 0");
		}
		int quantityNew = addListSizeColor(productDTO.getSizeColors(), detailEntity);
		int totalQuantity = detailEntity.getProduct().getQuantity() + quantityNew;
		sumQuantity(productDTO.getId(), totalQuantity);
		return res.successRes("success");
	}

	@Override
	public Response<ProductDetailDTO> updateSizeColorProduct(SizeColorDTO sizeColorDTO) {
		SizeColorEntity sizeColorEntity = sizeColorRepository.findOneById(sizeColorDTO.getId());
		if (sizeColorEntity == null) {
			return res.successRes("Not found");
		}
		if (sizeColorEntity.getDetail().getProduct().getIsRent()) {
			if (sizeColorDTO.getDeposit() <= 0 || sizeColorDTO.getRentDueId() <= 0
					|| sizeColorDTO.getRentPrice() <= 0) {
				return res.successRes("The rent price, rent due id, deposit must be greater than 0");
			}
		}
		int quantity = sizeColorEntity.getDetail().getProduct().getQuantity() + sizeColorDTO.getQuantity()
				- sizeColorEntity.getQuantity();
		sumQuantity(sizeColorEntity.getDetail().getId(), quantity);

		sizeColorRepository.updateImageProduct(sizeColorDTO.getId(), sizeColorDTO.getColor(), sizeColorDTO.getPrice(),
				sizeColorDTO.getQuantity(), sizeColorDTO.getSize(), sizeColorDTO.getRentDueId(),
				sizeColorDTO.getRentPrice(), sizeColorDTO.getDeposit());
		return res.successRes("Success");
	}

	@Override
	public Response<ProductDetailDTO> deleteSizeColor(SizeColorDTO sizeColorDTO) {
		SizeColorEntity sizeColorEntity = sizeColorRepository.findOneById(sizeColorDTO.getId());
		if (sizeColorEntity == null) {
			return res.successRes("not found");
		}
		int totalQuantity = sizeColorEntity.getDetail().getProduct().getQuantity() - sizeColorEntity.getQuantity();
		sumQuantity(sizeColorEntity.getDetail().getId(), totalQuantity);
		sizeColorRepository.deleteById(sizeColorDTO.getId());
		return res.successRes("success");
	}
	
	@Override
	public ResponseList<PaymentDTO> getListPaymentBySizeProduct(Long id) {
		SizeColorEntity sizeColorEntity = sizeColorRepository.findOneById(id);
		List<PaymentEntity> paymentEntities = sizeColorEntity.getDetail().getPaymentEntities();
		List<PaymentDTO> paymentDTOs = new ArrayList<PaymentDTO>();
		for (PaymentEntity paymentEntity : paymentEntities) {
			PaymentDTO paymentDTO = new PaymentDTO();
			paymentDTO.setId(paymentEntity.getId());
			paymentDTO.setPayment(paymentEntity.getPayment());
			paymentDTOs.add(paymentDTO);
		}
		return resPay.successResList("sucess", paymentDTOs);
	}

	// Logic product
	public void sumQuantity(Long detailId, int quantity) {
		detailRepository.updateProduct(detailId, null, null, null, null, null, null, quantity, null, null, null,
				null, null, null, null, null, null, null);
	}

	public void updateQuantitySizeColor(Long detailId, int quantity, Long sizeId, int sizeQuantity) {
		sumQuantity(detailId, quantity);

		sizeColorRepository.updateImageProduct(sizeId, null, null, sizeQuantity, null, null, null, null);
	}

	public void addListImage(List<ImageProductDTO> listImageDTO, ProductDetailEntity detailEntity) {
		List<ImagesProductEntity> imageEntities = productConverter.toImageEntity(listImageDTO, detailEntity);
		imageRepository.saveAll(imageEntities);
	}

	public int addListSizeColor(List<SizeColorDTO> sizeColorDTO, ProductDetailEntity detailEntity) {
		List<SizeColorEntity> sizeColorEntities = productConverter.toSizeEntity(sizeColorDTO, detailEntity);
		int quantity = 0;
		for (SizeColorDTO sizeColor : sizeColorDTO) {
			quantity += sizeColor.getQuantity();

		}
		sizeColorRepository.saveAll(sizeColorEntities);
		return quantity;
	}

	public Boolean checkIsrent(Boolean isRent, List<SizeColorDTO> listSizeColorDTO) {
		if (isRent) {
			for (SizeColorDTO sizeColorDTO : listSizeColorDTO) {
				if (sizeColorDTO.getRentDueId() <= 0 || sizeColorDTO.getRentPrice() <= 0
						|| sizeColorDTO.getDeposit() <= 0) {
					return false;
				}
			}
		}
		return true;
	}
}
