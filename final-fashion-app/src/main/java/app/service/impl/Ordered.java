package app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.converter.OrderConverter;
import app.dto.OrderDTO;
import app.entity.CustomerEntity;
import app.entity.OrderedEntity;
import app.entity.SizeColorEntity;
import app.repository.CustomerRepository;
import app.repository.OrderRepository;
import app.repository.SizeColorRepository;
import app.response.Responesive;
import app.response.Response;
import app.service.IOrdered;

@Service
public class Ordered implements IOrdered {

	@Autowired
	OrderRepository orderRepository;

	@Autowired
	SizeColorRepository sizeColorRepository;

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	OrderConverter orderConverter;

	@Autowired
	Responesive<OrderDTO> res;

	@Override
	public Response<OrderDTO> addProduct(OrderDTO orderDTO) {
		SizeColorEntity sizeColorEntity = sizeColorRepository.findOneById(orderDTO.getSizeId());
		CustomerEntity customerEntity = customerRepository.findOneById(orderDTO.getUserId());
		if (customerEntity != null) {
			if (sizeColorEntity != null) {
				if (orderDTO.getQuantity() <= sizeColorEntity.getQuantity()) {
					OrderedEntity orderEntity = orderConverter.toOrderEntity(orderDTO);
					Long cartId = getCartIdByIdProduct(orderDTO.getUserId(), orderDTO.getSizeId());
					if(orderDTO.getRent()) {
						if(!sizeColorEntity.getDetail().getProduct().getIsRent()) {
							return res.successRes("Cannot rent this product");
						}
					}
					if (cartId >= 0) {
						OrderedEntity orderEntity1 = orderRepository.findOneById(cartId);
						if(orderDTO.getRent() == orderEntity1.getIsRent()) {
							int quantity = orderEntity1.getQuantity() + orderDTO.getQuantity();
							orderRepository.updateCart(quantity, 0, orderDTO.getUserId(), cartId);
							orderDTO = orderConverter.toOrderDTO(orderEntity1);
							orderDTO.setQuantity(quantity);
						} else {
							orderEntity.setStatus(0);
							orderEntity.setCustomerOrderd(customerEntity);
							orderEntity = orderRepository.save(orderEntity);
							orderDTO = orderConverter.toOrderDTO(orderEntity);
						}
					} else {
						orderEntity.setStatus(0);
						orderEntity.setCustomerOrderd(customerEntity);
						orderEntity = orderRepository.save(orderEntity);
						orderDTO = orderConverter.toOrderDTO(orderEntity);	
					}
//					System.out.println(orderDTO.getId());
					return res.successRes("success", orderDTO);

				} else {
					return res.successRes("The quantity is not enough");
				}
			}
			return res.successRes("Product is not found");
		}
		return res.successRes("User is not found");
	}

	@Override
	public Response<OrderDTO> removeProductFromCart(OrderDTO orderDTO) {
		for (Long id : orderDTO.getIds()) {
			OrderedEntity orderEntity = orderRepository.findOneById(id);
			if (orderEntity == null) {
				return res.successRes("The id " + id + " not found");
			}
			orderRepository.removeProductFromCart(id);
		}
		return res.successRes("success");
	}

	public Long getCartIdByIdProduct(Long userId, Long productId) {
		CustomerEntity customerEntity = customerRepository.findOneById(userId);
		List<OrderedEntity> listOrder = new ArrayList<OrderedEntity>();
		listOrder = customerEntity.getOrders();
		for (OrderedEntity order : listOrder) {
			if (order.getSizeId() == productId) {
				return order.getId();
			}
		}
		return -1L;
	}
}
