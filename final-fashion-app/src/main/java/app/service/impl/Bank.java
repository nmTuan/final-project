package app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.converter.BankConverter;
import app.dto.BankCustDTO;
import app.dto.BankShopDTO;
import app.entity.BankCustEntity;
import app.entity.BankEntity;
import app.entity.BankShopEntity;
import app.entity.CustomerEntity;
import app.entity.ShopEntity;
import app.repository.BankCustRepository;
import app.repository.BankRepository;
import app.repository.BankShopRepository;
import app.repository.CustomerRepository;
import app.repository.ShopRepository;
import app.response.Responesive;
import app.response.Response;
import app.response.ResponseList;
import app.service.IBank;

@Service
public class Bank implements IBank {

	@Autowired
	BankConverter bankConveter;

	@Autowired
	BankShopRepository bankShopRepository;

	@Autowired
	ShopRepository shopRepository;
	
	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	BankCustRepository bankCustRepository;
	
	@Autowired
	BankRepository bankRepository;

	@Autowired
	Responesive<BankShopDTO> resBankShop;
	
	@Autowired
	Responesive<BankCustDTO> resBankCust;
	
	@Autowired
	Responesive<BankEntity> resBank;
	
	@Override
	public ResponseList<BankEntity> findBankByName(String name) {
		List<BankEntity> bankEntities = bankRepository.findByName(name);
		return resBank.successResList("success", bankEntities);
	}

	@Override
	public Response<BankShopDTO> saveBankShop(BankShopDTO bankShopDTO) {
		if (bankShopDTO.getCreditName().isEmpty() && bankShopDTO.getCreditNumber().isEmpty()
				&& bankShopDTO.getMonth().isEmpty() && bankShopDTO.getYear().isEmpty()) {
			return resBankShop.successRes("Thiếu thông tin");
		}
		ShopEntity shopEntity = shopRepository.findOneById(bankShopDTO.getShopId());
		if (shopEntity != null) {
			if (shopEntity.getBankShops().size() < 1) {
				BankShopEntity bankShopEntity = (BankShopEntity) bankConveter.toBankEntity(bankShopDTO);
				bankShopEntity.setShopBank(shopEntity);
				bankShopEntity.setBalance(0);
				bankShopEntity = bankShopRepository.save(bankShopEntity);
				BankShopDTO bankShopDTO2 = bankConveter.toBankDTO(bankShopEntity);
				return resBankShop.successRes("success", bankShopDTO2);
			}
			return resBankShop.successRes("Đã liên kết với ngân hàng " + shopEntity.getBankShops().get(0).getBankName());
		}
		return resBankShop.successRes("Không tìm thấy cửa hàng");
	}
	
	@Override
	public ResponseList<BankShopDTO> getListBankByShopId(Long shopId) {
		ShopEntity shopEntity = shopRepository.findOneById(shopId);
		if (shopEntity == null) {
			return resBankShop.successResList("Không tìm thấy cửa hàng");
		}
		List<BankShopEntity> bankShopEntities = bankShopRepository.getListBankByShopId(shopId);
		List<BankShopDTO> bankShopDTOs = new ArrayList<BankShopDTO>();
		for (BankShopEntity bankShopEntity : bankShopEntities) {
			BankShopDTO bankShopDTO = bankConveter.toBankDTO(bankShopEntity);
			bankShopDTOs.add(bankShopDTO);
		}
		return resBankShop.successResList("success", bankShopDTOs);
	}
	
	@Override
	public Response<String> deleteBankShop(int bankId) {
		BankShopEntity bankShopEntity = bankShopRepository.findOneById(bankId);
		if (bankShopEntity == null) {
			return new Response<String>(404, "Không tìm thấy ngân hàng");
		}
		bankShopRepository.deleteBank(bankId);
		return new Response<String>(200, "Thành công");
	}
	
	@Override
	public Response<BankCustDTO> saveCustShop(BankCustDTO bankCustDTO) {
		if (bankCustDTO.getCreditName().isEmpty() && bankCustDTO.getCreditNumber().isEmpty()
				&& bankCustDTO.getMonth().isEmpty() && bankCustDTO.getYear().isEmpty()) {
			return resBankCust.successRes("Thiếu thông tin");
		}
		CustomerEntity customerEntity = customerRepository.findOneById(bankCustDTO.getCustId());
		if (customerEntity != null) {
			BankCustEntity bankCustEntity = bankConveter.toBankCustEntity(bankCustDTO);
			bankCustEntity.setCustBank(customerEntity);
			bankCustEntity.setBalance(0);
			bankCustEntity = bankCustRepository.save(bankCustEntity);
			BankCustDTO bankCustDTO2 = bankConveter.toBankCustDTO(bankCustEntity);
			bankCustDTO2.setCustId(bankCustDTO.getCustId());
			return resBankCust.successRes("success", bankCustDTO2);
		}
		return resBankCust.successRes("Không tìm thấy khách hàng");

	}
	
	@Override
	public ResponseList<BankCustDTO> getListBankByCustId(Long custId) {
		CustomerEntity customerEntity = customerRepository.findOneById(custId);
		if (customerEntity == null) {
			return resBankCust.successResList("Không tìm thấy cửa hàng");
		}
		List<BankCustEntity> bankCustEntities = bankCustRepository.getListBankByCustId(custId);
		List<BankCustDTO> bankCustDTOs = new ArrayList<BankCustDTO>();
		for (BankCustEntity bankCustEntity : bankCustEntities) {
			BankCustDTO bankCustDTO = bankConveter.toBankCustDTO(bankCustEntity);
			bankCustDTOs.add(bankCustDTO);
		}
		return resBankCust.successResList("success", bankCustDTOs);
	}
	
	@Override
	public Response<String> deleteBankCust(int bankId) {
		BankCustEntity bankCustEntity = bankCustRepository.findOneById(bankId);
		if (bankCustEntity == null) {
			return new Response<String>(404, "Không tìm thấy ngân hàng");
		}
		bankCustRepository.deleteBank(bankId);
		return new Response<String>(200, "Thành công");
	}
}
