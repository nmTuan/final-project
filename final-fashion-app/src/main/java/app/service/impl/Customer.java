package app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.config.OtpConfig;
import app.converter.CustomerConverter;
import app.converter.OrderConverter;
import app.dto.AccountDTO;
import app.dto.CustomerDTO;
import app.dto.OrderDTO;
import app.entity.AccountEntity;
import app.entity.CustomerEntity;
import app.entity.OrderedEntity;
import app.entity.SizeColorEntity;
import app.repository.AccountRepository;
import app.repository.CustomerRepository;
import app.repository.OrderRepository;
import app.repository.SizeColorRepository;
import app.response.Responesive;
import app.response.Response;
import app.response.ResponseList;
import app.service.ICustomer;

@Service
public class Customer implements ICustomer {

	@Autowired
	Responesive<CustomerDTO> res;

	@Autowired
	Responesive<OrderDTO> resOrder;

	@Autowired
	CustomerConverter customerConverter;

	@Autowired
	OrderConverter orderConverter;

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	OrderRepository orderRepository;

	@Autowired
	SizeColorRepository sizeColorRepository;

	@Autowired
	AccountRepository accountRepository;
	
	@Autowired
	OtpConfig otpConfig;

	@Override
	public Response<CustomerDTO> saveInforCust(CustomerDTO customerDTO) {
		AccountEntity accountEntity = accountRepository.findOneById(customerDTO.getAccId());
		if (accountEntity != null) {
			if (accountEntity.getCustomerEntity() == null) {
				CustomerEntity customerEntity = customerRepository.findOneByPhoneNumber(customerDTO.getPhoneNumber());
				if (customerEntity != null) {
					return res.successRes("This phone number is already registed");
				} else {
					if (customerDTO.getPhoneNumber().isEmpty() || customerDTO.getAddress().isEmpty()) {
						return res.successRes("invalidate");
					}
					customerEntity = customerConverter.toCustEntity(customerDTO);
					customerEntity.setAccCust(accountEntity);
					customerRepository.save(customerEntity);
					CustomerDTO responseCust = customerConverter.toCustDto(customerEntity);
					return res.successRes("success", responseCust);
				}
			}
			return res.successRes("Account already resgistered");
		}
		return res.successRes("Account does not found");
	}
	
	@Override
	public Response<CustomerDTO> checkPhoneNumber(CustomerDTO customerDTO) {
		CustomerEntity customerEntity = customerRepository.findOneByPhoneNumber(customerDTO.getPhoneNumber());
		if (customerEntity == null) {
			if(!customerDTO.getOtp().isEmpty()) {
				int otp = otpConfig.getOtp(customerDTO.getPhoneNumber());
				if (otp > 0) {
					if (otp == Integer.parseInt(customerDTO.getOtp())) {
						return res.successRes("success");
					}
					return res.successRes("Otp is wrong");
				} 
				return res.successRes("Otp is not valid");
			}
			return res.successRes("Otp không được để ");
		}
		return new Response<CustomerDTO>(202, "Số điện thoại đã tồn tại");
	}

	@Override
	public Response<CustomerDTO> findCust(AccountDTO accountDTO) {
		CustomerEntity customerEntity = new CustomerEntity();
		if (accountDTO.getId() != null) {
			customerEntity = customerRepository.findOneByAccId(accountDTO.getId());
		} else if (accountDTO.getUsername() != null) {
			AccountEntity accountEntity = accountRepository.findOneByUsername(accountDTO.getUsername());
			if(accountEntity == null) {
				return new Response<CustomerDTO>(404, "không tồn tại thông tin tài khoản");
			}
			customerEntity = accountEntity.getCustomerEntity();
		} else {
			return res.successRes("invalidate!");
		}
		if (customerEntity == null) {
			return new Response<CustomerDTO>(404, "không tồn tại thông tin tài khoản");
		}
		CustomerDTO customerDTO2 = customerConverter.toCustDto(customerEntity);
		customerDTO2.setSocketId(customerEntity.getSocketId());
		return res.successRes("success", customerDTO2);
	}

	@Override
	public Response<CustomerDTO> updateUserInfo(CustomerDTO customerDTO) {
		CustomerEntity customerEntity = customerRepository.findOneById(customerDTO.getId());
		CustomerEntity customerEntity2 = customerRepository.findOneByPhoneNumber(customerDTO.getPhoneNumber());
		if (customerEntity == null) {
			return res.successRes("User doesn't existed");
		} else if (customerEntity2 != null) {
			return res.successRes("The phone number is already registed");
		} else {
			String address = customerDTO.getAddress();
			String avatar = customerDTO.getAvatar();
			String coverava = customerDTO.getCoverave();
			String gender = customerDTO.getGender();
			Long id = customerDTO.getId();
			String city = customerDTO.getCity();
			String district = customerDTO.getDistrict();
			String village = customerDTO.getVillage();
			customerRepository.updateUserInfo(address, avatar, coverava, customerDTO.getDob(), gender, city, district,
					village, id);
			return res.successRes("update success");
		}
	}
	
	@Override
	public Response<CustomerDTO> updatePhoneNumber(CustomerDTO customerDto) {
		CustomerEntity customerEntity = customerRepository.findOneById(customerDto.getId());
		CustomerEntity customerEntity2 = customerRepository.findOneByPhoneNumber(customerDto.getPhoneNumber());
		if (customerEntity != null) {
			if (customerEntity2 == null) {
				if (!customerDto.getOtp().isEmpty()) {
					int otp = otpConfig.getOtp(customerDto.getPhoneNumber());
					if (otp > 0) {
						if (otp == Integer.parseInt(customerDto.getOtp())) {
							customerRepository.updatePhoneNumber(customerDto.getPhoneNumber(), customerDto.getId());
							return res.successRes("update success");
						}
						return res.successRes("Otp is wrong");
					} 
					return res.successRes("Otp is not valid");
				}
				return res.successRes("Otp can not be empty");
			}
			return res.successRes("The phone number is already regisrter");
		}
		return res.successRes("User not found");
	}

	// Order
	@Override
	public ResponseList<OrderDTO> getListProductCart(OrderDTO orderDTO) {
		List<OrderDTO> listOrderDTO = getListOrder(orderDTO.getUserId(), orderDTO.getStatus());
		return resOrder.successResList("success", listOrderDTO.size(), listOrderDTO);
	}

	// status: 0-in cart; 1-wait for confirm; 2-confirmed
	public List<OrderDTO> getListOrder(Long id, int status) {
		List<OrderedEntity> listOrderEntity = orderRepository.findByUserId(id);
		List<OrderDTO> listOrderDTO = new ArrayList<OrderDTO>();
		for (OrderedEntity order : listOrderEntity) {
			SizeColorEntity sizeColorEntity = sizeColorRepository.findOneById(order.getSizeId());
			if (sizeColorEntity != null) {
				OrderDTO orderDTO1 = orderConverter.toOrderDTO(order);
				listOrderDTO.add(orderDTO1);
			} else {
				orderRepository.removeProductBySizeId(order.getSizeId());
			}
		}
		return listOrderDTO;
	}

}
