package app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import app.config.OtpConfig;

@Service
public class OtpService {
	
	@Autowired
	OtpConfig otpConfig;
	
	public String generateOtp(String phoneNumber) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication(); 
		String username = auth.getName();
		if (!username.isEmpty()) {
			if(!phoneNumber.isEmpty()) {
				int otp = otpConfig.generateOTP(phoneNumber); 
//	           Map<String,String> replacements = new HashMap<String,String>();
//	           replacements.put("user", username);
//	           replacements.put("otpnum", String.valueOf(otp));
	           return String.valueOf(otp);
//	           String message = template.getTemplate(replacements);
			} else {
				return "Phone number cannot be null";
			}
		}
		return "UserNotfound";
	}
}
