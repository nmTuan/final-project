package app.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import app.service.IFileStorage;

@Service
public class FileStorage implements IFileStorage {
	private static final String PATH = "src//main//resources//static//images//";
	private static final String PATH_ROOM = "src//main//resources//static//images//rooms//";
	
	private final Path root = Paths.get(PATH);

	@Override
	public void init(String path) {
		try {
			Files.createDirectory(Paths.get(path));
		} catch (IOException e) {
			throw new RuntimeException("Could not initialize folder for upload!");
		}
	}

	@Override
	public void save(MultipartFile file) {
		File convertFile = new File(PATH + file.getOriginalFilename());
		try {
			convertFile.createNewFile();
			FileOutputStream fout = new FileOutputStream(convertFile);
			fout.write(file.getBytes());
			fout.close();
		} catch (IOException e) {
			throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
		}
	}
	
	@Override
	public void saveImageInRoom(MultipartFile file, Long roomId) {
		String path = PATH_ROOM + roomId;
		File f = new File(path);
		if (!f.exists()) {
			init(path);
		}
		try {
			File convertFile = new File(path + "//" + file.getOriginalFilename());
			convertFile.createNewFile();
			FileOutputStream fout = new FileOutputStream(convertFile);
			fout.write(file.getBytes());
			fout.close();
		} catch (IOException e) {
			throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
		}
		
	}

	@Override
	public Resource load(String fileName) {
		try {
			Path file = root.resolve(fileName);
			Resource resource = new UrlResource(file.toUri());

			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new RuntimeException("Could not read the file!");
			}
		} catch (MalformedURLException e) {
			throw new RuntimeException("Error: " + e.getMessage());
		}
	}
}
