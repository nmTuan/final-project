package app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import app.converter.NotificationConverter;
import app.dto.NotificationShopDTO;
import app.dto.ShopDTO;
import app.entity.NotifiShopEntity;
import app.entity.ShopEntity;
import app.repository.NotifiShopRepository;
import app.repository.ShopRepository;
import app.response.Responesive;
import app.response.Response;
import app.service.INotifiShop;

@Service
public class NotifiShop implements INotifiShop {

	@Autowired
	NotifiShopRepository notificationRepository;

	@Autowired
	ShopRepository shopRepository;

	@Autowired
	NotificationConverter notificationConverter;

	@Autowired
	Responesive<ShopDTO> responseShop;

	@Autowired
	Responesive<String> res;

	@Override
	public void addNewNotification(NotificationShopDTO notifiShopDTO, ShopEntity shopEntity) {
		NotifiShopEntity notifiShopEntity = notificationConverter.toNotifiShopEntity(notifiShopDTO);
		notifiShopEntity.setNotifiShop(shopEntity);
		notificationRepository.save(notifiShopEntity);
	}

	@Override
	public Response<ShopDTO> getListNotifiByShopId(Long shopId, int page, int size) {
		ShopEntity shopEntity = shopRepository.findOneById(shopId);
		ShopDTO shopDTO = new ShopDTO();
		if (shopEntity != null) {
			Order order = new Order(Direction.DESC, "createddate");
			Pageable pages = PageRequest.of(page, size, Sort.by(order));
			List<NotifiShopEntity> notifiShopEntities = new ArrayList<NotifiShopEntity>();
			List<NotificationShopDTO> notificationShopDTOs = new ArrayList<NotificationShopDTO>();
			notifiShopEntities = notificationRepository.findNotifiByShopId(shopId, pages);
			for (NotifiShopEntity notifiShopEntity : notifiShopEntities) {
				NotificationShopDTO notificationShopDTO = notificationConverter.toNotifiShopDTO(notifiShopEntity);
				notificationShopDTOs.add(notificationShopDTO);
			}
			shopDTO.setId(shopEntity.getId());
			shopDTO.setNotificationShopDTOs(notificationShopDTOs);
			return responseShop.successRes("success", shopDTO);
		}
		return responseShop.successRes("Shop does not found");
	}

	@Override
	public Response<Integer> countNotification(Long shopId, int status) {
		int quantity = notificationRepository.countNotificationByStatus(shopId, status);
		return new Response<Integer>(200, "success", quantity);
	}
	
	@Override
	public Response<String> updateStatusNotifiShop(NotificationShopDTO notificationShopDTO) {
		NotifiShopEntity notifiShopEntity = notificationRepository.findOneById(notificationShopDTO.getShopId(),
				notificationShopDTO.getId());
		if (notifiShopEntity != null) {
			notificationRepository.updateStatusNotifi(1, notificationShopDTO.getShopId(), notificationShopDTO.getId());
			return res.successRes("success");
		}
		return res.successRes("Not found");
	}
	
	
}
