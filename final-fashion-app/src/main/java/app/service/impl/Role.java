package app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.entity.RoleEntity;
import app.repository.RoleRepository;
import app.service.IRole;

@Service
public class Role implements IRole {

	@Autowired
	RoleRepository roleRepository;

	@Override
	public void addNewRole(String roleName) {
		RoleEntity roleEntity = new RoleEntity();
		roleEntity.setRoleName(roleName);
		roleRepository.save(roleEntity);
	}

}
