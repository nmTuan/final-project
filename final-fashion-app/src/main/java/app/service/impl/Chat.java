package app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import app.converter.ChatConverter;
import app.dto.ChatContentDTO;
import app.dto.CustomerDTO;
import app.dto.RoomDTO;
import app.dto.ShopDTO;
import app.entity.ChatContentEntity;
import app.entity.CustomerEntity;
import app.entity.RoomEntity;
import app.entity.ShopEntity;
import app.repository.ChatContentRepository;
import app.repository.CustomerRepository;
import app.repository.RoomRepository;
import app.repository.ShopRepository;
import app.response.Responesive;
import app.response.Response;
import app.response.ResponseList;
import app.service.IChat;

@Service
public class Chat implements IChat {

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	ShopRepository shopRepository;

	@Autowired
	RoomRepository roomRepository;
	
	@Autowired
	ChatContentRepository chatContentRepository;

	@Autowired
	ChatConverter chatConverter;

	@Autowired
	Responesive<CustomerDTO> resCustomer;

	@Autowired
	Responesive<ShopDTO> resShop;

	@Autowired
	Responesive<RoomDTO> resRoom;
	
	@Autowired
	Responesive<ChatContentDTO> resChatContent;

	public Response<CustomerDTO> getListRoomChatByCust(Long custId, int page, int size) {
		Order order = new Order(Direction.DESC, "modifieddate");
		Pageable pageRoom = PageRequest.of(page, size, Sort.by(order));
		CustomerEntity customerEntity = customerRepository.findOneById(custId);
		if (customerEntity != null) {
			List<RoomEntity> roomEntities = roomRepository.findByCustId(custId, pageRoom);
			List<RoomDTO> roomDTOs = new ArrayList<RoomDTO>();
			CustomerDTO customerDTO = new CustomerDTO();
			for (RoomEntity roomEntity : roomEntities) {
				RoomDTO roomDTO = chatConverter.toRoomDTOWithoutContent(roomEntity);
				roomDTOs.add(roomDTO);
			}
			customerDTO.setId(custId);
			customerDTO.setRoomChats(roomDTOs);
			return resCustomer.successRes("Success", customerDTO);
		}
		return resCustomer.successRes("Customer does not found");
	}
	
	@Override
	public Response<String> updateStatusRoomCust(Long roomId, int status) {
		RoomEntity roomEntity = roomRepository.findOneById(roomId);
		if(roomEntity != null) {
			roomRepository.updateStatusRoomCust(roomId, status);
			return new Response<String>(200, "success");
		}
		return new Response<String>(200, "Không tìm thấy phòng");
	}

	@Override
	public Response<ShopDTO> getListRoomChatByShop(Long shopId, int page, int size) {
		Order order = new Order(Direction.DESC, "modifieddate");
		Pageable pageRoom = PageRequest.of(page, size, Sort.by(order));
		ShopEntity shopEntity = shopRepository.findOneById(shopId);
		if (shopEntity != null) {
			List<RoomEntity> roomEntities = roomRepository.findByShopId(shopId, pageRoom);
			List<RoomDTO> roomDTOs = new ArrayList<RoomDTO>();
			ShopDTO shopDTO = new ShopDTO();
			for (RoomEntity roomEntity : roomEntities) {
				RoomDTO roomDTO = chatConverter.toRoomDTOWithoutContent(roomEntity);
				roomDTOs.add(roomDTO);
			}
			shopDTO.setId(shopId);
			shopDTO.setRoomChats(roomDTOs);
			return resShop.successRes("Success", shopDTO);
		}
		return resShop.successRes("Shop does not found");
	}
	
	@Override
	public Response<String> updateStatusRoomShop(Long roomId, int status) {
		RoomEntity roomEntity = roomRepository.findOneById(roomId);
		if(roomEntity != null) {
			roomRepository.updateStatusRoomShop(roomId, status);
			return new Response<String>(200, "success");
		}
		return new Response<String>(200, "Không tìm thấy phòng");
		
	}

	@Override
	public ResponseList<ChatContentDTO> getListContentByRoom(Long roomId, int page, int size) {
		RoomEntity roomEntity = roomRepository.findOneById(roomId);
		Order order = new Order(Direction.DESC, "createdate");
		Pageable pageRoom = PageRequest.of(page, size, Sort.by(order));
		if (roomEntity != null) {
			List<ChatContentEntity> chatContentEntities = chatContentRepository.getListContent(roomId, pageRoom);
			List<ChatContentDTO> chatContentDTOs = new ArrayList<ChatContentDTO>();
			for (ChatContentEntity chatContentEntity : chatContentEntities) {
				ChatContentDTO chatContentDTO = chatConverter.toChatContentDTO(chatContentEntity);
				chatContentDTOs.add(chatContentDTO);
			}
			return resChatContent.successResList("success", chatContentDTOs);
		}
		return new ResponseList<ChatContentDTO>(404, "not found");
	}
	
	@Override
	public Response<RoomDTO> getRoomByCustShop(Long custId, Long shopId) {
		RoomEntity roomEntity = roomRepository.findRoomByCustShopId(custId, shopId);
		if (roomEntity != null) {
			RoomDTO roomDTO = chatConverter.toRoomDTOWithoutContent(roomEntity);
			return new Response<RoomDTO>(200, "success", roomDTO);
		}
		return new Response<RoomDTO>(404, "kkhông tìm thấy phòng");
	}
	
	@Override
	public Response<Integer> countChatByStatusCust(Long custId, int status) {
		CustomerEntity customerEntity = customerRepository.findOneById(custId);
		if (customerEntity == null) {
			return new Response<Integer>(404, "không tìm thấy khách hàng");
		}
		Integer count = customerRepository.countChatByStatus(custId, status);
		return new Response<Integer>(200, "success", count);
	}
	
	@Override
	public Response<Integer> countChatByStatusShop(Long shopId, int status) {
		ShopEntity shopEntity = shopRepository.findOneById(shopId);
		if (shopEntity == null) {
			return new Response<Integer>(404, "không tìm thấy cửa hàng");
		}
		Integer count = shopRepository.countChatByStatus(shopId, status);
		return new Response<Integer>(200, "success", count);
	}

}
