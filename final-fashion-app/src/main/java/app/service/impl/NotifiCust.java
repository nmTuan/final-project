package app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import app.converter.NotificationConverter;
import app.dto.CustomerDTO;
import app.dto.NotificationCustDTO;
import app.entity.CustomerEntity;
import app.entity.NotifiCustEntity;
import app.repository.CustomerRepository;
import app.repository.NotifiCustRepository;
import app.response.Responesive;
import app.response.Response;
import app.service.INotifiCust;

@Service
public class NotifiCust implements INotifiCust {

	@Autowired
	NotifiCustRepository notifiCustRepository;

	@Autowired
	NotificationConverter notificationConverter;

	@Autowired
	Responesive<CustomerDTO> responseCust;

	@Autowired
	Responesive<String> res;

	@Autowired
	CustomerRepository customerRepository;

	@Override
	public void addNewNotification(NotificationCustDTO notificationCustDTO, CustomerEntity custtomerEntity) {
		NotifiCustEntity notifiCustEntity = notificationConverter.toNotifiCustEntity(notificationCustDTO);
		notifiCustEntity.setNotifiCust(custtomerEntity);
		notifiCustRepository.save(notifiCustEntity);
	}

	@Override
	public Response<CustomerDTO> getListNotifiByCustId(Long custId, int page, int size) {
		CustomerEntity customerEntity = customerRepository.findOneById(custId);
		if (customerEntity != null) {
			Order order = new Order(Direction.DESC, "createddate");
			Pageable pages = PageRequest.of(page, size, Sort.by(order));
			CustomerDTO customerDTO = new CustomerDTO();
			List<NotifiCustEntity> notifiCustEntites = new ArrayList<NotifiCustEntity>();
			notifiCustEntites = notifiCustRepository.findByCustId(custId, pages);
			List<NotificationCustDTO> notificationCustDTOs = new ArrayList<NotificationCustDTO>();
			for (NotifiCustEntity notifiCustEntity : notifiCustEntites) {
				NotificationCustDTO notificationCustDTO = notificationConverter.toNotifiCustDTO(notifiCustEntity);
				notificationCustDTOs.add(notificationCustDTO);
			}
			customerDTO.setId(custId);
			customerDTO.setNotificationCustDTOs(notificationCustDTOs);
			return responseCust.successRes("success", customerDTO);
		}
		return responseCust.successRes("Customer does not found");
	}

	@Override
	public Response<String> updateStatusNotifi(NotificationCustDTO notfiCustDTO) {
		NotifiCustEntity notifiCustEntity = notifiCustRepository.findOneById(notfiCustDTO.getCustId(),
				notfiCustDTO.getId());
		if (notifiCustEntity != null) {
			notifiCustRepository.updateStatusNotifi(1, notfiCustDTO.getCustId(), notfiCustDTO.getId());
			return res.successRes("sucess");
		}
		return res.successRes("Notification does not found");
	}
	
	@Override
	public Response<Integer> countNotification(Long custId, int status) {
		int quantity = notifiCustRepository.countNotificationByStatus(custId, status);
		return new Response<Integer>(200, "success", quantity);
	}
}
