package app.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.common.Common;
import app.converter.BillConverter;
import app.dto.BillDTO;
import app.dto.DeliveryDTO;
import app.dto.NotificationCustDTO;
import app.dto.NotificationShopDTO;
import app.dto.ProductInBillDTO;
import app.entity.BankCustEntity;
import app.entity.BankShopEntity;
import app.entity.BillEntity;
import app.entity.CustomerEntity;
import app.entity.DeliveryEntity;
import app.entity.ProductInBillEntity;
import app.entity.ShopEntity;
import app.entity.SizeColorEntity;
import app.repository.BankCustRepository;
import app.repository.BankShopRepository;
import app.repository.BillRepository;
import app.repository.CustomerRepository;
import app.repository.DeliveryRepository;
import app.repository.ProductBillRepository;
import app.repository.ProductRepository;
import app.repository.ShopRepository;
import app.repository.SizeColorRepository;
import app.response.Responesive;
import app.response.Response;
import app.response.ResponseList;
import app.service.IBIll;

@Service
public class Bill implements IBIll {

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	ShopRepository shopRepository;

	@Autowired
	SizeColorRepository sizeColorRepository;

	@Autowired
	ProductRepository productRepository;

	@Autowired
	BillRepository billRepository;

	@Autowired
	ProductBillRepository productBillRepository;

	@Autowired
	DeliveryRepository deliveryRepository;

	@Autowired
	BankCustRepository bankCustRepository;

	@Autowired
	BankShopRepository bankShopRepository;

	@Autowired
	BillConverter billConverter;

	@Autowired
	Responesive<BillDTO> res;

	@Autowired
	Responesive<ProductInBillDTO> resProductBill;

	@Autowired
	Product product;

	@Autowired
	Revenue revenue;

	@Autowired
	Common common;

	@Autowired
	NotifiShop notifiShop;

	@Autowired
	NotifiCust notifiCust;

	@Override
	public Response<BillDTO> save(BillDTO billDTO) {
		BillEntity billEntity = new BillEntity();
		if (billDTO.getPaymentId() > 0) {
			CustomerEntity customerEntity = customerRepository.findOneById(billDTO.getCustomerId());
			if (customerEntity != null) {
				if (billDTO.getPaymentId() != 1) {
					if (billDTO.getBankId() <= 0 && billDTO.getBankName().isEmpty()
							&& billDTO.getCreditNumber().isEmpty()) {
						return res.successRes("Thiếu thông tin");
					}
					BankCustEntity bankCustEntity = bankCustRepository.findOneById(billDTO.getBankId());
					if (bankCustEntity == null) {
						return res.successRes("Không tìm thấy ngân hàng");
					} else if (bankCustEntity.getBalance() - billDTO.getTotalBill() < 50000) {
						return res.successRes("Số dư không khả dụng");
					}
					int balanceCust = bankCustEntity.getBalance() - billDTO.getTotalBill();
					bankCustRepository.updateBalanceCust(billDTO.getBankId(), balanceCust);
				}
				Timestamp createdDate = common.dateToTimestamp(new Date());
				billDTO.setAddress(customerEntity.getAddress());
				billEntity = billConverter.toBillEntity(billDTO);
				billEntity.setCustomerBill(customerEntity);
				billEntity.setCreatedDate(createdDate);
				ProductInBillEntity productBillEntity = new ProductInBillEntity();
				List<ProductInBillEntity> productBillEntities = new ArrayList<ProductInBillEntity>();
				for (ProductInBillDTO productBillDTO : billDTO.getProductBillDTOs()) {
					SizeColorEntity sizeColorEntity = sizeColorRepository.findOneById(productBillDTO.getSizeId());
					if (sizeColorEntity != null && productBillDTO.getDeliveryId() > 0) {
						if (productBillDTO.getQuantity() <= sizeColorEntity.getQuantity()) {
							// Bill
							billEntity = billRepository.save(billEntity);
							productBillDTO.setStatus(0);
							productBillEntity = billConverter.toProductBillEntity(productBillDTO);
							ShopEntity shop = sizeColorEntity.getDetail().getProduct().getShopProduct();
							
							if (billDTO.getPaymentId() != 1) {
								BankShopEntity bankShopEntity = bankShopRepository.findOneById(shop.getBankShops().get(0).getId());
								int balance = bankShopEntity.getBalance() + sizeColorEntity.getPrice();
								bankShopRepository.updateBalanceShop(sizeColorEntity.getDetail().getBankId(), balance);
								productBillEntity.setPaymentStatus(1);
							}
							
							productBillEntity.setRentDue(sizeColorEntity.getRentDueId());
							productBillEntity.setRentPrice(sizeColorEntity.getRentPrice());
							productBillEntity.setDeposit(sizeColorEntity.getDeposit());
							productBillEntity.setShopBill(shop);

							productBillEntity.setBill(billEntity);
							productBillEntity.setCustBillProduct(customerEntity);
							productBillEntity = productBillRepository.save(productBillEntity);
							productBillEntities.add(productBillEntity); // list product in bill
							
							// Notification
							String content = "Đơn đặt hàng từ khách " + customerEntity.getFullName();
							Long postId = productBillEntity.getId();
							String thumbnail = sizeColorEntity.getDetail().getProduct().getThumbnail();
							String title = "Yêu cầu xác nhận đơn hàng";
							int status = 0;
							NotificationShopDTO notifiShopDTO = new NotificationShopDTO(postId, thumbnail, title,
									content, createdDate, status);
							notifiShop.addNewNotification(notifiShopDTO, shop);
						}
					} else {
						return res.successRes("Error !!");
					}
				} // end for
				billEntity.setProductInBills(productBillEntities);
				billDTO = billConverter.toBillDTO(billEntity);
				return res.successRes("Success");
			}
			return res.successRes("Username Not found");
		}
		return res.successRes("Payment, delivery or address method must not be empty");
	}

	// Customer
	@Override
	public ResponseList<BillDTO> findBillByCustId(BillDTO billDTO) {
		List<BillEntity> billEntities = billRepository.findByCustId(billDTO.getCustomerId());
		if (billEntities.size() > 0) {
			List<BillDTO> billDTOs = new ArrayList<>();
			for (BillEntity billEntity : billEntities) {
				BillDTO billDTO1 = billConverter.toBillDTO(billEntity);
				billDTOs.add(billDTO1);
			}
			return res.successResList("success", billEntities.size(), billDTOs);
		}
		return res.successResList("Don't have bill");
	}

	@Override
	public ResponseList<ProductInBillDTO> findProductByStatusCust(ProductInBillDTO productInBillDTO) {
		List<ProductInBillEntity> productBillEntities = productBillRepository
				.findProductByStatusCust(productInBillDTO.getCustId(), productInBillDTO.getStatus());
		List<ProductInBillDTO> productBillDTOs = new ArrayList<ProductInBillDTO>();
		if (productBillEntities.size() > 0) {
			for (ProductInBillEntity productBillEntity : productBillEntities) {
				DeliveryDTO deliveryDTO = new DeliveryDTO();
				DeliveryEntity deliveryEntity = deliveryRepository.findOneById(productBillEntity.getDeliveryId());
				deliveryDTO.setName(deliveryEntity.getDelivery());
				deliveryDTO.setPrice(deliveryEntity.getPrice());
				ProductInBillDTO productBillDTO = billConverter.toProductBillDTO(productBillEntity);
				productBillDTO.setDelivery(deliveryDTO);
				productBillDTOs.add(productBillDTO);
			}
			return resProductBill.successResList("success", productBillEntities.size(), productBillDTOs);
		}
		return resProductBill.successResList("Bill not found", productBillDTOs);
	}

	@Override
	public Response<ProductInBillDTO> getBillDetail(ProductInBillDTO productInBill) {
		ProductInBillEntity productInBillEntity = productBillRepository.findOneById(productInBill.getId());
		if (productInBillEntity != null) {
			ProductInBillDTO productInBillDTO = billConverter.toProductBillDTO(productInBillEntity);
//			DeliveryDTO deliveryDTO = new DeliveryDTO();
//			DeliveryEntity deliveryEntity = deliveryRepository.findOneById(productInBillEntity.getDeliveryId());
//			deliveryDTO.setName(deliveryEntity.getDelivery());
//			deliveryDTO.setPrice(deliveryEntity.getPrice());
//			BillEntity billEntity = billRepository.findOneById(productInBillEntity.getBill().getId());
//			List<ProductInBillDTO> productInBillDTOs = new ArrayList<ProductInBillDTO>();
//			ProductInBillDTO productInBillDTO = billConverter.toProductBillDTO(productInBillEntity);
//			productInBillDTO.setDelivery(deliveryDTO);
//			productInBillDTOs.add(productInBillDTO);
//			BillDTO billDTO = billConverter.toBillDTOSingleProduct(billEntity);
//			billDTO.setProductBillDTOs(productInBillDTOs);
			return resProductBill.successRes("Success", productInBillDTO);
		}
		return resProductBill.successRes("Not found");
	}

	// status: 0: cho xac nhan, 1, cho giao hang, 2 dang giao hang,
	// 3 giao hang thanh cong, 4 giao hang that bai, 5 shop Huy don hang, 6 khach
	// huy

	@Override
	public Response<ProductInBillDTO> updateStatusProductCust(ProductInBillDTO productInBillDTO) {
		ProductInBillEntity productBillEntity = productBillRepository.findOneById(productInBillDTO.getId());
		if (productBillEntity != null) {
			SizeColorEntity sizeColorEntity = sizeColorRepository.findOneById(productBillEntity.getSizeId());
			if (sizeColorEntity != null) {
				if (productInBillDTO.getStatus() == 6) {
					if (productBillEntity.getStatus() == 0) {
						Long postId = productBillEntity.getId();
						String thumbnail = sizeColorEntity.getDetail().getProduct().getThumbnail();
						String title = "Huỷ đơn hàng";
						String content = "Khách hàng " + productBillEntity.getBill().getCustomerBill().getFullName()
								+ " đã huỷ đơn hàng"; //
						Timestamp actionDate = common.dateToTimestamp(new Date());
						int status = 0;
						ShopEntity shopEntity = productBillEntity.getShopBill();
						NotificationShopDTO notificationShopDTO = new NotificationShopDTO(postId, thumbnail, title,
								content, actionDate, status);
						notifiShop.addNewNotification(notificationShopDTO, shopEntity);
						productBillRepository.updateStatusProductBill(productInBillDTO.getId(),
								productInBillDTO.getStatus(), productInBillDTO.getReason(), actionDate);
						return resProductBill.successRes("success");
					}
				}
				return resProductBill.successRes("You can not change status");
			}
			return resProductBill.successRes("Product does not exist");
		}
		return resProductBill.successRes("Not found");
	}

	// Shop
	@Override
	public ResponseList<ProductInBillDTO> findProductByStatusShop(ProductInBillDTO productInBillDTO) {
		List<ProductInBillEntity> productBillEntities = productBillRepository
				.findProductByStatusShop(productInBillDTO.getShopId(), productInBillDTO.getStatus());
		List<ProductInBillDTO> productBillDTOs = new ArrayList<ProductInBillDTO>();
		if (productBillEntities.size() > 0) {
			for (ProductInBillEntity productBillEntity : productBillEntities) {
				DeliveryDTO deliveryDTO = new DeliveryDTO();
				DeliveryEntity deliveryEntity = deliveryRepository.findOneById(productBillEntity.getDeliveryId());
				deliveryDTO.setName(deliveryEntity.getDelivery());
				deliveryDTO.setPrice(deliveryEntity.getPrice());
				ProductInBillDTO productBillDTO = billConverter.toProductBillDTO(productBillEntity);
				productBillDTO.setCustId(productBillEntity.getBill().getCustomerBill().getId());
				productBillDTO.setDelivery(deliveryDTO);
				productBillDTOs.add(productBillDTO);
			}
			return resProductBill.successResList("success", productBillEntities.size(), productBillDTOs);
		}
		return resProductBill.successResList("Bill not found", productBillDTOs);
	}

	@Override
	public Response<ProductInBillDTO> updateStatusProductShop(ProductInBillDTO productInBillDTO) {
		ProductInBillEntity productBillEntity = productBillRepository.findOneById(productInBillDTO.getId());
		if (productBillEntity != null) {
			SizeColorEntity sizeColorEntity = sizeColorRepository.findOneById(productBillEntity.getSizeId());
			if (sizeColorEntity != null) {
				Long postId = productInBillDTO.getId();
				String title = "";
				String content = "";
				String thumbnail = sizeColorEntity.getDetail().getProduct().getThumbnail();
				Timestamp actionDate = common.dateToTimestamp(new Date());
				int status = 0;
				if (productInBillDTO.getStatus() == 1) {
					if (productBillEntity.getQuantity() <= sizeColorEntity.getQuantity()) {
						title = "Xác nhận đơn hàng";
						content = "Cửa hàng " + productBillEntity.getShopBill().getShopName()
								+ " đã xác nhận đơn hàng, Chờ giao hàng";
						status = 1;
						// Tru so luong san pham trong kho hang
						int detailQuantity = sizeColorEntity.getDetail().getProduct().getQuantity()
								- productBillEntity.getQuantity();
						int sizeQuantity = sizeColorEntity.getQuantity() - productBillEntity.getQuantity();
						product.updateQuantitySizeColor(sizeColorEntity.getDetail().getId(), detailQuantity,
								sizeColorEntity.getId(), sizeQuantity);

						// update status bill
						productBillRepository.updateStatusProductBill(productInBillDTO.getId(),
								productInBillDTO.getStatus(), productInBillDTO.getReason(), actionDate);

						NotificationCustDTO notificationCustDTO = new NotificationCustDTO(postId, thumbnail, title,
								content, actionDate, status);
						notifiCust.addNewNotification(notificationCustDTO,
								productBillEntity.getBill().getCustomerBill());

						return resProductBill.successRes("success");
					} else {
						return resProductBill.successRes("Không đủ số lượng sản phẩm");
					}
				} else if (productInBillDTO.getStatus() == 2) {
					// update status bill
					productBillRepository.updateStatusProductBill(productInBillDTO.getId(),
							productInBillDTO.getStatus(), productInBillDTO.getReason(), actionDate);

					return resProductBill.successRes("success");
				} else if (productInBillDTO.getStatus() == 3) {
					String year = common.dateToString(new Date(), "y");
					String month = common.dateToString(new Date(), "m");
					revenue.getRevenueYearEntity(year, productBillEntity.getShopBill());
					revenue.updateRevenue(month, year, productBillEntity.getShopBill().getId(),
							sizeColorEntity.getPrice());

//					// Cong so luong san pham da ban
//					int soldProduct = sizeColorEntity.getDetail().getProduct().getSold();
//					Long productId = sizeColorEntity.getDetail().getProduct().getId();
//					int total = soldProduct + productBillEntity.getQuantity();
//					productRepository.updateSold(total, productId);

					// update status bill
					productBillRepository.updateStatusProductBill(productInBillDTO.getId(),
							productInBillDTO.getStatus(), productInBillDTO.getReason(), actionDate);

					return resProductBill.successRes("success");
				} else if (productInBillDTO.getStatus() == 4) {
					if (!productInBillDTO.getReason().isEmpty()) {
						productBillRepository.updateStatusProductBill(productInBillDTO.getId(),
								productInBillDTO.getStatus(), productInBillDTO.getReason(), actionDate);

						return resProductBill.successRes("success");
					} else {
						return resProductBill.successRes("Bạn phải nhập lý do");
					}
				} else if (productInBillDTO.getStatus() == 5) {
					if (!productInBillDTO.getReason().isEmpty()) {
						title = "Huỷ đơn hàng";
						content = "Cửa hàng " + productBillEntity.getShopBill().getShopName() + " đã huỷ đơn hàng";
						status = 5;
						
						if(productInBillDTO.getIsFail()) {
							// Cong so luong san pham trong kho hang da bi tru
							int detailQuantity = sizeColorEntity.getDetail().getProduct().getQuantity()
									+ productBillEntity.getQuantity();
							int sizeQuantity = sizeColorEntity.getQuantity() + productBillEntity.getQuantity();
							product.updateQuantitySizeColor(sizeColorEntity.getDetail().getId(), detailQuantity,
									sizeColorEntity.getId(), sizeQuantity);
						}
						
						productBillRepository.updateStatusProductBill(productInBillDTO.getId(),
								productInBillDTO.getStatus(), productInBillDTO.getReason(), actionDate);
						
						NotificationCustDTO notificationCustDTO = new NotificationCustDTO(postId, thumbnail, title,
								content, actionDate, status);
						notifiCust.addNewNotification(notificationCustDTO,
								productBillEntity.getBill().getCustomerBill());

						return resProductBill.successRes("success");
					} else {
						return resProductBill.successRes("Bạn phải nhập lý do");
					}
				} else {
					return resProductBill.successRes("error!");
				}
			}
			return resProductBill.successRes("Product does not exist");
		}
		return resProductBill.successRes("Not found");
	}

}
