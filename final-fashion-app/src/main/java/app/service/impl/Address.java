package app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.dto.AddressDTO;
import app.entity.CityEntity;
import app.entity.DistrictEntity;
import app.entity.VillageEntity;
import app.repository.CityRepository;
import app.repository.DistrictRepository;
import app.repository.VillageRepository;
import app.response.Responesive;
import app.response.ResponseList;
import app.service.IAddress;

@Service
public class Address implements IAddress {
	@Autowired
	CityRepository cityRepository;
	
	@Autowired
	DistrictRepository districtRepository;
	
	@Autowired
	VillageRepository villageRepository;

	@Autowired
	Responesive<AddressDTO> res;

	@Override
	public ResponseList<AddressDTO> findAllCity(String name, int page, int size) {
		Pageable pageCity = PageRequest.of(page, size);
		List<CityEntity> cities = cityRepository.findByName(name, pageCity);
		List<AddressDTO> addresses = new ArrayList<AddressDTO>();
		for(CityEntity city : cities) {
			AddressDTO address = toCityDTO(city);
			addresses.add(address);
		}
		return res.successResList("sucess", addresses);
	}

	@Override
	public ResponseList<AddressDTO> findDistrictByCityCode(int code, String name, int page, int size) {
		Pageable pageDistrict = PageRequest.of(page, size);
		List<DistrictEntity> districtes = districtRepository.findByCitycode(code, name, pageDistrict);
		List<AddressDTO> addresses = new ArrayList<AddressDTO>();
		for(DistrictEntity district : districtes) {
			AddressDTO address = toDistrictDTO(district);
			addresses.add(address);
		}
		return res.successResList("sucess", addresses);
	}

	@Override
	public ResponseList<AddressDTO> findVillageByDistrictCode(int code, String name, int page, int size) {
		Pageable pageVillage = PageRequest.of(page, size);
		List<VillageEntity> villages = villageRepository.findByDistrictCode(code, name, pageVillage);
		List<AddressDTO> addresses = new ArrayList<AddressDTO>();
		for(VillageEntity village : villages) {
			AddressDTO address = toVillageDTO(village);
			addresses.add(address);
		}
		return res.successResList("sucess", addresses);
	}

	public AddressDTO toCityDTO(CityEntity cityEntity) {
		AddressDTO addressDTO = new AddressDTO();
		addressDTO.setCode(cityEntity.getCityCode());
		addressDTO.setLevel(cityEntity.getLevel());
		addressDTO.setName(cityEntity.getCity());
		return addressDTO;
	}

	public AddressDTO toDistrictDTO(DistrictEntity districtEntity) {
		AddressDTO addressDTO = new AddressDTO();
		addressDTO.setCode(districtEntity.getDistrictCode());
		addressDTO.setLevel(districtEntity.getLevel());
		addressDTO.setName(districtEntity.getDistrict());
		return addressDTO;
	}

	public AddressDTO toVillageDTO(VillageEntity villageEntity) {
		AddressDTO addressDTO = new AddressDTO();
		addressDTO.setCode(villageEntity.getVillageCode());
		addressDTO.setLevel(villageEntity.getLevel());
		addressDTO.setName(villageEntity.getVillage());
		return addressDTO;
	}
}
