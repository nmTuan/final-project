package app.service;

import app.dto.CustomerDTO;
import app.dto.NotificationCustDTO;
import app.entity.CustomerEntity;
import app.response.Response;

public interface INotifiCust {

	void addNewNotification(NotificationCustDTO notificationCustDTO, CustomerEntity customerEntity);

	Response<CustomerDTO> getListNotifiByCustId(Long custId, int page, int size);

	Response<String> updateStatusNotifi(NotificationCustDTO notfiCustDTO);
	
	Response<Integer> countNotification(Long custId, int status);

}
