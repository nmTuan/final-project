package app.service;

import org.springframework.http.ResponseEntity;

import app.dto.AccountDTO;
import app.response.Response;

public interface IAccount {

	Response<AccountDTO> saveAccount(AccountDTO accountDTO);

	Response<AccountDTO> updatePassword(AccountDTO accountDTO);

	ResponseEntity<?> login(AccountDTO accountDTO);
}
