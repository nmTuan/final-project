package app.service;


import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface IFileStorage {
	
	public void init(String path);
	
	public void save(MultipartFile file);

	public Resource load(String fileName);
	
	public void saveImageInRoom(MultipartFile file, Long roomId);
}
