package app.response;

import app.dto.ResponseListDTO;

public class ResponseList<T> extends ResponseBase {

	private ResponseListDTO<T> result;

	public ResponseList(int errorCode, String message) {
		super(errorCode, message);
	}

	public ResponseList(int errorCode, String message, ResponseListDTO<T> result) {
		super(errorCode, message);
		this.result = result;
	}
	
	public ResponseListDTO<T> getResult() {
		return result;
	}

	public void setResult(ResponseListDTO<T> result) {
		this.result = result;
	}

}
