package app.response;

public class JwtAuthenticationReponse {
	private String accessToken;
	private String tokenType = "Bearer";
	private Long userId;

	public JwtAuthenticationReponse(String accessToken) {
		this.accessToken = accessToken;
	}

	public JwtAuthenticationReponse(String accessToken, Long userId) {
		this.accessToken = accessToken;
		this.userId = userId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}
}
