package app.response;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Response<T> extends ResponseBase {
	
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private T result;

	public Response(int errorCode, String message) {
		super(errorCode, message);
	}

	public Response(int errorCode, String message, T result) {
		super(errorCode, message);
		this.result = result;
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

}
