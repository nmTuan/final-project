package app.response;

import java.util.List;

import org.springframework.stereotype.Component;

import app.dto.ResponseListDTO;

@Component
public class Responesive<T> {
	public Response<T> successRes(String message, T result) {
		Response<T> res = new Response<T>(200, message, result);
		return res;
	}

	public Response<T> successRes(String message) {
		Response<T> res = new Response<T>(200, message);
		return res;
	}

	public ResponseList<T> successResList(String message, int size, List<T> result) {
		ResponseListDTO<T> resDto = new ResponseListDTO<>();
		resDto.setSize(size);
		resDto.setResult(result);
		ResponseList<T> res = new ResponseList<T>(200, message, resDto);
		return res;
	}
	
	public ResponseList<T> successResList(String message, List<T> result) {
		ResponseListDTO<T> resDto = new ResponseListDTO<>();
		resDto.setResult(result);
		ResponseList<T> res = new ResponseList<T>(200, message, resDto);
		return res;
	}

	public ResponseList<T> successResList(String message) {
		ResponseList<T> res = new ResponseList<T>(200, message);
		return res;
	}
}
