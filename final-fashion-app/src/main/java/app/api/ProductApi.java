package app.api;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import app.common.CommonApi;
import app.dto.PaymentDTO;
import app.dto.product.ImageProductDTO;
import app.dto.product.ProductDTO;
import app.dto.product.ProductDetailDTO;
import app.dto.product.SizeColorDTO;
import app.response.Response;
import app.response.ResponseList;
import app.service.impl.Product;

@RestController
public class ProductApi {

	@Autowired
	Product product;

	@PostMapping(value = CommonApi.PRODUCT + "/addNewProduct")
	public Response<ProductDetailDTO> addNewProduct(@Valid @RequestBody ProductDetailDTO productDTO) {
		return product.save(productDTO);
	}

	@PostMapping(value = CommonApi.PRODUCT + "/getListProductByName")
	public ResponseList<ProductDTO> getListProductByName(@RequestBody ProductDTO productDTO) {
		if (productDTO.getSize() == 0) {
			return product.findProductByName(productDTO, productDTO.getPage(), 10);
		}
		return product.findProductByName(productDTO, productDTO.getPage(), productDTO.getSize());
	}

	@PostMapping(value = CommonApi.PRODUCT + "/detailProduct")
	public Response<ProductDetailDTO> getDetailProduct(@RequestBody ProductDTO productDTO) {
		return product.getProductDetail(productDTO);
	}

	@PostMapping(value = CommonApi.PRODUCT + "/updateProduct")
	public Response<ProductDetailDTO> updateProduct(@Valid @RequestBody ProductDetailDTO productDTO) {
		return product.updateProduct(productDTO);
	}
	
	@PostMapping(value = CommonApi.PRODUCT + "/deleteProductById")
	public Response<ProductDTO> deleteProduct(@RequestBody ProductDTO productDTO) {
		return product.deleteProduct(productDTO);
	}
	
	//image api
	@PostMapping(value = CommonApi.PRODUCT + "/addListImage")
	public Response<ProductDetailDTO> addListImage(@Valid @RequestBody ProductDetailDTO productDTO) {
		return product.addNewListImage(productDTO);
	}
	
	@PostMapping(value = CommonApi.PRODUCT + "/updateImageProduct")
	public Response<ProductDetailDTO> updateImageProduct(@RequestBody ImageProductDTO imageDTO) {
		return product.updateImageProduct(imageDTO);
	}
	
	@PostMapping(value = CommonApi.PRODUCT + "/deleteImage")
	public Response<ProductDetailDTO> deleteImage(@RequestBody ImageProductDTO imageDTO) {
		return product.deleteImage(imageDTO);
	}
	
	//size color api
	@PostMapping(value = CommonApi.PRODUCT + "/addListSizeColor")
	public Response<ProductDetailDTO> addListSizeColor(@Valid @RequestBody ProductDetailDTO productDTO) {
		return product.addNewListSizeColor(productDTO);
	}

	@PostMapping(value = CommonApi.PRODUCT + "/updateSizeColor")
	public Response<ProductDetailDTO> updateSizeColor(@Valid @RequestBody SizeColorDTO sizeColorDTO) {
		return product.updateSizeColorProduct(sizeColorDTO);
	}
	
	@PostMapping(value = CommonApi.PRODUCT + "/deleteSizeColor")
	public Response<ProductDetailDTO> deleteSizeColor(@RequestBody SizeColorDTO sizeColorDTO) {
		return product.deleteSizeColor(sizeColorDTO);
	}
	
	@GetMapping(value = CommonApi.PRODUCT + "/listPaymentByProduct")
	public ResponseList<PaymentDTO> getListPayment(@RequestParam("sizeId") Long id) {
		return product.getListPaymentBySizeProduct(id);
	}
}
