package app.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import app.dto.PaymentDTO;
import app.entity.AgeEntity;
import app.entity.CategoryEntity;
import app.entity.GenderEntity;
import app.response.ResponseList;
import app.service.impl.Filter;

@RestController
public class FilterApi {

	@Autowired
	Filter filter;
	
	@GetMapping(value = "filter/categories")
	public ResponseList<CategoryEntity> getListCategory(){
		return filter.getListCategory();
	}
	
	@GetMapping(value = "filter/age")
	public ResponseList<AgeEntity> getListAge(){
		return filter.getListAge();
	}
	
	@GetMapping(value = "filter/gender")
	public ResponseList<GenderEntity> getListGender(){
		return filter.getListGender();
	}
	
	@GetMapping(value = "listPayment")
	public ResponseList<PaymentDTO> getListPayment(){
		return filter.getListPayment();
	}
	
}
