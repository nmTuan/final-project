package app.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import app.common.CommonApi;
import app.dto.BankCustDTO;
import app.dto.BankShopDTO;
import app.entity.BankEntity;
import app.response.Response;
import app.response.ResponseList;
import app.service.impl.Bank;

@RestController
public class BankApi {

	@Autowired
	Bank bank;
	
	@GetMapping(value = "bank/getListBank")
	public ResponseList<BankEntity> getListBank(@RequestParam("bank") String name) {
		return bank.findBankByName(name);
	}
	
	@PostMapping(value = "bank/setBankForShop")
	public Response<BankShopDTO> setBankForShop(@RequestBody BankShopDTO bankShopDTO) {
		return bank.saveBankShop(bankShopDTO);
	}
	
	@GetMapping(value = CommonApi.SHOPAPI + "/deleteBankShop")
	public Response<String> deleteBankShop(@RequestParam("bankId") int bankId) {
		return bank.deleteBankShop(bankId);
	}
	
	@GetMapping(value = CommonApi.SHOPAPI +  "/getListBankByShopId")
	public ResponseList<BankShopDTO> getListBankByShopId(@RequestParam("shopId") long shopId) {
		return bank.getListBankByShopId(shopId);
	}
	
	@PostMapping(value = "bank/setBankForCust")
	public Response<BankCustDTO> setBankForCust(@RequestBody BankCustDTO bankCustDTO) {
		return bank.saveCustShop(bankCustDTO);
	}
	
	@GetMapping(value = CommonApi.CUSTOMERAPI +  "/getListBankByCustId")
	public ResponseList<BankCustDTO> getListBankByCustId(@RequestParam("custId") long custId) {
		return bank.getListBankByCustId(custId);
	}
	
	@GetMapping(value = CommonApi.CUSTOMERAPI + "/deleteBankCust")
	public Response<String> deleteBankCust(@RequestParam("bankId") int bankId) {
		return bank.deleteBankCust(bankId);
	}
}
