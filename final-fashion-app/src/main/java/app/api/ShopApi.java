package app.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import app.common.CommonApi;
import app.dto.ShopDTO;
import app.dto.product.ProductDTO;
import app.response.Response;
import app.response.ResponseList;
import app.service.impl.Shop;

@RestController
public class ShopApi {

	@Autowired
	Shop shop;

	@PostMapping(value = CommonApi.SHOPAPI + "/create")
	public Response<ShopDTO> save(@RequestBody ShopDTO shopDto) {
		return shop.save(shopDto);
	}

	@PostMapping(value = CommonApi.SHOPAPI + "/shopInforByName")
	public ResponseList<ShopDTO> getShopInfoByName(@RequestBody ShopDTO shopDto) {
		if(shopDto.getSize() <= 0) {
			return shop.getShopInfoByShopname(shopDto.getShopName(), shopDto.getPage(), 10);
		}
		return shop.getShopInfoByShopname(shopDto.getShopName(), shopDto.getPage(), shopDto.getSize());
	}

	@PostMapping(value = CommonApi.SHOPAPI + "/shopInforById")
	public Response<ShopDTO> getShopInfoByid(@RequestBody ShopDTO shopDto) {
		return shop.getShopInfoById(shopDto.getId());
	}
	
	@PostMapping(value = CommonApi.SHOPAPI + "/updateShopInfo")
	public Response<ShopDTO> updateShopInfo(@RequestBody ShopDTO shopDto) {
		return shop.updateShopInfo(shopDto);
	}
	
	@PostMapping(value = CommonApi.SHOPAPI + "/shopInforByAccId")
	public Response<ShopDTO> getShopInfoByAccid(@RequestBody ShopDTO shopDto) {
		return shop.getShopInfoByAccId(shopDto.getAccId());
	}
	
	@PostMapping(value = CommonApi.SHOPAPI + "/checkPhoneNumberShop")
	public Response<String> checkPhoneNumber(@RequestBody ShopDTO shopDto) {
		return shop.checkPhoneNumber(shopDto);
	}
	
	@PostMapping(value = CommonApi.SHOPAPI + "/getListProduct")
	public ResponseList<ProductDTO> getListProduct(@RequestBody ProductDTO productDTO) {
		if(productDTO.getSize() <= 0) {
			return shop.getListProduct(productDTO, productDTO.getPage(), 10);
		}
		return shop.getListProduct(productDTO, productDTO.getPage(), productDTO.getSize());
	}
	
}
