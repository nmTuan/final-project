package app.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import app.common.CommonApi;
import app.dto.AccountDTO;
import app.dto.CustomerDTO;
import app.dto.OrderDTO;
import app.response.Response;
import app.response.ResponseList;
import app.service.impl.Customer;

@RestController
public class CustomerApi {

	@Autowired
	Customer customer;

	@PostMapping(value = CommonApi.CUSTOMERAPI + "/create")
	public Response<CustomerDTO> save(@RequestBody CustomerDTO customerDTO) {
		return customer.saveInforCust(customerDTO);
	}
	
	@PostMapping(value = CommonApi.CUSTOMERAPI + "/checkPhoneNumber")
	public Response<CustomerDTO> checkPhoneNumber(@RequestBody CustomerDTO customerDTO) {
		return customer.checkPhoneNumber(customerDTO);
	}

	@PostMapping(value = CommonApi.CUSTOMERAPI + "/findUserInfo")
	public Response<CustomerDTO> findUserInfo(@RequestBody AccountDTO accountDTO) {
		return customer.findCust(accountDTO);
	}

	@PostMapping(value = CommonApi.CUSTOMERAPI + "/updateUserInfo")
	public Response<CustomerDTO> updateUserInfo(@RequestBody CustomerDTO customerDTO) {
		return customer.updateUserInfo(customerDTO);
	}
	
	@PostMapping(value = CommonApi.CUSTOMERAPI + "/updatePhoneNumber")
	public Response<CustomerDTO> updatePhoneNumber(@RequestBody CustomerDTO customerDTO) {
		return customer.updatePhoneNumber(customerDTO);
	}
	
	@PostMapping(value = CommonApi.CUSTOMERAPI + "/getListProductInCart")
	public ResponseList<OrderDTO> getListProductInCart(@RequestBody OrderDTO orderDTO){
		return customer.getListProductCart(orderDTO);
	}

}
