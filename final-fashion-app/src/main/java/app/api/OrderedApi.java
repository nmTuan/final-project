package app.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import app.common.CommonApi;
import app.dto.OrderDTO;
import app.response.Response;
import app.service.impl.Ordered;

@RestController
public class OrderedApi {

	@Autowired
	Ordered ordered;

	@PostMapping(value = CommonApi.ORDERED + "/addProdcutToCart")
	public Response<OrderDTO> addProductToCart(@RequestBody OrderDTO orderDTO) {
		return ordered.addProduct(orderDTO);
	}
	
	@PostMapping(value = CommonApi.ORDERED + "/removeProductFromCart")
	public Response<OrderDTO> removeProductFromCart(@RequestBody OrderDTO orderDTO) {
		return ordered.removeProductFromCart(orderDTO);
	}
}
