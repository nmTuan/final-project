package app.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import app.common.CommonApi;
import app.dto.RevenueYearDTO;
import app.response.Response;
import app.response.ResponseList;
import app.service.impl.Revenue;

@RestController
public class RevenueApi {

	@Autowired
	Revenue revenue;
	
	@GetMapping(value = CommonApi.REVENUE + "/getListYearByShop")
	public ResponseList<RevenueYearDTO> getListYearByShop(@RequestParam Long shopId) {
		return revenue.getListYearByShopId(shopId);
	}
	
	@GetMapping(value = CommonApi.REVENUE + "/getListMonthByYear")
	public Response<RevenueYearDTO> getListMonthByYear(@RequestParam Long yearId) {
		return revenue.getListMonthByYear(yearId);
	}
	
	
}
