package app.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import app.common.CommonApi;
import app.dto.CustomerDTO;
import app.dto.NotificationCustDTO;
import app.dto.NotificationShopDTO;
import app.dto.ShopDTO;
import app.response.Response;
import app.service.impl.NotifiCust;
import app.service.impl.NotifiShop;

@RestController
public class NotificationApi {

	@Autowired
	NotifiShop notifiShop;

	@Autowired
	NotifiCust notifiCust;

	// SHOP
	@GetMapping(value = CommonApi.NOTIFICATION + "/getListNotifiByShopid")
	public Response<ShopDTO> getListNotifiByShopId(@RequestParam Long shopId, @RequestParam int page,
			@RequestParam int size) {
		return notifiShop.getListNotifiByShopId(shopId, page, size);
	}

	@PostMapping(value = CommonApi.NOTIFICATION + "/updateStatusNotifiShop")
	public Response<String> updateStatusNotifiShop(@RequestBody NotificationShopDTO notificationShopDTO) {
		return notifiShop.updateStatusNotifiShop(notificationShopDTO);
	}
	
	@GetMapping(value = CommonApi.NOTIFICATION + "/countNotificationShop")
	public Response<Integer> coungNotificationShop(@RequestParam("shopId") Long shopId, @RequestParam("status") int status) {
		return notifiShop.countNotification(shopId, status);
	}

	// CUSTOMER
	@GetMapping(value = CommonApi.NOTIFICATION + "/getListNotifiByCustid")
	public Response<CustomerDTO> getListNotifiByCustId(@RequestParam Long custId, @RequestParam int page,
			@RequestParam int size) {
		return notifiCust.getListNotifiByCustId(custId, page, size);
	}

	@PostMapping(value = CommonApi.NOTIFICATION + "/updateStatusNotifiCust")
	public Response<String> updateStatusNotifiCust(@RequestBody NotificationCustDTO notfiCustDTO) {
		return notifiCust.updateStatusNotifi(notfiCustDTO);
	}
	
	@GetMapping(value = CommonApi.NOTIFICATION + "/countNotificationCust")
	public Response<Integer> coungNotificationCust(@RequestParam("custId") Long custId, @RequestParam("status") int status) {
		return notifiCust.countNotification(custId, status);
	}
}
