package app.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import app.service.impl.FileStorage;

@RestController
public class FileApi {

	@Autowired
	FileStorage fileStorage;

	@PostMapping(value = "/upload/single", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String uploadSingleFile(@RequestParam("file") MultipartFile file) {
		fileStorage.save(file);
		String url = "images/" + file.getOriginalFilename();
		return url;
	}

	@PostMapping(value = "/upload/multiple", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public List<String> uploadMultipleFile(@RequestParam("files") MultipartFile[] files) {
		List<String> urls = new ArrayList<String>();
		Arrays.asList(files).stream().forEach(file -> {
			fileStorage.save(file);
			String url = "images/" + file.getOriginalFilename();
			urls.add(url);
		});
		return urls;
	}
	
	@PostMapping(value = "/upload/singleInRoomChat", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String uploadSingleFileInRoomChat(@RequestParam("file") MultipartFile file, @RequestParam Long roomId) {
		fileStorage.saveImageInRoom(file, roomId);
		String url = "images/rooms" + "/" + roomId + "/" + file.getOriginalFilename();
		return url;
	}
	
	@PostMapping(value = "/upload/multipleInRoomChat", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public List<String> uploadMultipleInRoomChat(@RequestParam("files") MultipartFile[] files, @RequestParam Long roomId) {
		List<String> urls = new ArrayList<String>();
		Arrays.asList(files).stream().forEachOrdered(file -> {
			fileStorage.saveImageInRoom(file, roomId);
			String url = "images/rooms" + "/" + roomId + "/" + file.getOriginalFilename();
			urls.add(url);
		});
		return urls;
	}
	

//	@GetMapping("uploads/{filename:.+}")
//	@ResponseBody
//	public ImageProductDTO getFile(@PathVariable String filename) {
//		Resource file = fileStorage.load(filename);
//		String name = file.getFilename();
//		String url = "uploads/" + file.getFilename();
//		return new ImageProductDTO(name, url);
//	}

}
