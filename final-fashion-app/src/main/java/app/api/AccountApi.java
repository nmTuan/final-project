package app.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import app.common.CommonApi;
import app.dto.AccountDTO;
import app.response.Response;
import app.service.impl.Account;
import app.service.impl.OtpService;

@RestController
public class AccountApi {

	@Autowired
	Account account;
	
	@Autowired
	OtpService otpService;

	@PostMapping(value = CommonApi.ACCOUNT + "/signin")
	public ResponseEntity<?> authenticate(@RequestBody AccountDTO accountDTO) {
		return account.login(accountDTO);
	}

	@PostMapping(value = CommonApi.ACCOUNT + "/signUp")
	public Response<AccountDTO> signUp(@RequestBody AccountDTO accountDTO) {
		return account.saveAccount(accountDTO);
	}

	@PostMapping(value = CommonApi.ACCOUNT + "/changePass")
	public Response<AccountDTO> updatePass(@RequestBody AccountDTO accountDTO) {
		return account.updatePassword(accountDTO);
	}
	
	@GetMapping(value = "/generateOtpWithToken")
	public String generateOtpWithToken(@RequestParam(name = "phonenumber") String phoneNumber) {
		return otpService.generateOtp(phoneNumber);
	}
	
	@GetMapping(value = "/generateOtp")
	public String generateOtp(@RequestParam(name = "phonenumber") String phoneNumber) {
		return otpService.generateOtp(phoneNumber);
	}
	
}
