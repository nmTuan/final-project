package app.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import app.common.CommonApi;
import app.dto.FeedbackDTO;
import app.response.Response;
import app.response.ResponseList;
import app.service.impl.Feedback;

@RestController
public class FeedbackApi {

	@Autowired
	Feedback feedback;

	@PostMapping(value = CommonApi.FEEDBACK + "/createFeedback")
	public Response<String> createFeedback(@RequestBody FeedbackDTO feedbackDTO) {
		return feedback.createNewFeedback(feedbackDTO);
	}

	@PostMapping(value = CommonApi.FEEDBACK + "/updateFeedback")
	public Response<String> updateFeedback(@RequestBody FeedbackDTO feedbackDTO) {
		return feedback.updateFeedback(feedbackDTO);
	}

	@GetMapping(value = CommonApi.FEEDBACK + "/getListFeedbackByDetailId")
	public ResponseList<FeedbackDTO> getListFeedbackByDetailId(@RequestParam long detailId, @RequestParam String vote,
			@RequestParam int page, @RequestParam int size) {
		return feedback.getListFeedbackByDetail(detailId, vote, 0, 5);
	}
}
