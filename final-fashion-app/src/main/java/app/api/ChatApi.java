package app.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import app.common.CommonApi;
import app.dto.ChatContentDTO;
import app.dto.CustomerDTO;
import app.dto.RoomDTO;
import app.dto.ShopDTO;
import app.response.Response;
import app.response.ResponseList;
import app.service.impl.Chat;

@RestController
public class ChatApi {

	@Autowired
	Chat chat;

	@GetMapping(value = CommonApi.CHAT + "/getRoomByCust")
	Response<CustomerDTO> getRoomByCust(@RequestParam Long custId, @RequestParam int page, @RequestParam int size) {
		return chat.getListRoomChatByCust(custId, page, size);
	}
	
	@GetMapping(value = CommonApi.CHAT + "/updateStatusRoomCust")
	Response<String> updateStatusRoomCust(@RequestParam("roomId") Long roomId, @RequestParam("status") int status) {
		return chat.updateStatusRoomCust(roomId, status);
	}

	@GetMapping(value = CommonApi.CHAT + "/getRoomByShop")
	Response<ShopDTO> getRoomByShop(@RequestParam Long shopId, @RequestParam int page, @RequestParam int size) {
		return chat.getListRoomChatByShop(shopId, page, size);
	}
	
	@GetMapping(value = CommonApi.CHAT + "/updateStatusRoomShop")
	Response<String> updateStatusRoomShop(@RequestParam("roomId") Long roomId, @RequestParam("status") int status) {
		return chat.updateStatusRoomShop(roomId, status);
	}
	
	@GetMapping(value = CommonApi.CHAT + "/getListContentByRoom")
	ResponseList<ChatContentDTO> getListContentByRoom(@RequestParam Long roomId, @RequestParam int page, @RequestParam int size) {
		return chat.getListContentByRoom(roomId, page, size);
	}
	
	@GetMapping(value = CommonApi.CHAT + "/getRoomByCustShop")
	Response<RoomDTO> getRoomByCustShop(@RequestParam Long custId, @RequestParam Long shopId) {
		return chat.getRoomByCustShop(custId, shopId);
	}
	
	@GetMapping(value = CommonApi.CHAT + "/countChatByCustStatus")
	Response<Integer> countChatByCustStatus(@RequestParam Long custId, @RequestParam int status) {
		return chat.countChatByStatusCust(custId, status);
	}
	
	@GetMapping(value = CommonApi.CHAT + "/countChatByShopStatus")
	Response<Integer> countChatByShopStatus(@RequestParam Long shopId, @RequestParam int status) {
		return chat.countChatByStatusShop(shopId, status);
	}
}
