package app.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import app.dto.AddressDTO;
import app.response.ResponseList;
import app.service.impl.Address;

@RestController
public class AddressApi {
	
	@Autowired
	Address address;
	
	@GetMapping(value = "address/city")
	public ResponseList<AddressDTO> getListCity(@RequestParam("name") String name, @RequestParam("page") String page, @RequestParam("size") String size){
		int sizePage = size.isEmpty() ? 10 : Integer.parseInt(size);
		int pageCity = page.isEmpty() ? 0 : Integer.parseInt(page);
		return address.findAllCity(name, pageCity, sizePage);
	}
	
	@GetMapping(value = "address/district")
	public ResponseList<AddressDTO> getDistricyByCityCode(@RequestParam("cityCode") int code, @RequestParam("name") String name, 
			@RequestParam("page") String page, @RequestParam("size") String size) {
		int sizePage = size.isEmpty() ? 10 : Integer.parseInt(size);
		int pageDistrict = page.isEmpty() ? 0 : Integer.parseInt(page);
		return address.findDistrictByCityCode(code, name, pageDistrict, sizePage);
	}
	
	@GetMapping(value = "address/village")
	public ResponseList<AddressDTO> getVillageByDistrict(@RequestParam("districtCode") int code, @RequestParam("name") String name, 
			@RequestParam("page") String page, @RequestParam("size") String size){
		int sizePage = size.isEmpty() ? 10 : Integer.parseInt(size);
		int pageVillage = page.isEmpty() ? 0 : Integer.parseInt(page);
		return address.findVillageByDistrictCode(code, name, pageVillage, sizePage);
	}	

}
