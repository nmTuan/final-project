package app.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestApi {

	@GetMapping("/api/test")
	public ResponseEntity<String> testSpringBoot() {
		return ResponseEntity.ok("sdd");
	}
}
