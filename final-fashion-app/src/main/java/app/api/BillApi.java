package app.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import app.common.CommonApi;
import app.dto.BillDTO;
import app.dto.ProductInBillDTO;
import app.response.Response;
import app.response.ResponseList;
import app.service.impl.Bill;

@RestController
public class BillApi {

	@Autowired
	Bill bill;

	@PostMapping(value = CommonApi.BILL + "/createBill")
	public Response<BillDTO> createBill(@RequestBody BillDTO billDTO) {
		return bill.save(billDTO);
	}

	@PostMapping(value = CommonApi.CUSTOMERAPI + "/getListBillByCustId")
	public ResponseList<BillDTO> getListBill(@RequestBody BillDTO billDTO) {
		return bill.findBillByCustId(billDTO);
	}
	
	@PostMapping(value = CommonApi.CUSTOMERAPI + "/getListProductByStatusCustomer")
	public ResponseList<ProductInBillDTO> getListProductByStatusCustomer(@RequestBody ProductInBillDTO productBillDto) {
		return bill.findProductByStatusCust(productBillDto);
	}
	
	@PostMapping(value = CommonApi.CUSTOMERAPI + "/detailProductBill")
	public Response<ProductInBillDTO> getDetailProductBill(@RequestBody ProductInBillDTO productBillDto) {
		return bill.getBillDetail(productBillDto);
	}
	
	@PostMapping(value = CommonApi.CUSTOMERAPI + "/changeStatusProductCust")
	public Response<ProductInBillDTO> changeStatusProductCust(@RequestBody ProductInBillDTO productBillDTO) {
		return bill.updateStatusProductCust(productBillDTO);
	}
	
	@PostMapping(value = CommonApi.SHOPAPI + "/getListProductByStatusShop")
	public ResponseList<ProductInBillDTO> getListProductByStatusShop(@RequestBody ProductInBillDTO productBillDTO) {
		return bill.findProductByStatusShop(productBillDTO);
	}
	
	@PostMapping(value = CommonApi.SHOPAPI + "/changeStatusProductShop")
	public Response<ProductInBillDTO> changeStatusProductShop(@RequestBody ProductInBillDTO productBillDTO) {
		return bill.updateStatusProductShop(productBillDTO);
	}

}
