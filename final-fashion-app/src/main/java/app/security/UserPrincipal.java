package app.security;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import app.entity.AccountEntity;

public class UserPrincipal implements UserDetails {
	
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String usename;
	private String password;
	private Collection<? extends GrantedAuthority> authorities;

	public UserPrincipal(Long id, String usename, String password, Collection<? extends GrantedAuthority> authorities) {
		this.id = id;
		this.usename = usename;
		this.password = password;
		this.authorities = authorities;
	}

	public static UserPrincipal createPrincipal(AccountEntity account){
	        List<GrantedAuthority> authorities =  account.getRoles().stream().map(role ->
	        new SimpleGrantedAuthority(role.getRoleName())).collect(Collectors.toList());
	        return new UserPrincipal(
	        		account.getId(),
	        		account.getUsername(),
	        		account.getPassword(),
	                authorities
	        );
	    }

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public Long getId() {
		return id;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return usename;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
	
	@Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        UserPrincipal that =(UserPrincipal) obj;
        return Objects.equals(id, that.id);
    }


}
