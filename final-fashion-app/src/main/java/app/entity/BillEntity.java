package app.entity;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "bill")
public class BillEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "payment_id", nullable = false)
	private int payment_id;

	@Column(name = "createddate", nullable = false)
	private Timestamp createdDate;

	@Column(name = "orderplace")
	private String orderplace;
	
	@Column(name = "bank_name")
	private String bankName;
	
	@Column(name = "credit_number")
	private String creditNumber;

	@ManyToOne
	@JoinTable(name = "bill_cust", joinColumns = @JoinColumn(name = "bill_id"), inverseJoinColumns = @JoinColumn(name = "cust_id"))
	private CustomerEntity customerBill;

	@OneToMany(mappedBy = "bill")
	private List<ProductInBillEntity> productInBills;

	public List<ProductInBillEntity> getProductInBills() {
		return productInBills;
	}

	public void setProductInBills(List<ProductInBillEntity> productInBills) {
		this.productInBills = productInBills;
	}

	public String getOrderplace() {
		return orderplace;
	}

	public void setOrderplace(String orderplace) {
		this.orderplace = orderplace;
	}

	public void setPayment_id(int payment_id) {
		this.payment_id = payment_id;
	}

	public int getPayment_id() {
		return payment_id;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public CustomerEntity getCustomerBill() {
		return customerBill;
	}

	public void setCustomerBill(CustomerEntity customerBill) {
		this.customerBill = customerBill;
	}

	public Long getId() {
		return id;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getCreditNumber() {
		return creditNumber;
	}

	public void setCreditNumber(String creditNumber) {
		this.creditNumber = creditNumber;
	}
}
