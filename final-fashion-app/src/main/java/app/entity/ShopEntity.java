package app.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

@Entity
@Table(name = "shop")
public class ShopEntity extends InfoEntityBase {

	@Column(name = "shopcode", nullable = false)
	private String shopCode;

	@Column(name = "shopname", nullable = false)
	@Size(max = 20)
	private String shopName;

	@Column(name = "vote")
	@Min(value = 0)
	private double vote;

	@Column(name = "socket_id")
	private String socketId;

	@Column(name = "description")
	private String description;

	@OneToOne
	@JoinTable(name = "account_shop", joinColumns = @JoinColumn(name = "shop_id"), inverseJoinColumns = @JoinColumn(name = "acc_id"))
	private AccountEntity accShop;

	@OneToMany(mappedBy = "shopProduct")
	private List<ItemProductEntity> products;

	@OneToMany(mappedBy = "shopRoom")
	private List<RoomEntity> rooms;

	@OneToMany(mappedBy = "shopBill")
	private List<ProductInBillEntity> productBills;

	@OneToMany(mappedBy = "RevenueShop")
	private List<RevenueYearEntity> revenueYearEntities;

	@OneToMany(mappedBy = "notifiShop")
	private List<NotifiShopEntity> notifications;

	@OneToMany(mappedBy = "shopBank")
	private List<BankShopEntity> bankShops;

	public List<NotifiShopEntity> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<NotifiShopEntity> notifications) {
		this.notifications = notifications;
	}

	public List<RevenueYearEntity> getRevenueYearEntities() {
		return revenueYearEntities;
	}

	public void setRevenueYearEntities(List<RevenueYearEntity> revenueYearEntities) {
		this.revenueYearEntities = revenueYearEntities;
	}

	public List<ProductInBillEntity> getProductBills() {
		return productBills;
	}

	public void setProductBills(List<ProductInBillEntity> productBills) {
		this.productBills = productBills;
	}

	public List<RoomEntity> getRooms() {
		return rooms;
	}

	public void setRooms(List<RoomEntity> rooms) {
		this.rooms = rooms;
	}

	public List<ItemProductEntity> getProducts() {
		return products;
	}

	public void setProducts(List<ItemProductEntity> products) {
		this.products = products;
	}

	public AccountEntity getAccShop() {
		return accShop;
	}

	public void setAccShop(AccountEntity accShop) {
		this.accShop = accShop;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public double getVote() {
		return vote;
	}

	public void setVote(double vote) {
		this.vote = vote;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSocketId() {
		return socketId;
	}

	public void setSocketId(String socketId) {
		this.socketId = socketId;
	}

	public List<BankShopEntity> getBankShops() {
		return bankShops;
	}

	public void setBankShops(List<BankShopEntity> bankShops) {
		this.bankShops = bankShops;
	}

}
