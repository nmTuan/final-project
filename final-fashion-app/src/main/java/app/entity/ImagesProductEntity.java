package app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "images")
public class ImagesProductEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "name", nullable = false)
	@Size(max = 100)
	private String name;

	@Column(name = "image", nullable = false)
	@Size(max = 50)
	private String image;

	@ManyToOne
	@JoinTable(name = "image_detail", joinColumns = @JoinColumn(name = "image_id"), inverseJoinColumns = @JoinColumn(name = "detail_id"))
	private ProductDetailEntity imageProduct;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public ProductDetailEntity getDetail() {
		return imageProduct;
	}

	public void setDetail(ProductDetailEntity imageProduct) {
		this.imageProduct = imageProduct;
	}

	public Long getId() {
		return id;
	}

}
