package app.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "product_in_bill")
public class ProductInBillEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "size_id", nullable = false)
	private Long sizeId;

	@Column(name = "delivery_id", nullable = false)
	private int deliveryId;

	@Column(name = "note")
	private String note;

	@Column(name = "quantity", nullable = false)
	private int quantity;

	@Column(name = "actiondate")
	private Timestamp actionDate;

	@Column(name = "reason")
	private String reason;

	@Column(name = "status", nullable = false)
	private int status;

	@Column(name = "isrent", nullable = false)
	private boolean isRent;

	@Column(name = "rentPrice", nullable = false)
	private int rentPrice;

	@Column(name = "deposit", nullable = false)
	private int deposit;

	@Column(name = "rentDue", nullable = false)
	private int rentDue;
	
	@Column(name = "payment_status")
	private int paymentStatus;

	@ManyToOne
	@JoinTable(name = "product_of_bill", joinColumns = @JoinColumn(name = "product_bill_id"), inverseJoinColumns = @JoinColumn(name = "bill_id"))
	private BillEntity bill;

	@ManyToOne
	@JoinTable(name = "bill_shop", joinColumns = @JoinColumn(name = "product_bill_id"), inverseJoinColumns = @JoinColumn(name = "shop_id"))
	private ShopEntity shopBill;

	@ManyToOne
	@JoinTable(name = "bill_product_cust", joinColumns = @JoinColumn(name = "product_bill_id"), inverseJoinColumns = @JoinColumn(name = "cust_id"))
	private CustomerEntity custBillProduct;

	public CustomerEntity getCustBillProduct() {
		return custBillProduct;
	}

	public void setCustBillProduct(CustomerEntity custBillProduct) {
		this.custBillProduct = custBillProduct;
	}

	public boolean getIsRent() {
		return isRent;
	}

	public void setIsRent(boolean isRent) {
		this.isRent = isRent;
	}

	public ShopEntity getShopBill() {
		return shopBill;
	}

	public void setShopBill(ShopEntity shopBill) {
		this.shopBill = shopBill;
	}

	public Long getSizeId() {
		return sizeId;
	}

	public void setSizeId(Long sizeId) {
		this.sizeId = sizeId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BillEntity getBill() {
		return bill;
	}

	public void setBill(BillEntity bill) {
		this.bill = bill;
	}

	public Long getId() {
		return id;
	}

	public Timestamp getActionDate() {
		return actionDate;
	}

	public void setActionDate(Timestamp actionDate) {
		this.actionDate = actionDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(int deliveryId) {
		this.deliveryId = deliveryId;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public int getRentPrice() {
		return rentPrice;
	}

	public void setRentPrice(int rentPrice) {
		this.rentPrice = rentPrice;
	}

	public int getDeposit() {
		return deposit;
	}

	public void setDeposit(int deposit) {
		this.deposit = deposit;
	}

	public int getRentDue() {
		return rentDue;
	}

	public void setRentDue(int rentDue) {
		this.rentDue = rentDue;
	}

	public int getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(int paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
}
