package app.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Size;

@MappedSuperclass
public class InfoEntityBase {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "avatar")
	@Size(max = 50)
	private String avatar;

	@Column(name = "coverava")
	@Size(max = 50)
	private String coverava;

	@Column(name = "phonenumber", nullable = false)
	@Size(max = 11)
	private String phoneNumber;

	@Column(name = "address", nullable = false)
	@Size(max = 255)
	private String address;

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getCoverava() {
		return coverava;
	}

	public void setCoverava(String coverava) {
		this.coverava = coverava;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getId() {
		return id;
	}

}
