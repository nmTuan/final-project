package app.entity;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "room")
public class RoomEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "name")
	@Size(max = 255)
	private String name;

	@Column(name = "modifieddate")
	private Timestamp modifiedDate;

	@Column(name = "cust_status")
	private int statusCust;
	
	@Column(name = "shop_status")
	private int statusShop;
	
	@Column(name = "last_content", columnDefinition = "TEXT")
	private String lastContent;

	@ManyToOne
	@JoinTable(name = "room_cust", joinColumns = @JoinColumn(name = "room_id"), inverseJoinColumns = @JoinColumn(name = "cust_id"))
	private CustomerEntity customerRoom;

	@ManyToOne
	@JoinTable(name = "room_shop", joinColumns = @JoinColumn(name = "room_id"), inverseJoinColumns = @JoinColumn(name = "shop_id"))
	private ShopEntity shopRoom;

	@OneToMany(mappedBy = "room")
	private List<ChatContentEntity> contents;

	public List<ChatContentEntity> getContents() {
		return contents;
	}

	public void setContents(List<ChatContentEntity> contents) {
		this.contents = contents;
	}

	public CustomerEntity getCustomerRoom() {
		return customerRoom;
	}

	public void setCustomerRoom(CustomerEntity customerRoom) {
		this.customerRoom = customerRoom;
	}

	public ShopEntity getShopRoom() {
		return shopRoom;
	}

	public void setShopRoom(ShopEntity shopRoom) {
		this.shopRoom = shopRoom;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public int getStatusCust() {
		return statusCust;
	}

	public void setStatusCust(int statusCust) {
		this.statusCust = statusCust;
	}

	public int getStatusShop() {
		return statusShop;
	}

	public void setStatusShop(int statusShop) {
		this.statusShop = statusShop;
	}

	public String getLastContent() {
		return lastContent;
	}

	public void setLastContent(String lastContent) {
		this.lastContent = lastContent;
	}
}
