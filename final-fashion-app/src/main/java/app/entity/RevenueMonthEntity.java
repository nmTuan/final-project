package app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "revenue_month")
public class RevenueMonthEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "month", nullable = false)
	@Size(max = 8)
	private String month;

	@Column(name = "revenue", nullable = false)
	private int revenue;

	@ManyToOne
	@JoinTable(name = "revenue_year_month", joinColumns = @JoinColumn(name = "month_id"), inverseJoinColumns = @JoinColumn(name = "year_id"))
	private RevenueYearEntity revenueYearEntity;

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public int getRevenue() {
		return revenue;
	}

	public void setRevenue(int revenue) {
		this.revenue = revenue;
	}

	public RevenueYearEntity getRevenueYearEntity() {
		return revenueYearEntity;
	}

	public void setRevenueYearEntity(RevenueYearEntity revenueYearEntity) {
		this.revenueYearEntity = revenueYearEntity;
	}

	public Long getId() {
		return id;
	}

}
