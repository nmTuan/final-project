package app.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cust_notification")
public class NotifiCustEntity extends BaseNotifyEntity {

	@ManyToOne
	@JoinTable(name = "notification_customer", joinColumns = @JoinColumn(name = "notification_id"), inverseJoinColumns = @JoinColumn(name = "cust_id"))
	private CustomerEntity notifiCust;

	public CustomerEntity getNotifiCust() {
		return notifiCust;
	}

	public void setNotifiCust(CustomerEntity notifiCust) {
		this.notifiCust = notifiCust;
	}

}
