package app.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "shop_bank")
public class BankShopEntity extends BankInfoBaseEntity{

	@ManyToOne
	@JoinTable(name = "shop_to_bank", joinColumns = @JoinColumn(name = "bank_id"), inverseJoinColumns = @JoinColumn(name = "shop_id"))
	private ShopEntity shopBank;

	public ShopEntity getShopBank() {
		return shopBank;
	}

	public void setShopBank(ShopEntity shopBank) {
		this.shopBank = shopBank;
	}
	
}
