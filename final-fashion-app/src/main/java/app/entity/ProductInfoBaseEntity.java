package app.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

@MappedSuperclass
public class ProductInfoBaseEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "thumbnail", nullable = false)
	@Size(max = 50)
	private String thumbnail;

	@Column(name = "productname", nullable = false)
	@Size(max = 100)
	private String productName;

	@Column(name = "shortdescription", nullable = false, columnDefinition = "TEXT")
	private String shortDescription;

	@Column(name = "minprice")
	@Min(value = 0)
	private int minPrice;

	@Column(name = "maxprice", nullable = false)
	@Min(value = 0)
	private int maxPrice;

	@Column(name = "oldprice")
	@Min(value = 0)
	private int oldPrice;

	@Column(name = "quantity")
	@Min(value = 0)
	private int quantity;

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public int getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(int minPrice) {
		this.minPrice = minPrice;
	}

	public int getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(int maxPrice) {
		this.maxPrice = maxPrice;
	}

	public int getOldPrice() {
		return oldPrice;
	}

	public void setOldPrice(int oldPrice) {
		this.oldPrice = oldPrice;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Long getId() {
		return id;
	}

}
