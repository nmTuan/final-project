package app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;

@Entity
@Table(name = "product")
public class ItemProductEntity extends ProductInfoBaseEntity {

	@Column(name = "vote", nullable = false)
	@Min(value = 0)
	private double vote;

	@Column(name = "category_id", nullable = false)
	private Long category_id;

	@Column(name = "gender_id", nullable = false)
	private Long gender_id;

	@Column(name = "age_id", nullable = false)
	private Long age_id;

	@Column(name = "isrent", nullable = false)
	private Boolean isRent;
	
	@Column(name = "sold", nullable = false)
	private int sold;

	@ManyToOne
	@JoinTable(name = "shop_product", joinColumns = @JoinColumn(name = "product_id"), inverseJoinColumns = @JoinColumn(name = "shop_id"))
	private ShopEntity shopProduct;

	@OneToOne(mappedBy = "product")
	private ProductDetailEntity detail;

	public Boolean getIsRent() {
		return isRent;
	}

	public void setIsRent(Boolean isRent) {
		this.isRent = isRent;
	}

	public Long getAge_id() {
		return age_id;
	}

	public void setAge_id(Long age_id) {
		this.age_id = age_id;
	}

	public ProductDetailEntity getDetail() {
		return detail;
	}

	public void setDetail(ProductDetailEntity detail) {
		this.detail = detail;
	}

	public ShopEntity getShopProduct() {
		return shopProduct;
	}

	public void setShopProduct(ShopEntity shopProduct) {
		this.shopProduct = shopProduct;
	}

	public double getVote() {
		return vote;
	}

	public void setVote(double vote) {
		this.vote = vote;
	}

	public Long getCategory_id() {
		return category_id;
	}

	public void setCategory_id(Long category_id) {
		this.category_id = category_id;
	}

	public Long getGender_id() {
		return gender_id;
	}

	public void setGender_id(Long gender_id) {
		this.gender_id = gender_id;
	}

	public int getSold() {
		return sold;
	}

	public void setSold(int sold) {
		this.sold = sold;
	}
}
