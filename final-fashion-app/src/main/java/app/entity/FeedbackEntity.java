package app.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "feedback")
public class FeedbackEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "content", nullable = false)
	private String content;

	@Column(name = "vote")
	private double vote;

	@Column(name = "avatar")
	@Size(max = 50)
	private String avatar;

	@Column(name = "username", nullable = false)
	@Size(max = 20)
	private String username;

	@Column(name = "createddate", nullable = false)
	private Timestamp createdDate;

	@Column(name = "modifieddate")
	private Timestamp modifiedDate;

	@Column(name = "bill_id", nullable = false)
	private Long billId;

	@ManyToOne
	@JoinTable(name = "feeback_detailProduct", joinColumns = @JoinColumn(name = "feedback_id"), inverseJoinColumns = @JoinColumn(name = "detail_id"))
	private ProductDetailEntity productFeedback;

	@ManyToOne
	@JoinTable(name = "feeback_customer", joinColumns = @JoinColumn(name = "feedback_id"), inverseJoinColumns = @JoinColumn(name = "cust_id"))
	private CustomerEntity customerFeedback;

	public Long getBillId() {
		return billId;
	}

	public void setBillId(Long billId) {
		this.billId = billId;
	}

	public ProductDetailEntity getProductFeedback() {
		return productFeedback;
	}

	public void setProductFeedback(ProductDetailEntity productFeedback) {
		this.productFeedback = productFeedback;
	}

	public CustomerEntity getCustomerFeedback() {
		return customerFeedback;
	}

	public void setCustomerFeedback(CustomerEntity customerFeedback) {
		this.customerFeedback = customerFeedback;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public double getVote() {
		return vote;
	}

	public void setVote(double vote) {
		this.vote = vote;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Long getId() {
		return id;
	}

}
