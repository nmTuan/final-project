package app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "district")
public class DistrictEntity {

	@Id
	@Column(name = "districtcode")
	private int districtCode;

	@Column(name = "district")
	@Size(max = 150)
	private String district;

	@Column(name = "level")
	@Size(max = 20)
	private String level;

	@Column(name = "citycode")
	private int citycode;

	public int getDistrictCode() {
		return districtCode;
	}

	public String getDistrict() {
		return district;
	}

	public String getLevel() {
		return level;
	}

	public int getCitycode() {
		return citycode;
	}

}
