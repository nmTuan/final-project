package app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "city")
public class CityEntity {

	@Id
	@Column(name = "citycode")
	private int cityCode;

	@Column(name = "city")
	@Size(max = 100)
	private String city;

	@Column(name = "level")
	@Size(max = 20)
	private String level;

	public int getCityCode() {
		return cityCode;
	}

	public String getCity() {
		return city;
	}

	public String getLevel() {
		return level;
	}

}
