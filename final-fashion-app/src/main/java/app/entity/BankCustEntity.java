package app.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bank_cust")
public class BankCustEntity extends BankInfoBaseEntity{

	@ManyToOne
	@JoinTable(name = "bank_to_cust", joinColumns = @JoinColumn(name = "bank_id"), inverseJoinColumns = @JoinColumn(name = "cust_id"))
	private CustomerEntity custBank;

	public CustomerEntity getCustBank() {
		return custBank;
	}

	public void setCustBank(CustomerEntity custBank) {
		this.custBank = custBank;
	}

}
