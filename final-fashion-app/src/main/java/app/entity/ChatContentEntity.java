package app.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "chat_content")
public class ChatContentEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "content", nullable = false)
	private String content;

	@Column(name = "sender", nullable = false)
	@Size(max = 20)
	private String sender;

	@Column(name = "senderava", nullable = false)
	@Size(max = 50)
	private String senderAva;

	@Column(name = "receiver", nullable = false)
	@Size(max = 20)
	private String receiver;

	@Column(name = "receiverava", nullable = false)
	@Size(max = 50)
	private String receiverAva;

	@Column(name = "createdate", nullable = false)
	private Timestamp createdDate;

	@Column(name = "modifieddate")
	private Timestamp modifiedDate;

	@ManyToOne
	@JoinTable(name = "room_content", joinColumns = @JoinColumn(name = "content_id"), inverseJoinColumns = @JoinColumn(name = "room_id"))
	private RoomEntity room;

	public RoomEntity getRoom() {
		return room;
	}

	public void setRoom(RoomEntity room) {
		this.room = room;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getSenderAva() {
		return senderAva;
	}

	public void setSenderAva(String senderAva) {
		this.senderAva = senderAva;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getReceiverAva() {
		return receiverAva;
	}

	public void setReceiverAva(String receiverAva) {
		this.receiverAva = receiverAva;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Long getId() {
		return id;
	}

}
