package app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

@Entity
@Table(name = "size_color")
public class SizeColorEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "size", nullable = false)
	@Size(max = 20)
	private String size;

	@Column(name = "color", nullable = false)
	@Size(max = 100)
	private String color;

	@Column(name = "quantity", nullable = false)
	@Min(value = 0)
	private int quantity;

	@Column(name = "price", nullable = false)
	@Min(value = 0)
	private int price;

	@ManyToOne
	@JoinTable(name = "size_detail", joinColumns = @JoinColumn(name = "size_id"), inverseJoinColumns = @JoinColumn(name = "detail_id"))
	private ProductDetailEntity sizeProduct;

	@Column(name = "rent_due_id")
	private int rentDueId;

	@Column(name = "rentprice")
	private int rentPrice;

	@Column(name = "deposit")
	private int deposit;

	public int getDeposit() {
		return deposit;
	}

	public void setDeposit(int deposit) {
		this.deposit = deposit;
	}

	public int getRentDueId() {
		return rentDueId;
	}

	public void setRentDueId(int rentDueId) {
		this.rentDueId = rentDueId;
	}

	public int getRentPrice() {
		return rentPrice;
	}

	public void setRentPrice(int rentPrice) {
		this.rentPrice = rentPrice;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public ProductDetailEntity getDetail() {
		return sizeProduct;
	}

	public void setDetail(ProductDetailEntity sizeProduct) {
		this.sizeProduct = sizeProduct;
	}

	public Long getId() {
		return id;
	}

}
