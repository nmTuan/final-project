package app.entity;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "customer")
public class CustomerEntity extends InfoEntityBase {

	@Column(name = "fullname", nullable = false)
	@Size(max = 100)
	private String fullName;

	@Column(name = "city", nullable = false)
	@Size(max = 100)
	private String city;

	@Column(name = "district", nullable = false)
	@Size(max = 100)
	private String district;

	@Column(name = "village", nullable = false)
	@Size(max = 100)
	private String village;
	
	@Column(name = "socket_id")
	private String socketId;

	@Column(name = "dob")
	private Timestamp dob;

	@Column(name = "gender")
	@Size(max = 4)
	private String gender;

	@OneToOne
	@JoinTable(name = "account_cust", joinColumns = @JoinColumn(name = "cust_id"), inverseJoinColumns = @JoinColumn(name = "acc_id"))
	private AccountEntity accCust;

	@OneToMany(mappedBy = "customerOrderd")
	private List<OrderedEntity> orders;

	@OneToMany(mappedBy = "customerBill")
	private List<BillEntity> bills;
	
	@OneToMany(mappedBy = "custBillProduct")
	private List<ProductInBillEntity> productInBill;

	@OneToMany(mappedBy = "customerFeedback")
	private List<FeedbackEntity> feedbacks;

	@OneToMany(mappedBy = "customerRoom")
	private List<RoomEntity> rooms;

	@OneToMany(mappedBy = "notifiCust")
	private List<NotifiCustEntity> notifications;
	
	@OneToMany(mappedBy = "custBank")
	private List<BankCustEntity> bankCustEntities;

	public List<ProductInBillEntity> getProductInBill() {
		return productInBill;
	}

	public void setProductInBill(List<ProductInBillEntity> productInBill) {
		this.productInBill = productInBill;
	}

	public List<NotifiCustEntity> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<NotifiCustEntity> notifications) {
		this.notifications = notifications;
	}

	public List<BillEntity> getBills() {
		return bills;
	}

	public void setBills(List<BillEntity> bills) {
		this.bills = bills;
	}

	public List<RoomEntity> getRooms() {
		return rooms;
	}

	public void setRooms(List<RoomEntity> rooms) {
		this.rooms = rooms;
	}

	public List<FeedbackEntity> getFeedbacks() {
		return feedbacks;
	}

	public void setFeedbacks(List<FeedbackEntity> feedbacks) {
		this.feedbacks = feedbacks;
	}

	public List<OrderedEntity> getOrders() {
		return orders;
	}

	public void setOrders(List<OrderedEntity> orders) {
		this.orders = orders;
	}

	public AccountEntity getAccCust() {
		return accCust;
	}

	public void setAccCust(AccountEntity accCust) {
		this.accCust = accCust;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Timestamp getDob() {
		return dob;
	}

	public void setDob(Timestamp dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public String getSocketId() {
		return socketId;
	}

	public void setSocketId(String socketId) {
		this.socketId = socketId;
	}

	public List<BankCustEntity> getBankCustEntities() {
		return bankCustEntities;
	}

	public void setBankCustEntities(List<BankCustEntity> bankCustEntities) {
		this.bankCustEntities = bankCustEntities;
	}
}
