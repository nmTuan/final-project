package app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "village")
public class VillageEntity {

	@Id
	@Column(name = "villagecode")
	private int villageCode;

	@Column(name = "village")
	@Size(max = 150)
	private String village;

	@Column(name = "level")
	@Size(max = 20)
	private String level;

	@Column(name = "districtcode")
	private int districtCode;

	@Column(name = "citycode")
	private int citycode;

	public int getVillageCode() {
		return villageCode;
	}

	public String getVillage() {
		return village;
	}

	public String getLevel() {
		return level;
	}

	public int getDistrictCode() {
		return districtCode;
	}

	public int getCitycode() {
		return citycode;
	}

}
