package app.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "shop_notification")
public class NotifiShopEntity extends BaseNotifyEntity {

	@ManyToOne
	@JoinTable(name = "notification_shop", joinColumns = @JoinColumn(name = "notification_id"), inverseJoinColumns = @JoinColumn(name = "shop_id"))
	private ShopEntity notifiShop;

	public ShopEntity getNotifiShop() {
		return notifiShop;
	}

	public void setNotifiShop(ShopEntity notifiShop) {
		this.notifiShop = notifiShop;
	}

}
