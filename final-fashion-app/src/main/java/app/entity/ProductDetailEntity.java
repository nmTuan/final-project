package app.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "product_detail")
public class ProductDetailEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "description", nullable = false, columnDefinition = "TEXT")
	private String description;

	@Column(name = "brand", nullable = false)
	private String brand;

	@Column(name = "type")
	private String type;

	@Column(name = "material", nullable = false)
	private String material;
	
	@Column(name = "price_delivery")
	private int priceDelivery;

	@Column(name = "bank_id")
	private int bankId;
	
	@Column(name = "factory")
	private String factory;
	
	@OneToOne
	@JoinTable(name = "item_product_detail", joinColumns = @JoinColumn(name = "detail_id"), inverseJoinColumns = @JoinColumn(name = "product_id"))
	private ItemProductEntity product;

	@OneToMany(mappedBy = "imageProduct")
	private List<ImagesProductEntity> images;

	@OneToMany(mappedBy = "sizeProduct")
	private List<SizeColorEntity> sizeColor;

	@OneToMany(mappedBy = "productFeedback")
	private List<FeedbackEntity> feedbacks;
	
	@ManyToMany
	@JoinTable(name = "payment_product", joinColumns = @JoinColumn(name = "detail_id"), inverseJoinColumns = @JoinColumn(name = "payment_id"))
	private List<PaymentEntity> paymentEntities;

	public List<FeedbackEntity> getFeedbacks() {
		return feedbacks;
	}

	public void setFeedbacks(List<FeedbackEntity> feedbacks) {
		this.feedbacks = feedbacks;
	}

	public List<SizeColorEntity> getSizeColor() {
		return sizeColor;
	}

	public void setSizeColor(List<SizeColorEntity> sizeColor) {
		this.sizeColor = sizeColor;
	}

	public List<ImagesProductEntity> getImages() {
		return images;
	}

	public void setImages(List<ImagesProductEntity> images) {
		this.images = images;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public ItemProductEntity getProduct() {
		return product;
	}

	public void setProduct(ItemProductEntity product) {
		this.product = product;
	}

	public Long getId() {
		return id;
	}

	public List<PaymentEntity> getPaymentEntities() {
		return paymentEntities;
	}

	public void setPaymentEntities(List<PaymentEntity> paymentEntities) {
		this.paymentEntities = paymentEntities;
	}

	public int getPriceDelivery() {
		return priceDelivery;
	}

	public void setPriceDelivery(int priceDelivery) {
		this.priceDelivery = priceDelivery;
	}

	public int getBankId() {
		return bankId;
	}

	public void setBankId(int bankId) {
		this.bankId = bankId;
	}

	public String getFactory() {
		return factory;
	}

	public void setFactory(String factory) {
		this.factory = factory;
	}
	
}
