package app.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "revenue_year")
public class RevenueYearEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "year", nullable = false)
	@Size(max = 4)
	private String year;

	@ManyToOne
	@JoinTable(name = "revenue_year_shop", joinColumns = @JoinColumn(name = "revenue_year_id"), inverseJoinColumns = @JoinColumn(name = "shop_id"))
	private ShopEntity RevenueShop;

	@OneToMany(mappedBy = "revenueYearEntity")
	private List<RevenueMonthEntity> revenueMonthEntities;

	public List<RevenueMonthEntity> getRevenueMonthEntities() {
		return revenueMonthEntities;
	}

	public void setRevenueMonthEntities(List<RevenueMonthEntity> revenueMonthEntities) {
		this.revenueMonthEntities = revenueMonthEntities;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public ShopEntity getRevenueShop() {
		return RevenueShop;
	}

	public void setRevenueShop(ShopEntity revenueShop) {
		RevenueShop = revenueShop;
	}

	public Long getId() {
		return id;
	}

}
