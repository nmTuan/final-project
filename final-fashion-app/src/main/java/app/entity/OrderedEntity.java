package app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ordered")
public class OrderedEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "status", nullable = false)
	private int status;

	@Column(name = "quantity", nullable = false)
	private int quantity;

	@Column(name = "size_id", nullable = false)
	private Long sizeId;
	
	@Column(name = "rent", nullable = false)
	private Boolean isRent;
	
	@Column(name = "rentdue", nullable = false)
	private int rentDue;

	@ManyToOne
	@JoinTable(name = "order_cust", joinColumns = @JoinColumn(name = "order_id"), inverseJoinColumns = @JoinColumn(name = "customer_id"))
	private CustomerEntity customerOrderd;

	public Long getSizeId() {
		return sizeId;
	}

	public void setSizeId(Long sizeId) {
		this.sizeId = sizeId;
	}

	public CustomerEntity getCustomerOrderd() {
		return customerOrderd;
	}

	public void setCustomerOrderd(CustomerEntity customerOrderd) {
		this.customerOrderd = customerOrderd;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Long getId() {
		return id;
	}

	public Boolean getIsRent() {
		return isRent;
	}

	public void setIsRent(Boolean isRent) {
		this.isRent = isRent;
	}

	public int getRentDue() {
		return rentDue;
	}

	public void setRentDue(int rentDue) {
		this.rentDue = rentDue;
	}

}
