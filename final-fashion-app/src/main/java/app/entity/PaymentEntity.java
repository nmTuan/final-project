package app.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "payment")
public class PaymentEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ManyToMany(mappedBy = "paymentEntities")
	private List<ProductDetailEntity> productPayment;

	@Column(name = "payment")
	@Size(max = 100)
	private String payment;

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public int getId() {
		return id;
	}

	public List<ProductDetailEntity> getProductPayment() {
		return productPayment;
	}

	public void setProductPayment(List<ProductDetailEntity> productPayment) {
		this.productPayment = productPayment;
	}

}
