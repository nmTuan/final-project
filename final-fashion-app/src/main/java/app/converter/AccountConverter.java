package app.converter;

import org.springframework.stereotype.Component;

import app.dto.AccountDTO;
import app.entity.AccountEntity;

@Component
public class AccountConverter {

	public AccountEntity toAccountEntity(AccountDTO accountDTO) {
		AccountEntity accountEntity = new AccountEntity();
		accountEntity.setUsername(accountDTO.getUsername());
		accountEntity.setPassword(accountDTO.getPassword());
		accountEntity.setIsShop(accountDTO.getIsShop());
		return accountEntity;
	}
	
	public AccountDTO toAccountDTO(AccountEntity accountEntity) {
		AccountDTO accountDTO = new AccountDTO();
		accountDTO.setId(accountEntity.getId());
		accountDTO.setUsername(accountEntity.getUsername());
		accountDTO.setIsShop(accountEntity.getIsShop());
		return accountDTO;
	}

}
