package app.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import app.dto.OrderDTO;
import app.entity.OrderedEntity;
import app.entity.SizeColorEntity;
import app.repository.SizeColorRepository;

@Component
public class OrderConverter {

	@Autowired
	SizeColorRepository sizeColorRepository;
	
	public OrderedEntity toOrderEntity(OrderDTO orderDTO) {
		OrderedEntity orderEntity = new OrderedEntity();
		orderEntity.setQuantity(orderDTO.getQuantity());
		orderEntity.setStatus(orderDTO.getStatus());
		orderEntity.setSizeId(orderDTO.getSizeId());
		orderEntity.setIsRent(orderDTO.getRent());
		orderEntity.setRentDue(orderDTO.getRentDue());
		return orderEntity;
	}

	public OrderDTO toOrderDTO(OrderedEntity orderEntity) {
		SizeColorEntity sizeColorEntity = sizeColorRepository.findOneById(orderEntity.getSizeId());
		OrderDTO orderDTO = new OrderDTO();
		orderDTO.setId(orderEntity.getId());
		orderDTO.setQuantity(orderEntity.getQuantity());
		orderDTO.setStatus(orderEntity.getStatus());
		orderDTO.setSizeId(orderEntity.getSizeId());
		orderDTO.setUserId(orderEntity.getCustomerOrderd().getId());
		orderDTO.setRent(orderEntity.getIsRent());
		orderDTO.setRentDue(orderEntity.getRentDue());
		orderDTO.setDeposit(sizeColorEntity.getDeposit());
		orderDTO.setRentPrice(sizeColorEntity.getRentPrice());
		orderDTO.setOldPrice(sizeColorEntity.getDetail().getProduct().getOldPrice());
		orderDTO.setPrice(sizeColorEntity.getPrice());
		orderDTO.setProductImg(sizeColorEntity.getDetail().getProduct().getThumbnail());
		orderDTO.setProductName(sizeColorEntity.getDetail().getProduct().getProductName());
		orderDTO.setVote(sizeColorEntity.getDetail().getProduct().getVote());
		orderDTO.setShopId(sizeColorEntity.getDetail().getProduct().getShopProduct().getId());
		orderDTO.setPriceDelivery(sizeColorEntity.getDetail().getPriceDelivery());
		return orderDTO;
	}

}
