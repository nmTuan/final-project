package app.converter;

import org.springframework.stereotype.Component;

import app.dto.NotificationCustDTO;
import app.dto.NotificationShopDTO;
import app.entity.NotifiCustEntity;
import app.entity.NotifiShopEntity;

@Component
public class NotificationConverter {

	public NotificationCustDTO toNotifiCustDTO(NotifiCustEntity notifiCustEntity) {
		NotificationCustDTO notificationCustDTO = new NotificationCustDTO();
		notificationCustDTO.setContent(notifiCustEntity.getContent());
		notificationCustDTO.setCreatedDate(notifiCustEntity.getCreatedDate());
		notificationCustDTO.setId(notifiCustEntity.getId());
		notificationCustDTO.setPostId(notifiCustEntity.getPostId());
		notificationCustDTO.setStatus(notifiCustEntity.getStatus());
		notificationCustDTO.setThumbnail(notifiCustEntity.getThumbnail());
		notificationCustDTO.setTitle(notifiCustEntity.getTitle());
		return notificationCustDTO;
	}
	
	public NotifiCustEntity toNotifiCustEntity(NotificationCustDTO notificationCustDTO) {
		NotifiCustEntity notifiCustEntity = new NotifiCustEntity();
		notifiCustEntity.setContent(notificationCustDTO.getContent());
		notifiCustEntity.setCreatedDate(notificationCustDTO.getCreatedDate());
		notifiCustEntity.setPostId(notificationCustDTO.getPostId());
		notifiCustEntity.setStatus(notificationCustDTO.getStatus());
		notifiCustEntity.setThumbnail(notificationCustDTO.getThumbnail());
		notifiCustEntity.setTitle(notificationCustDTO.getTitle());
		return notifiCustEntity;
	}
	
	public NotificationShopDTO toNotifiShopDTO(NotifiShopEntity notifiShopEntity) {
		NotificationShopDTO notificationShopDTO = new NotificationShopDTO();
		notificationShopDTO.setContent(notifiShopEntity.getContent());
		notificationShopDTO.setCreatedDate(notifiShopEntity.getCreatedDate());
		notificationShopDTO.setId(notifiShopEntity.getId());
		notificationShopDTO.setPostId(notifiShopEntity.getPostId());
		notificationShopDTO.setStatus(notifiShopEntity.getStatus());
		notificationShopDTO.setThumbnail(notifiShopEntity.getThumbnail());
		notificationShopDTO.setTitle(notifiShopEntity.getTitle());
		return notificationShopDTO;
	}
	
	public NotifiShopEntity toNotifiShopEntity(NotificationShopDTO notificationShopDTO) {
		NotifiShopEntity notifiShopEntity = new NotifiShopEntity();
		notifiShopEntity.setContent(notificationShopDTO.getContent());
		notifiShopEntity.setCreatedDate(notificationShopDTO.getCreatedDate());
		notifiShopEntity.setPostId(notificationShopDTO.getPostId());
		notifiShopEntity.setStatus(notificationShopDTO.getStatus());
		notifiShopEntity.setThumbnail(notificationShopDTO.getThumbnail());
		notifiShopEntity.setTitle(notificationShopDTO.getTitle());
		return notifiShopEntity;
	}
}
