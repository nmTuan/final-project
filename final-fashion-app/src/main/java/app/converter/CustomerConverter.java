package app.converter;

import org.springframework.stereotype.Component;

import app.dto.CustomerDTO;
import app.entity.CustomerEntity;

@Component
public class CustomerConverter {
	public CustomerEntity toCustEntity(CustomerDTO customerDTO) {
		CustomerEntity customerEntity = new CustomerEntity();
		customerEntity.setAddress(customerDTO.getAddress());
		customerEntity.setAvatar(customerDTO.getAvatar());
		customerEntity.setCoverava(customerDTO.getCoverave());
		customerEntity.setDob(customerDTO.getDob());
		customerEntity.setGender(customerDTO.getGender());
		customerEntity.setCity(customerDTO.getCity());
		customerEntity.setDistrict(customerDTO.getDistrict());
		customerEntity.setVillage(customerDTO.getVillage());
		customerEntity.setPhoneNumber(customerDTO.getPhoneNumber());
		customerEntity.setFullName(customerDTO.getFullName());
		return customerEntity;
	}

	public CustomerDTO toCustDto(CustomerEntity customerEntity) {
		CustomerDTO customerDTO = new CustomerDTO();
		customerDTO.setId(customerEntity.getId());
		customerDTO.setAddress(customerEntity.getAddress());
		customerDTO.setAvatar(customerEntity.getAvatar());
		customerDTO.setCoverave(customerEntity.getCoverava());
		customerDTO.setDob(customerEntity.getDob());
		customerDTO.setGender(customerEntity.getGender());
		customerDTO.setPhoneNumber(customerEntity.getPhoneNumber());
		customerDTO.setCity(customerEntity.getCity());
		customerDTO.setDistrict(customerEntity.getDistrict());
		customerDTO.setVillage(customerEntity.getVillage());
		customerDTO.setFullName(customerEntity.getFullName());
		customerDTO.setIsShop(customerEntity.getAccCust().getIsShop());
		return customerDTO;
	}
}
