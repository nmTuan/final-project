package app.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import app.dto.ChatContentDTO;
import app.dto.RoomDTO;
import app.entity.ChatContentEntity;
import app.entity.RoomEntity;

@Component
public class ChatConverter {

	public ChatContentEntity toChatContentEntity(ChatContentDTO chatContentDTO) {
		ChatContentEntity chatContentEntity = new ChatContentEntity();
		chatContentEntity.setContent(chatContentDTO.getContent());
		chatContentEntity.setCreatedDate(chatContentDTO.getCreatedDate());
		chatContentEntity.setModifiedDate(chatContentDTO.getModifiedDate());
		chatContentEntity.setReceiver(chatContentDTO.getReceiver());
		chatContentEntity.setReceiverAva(chatContentDTO.getReceiverAva());
		chatContentEntity.setSender(chatContentDTO.getSender());
		chatContentEntity.setSenderAva(chatContentDTO.getSenderAva());
		return chatContentEntity;
	}

	public ChatContentDTO toChatContentDTO(ChatContentEntity chatContentEntity) {
		ChatContentDTO chatContentDTO = new ChatContentDTO();
		chatContentDTO.setId(chatContentEntity.getId());
		chatContentDTO.setContent(chatContentEntity.getContent());
		chatContentDTO.setCreatedDate(chatContentEntity.getCreatedDate());
		chatContentDTO.setModifiedDate(chatContentEntity.getModifiedDate());
		chatContentDTO.setReceiver(chatContentEntity.getReceiver());
		chatContentDTO.setReceiverAva(chatContentEntity.getReceiverAva());
		chatContentDTO.setSender(chatContentEntity.getSender());
		chatContentDTO.setSenderAva(chatContentEntity.getSenderAva());
		return chatContentDTO;
	}

	public RoomEntity toRoomEntity(RoomDTO roomDTO) {
		RoomEntity roomEntity = new RoomEntity();
		roomEntity.setName(roomDTO.getNameRoom());
		return roomEntity;
	}

	public RoomDTO toRoomDTOWithoutContent(RoomEntity roomEntity) {
		RoomDTO roomDTO = new RoomDTO();
		roomDTO.setId(roomEntity.getId());
		roomDTO.setNameRoom(roomEntity.getName());
		roomDTO.setLastContent(roomEntity.getLastContent());
		roomDTO.setShopImage(roomEntity.getShopRoom().getAvatar());
		roomDTO.setCustImage(roomEntity.getCustomerRoom().getAvatar());
		roomDTO.setCustId(roomEntity.getCustomerRoom().getId());
		roomDTO.setShopId(roomEntity.getShopRoom().getId());
		roomDTO.setCustStatus(roomEntity.getStatusCust());
		roomDTO.setShopStatus(roomEntity.getStatusShop());
		return roomDTO;
	}

	public RoomDTO toRoomDTO(RoomEntity roomEntity) {
		RoomDTO roomDTO = new RoomDTO();
		roomDTO.setId(roomEntity.getId());
		roomDTO.setNameRoom(roomEntity.getName());
		List<ChatContentDTO> chatContentDTOs = new ArrayList<ChatContentDTO>();
		for (ChatContentEntity chatContentEntity : roomEntity.getContents()) {
			ChatContentDTO chatContentDTO = toChatContentDTO(chatContentEntity);
			chatContentDTOs.add(chatContentDTO);
		}
		roomDTO.setContents(chatContentDTOs);
		return roomDTO;
	}
}
