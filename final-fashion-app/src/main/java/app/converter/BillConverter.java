package app.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import app.common.Common;
import app.dto.BillDTO;
import app.dto.ProductInBillDTO;
import app.entity.BillEntity;
import app.entity.ItemProductEntity;
import app.entity.ProductInBillEntity;
import app.entity.SizeColorEntity;
import app.repository.SizeColorRepository;

@Component
public class BillConverter {
	@Autowired
	Common common;
	
	@Autowired
	SizeColorRepository sizeColorRepository;

	public BillEntity toBillEntity(BillDTO billDTO) {
		BillEntity billEntity = new BillEntity();
		billEntity.setCreatedDate(billDTO.getCreatedDate());
		billEntity.setPayment_id(billDTO.getPaymentId());
		if (billDTO.getOrderPlace() != null) {
			billEntity.setOrderplace(billDTO.getOrderPlace());
		} else {
			billEntity.setOrderplace(billDTO.getAddress());
		}
		billEntity.setBankName(billDTO.getBankName());
		billEntity.setCreditNumber(billDTO.getCreditNumber());
		return billEntity;
	}

	public BillDTO toBillDTO(BillEntity billEntity) {
		BillDTO billDTO = new BillDTO();
		billDTO.setId(billEntity.getId());
		billDTO.setAddress(billEntity.getCustomerBill().getAddress());
		billDTO.setCustomerId(billEntity.getCustomerBill().getId());
		billDTO.setOrderPlace(billEntity.getOrderplace());
		billDTO.setCreatedDate(billEntity.getCreatedDate());
		billDTO.setPaymentId(billEntity.getPayment_id());
		billDTO.setPaymentMethod(common.getPaymentEntity(billEntity.getPayment_id()).getPayment());
		billDTO.setBankName(billEntity.getBankName());
		billDTO.setCreditNumber(billEntity.getCreditNumber());
		if (billEntity.getProductInBills() != null) {
			List<ProductInBillDTO> productBillDTOs = new ArrayList<ProductInBillDTO>();
			for (ProductInBillEntity productBillEntity : billEntity.getProductInBills()) {
				ProductInBillDTO productBillDTO = toProductBillDTO(productBillEntity);
				productBillDTO.setId(productBillEntity.getId());
				productBillDTOs.add(productBillDTO);
			}
			billDTO.setProductBillDTOs(productBillDTOs);
		}
		return billDTO;
	}
	
	public BillDTO toBillDTOSingleProduct(BillEntity billEntity) {
		BillDTO billDTO = new BillDTO();
		billDTO.setId(billEntity.getId());
		billDTO.setAddress(billEntity.getCustomerBill().getAddress());
		billDTO.setCustomerId(billEntity.getCustomerBill().getId());
		billDTO.setOrderPlace(billEntity.getOrderplace());
		billDTO.setCreatedDate(billEntity.getCreatedDate());
		billDTO.setPaymentId(billEntity.getPayment_id());
		billDTO.setPaymentMethod(common.getPaymentEntity(billEntity.getPayment_id()).getPayment());
		billDTO.setBankName(billEntity.getBankName());
		billDTO.setCreditNumber(billEntity.getCreditNumber());
		return billDTO;
	}

	public ProductInBillEntity toProductBillEntity(ProductInBillDTO productBillDTO) {
		ProductInBillEntity productBillEntity = new ProductInBillEntity();
		if (productBillDTO.getActionDate() != null) {
			productBillEntity.setActionDate(productBillDTO.getActionDate());
		}
		productBillEntity.setStatus(productBillDTO.getStatus());
		productBillEntity.setQuantity(productBillDTO.getQuantity());
		productBillEntity.setSizeId(productBillDTO.getSizeId());
		productBillEntity.setDeliveryId(productBillDTO.getDeliveryId());
		productBillEntity.setNote(productBillDTO.getNote());
		productBillEntity.setIsRent(productBillDTO.getIsRent());
		return productBillEntity;
	}

	public ProductInBillDTO toProductBillDTO(ProductInBillEntity productBillEntity) {
		SizeColorEntity sizeColorEntity = sizeColorRepository.findOneById(productBillEntity.getSizeId());
		ItemProductEntity productEntity = sizeColorEntity.getDetail().getProduct();
		ProductInBillDTO productBillDTO = new ProductInBillDTO();
		if (productBillEntity.getActionDate() != null) {
			productBillDTO.setActionDate(productBillEntity.getActionDate());
		}
		productBillDTO.setIdBill(productBillEntity.getBill().getId());
		productBillDTO.setId(productBillEntity.getId());
		productBillDTO.setStatus(productBillEntity.getStatus());
		productBillDTO.setQuantity(productBillEntity.getQuantity());
		productBillDTO.setSizeId(productBillEntity.getSizeId());
		productBillDTO.setStatus(productBillEntity.getStatus());
		productBillDTO.setShopId(productBillEntity.getShopBill().getId());
//		productBillDTO.setDeliveryId(productBillEntity.getDeliveryId());
		productBillDTO.setPriceDelivery(sizeColorEntity.getDetail().getPriceDelivery());
		productBillDTO.setNote(productBillEntity.getNote());
		productBillDTO.setShopName(productBillEntity.getShopBill().getShopName());
		productBillDTO.setShopAddress(productBillEntity.getShopBill().getAddress());
		productBillDTO.setProductName(productEntity.getProductName());
		productBillDTO.setThumbnail(productEntity.getThumbnail());
		productBillDTO.setMinPrice(productEntity.getMinPrice());
		productBillDTO.setMaxPrice(productEntity.getMaxPrice());
		productBillDTO.setOldPrice(productEntity.getOldPrice());
		productBillDTO.setVote(productEntity.getVote());
		productBillDTO.setCreatedDate(productBillEntity.getBill().getCreatedDate());
		productBillDTO.setActionDate(productBillEntity.getActionDate());
		productBillDTO.setCustName(productBillEntity.getBill().getCustomerBill().getFullName());
		productBillDTO.setPhoneNumber(productBillEntity.getCustBillProduct().getPhoneNumber());
		productBillDTO.setOrderPlace(productBillEntity.getBill().getOrderplace());
		productBillDTO.setIsRent(productBillEntity.getIsRent());
		productBillDTO.setRentDue(productBillEntity.getRentDue());
		productBillDTO.setRentPrice(productBillEntity.getRentPrice());
		productBillDTO.setDeposit(productBillEntity.getDeposit());
		productBillDTO.setPaymentStatus(productBillEntity.getPaymentStatus());
		productBillDTO.setBankName(productBillEntity.getBill().getBankName());
		productBillDTO.setCreditNumber(productBillEntity.getBill().getCreditNumber());
		return productBillDTO;
	}
}
