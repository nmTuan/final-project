package app.converter;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class Converter {
	public Timestamp dateToTimestamp(Date date) {
		Timestamp ts = new Timestamp(date.getTime());
		return ts;
	}
	
	public Date timestampToDate(Timestamp time) {
		return new Date(time.getTime());
	}
}
