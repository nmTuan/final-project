package app.converter;

import org.springframework.stereotype.Component;

import app.dto.RevenueMonthDTO;
import app.dto.RevenueYearDTO;
import app.entity.RevenueMonthEntity;
import app.entity.RevenueYearEntity;

@Component
public class RevenueConverter {
	public RevenueYearEntity toRevenueYearEntity(RevenueYearDTO revenueYearDTO) {
		RevenueYearEntity revenueYearEntity = new RevenueYearEntity();
		revenueYearEntity.setYear(revenueYearDTO.getYear());
		return revenueYearEntity;
	}
	
	public RevenueYearDTO toRevenueYearDTO(RevenueYearEntity revenueYearEntity) {
		RevenueYearDTO revenueYearDTO = new RevenueYearDTO();
		revenueYearDTO.setYear(revenueYearEntity.getYear());
		revenueYearDTO.setId(revenueYearEntity.getId());
		return revenueYearDTO;
	}
	
	public RevenueMonthEntity toRevenueMonthEntity(RevenueMonthDTO revenueMonthDTO) {
		RevenueMonthEntity revenueMonthEntity = new RevenueMonthEntity();
		revenueMonthEntity.setMonth(revenueMonthDTO.getMonth());
		revenueMonthEntity.setRevenue(revenueMonthDTO.getRevenue());
		return revenueMonthEntity;
	}
	
	public RevenueMonthDTO toRevenueMonthDTO(RevenueMonthEntity revenueMonthEntity) {
		RevenueMonthDTO revenueMonthDTO = new RevenueMonthDTO();
		revenueMonthDTO.setId(revenueMonthEntity.getId());
		revenueMonthDTO.setMonth(revenueMonthEntity.getMonth());
		revenueMonthDTO.setRevenue(revenueMonthEntity.getRevenue());
		return revenueMonthDTO;
	}
}
