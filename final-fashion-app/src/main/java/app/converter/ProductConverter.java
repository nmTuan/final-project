package app.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import app.common.Common;
import app.dto.PaymentDTO;
import app.dto.product.ImageProductDTO;
import app.dto.product.ProductDTO;
import app.dto.product.ProductDetailDTO;
import app.dto.product.ProductInfoBaseDTO;
import app.dto.product.SizeColorDTO;
import app.entity.ImagesProductEntity;
import app.entity.ItemProductEntity;
import app.entity.PaymentEntity;
import app.entity.ProductDetailEntity;
import app.entity.RentDueEntity;
import app.entity.SizeColorEntity;
import app.repository.RentDueRepository;

@Component
public class ProductConverter {

	@Autowired
	RentDueRepository rentDueRepository;
	
	@Autowired
	Common common;

	public ItemProductEntity toItemProductEntity(ProductInfoBaseDTO productDTO) {
		ItemProductEntity itemProductEntity = new ItemProductEntity();
		itemProductEntity.setProductName(productDTO.getProductName());
		itemProductEntity.setMaxPrice(productDTO.getMaxPrice());
		itemProductEntity.setMinPrice(productDTO.getMinPrice());
		itemProductEntity.setOldPrice(productDTO.getOldPrice());
		itemProductEntity.setQuantity(productDTO.getQuantity());
		itemProductEntity.setShortDescription(productDTO.getShortDescription());
		itemProductEntity.setThumbnail(productDTO.getThumbnail());
		itemProductEntity.setVote(productDTO.getVote());
		itemProductEntity.setCategory_id(productDTO.getCategory_id());
		itemProductEntity.setGender_id(productDTO.getGender_id());
		itemProductEntity.setAge_id(productDTO.getAge_id());
		itemProductEntity.setIsRent(productDTO.getIsRent());
		return itemProductEntity;
	}

	public ProductDTO toProductDTO(ItemProductEntity itemProductEntity) {
		ProductDTO productDTO = new ProductDTO();
		productDTO.setProductName(itemProductEntity.getProductName());
		productDTO.setMaxPrice(itemProductEntity.getMaxPrice());
		productDTO.setMinPrice(itemProductEntity.getMinPrice());
		productDTO.setOldPrice(itemProductEntity.getOldPrice());
		productDTO.setQuantity(itemProductEntity.getQuantity());
		productDTO.setShortDescription(itemProductEntity.getShortDescription());
		productDTO.setThumbnail(itemProductEntity.getThumbnail());
		productDTO.setVote(itemProductEntity.getVote());
		productDTO.setIsRent(itemProductEntity.getIsRent());
		productDTO.setSold(itemProductEntity.getSold());
		productDTO.setCategory(common.getCategory(itemProductEntity.getCategory_id()));
		productDTO.setGender(common.getGender(itemProductEntity.getGender_id()));
		productDTO.setSold(itemProductEntity.getSold());
		productDTO.setShopAddress(itemProductEntity.getShopProduct().getAddress());
		return productDTO;
	}

	public ProductDetailEntity toDetailEntity(ProductDetailDTO productDTO) {
		ProductDetailEntity productDetailEntity = new ProductDetailEntity();
		productDetailEntity.setMaterial(productDTO.getMaterial());
		productDetailEntity.setBrand(productDTO.getBrand());
		productDetailEntity.setDescription(productDTO.getDescription());
		productDetailEntity.setType(productDTO.getType());
		productDetailEntity.setPriceDelivery(productDTO.getPriceDelivery());
		productDetailEntity.setBankId(productDTO.getBankId());
		return productDetailEntity;
	}

	public ProductDetailDTO toDetailDTO(ProductDetailEntity productDetailEntity) {
		ProductDetailDTO productDTO = new ProductDetailDTO();
		List<PaymentDTO> paymentDTOs = new ArrayList<PaymentDTO>();
		for (PaymentEntity paymentEntity : productDetailEntity.getPaymentEntities()) {
			PaymentDTO paymentDTO = new PaymentDTO();
			paymentDTO.setId(paymentEntity.getId());
			paymentDTO.setPayment(paymentEntity.getPayment());
			paymentDTOs.add(paymentDTO);
		}
		productDTO.setThumbnail(productDetailEntity.getProduct().getThumbnail());
		productDTO.setProductName(productDetailEntity.getProduct().getProductName());
		productDTO.setShortDescription(productDetailEntity.getProduct().getShortDescription());
		productDTO.setMaxPrice(productDetailEntity.getProduct().getMaxPrice());
		productDTO.setIsRent(productDetailEntity.getProduct().getIsRent());
		productDTO.setMinPrice(productDetailEntity.getProduct().getMinPrice());
		productDTO.setOldPrice(productDetailEntity.getProduct().getOldPrice());
		productDTO.setQuantity(productDetailEntity.getProduct().getQuantity());
		productDTO.setVote(productDetailEntity.getProduct().getVote());
		productDTO.setMaterial(productDetailEntity.getMaterial());
		productDTO.setBrand(productDetailEntity.getBrand());
		productDTO.setDescription(productDetailEntity.getDescription());
		productDTO.setSold(productDetailEntity.getProduct().getSold());
		productDTO.setType(productDetailEntity.getType());
		productDTO.setImages(toImageDTO(productDetailEntity.getImages()));
		productDTO.setSizeColors(toSizeDTO(productDetailEntity.getSizeColor()));
		productDTO.setGender(common.getGender(productDetailEntity.getProduct().getGender_id()));
		productDTO.setCategory(common.getCategory(productDetailEntity.getProduct().getCategory_id()));
		productDTO.setShopAddress(productDetailEntity.getProduct().getShopProduct().getAddress());
		productDTO.setAge(common.getAge(productDetailEntity.getProduct().getAge_id()));
		productDTO.setShopName(productDetailEntity.getProduct().getShopProduct().getShopName());
		productDTO.setPayments(paymentDTOs);
		productDTO.setPriceDelivery(productDetailEntity.getPriceDelivery());
		productDTO.setBankId(productDetailEntity.getBankId());
		return productDTO;
	}

	public RentDueEntity getRentDueEntity(Integer id) {
		if (id != null) {
			return rentDueRepository.findOneById(id);
		}
		return null;
	}

	public List<ImagesProductEntity> toImageEntity(List<ImageProductDTO> imageProductDTO,
			ProductDetailEntity detailProduct) {
		List<ImagesProductEntity> listImageEntity = new ArrayList<ImagesProductEntity>();
		for (ImageProductDTO images : imageProductDTO) {
			ImagesProductEntity imagesProductEntity = new ImagesProductEntity();
			imagesProductEntity.setImage(images.getImage());
			imagesProductEntity.setName(images.getName());
			if(detailProduct != null) {
				imagesProductEntity.setDetail(detailProduct);
			}
			listImageEntity.add(imagesProductEntity);
		}
		return listImageEntity;
	}

	public List<ImageProductDTO> toImageDTO(List<ImagesProductEntity> imagesProductEntities) {
		List<ImageProductDTO> listImageDTO = new ArrayList<ImageProductDTO>();
		for (ImagesProductEntity imageEntity : imagesProductEntities) {
			ImageProductDTO imageProductDTO = new ImageProductDTO();
			imageProductDTO.setImage(imageEntity.getImage());
			imageProductDTO.setName(imageEntity.getName());
			imageProductDTO.setId(imageEntity.getId());
			listImageDTO.add(imageProductDTO);
		}
		return listImageDTO;
	}

	public List<SizeColorEntity> toSizeEntity(List<SizeColorDTO> sizeColorDTO, ProductDetailEntity detailProduct) {
		List<SizeColorEntity> listColorEntity = new ArrayList<SizeColorEntity>();
		for (SizeColorDTO element : sizeColorDTO) {
			SizeColorEntity sizeColorEntity = new SizeColorEntity();
			sizeColorEntity.setColor(element.getColor());
			sizeColorEntity.setPrice(element.getPrice());
			sizeColorEntity.setQuantity(element.getQuantity());
			sizeColorEntity.setSize(element.getSize());
			sizeColorEntity.setRentDueId(element.getRentDueId());
			sizeColorEntity.setRentPrice(element.getRentPrice());
			sizeColorEntity.setDeposit(element.getDeposit());
			if(detailProduct != null) {
				sizeColorEntity.setDetail(detailProduct);
			}
			listColorEntity.add(sizeColorEntity);
		}
		return listColorEntity;
	}

	public List<SizeColorDTO> toSizeDTO(List<SizeColorEntity> sizeColorEntities) {
		List<SizeColorDTO> listSizeColorDTO = new ArrayList<SizeColorDTO>();
		for (SizeColorEntity sizeColor : sizeColorEntities) {
			RentDueEntity rentDueEntity = getRentDueEntity(sizeColor.getRentDueId());
			SizeColorDTO sizeColorDTO = new SizeColorDTO();
			sizeColorDTO.setColor(sizeColor.getColor());
			sizeColorDTO.setPrice(sizeColor.getPrice());
			sizeColorDTO.setQuantity(sizeColor.getQuantity());
			sizeColorDTO.setSize(sizeColor.getSize());
			sizeColorDTO.setId(sizeColor.getId());
			sizeColorDTO.setRentDueId(sizeColor.getRentDueId());
			sizeColorDTO.setRentPrice(sizeColor.getRentPrice());
			sizeColorDTO.setDeposit(sizeColor.getDeposit());
			if (rentDueEntity != null) {
				sizeColorDTO.setRentDue(rentDueEntity.getName());
			}
			listSizeColorDTO.add(sizeColorDTO);
		}
		return listSizeColorDTO;
	}
}
