package app.converter;

import org.springframework.stereotype.Component;

import app.dto.BankCustDTO;
import app.dto.BankInfoBaseDTO;
import app.dto.BankShopDTO;
import app.entity.BankCustEntity;
import app.entity.BankInfoBaseEntity;
import app.entity.BankShopEntity;

@Component
public class BankConverter {

	public BankShopEntity toBankEntity(BankInfoBaseDTO bankInfoBaseDTO) {
		BankShopEntity bankInfoBaseEntity = new BankShopEntity();
//		bankShopEntity.setBalance(bankShopDTO.getBalance());
		bankInfoBaseEntity.setBankName(bankInfoBaseDTO.getBankName());
		bankInfoBaseEntity.setCreditName(bankInfoBaseDTO.getCreditName());
		bankInfoBaseEntity.setCreditNumber(bankInfoBaseDTO.getCreditNumber());
		bankInfoBaseEntity.setMonth(bankInfoBaseDTO.getMonth());
		bankInfoBaseEntity.setYear(bankInfoBaseDTO.getYear());
		return bankInfoBaseEntity;
	}
	
	public BankShopDTO toBankDTO(BankInfoBaseEntity bankInfoBaseEntity) {
		BankShopDTO bankInfoBaseDTO = new BankShopDTO();
		bankInfoBaseDTO.setId(bankInfoBaseEntity.getId());
		bankInfoBaseDTO.setBalance(bankInfoBaseEntity.getBalance());
		bankInfoBaseDTO.setBankName(bankInfoBaseEntity.getBankName());
		bankInfoBaseDTO.setCreditName(bankInfoBaseEntity.getCreditName());
		bankInfoBaseDTO.setCreditNumber(bankInfoBaseEntity.getCreditNumber());
		bankInfoBaseDTO.setMonth(bankInfoBaseEntity.getMonth());
		bankInfoBaseDTO.setYear(bankInfoBaseEntity.getYear());
		return bankInfoBaseDTO;
	}
	
	public BankCustEntity toBankCustEntity(BankInfoBaseDTO bankInfoBaseDTO) {
		BankCustEntity bankInfoBaseEntity = new BankCustEntity();
//		bankShopEntity.setBalance(bankShopDTO.getBalance());
		bankInfoBaseEntity.setBankName(bankInfoBaseDTO.getBankName());
		bankInfoBaseEntity.setCreditName(bankInfoBaseDTO.getCreditName());
		bankInfoBaseEntity.setCreditNumber(bankInfoBaseDTO.getCreditNumber());
		bankInfoBaseEntity.setMonth(bankInfoBaseDTO.getMonth());
		bankInfoBaseEntity.setYear(bankInfoBaseDTO.getYear());
		return bankInfoBaseEntity;
	}
	
	public BankCustDTO toBankCustDTO(BankInfoBaseEntity bankInfoBaseEntity) {
		BankCustDTO bankInfoBaseDTO = new BankCustDTO();
		bankInfoBaseDTO.setId(bankInfoBaseEntity.getId());
		bankInfoBaseDTO.setBalance(bankInfoBaseEntity.getBalance());
		bankInfoBaseDTO.setBankName(bankInfoBaseEntity.getBankName());
		bankInfoBaseDTO.setCreditName(bankInfoBaseEntity.getCreditName());
		bankInfoBaseDTO.setCreditNumber(bankInfoBaseEntity.getCreditNumber());
		bankInfoBaseDTO.setMonth(bankInfoBaseEntity.getMonth());
		bankInfoBaseDTO.setYear(bankInfoBaseEntity.getYear());
		return bankInfoBaseDTO;
	}
}
