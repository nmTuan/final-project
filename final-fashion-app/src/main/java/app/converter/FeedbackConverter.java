package app.converter;

import org.springframework.stereotype.Component;

import app.dto.FeedbackDTO;
import app.entity.FeedbackEntity;

@Component
public class FeedbackConverter {

	public FeedbackEntity toFeedbackEntity(FeedbackDTO feedbackDTO) {
		FeedbackEntity feedbackEntity = new FeedbackEntity();
		feedbackEntity.setAvatar(feedbackDTO.getAvatar());
		feedbackEntity.setContent(feedbackDTO.getContent());
		feedbackEntity.setCreatedDate(feedbackDTO.getCreatedDate());
		feedbackEntity.setModifiedDate(feedbackDTO.getModifiedDate());
		feedbackEntity.setUsername(feedbackDTO.getUsername());
		feedbackEntity.setVote(feedbackDTO.getVote());
		feedbackEntity.setBillId(feedbackDTO.getProductBillId());
		return feedbackEntity;
	}

	public FeedbackDTO toFeedbackDTO(FeedbackEntity feedbackEntity) {
		FeedbackDTO feedbackDTO = new FeedbackDTO();
		feedbackDTO.setAvatar(feedbackEntity.getAvatar());
		feedbackDTO.setContent(feedbackEntity.getContent());
		feedbackDTO.setCreatedDate(feedbackEntity.getCreatedDate());
		feedbackDTO.setModifiedDate(feedbackEntity.getModifiedDate());
		feedbackDTO.setUsername(feedbackEntity.getUsername());
		feedbackDTO.setVote(feedbackEntity.getVote());
		feedbackDTO.setProductBillId(feedbackEntity.getBillId());
		return feedbackDTO;
	}
}
