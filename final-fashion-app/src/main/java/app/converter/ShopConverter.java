package app.converter;

import org.springframework.stereotype.Component;

import app.dto.ShopDTO;
import app.entity.ShopEntity;

@Component
public class ShopConverter {
	public ShopEntity toShopEntity(ShopDTO shopDto) {
		ShopEntity shopEntity = new ShopEntity();
		shopEntity.setAddress(shopDto.getAddress());
		shopEntity.setAvatar(shopDto.getAvatar());
		shopEntity.setCoverava(shopDto.getCoverave());
		shopEntity.setPhoneNumber(shopDto.getPhoneNumber());
		shopEntity.setShopName(shopDto.getShopName());
		shopEntity.setDescription(shopDto.getDescription());
		shopEntity.setVote(shopDto.getVote());
		return shopEntity;
	}

	public ShopDTO toShopDto(ShopEntity shopEntity) {
		ShopDTO shopDTO = new ShopDTO();
		shopDTO.setId(shopEntity.getId());
		shopDTO.setShopCode(shopEntity.getShopCode());
		shopDTO.setAddress(shopEntity.getAddress());
		shopDTO.setAvatar(shopEntity.getAvatar());
		shopDTO.setCoverave(shopEntity.getCoverava());
		shopDTO.setPhoneNumber(shopEntity.getPhoneNumber());
		shopDTO.setShopName(shopEntity.getShopName());
		shopDTO.setDescription(shopEntity.getDescription());
		shopDTO.setVote(shopDTO.getVote());
		
		return shopDTO;
	}
}
