package app.common;

public class CommonApi {
	public static final String ACCOUNT = "/account";
	public static final String SHOPAPI = "/shop";
	public static final String CUSTOMERAPI = "/customer";
	public static final String PRODUCT = "/product";
	public static final String ORDERED = "/ordered";
	public static final String BILL = "/bill";
	public static final String REVENUE = "/revenue";
	public static final String NOTIFICATION = "/notification";
	public static final String FEEDBACK = "/feedback";
	public static final String CHAT = "/chat";
}
