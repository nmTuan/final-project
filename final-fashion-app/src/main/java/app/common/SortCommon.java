package app.common;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Component;

import app.dto.SortDTO;

@Component
public class SortCommon {

	public Sort sortOrder(List<SortDTO> listSortDTO) {
		List<Order> listOrder = new ArrayList<>();
		if (listSortDTO.size() > 0) {
			Order order = null;
			for (SortDTO sortDTO : listSortDTO) {
				if (sortDTO.getDirection().toUpperCase().equalsIgnoreCase("ASC")) {
					order = new Order(Sort.Direction.ASC, sortDTO.getProperty().toLowerCase());
				} else if (sortDTO.getDirection().toUpperCase().equalsIgnoreCase("DESC")) {
					order = new Order(Sort.Direction.DESC, sortDTO.getProperty().toLowerCase());
				} else {
					order = null;
				}
				listOrder.add(order);
			}
		}
		return Sort.by(listOrder);
	}
}
