package app.common;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import app.entity.AgeEntity;
import app.entity.CategoryEntity;
import app.entity.DeliveryEntity;
import app.entity.GenderEntity;
import app.entity.PaymentEntity;
import app.entity.RentDueEntity;
import app.repository.AgeRepository;
import app.repository.CategoryRepository;
import app.repository.DeliveryRepository;
import app.repository.GenderRepository;
import app.repository.PaymentRepository;
import app.repository.RentDueRepository;

@Component
public class Common {

	@Autowired
	PaymentRepository paymentRepository;

	@Autowired
	DeliveryRepository deliveryRepository;

	@Autowired
	CategoryRepository categoryRepository;

	@Autowired
	GenderRepository genderRepository;

	@Autowired
	AgeRepository ageRepository;

	@Autowired
	RentDueRepository rentDueRepository;

	public PaymentEntity getPaymentEntity(int id) {
		PaymentEntity paymentEntity = paymentRepository.findOneById(id);
		if (paymentEntity == null)
			return null;
		return paymentEntity;
	}

	public DeliveryEntity getDeliveryEntity(int id) {
		DeliveryEntity deliveryEntity = deliveryRepository.findOneById(id);
		if (deliveryEntity == null)
			return null;
		return deliveryEntity;
	}

	public String getCategory(long id) {
		CategoryEntity categoryEntity = categoryRepository.findOneById(id);
		if (categoryEntity == null)
			return null;
		return categoryEntity.getName();
	}

	public String getGender(long id) {
		GenderEntity genderEntity = genderRepository.findOneById(id);
		if (genderEntity == null)
			return null;
		return genderEntity.getGender();
	}

	public String getAge(long id) {
		AgeEntity ageEntity = ageRepository.findOneById(id);
		if (ageEntity == null)
			return null;
		return ageEntity.getAge();
	}

	public String getRentDue(int id) {
		RentDueEntity rentDueEntity = rentDueRepository.findOneById(id);
		if (rentDueEntity == null)
			return null;
		return rentDueEntity.getName();
	}

	public String dateToString(Date date, String type) {
		String result = "";
		DateFormat dateFormat = new SimpleDateFormat();
		switch (type) {
		case "d":
			dateFormat = new SimpleDateFormat("dd");
			result = dateFormat.format(date);
			break;
		case "m":
			dateFormat = new SimpleDateFormat("M");
			result = dateFormat.format(date);
			break;
		case "y":
			dateFormat = new SimpleDateFormat("yyyy");
			result = dateFormat.format(date);
			break;
		default:
			break;
		}
		return result;
	}

	public Timestamp dateToTimestamp(Date date) {
		return new Timestamp(date.getTime());
	}

	public double formatNumber(String pattern, double value) {
		double number = 0;
		DecimalFormat formatter = new DecimalFormat(pattern);
		String numberFormat = formatter.format(value);
		number = Double.parseDouble(numberFormat);
		return number;
	}
	
	public char[] generateOTP() {
		int len = 4;
		System.out.println("Generating password using random() : "); 
        System.out.print("Your new password is : "); 
  
        // A strong password has Cap_chars, Lower_chars, 
        // numeric value and symbols. So we are using all of 
        // them to generate our password 
        String Capital_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; 
        String Small_chars = "abcdefghijklmnopqrstuvwxyz"; 
        String numbers = "0123456789"; 
                String symbols = "!@#$%^&*_=+-/.?<>)"; 
  
  
        String values = Capital_chars + Small_chars + 
                        numbers + symbols; 
  
        // Using random method 
        Random rndm_method = new Random(); 
  
        char[] password = new char[len]; 
  
        for (int i = 0; i < len; i++) 
        { 
            // Use of charAt() method : to get character value 
            // Use of nextInt() as it is scanning the value as int 
            password[i] = 
              values.charAt(rndm_method.nextInt(values.length())); 
  
        } 
        return password; 
	}
}
