package app.common;

public class Constant {
	public static final int BUY_ONLY = 0;
	public static final int RENT_BUY = 1;
	
	public static final String CUSTOMER = "CUSTOMER";
	public static final String SHOP = "SHOP";
	
}
