package app.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

public class AccountDTO {
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private Long id;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String username;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String password;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String oldPassword;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private Boolean isShop;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Boolean getIsShop() {
		return isShop;
	}

	public void setIsShop(Boolean isShop) {
		this.isShop = isShop;
	}

}
