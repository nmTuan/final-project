package app.dto;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonInclude;

public class NotificationCustDTO extends BaseNotificationDTO {

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private Long custId;

	public NotificationCustDTO() {
		super();
	}

	public NotificationCustDTO(Long postId, String thumbnail, String title, String content, Timestamp createdDate,
			int status) {
		super(postId, thumbnail, title, content, createdDate, status);
	}

	public Long getCustId() {
		return custId;
	}

	public void setCustId(Long custId) {
		this.custId = custId;
	}

}
