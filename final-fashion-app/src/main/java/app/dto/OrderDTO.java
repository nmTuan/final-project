package app.dto;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
public class OrderDTO {

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private Long id;
	private Long sizeId;
	private Long shopId;
	private int status;
	private int quantity;
	private Boolean rent;
	private int rentDue;
	private int deposit;
	private int oldPrice;
	private int price;
	private String productImg;
	private String productName;
	private int rentPrice;
	private Double vote;
	private int priceDelivery;
	
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private Long userId;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private Long[] ids;

	public Long getSizeId() {
		return sizeId;
	}

	public void setSizeId(Long sizeId) {
		this.sizeId = sizeId;
	}

	public Long[] getIds() {
		return ids;
	}

	public void setIds(Long[] ids) {
		this.ids = ids;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Boolean getRent() {
		return rent;
	}

	public void setRent(Boolean rent) {
		this.rent = rent;
	}

	public int getRentDue() {
		return rentDue;
	}

	public void setRentDue(int rentDue) {
		this.rentDue = rentDue;
	}

	public int getDeposit() {
		return deposit;
	}

	public void setDeposit(int deposit) {
		this.deposit = deposit;
	}

	public int getOldPrice() {
		return oldPrice;
	}

	public void setOldPrice(int oldPrice) {
		this.oldPrice = oldPrice;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getProductImg() {
		return productImg;
	}

	public void setProductImg(String productImg) {
		this.productImg = productImg;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getRentPrice() {
		return rentPrice;
	}

	public void setRentPrice(int rentPrice) {
		this.rentPrice = rentPrice;
	}

	public Double getVote() {
		return vote;
	}

	public void setVote(Double vote) {
		this.vote = vote;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public int getPriceDelivery() {
		return priceDelivery;
	}

	public void setPriceDelivery(int priceDelivery) {
		this.priceDelivery = priceDelivery;
	}

}
