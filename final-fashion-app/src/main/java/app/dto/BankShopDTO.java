package app.dto;

public class BankShopDTO extends BankInfoBaseDTO {

	private Long shopId;

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

}
