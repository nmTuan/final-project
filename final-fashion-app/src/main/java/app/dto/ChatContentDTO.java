package app.dto;

import java.sql.Timestamp;

public class ChatContentDTO {
	private Long id;
	private String content;
	private String sender;
	private String senderAva;
	private String receiver;
	private String receiverAva;
	private Timestamp createdDate;
	private Timestamp modifiedDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getSenderAva() {
		return senderAva;
	}

	public void setSenderAva(String senderAva) {
		this.senderAva = senderAva;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getReceiverAva() {
		return receiverAva;
	}

	public void setReceiverAva(String receiverAva) {
		this.receiverAva = receiverAva;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
