package app.dto;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
public class RevenueYearDTO {
	private Long id;
	private String year;
	
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private List<RevenueMonthDTO> revenueMonthDTOs;

	public List<RevenueMonthDTO> getRevenueMonthDTOs() {
		return revenueMonthDTOs;
	}

	public void setRevenueMonthDTOs(List<RevenueMonthDTO> revenueMonthDTOs) {
		this.revenueMonthDTOs = revenueMonthDTOs;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

}
