package app.dto.product;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

import app.dto.FeedbackDTO;
import app.dto.PaymentDTO;

@Component
public class ProductDetailDTO extends ProductInfoBaseDTO {

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String description;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String brand;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String type;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String material;
	
	private String factory;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private List<ImageProductDTO> images;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private List<SizeColorDTO> sizeColors;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<FeedbackDTO> feedbackDTOs;
	
	private int priceDelivery;
	
	private int bankId;

	public List<FeedbackDTO> getFeedbackDTOs() {
		return feedbackDTOs;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<PaymentDTO> payments;

	public void setFeedbackDTOs(List<FeedbackDTO> feedbackDTOs) {
		this.feedbackDTOs = feedbackDTOs;
	}

	public List<ImageProductDTO> getImages() {
		return images;
	}

	public void setImages(List<ImageProductDTO> images) {
		this.images = images;
	}

	public List<SizeColorDTO> getSizeColors() {
		return sizeColors;
	}

	public void setSizeColors(List<SizeColorDTO> sizeColors) {
		this.sizeColors = sizeColors;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public List<PaymentDTO> getPayments() {
		return payments;
	}

	public void setPayments(List<PaymentDTO> payments) {
		this.payments = payments;
	}

	public int getPriceDelivery() {
		return priceDelivery;
	}

	public void setPriceDelivery(int priceDelivery) {
		this.priceDelivery = priceDelivery;
	}

	public int getBankId() {
		return bankId;
	}

	public void setBankId(int bankId) {
		this.bankId = bankId;
	}

	public String getFactory() {
		return factory;
	}

	public void setFactory(String factory) {
		this.factory = factory;
	}
	
	
	
}
