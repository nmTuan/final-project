package app.dto.product;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
public class ImageProductDTO {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long id;
	private String name;
	private String image;
	
	public ImageProductDTO() {
		
	}
	
	public ImageProductDTO(String name, String image) {
		this.name = name;
		this.image = image;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}
