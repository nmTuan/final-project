package app.dto.product;

import javax.validation.constraints.Min;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
public class ProductInfoBaseDTO {
	
//	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private int sold;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private Long id;

//	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String thumbnail;

//	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String productName;

//	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String shortDescription;

//	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private Boolean isRent;

//	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String category;

//	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String gender;
	
	private String age;

//	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Min(value = 0, message = "minPrice must be greater than 0")
	private int minPrice;

//	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Min(value = 0, message = "maxPrice must be greater than 0")
	private int maxPrice;

//	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Min(value = 0, message = "oldPrice must be greater than 0")
	private int oldPrice;

//	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Min(value = 0, message = "quantity must be greater than 0")
	private int quantity;

//	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private Long shopId;

//	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Min(value = 0, message = "vote must be greater than 0")
	private double vote;

//	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private int page;

//	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private int size;
	
	private String shopAddress;
	
	private String shopName;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private Long category_id;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private Long gender_id;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private Long age_id;

	public Boolean getIsRent() {
		return isRent;
	}

	public void setIsRent(Boolean isRent) {
		this.isRent = isRent;
	}

	public Long getAge_id() {
		return age_id;
	}

	public void setAge_id(Long age_id) {
		this.age_id = age_id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public Long getCategory_id() {
		return category_id;
	}

	public void setCategory_id(Long category_id) {
		this.category_id = category_id;
	}

	public Long getGender_id() {
		return gender_id;
	}

	public void setGender_id(Long gender_id) {
		this.gender_id = gender_id;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public double getVote() {
		return vote;
	}

	public void setVote(double vote) {
		this.vote = vote;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public int getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(int minPrice) {
		this.minPrice = minPrice;
	}

	public int getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(int maxPrice) {
		this.maxPrice = maxPrice;
	}

	public int getOldPrice() {
		return oldPrice;
	}

	public void setOldPrice(int oldPrice) {
		this.oldPrice = oldPrice;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public int getSold() {
		return sold;
	}

	public void setSold(int sold) {
		this.sold = sold;
	}

	public String getShopAddress() {
		return shopAddress;
	}

	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

}
