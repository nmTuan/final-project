package app.dto.product;

import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonInclude;

public class SizeColorDTO {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long id;
	private String size;
	private String color;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private int rentDueId;

//	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private int rentPrice;

//	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String rentDue;

//	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private int deposit;

	@Min(value = 0, message = "Quantity must be greater than 0")
	private int quantity;

	@Min(value = 0, message = "price must be greater than 0")
	private int price;

	public Integer getDeposit() {
		return deposit;
	}

	public void setDeposit(int deposit) {
		this.deposit = deposit;
	}

	public int getRentDueId() {
		return rentDueId;
	}

	public void setRentDueId(int rentDueId) {
		this.rentDueId = rentDueId;
	}

	public int getRentPrice() {
		return rentPrice;
	}

	public void setRentPrice(int rentPrice) {
		this.rentPrice = rentPrice;
	}

	public String getRentDue() {
		return rentDue;
	}

	public void setRentDue(String rentDue) {
		this.rentDue = rentDue;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

}
