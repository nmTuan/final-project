package app.dto.product;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import app.dto.SortDTO;

public class ProductDTO extends ProductInfoBaseDTO {
	private Long detailId;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<SortDTO> listSort;

	public List<SortDTO> getListSort() {
		return listSort;
	}

	public void setListSort(List<SortDTO> listSort) {
		this.listSort = listSort;
	}

	public Long getDetailId() {
		return detailId;
	}

	public void setDetailId(Long detailId) {
		this.detailId = detailId;
	}

}
