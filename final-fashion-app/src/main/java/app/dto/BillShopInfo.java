package app.dto;

import java.util.List;

public class BillShopInfo {
	private Long shopId;
	
	private String shopName;
	
	private String shopAddress;
	
	private DeliveryDTO delivery;
	
	private String note;
	
	private int status;
	
	private List<ProductInBillDTO> products;

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShopAddress() {
		return shopAddress;
	}

	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}

	public DeliveryDTO getDelivery() {
		return delivery;
	}

	public void setDelivery(DeliveryDTO delivery) {
		this.delivery = delivery;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public List<ProductInBillDTO> getProducts() {
		return products;
	}

	public void setProducts(List<ProductInBillDTO> products) {
		this.products = products;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
