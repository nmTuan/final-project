package app.dto;

public class BankCustDTO extends BankInfoBaseDTO {
	private Long custId;

	public Long getCustId() {
		return custId;
	}

	public void setCustId(Long custId) {
		this.custId = custId;
	}
	
}
