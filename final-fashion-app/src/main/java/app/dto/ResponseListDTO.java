package app.dto;

import java.util.List;

public class ResponseListDTO<T> {

	private int size;
	private List<T> result;

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public List<T> getResult() {
		return result;
	}

	public void setResult(List<T> result) {
		this.result = result;
	}

}
