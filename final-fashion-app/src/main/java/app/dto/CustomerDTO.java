package app.dto;

import java.sql.Timestamp;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

public class CustomerDTO extends InfoDTOBase {

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private Timestamp dob;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String gender;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String city;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String district;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String village;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String fullName;
	
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String otp;
	
	private Boolean isShop;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<NotificationCustDTO> notificationCustDTOs;

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public List<NotificationCustDTO> getNotificationCustDTOs() {
		return notificationCustDTOs;
	}

	public void setNotificationCustDTOs(List<NotificationCustDTO> notificationCustDTOs) {
		this.notificationCustDTOs = notificationCustDTOs;
	}

	public Timestamp getDob() {
		return dob;
	}

	public void setDob(Timestamp dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public Boolean getIsShop() {
		return isShop;
	}

	public void setIsShop(Boolean isShop) {
		this.isShop = isShop;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

}
