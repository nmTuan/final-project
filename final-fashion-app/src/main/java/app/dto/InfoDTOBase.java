package app.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

public class InfoDTOBase {
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private Long accId;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private Long id;

//	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String avatar;

//	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String coverave;

//	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String phoneNumber;

//	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String address;
	
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String socketId;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<RoomDTO> roomChats;

	public Long getAccId() {
		return accId;
	}

	public void setAccId(Long accId) {
		this.accId = accId;
	}

	public List<RoomDTO> getRoomChats() {
		return roomChats;
	}

	public void setRoomChats(List<RoomDTO> roomChats) {
		this.roomChats = roomChats;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getCoverave() {
		return coverave;
	}

	public void setCoverave(String coverave) {
		this.coverave = coverave;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSocketId() {
		return socketId;
	}

	public void setSocketId(String socketId) {
		this.socketId = socketId;
	}

}
