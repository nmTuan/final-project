package app.dto;

import java.sql.Timestamp;

public class BaseNotificationDTO {

	private Long id;
	private Long postId;
	private String thumbnail;
	private String title;
	private String content;
	private Timestamp createdDate;
	private int status;

	public BaseNotificationDTO() {
	}

	public BaseNotificationDTO(Long postId, String thumbnail, String title, String content,
			Timestamp createdDate, int status) {
		super();
		this.postId = postId;
		this.thumbnail = thumbnail;
		this.title = title;
		this.content = content;
		this.createdDate = createdDate;
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
