package app.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

public class RoomDTO {

	private String nameRoom;
	private Long id;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<ChatContentDTO> contents;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String lastContent;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String shopImage;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String custImage;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long custId;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long shopId;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private int custStatus;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private int shopStatus;

	public String getNameRoom() {
		return nameRoom;
	}

	public void setNameRoom(String nameRoom) {
		this.nameRoom = nameRoom;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<ChatContentDTO> getContents() {
		return contents;
	}

	public void setContents(List<ChatContentDTO> contents) {
		this.contents = contents;
	}

	public String getLastContent() {
		return lastContent;
	}

	public void setLastContent(String lastContent) {
		this.lastContent = lastContent;
	}

	public String getShopImage() {
		return shopImage;
	}

	public void setShopImage(String shopImage) {
		this.shopImage = shopImage;
	}

	public String getCustImage() {
		return custImage;
	}

	public void setCustImage(String custImage) {
		this.custImage = custImage;
	}

	public Long getCustId() {
		return custId;
	}

	public void setCustId(Long custId) {
		this.custId = custId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public int getCustStatus() {
		return custStatus;
	}

	public void setCustStatus(int custStatus) {
		this.custStatus = custStatus;
	}

	public int getShopStatus() {
		return shopStatus;
	}

	public void setShopStatus(int shopStatus) {
		this.shopStatus = shopStatus;
	}
	
}
