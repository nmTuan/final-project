package app.dto;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonInclude;

public class NotificationShopDTO extends BaseNotificationDTO {

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private Long shopId;

	public NotificationShopDTO() {
		super();
	}

	public NotificationShopDTO(Long postId, String thumbnail, String title, String content, Timestamp createdDate,
			int status) {
		super(postId, thumbnail, title, content, createdDate, status);
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

}
