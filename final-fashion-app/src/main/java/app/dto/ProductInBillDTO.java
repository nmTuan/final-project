package app.dto;

import java.sql.Timestamp;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
public class ProductInBillDTO {

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private Long id;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private Long sizeId;
	
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private Boolean isFail;

	private String thumbnail;

	private Timestamp createdDate;

	private String productName;

	private int minPrice;

	private int maxPrice;

	private int oldPrice;

	private double vote;

	private int quantity;

	private String note;

	private String shopName;

	private String shopAddress;

	private String custName;

	private String phoneNumber;

	private String orderPlace;

	private String bankName;

	private String creditNumber;

	private int rentPrice;

	private int rentDue;

	private int deposit;

	private int paymentStatus;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private int deliveryId;

	private DeliveryDTO delivery;
	
	private int priceDelivery;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private Timestamp actionDate;

	private int status;

	private Boolean isRent;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private Long idBill;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private Long shopId;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private Long custId;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String reason;

	public int getRentPrice() {
		return rentPrice;
	}

	public void setRentPrice(int rentPrice) {
		this.rentPrice = rentPrice;
	}

	public int getRentDue() {
		return rentDue;
	}

	public void setRentDue(int rentDue) {
		this.rentDue = rentDue;
	}

	public int getDeposit() {
		return deposit;
	}

	public void setDeposit(int deposit) {
		this.deposit = deposit;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getOrderPlace() {
		return orderPlace;
	}

	public void setOrderPlace(String orderPlace) {
		this.orderPlace = orderPlace;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCustId() {
		return custId;
	}

	public void setCustId(Long custId) {
		this.custId = custId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getIdBill() {
		return idBill;
	}

	public void setIdBill(Long idBill) {
		this.idBill = idBill;
	}

	public Boolean getIsRent() {
		return isRent;
	}

	public void setIsRent(Boolean isRent) {
		this.isRent = isRent;
	}

	public Timestamp getActionDate() {
		return actionDate;
	}

	public void setActionDate(Timestamp actionDate) {
		this.actionDate = actionDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSizeId() {
		return sizeId;
	}

	public void setSizeId(Long sizeId) {
		this.sizeId = sizeId;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public int getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(int deliveryId) {
		this.deliveryId = deliveryId;
	}

	public DeliveryDTO getDelivery() {
		return delivery;
	}

	public void setDelivery(DeliveryDTO delivery) {
		this.delivery = delivery;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShopAddress() {
		return shopAddress;
	}

	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(int minPrice) {
		this.minPrice = minPrice;
	}

	public int getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(int maxPrice) {
		this.maxPrice = maxPrice;
	}

	public int getOldPrice() {
		return oldPrice;
	}

	public void setOldPrice(int oldPrice) {
		this.oldPrice = oldPrice;
	}

	public double getVote() {
		return vote;
	}

	public void setVote(double vote) {
		this.vote = vote;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public int getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(int paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getCreditNumber() {
		return creditNumber;
	}

	public void setCreditNumber(String creditNumber) {
		this.creditNumber = creditNumber;
	}

	public int getPriceDelivery() {
		return priceDelivery;
	}

	public void setPriceDelivery(int priceDelivery) {
		this.priceDelivery = priceDelivery;
	}

	public Boolean getIsFail() {
		return isFail;
	}

	public void setIsFail(Boolean isFail) {
		this.isFail = isFail;
	}
}
