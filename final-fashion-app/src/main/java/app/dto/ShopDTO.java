package app.dto;

import java.util.List;

import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonInclude;

public class ShopDTO extends InfoDTOBase {

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String shopName;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Min(value = 0, message = "vote must be greater than 0")
	private double vote;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String description;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String shopCode;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private int page;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private int size;
	
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private String otp;
	

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<NotificationShopDTO> notificationShopDTOs;

	public List<NotificationShopDTO> getNotificationShopDTOs() {
		return notificationShopDTOs;
	}

	public void setNotificationShopDTOs(List<NotificationShopDTO> notificationShopDTOs) {
		this.notificationShopDTOs = notificationShopDTOs;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShopName() {
		return shopName;
	}

	public double getVote() {
		return vote;
	}

	public void setVote(double vote) {
		this.vote = vote;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}
}
