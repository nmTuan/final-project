package app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import app.entity.RevenueYearEntity;

public interface RevenueYearRepository extends JpaRepository<RevenueYearEntity, Long> {
	
	RevenueYearEntity findOneById(Long id);

	@Query(value = "SELECT * FROM revenue_year y " + "INNER JOIN revenue_year_shop r ON y.id = r.revenue_year_id "
			+ "WHERE r.shop_id = ?1", nativeQuery = true)
	List<RevenueYearEntity> findByShopId(Long shopId);

	@Query(value = "SELECT * FROM revenue_year y " + "INNER JOIN revenue_year_shop r ON y.id = r.revenue_year_id "
			+ "WHERE r.shop_id = ?1 " + "ORDER BY y.year ASC", nativeQuery = true)
	List<RevenueYearEntity> findByShopIdPage(Long shopId, org.springframework.data.domain.Pageable pages);
	
}
