package app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import app.entity.AgeEntity;

public interface AgeRepository extends JpaRepository<AgeEntity, Integer>{
	AgeEntity findOneById(Long id);
}
