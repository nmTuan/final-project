package app.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import app.entity.VillageEntity;

public interface VillageRepository extends JpaRepository<VillageEntity, Integer>{
	
	@Query(value = "SELECT * FROM village WHERE districtcode = ?1 AND village LIKE CONCAT('%',?2,'%')", nativeQuery = true)
	List<VillageEntity> findByDistrictCode(int code, String name, Pageable page);

}
