package app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import app.entity.BankEntity;

public interface BankRepository extends JpaRepository<BankEntity, Integer> {
	
	@Query(value = "SELECT * FROM bank WHERE name LIKE CONCAT('%',?1,'%') ", nativeQuery = true)
	List<BankEntity> findByName(String name);
}
