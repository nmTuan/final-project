package app.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import app.entity.ProductDetailEntity;

public interface ProductDetailRepository extends JpaRepository<ProductDetailEntity, Long> {
	ProductDetailEntity findOneById(Long id);

	@Query(value = "SELECT * FROM product_detail d " + "INNER JOIN item_product_detail i ON d.id = i.detail_id "
			+ "WHERE i.product_id = ?1", nativeQuery = true)
	ProductDetailEntity findOneByProductId(Long productId);

	@Modifying
	@Transactional
	@Query(value = "UPDATE item_product_detail " + "INNER JOIN product_detail d ON detail_id = d.id "
			+ "INNER JOIN product p ON product_id = p.id "
			+ "SET thumbnail = IF(?2 = '' OR ?2 is null, thumbnail, ?2), productname = IF(?3 = '' OR ?3 is null, productname, ?3), "
			+ "shortdescription = IF(?4 = '' OR ?4 is null, shortdescription, ?4), minprice = IF(?5 is null, minprice, ?5),"
			+ "maxprice = IF(?6= '' OR ?6 is null, maxprice, ?6), oldprice = IF(?7 is null, oldprice, ?7),"
			+ "quantity = IF(?8 = '' OR ?8 is null, quantity, ?8), "
			+ "description = IF(?9 = '' OR ?9 is null, description, ?9), brand = IF(?10 = '' OR ?10 is null, brand, ?10),"
			+ "type = IF(?11 = '' OR ?11 is null, type, ?11), material = IF(?12 = '' OR ?12 is null, material, ?12), "
			+ "isrent = IF(?13 is null, isrent, ?13), " + "age_id = IF(?14 is null OR ?14 <= 0, age_id, ?14), "
			+ "gender_id = IF(?15 is null OR ?15 <= 0, gender_id, ?15), category_id = IF(?16 is null OR ?16 <= 0, category_id, ?16), "
			+ "bank_id = IF(?17 is null OR ?17 <= 0, bank_id, ?17), "
			+ "price_delivery = IF(?18 is null OR ?18 <= 0, price_delivery, ?18) " + "WHERE detail_id = ?1", nativeQuery = true)
	void updateProduct(Long id, String thumbnail, String productName, String shortDescription, Integer minprice,
			Integer maxprice, Integer oldprice, Integer quantity, String description, String brand, String type,
			String marerial, Boolean isRent, Long age_id, Long gender_id, Long category_id, Integer bankId, Integer priceDelivery);

	@Modifying
	@Transactional
	@Query(value = "DELETE p FROM payment_product p " + "WHERE p.detail_id = ?1", nativeQuery = true)
	void deletePayment(Long id);

	@Modifying
	@Transactional
	@Query(value = "INSERT payment_product (`detail_id`, `payment_id`) VALUES (?1, ?2)", nativeQuery = true)
	void insertValuePaymentProduct(Long detailId, int paymentId);
}
