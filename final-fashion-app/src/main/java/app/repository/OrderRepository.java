package app.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import app.entity.OrderedEntity;

public interface OrderRepository extends JpaRepository<OrderedEntity, Long> {
	OrderedEntity findOneById(Long id);
	
	@Modifying
	@Transactional
	@Query(value = "DELETE ordered, order_cust FROM ordered " + 
			"INNER JOIN order_cust ON id = order_cust.order_id " + 
			"WHERE status = 0 AND id = ?1", nativeQuery = true)
	void removeProductFromCart(Long orderId);
	
	@Modifying
	@Transactional
	@Query(value = "DELETE ordered, order_cust, order_shop FROM ordered " + 
			"INNER JOIN order_cust ON id = order_cust.order_id " + 
			"INNER JOIN order_shop ON id = order_shop.order_id " + 
			"WHERE size_id = ?1", nativeQuery = true)
	void removeProductBySizeId(Long sizeId);

	@Query(value = "SELECT * FROM ordered "
			+ "INNER JOIN order_cust ON id = order_cust.order_id "
			+ "WHERE order_cust.customer_id = ?1", nativeQuery = true)
	List<OrderedEntity> findByUserId(Long id);

	@Modifying
	@Transactional
	@Query(value = "UPDATE ordered " + "INNER JOIN order_cust ON id = order_cust.order_id "
			+ "SET quantity = IF(?1 = '' OR ?1 is null, quantity, ?1), status = IF(?2 = '' OR ?2 is null, status, ?2) "
			+ "WHERE order_cust.customer_id = ?3 AND id = ?4", nativeQuery = true)
	void updateCart(Integer quantity, Integer status, Long userID, Long orederId);

}
