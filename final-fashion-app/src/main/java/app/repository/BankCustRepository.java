package app.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import app.entity.BankCustEntity;

public interface BankCustRepository extends JpaRepository<BankCustEntity, Integer> {
	BankCustEntity findOneById(int Id);

	@Query(value = "SELECT * from bank_cust b " + "INNER JOIN bank_to_cust t ON t.bank_id = b.id "
			+ "INNER JOIN customer c ON t.cust_id = c.id " + "WHERE c.id = ?1", nativeQuery = true)
	List<BankCustEntity> getListBankByCustId(Long custId);

	@Modifying
	@Transactional
	@Query(value = "UPDATE bank_cust SET balance = ?2 WHERE id = ?1", nativeQuery = true)
	void updateBalanceCust(int bankId, int balance);

	@Modifying
	@Transactional
	@Query(value = "DELETE s, b FROM shop_bank s INNER JOIN shop_to_bank b ON s.id = b.bank_id WHERE s.id = ?1", nativeQuery = true)
	void deleteBank(int bankId);
}
