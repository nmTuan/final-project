package app.repository;

import java.sql.Timestamp;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import app.entity.ProductInBillEntity;

public interface ProductBillRepository extends JpaRepository<ProductInBillEntity, Long> {
	ProductInBillEntity findOneById(Long id);
	
	List<ProductInBillEntity> findBySizeId(Long sizeId);

	@Query(value = "SELECT * FROM product_in_bill p " + 
			"INNER JOIN bill_product_cust b ON b.product_bill_id = p.id " + 
			"INNER JOIN product_of_bill o ON o.product_bill_id = p.id " + 
			"INNER JOIN bill_shop s ON s.product_bill_id = p.id " + 
			"WHERE b.cust_id = ?1 AND p.status = ?2", nativeQuery = true)
	List<ProductInBillEntity> findProductByStatusCust(Long custId, int status);

	@Query(value = "SELECT * FROM product_in_bill p " + "INNER JOIN bill_shop s ON p.id = s.product_bill_id "
			+ "INNER JOIN product_of_bill b ON p.id = b.product_bill_id "
			+ "INNER JOIN bill_product_cust c ON c.product_bill_id = p.id " 
			+ "WHERE s.shop_id = ?1 AND p.status =?2", nativeQuery = true)
	List<ProductInBillEntity> findProductByStatusShop(Long shopId, int status);

	@Modifying
	@Transactional
	@Query(value = "UPDATE product_in_bill p " + "INNER JOIN bill_shop s ON p.id = s.product_bill_id "
			+ "INNER JOIN product_of_bill b ON p.id = b.product_bill_id " + "SET status = ?2, reason = ?3, actiondate=?4 "
			+ "WHERE p.id = ?1", nativeQuery = true)
	void updateStatusProductBill(Long id, int status, String reason, Timestamp actionDate);

}
