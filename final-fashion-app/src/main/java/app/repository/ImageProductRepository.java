package app.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import app.entity.ImagesProductEntity;

public interface ImageProductRepository extends JpaRepository<ImagesProductEntity, Long> {
	ImagesProductEntity findOneById(Long id);

	@Modifying
	@Transactional
	@Query(value = "UPDATE images SET image = IF(?2 = '' OR ?2 is null, image, ?2), name = IF(?3 = '' OR ?3 is null, name, ?3) "
			+ "WHERE id = ?1", nativeQuery = true)
	void updateImageProduct(Long id, String image, String name);
}
