package app.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import app.entity.RoomEntity;

public interface RoomRepository extends JpaRepository<RoomEntity, Long> {

	RoomEntity findOneById(Long id);

	@Query(value = "SELECT * FROM room r " + "INNER JOIN room_cust c ON r.id = c.room_id "
			+ "INNER JOIN room_shop s ON r.id = s.room_id " + "WHERE c.cust_id = ?1", nativeQuery = true)
	List<RoomEntity> findByCustId(Long custId, Pageable page);

	@Modifying
	@Transactional
	@Query(value = "UPDATE room r " + "SET cust_status = ?2 " + "WHERE id = ?1", nativeQuery = true)
	void updateStatusRoomCust(Long roomId, int status);

	@Query(value = "SELECT * FROM room r " + "INNER JOIN room_cust c ON r.id = c.room_id "
			+ "INNER JOIN room_shop s ON r.id = s.room_id " + "WHERE s.shop_id = ?1", nativeQuery = true)
	List<RoomEntity> findByShopId(Long shopId, Pageable page);

	@Query(value = "SELECT * FROM room r " + "INNER JOIN room_cust c ON c.room_id = r.id "
			+ "INNER JOIN room_shop s ON s.room_id = r.id "
			+ "WHERE c.cust_id = ?1 AND s.shop_id = ?2", nativeQuery = true)
	RoomEntity findRoomByCustShopId(Long custId, Long shopId);

	@Modifying
	@Transactional
	@Query(value = "UPDATE room r " + "SET shop_status = ?2 " + "WHERE id = ?1", nativeQuery = true)
	void updateStatusRoomShop(Long roomId, int status);

	@Query(value = "SELECT * FROM room r " + "INNER JOIN room_cust c ON c.room_id = r.id "
			+ "INNER JOIN room_shop s ON s.room_id = r.id "
			+ "WHERE c.cust_id = ?1 AND s.shop_id = ?2", nativeQuery = true)
	RoomEntity findRoomByCustShop(Long custId, Long shopId);
}
