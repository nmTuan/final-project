package app.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import app.entity.ChatContentEntity;

public interface ChatContentRepository extends JpaRepository<ChatContentEntity, Long> {

	@Query(value = "SELECT * FROM chat_content c " + "INNER JOIN room_content r ON r.content_id = c.id "
			+ "WHERE r.room_id = ?1", nativeQuery = true)
	List<ChatContentEntity> getListContent(Long roomId, Pageable page);

}
