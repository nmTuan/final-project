package app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import app.entity.BillEntity;

public interface BillRepository extends JpaRepository<BillEntity, Long> {
	BillEntity findOneById(Long id);

	@Query(value = "SELECT * FROM bill b " + "INNER JOIN bill_cust c ON b.id = c.bill_id "
			+ "WHERE c.cust_id =?1", nativeQuery = true)
	List<BillEntity> findByCustId(Long custId);
}
