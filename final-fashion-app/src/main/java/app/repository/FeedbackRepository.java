package app.repository;

import java.sql.Timestamp;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import app.entity.FeedbackEntity;

public interface FeedbackRepository extends JpaRepository<FeedbackEntity, Long> {
	FeedbackEntity findOneById(Long id);

	FeedbackEntity findOneByBillId(Long id);

	@Modifying
	@Transactional
	@Query(value = "UPDATE feedback f " + "INNER JOIN feeback_customer c ON f.id = c.feedback_id "
			+ "INNER JOIN feeback_detail_product p ON f.id = p.feedback_id "
			+ "SET f.content = ?2, f.modifieddate = ?3 " + "WHERE f.id = ?1", nativeQuery = true)
	void updateFeedback(Long feedbackId, String content, Timestamp modifiedDate);

	@Query(value = "SELECT * FROM feedback f " + "INNER JOIN feeback_customer c ON f.id = c.feedback_id "
			+ "INNER JOIN feeback_detail_product p ON f.id = p.feedback_id "
			+ "WHERE p.detail_id = ?1 AND f.vote LIKE CONCAT('%', ?2)", nativeQuery = true)
	List<FeedbackEntity> findByDetailId(Long detailId, String vote, Pageable pages);

	@Query(value = "SELECT COUNT(*) FROM feedback f " + "INNER JOIN feeback_customer c ON f.id = c.feedback_id "
			+ "INNER JOIN feeback_detail_product p ON f.id = p.feedback_id "
			+ "WHERE p.detail_id = ?1", nativeQuery = true)
	int countFeedback(Long detailId);
}
