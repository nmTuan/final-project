package app.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import app.entity.ItemProductEntity;

public interface ProductRepository extends JpaRepository<ItemProductEntity, Long> {
	public static String queryFindProduct = "FROM product p " + "INNER JOIN shop_product s ON p.id = s.product_id "
			+ "INNER JOIN item_product_detail d ON p.id = d.product_id  "
			+ "WHERE productname LIKE CONCAT('%', ?1, '%') "
			+ "AND IF(?2 = '' OR ?2 is null,category_id is not null,category_id  =?2) "
			+ "AND IF(?3 = '' OR ?3 is null,gender_id is not null,gender_id =?3) "
			+ "AND IF(?4 = '' OR ?4 is null,age_id is not null,age_id =?4) "
			+ "AND IF(?5 = '' OR ?5 is null, vote is not null,vote = ?5) "
			+ "AND IF(?6 < 0, isrent is not null,isrent = ?6) ";

	ItemProductEntity findOneById(Long id);

	@Query(value = "SELECT * " + "FROM product " + "INNER JOIN shop_product ON product.id = shop_product.product_id "
			+ "INNER JOIN item_product_detail ON product.id = item_product_detail.product_id  "
			+ "WHERE shop_id = ?1", nativeQuery = true)
	List<ItemProductEntity> findByShopId(Long id, Pageable page);

	@Query(value = "SELECT * " + queryFindProduct, nativeQuery = true)
	List<ItemProductEntity> findProduct(String productName, Long category_id, Long gender_id, Long age_id, Double vote,
			boolean isRent, Pageable page);

	@Query(value = "SELECT COUNT(*)" + queryFindProduct, nativeQuery = true)
	int countProductByName(String productName, Long category_id, Long gender_id, Long age_id, Double vote,
			boolean isRent);

	@Query(value = "SELECT COUNT(*) FROM shop_product WHERE shop_id = ?1", nativeQuery = true)
	int countProduct(Long id);

	@Modifying
	@Transactional
	@Query(value = "DELETE p, s, i, d, m, n, k, l "
			+ "FROM product p INNER JOIN shop_product s ON s.product_id = p.id "
			+ "INNER JOIN item_product_detail i ON i.product_id = p.id "
			+ "INNER JOIN product_detail d ON d.id = i.detail_id " + "INNER JOIN image_detail m ON m.detail_id = d.id "
			+ "INNER JOIN images n ON m.image_id = n.id " + "INNER JOIN size_detail k ON k.detail_id = d.id "
			+ "INNER JOIN size_color l ON l.id = k.size_id " + "WHERE d.id = ?1", nativeQuery = true)
	void deteleProduct(Long id);

	@Modifying
	@Transactional
	@Query(value = "UPDATE product " + "SET vote = IF(?1 = '' OR ?1 is null, vote,?1) "
			+ "WHERE id = ?2", nativeQuery = true)
	void updateVote(double vote, Long id);

	@Modifying
	@Transactional
	@Query(value = "UPDATE product " + "SET sold = IF(?1 = '' OR ?1 is null, sold,?1) "
			+ "WHERE id = ?2", nativeQuery = true)
	void updateSold(int sold, Long id);
}
