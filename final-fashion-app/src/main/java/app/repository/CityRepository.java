package app.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import app.entity.CityEntity;

public interface CityRepository extends JpaRepository<CityEntity, Integer>{
	@Query(value = "SELECT * FROM city WHERE city LIKE CONCAT('%',?1,'%')", nativeQuery = true)
	List<CityEntity> findByName(String name, Pageable page);
}
