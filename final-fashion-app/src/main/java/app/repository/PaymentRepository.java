package app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import app.entity.PaymentEntity;

public interface PaymentRepository extends JpaRepository<PaymentEntity, Integer>{
	PaymentEntity findOneById(int id);
}
