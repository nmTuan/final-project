package app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import app.entity.CategoryEntity;

public interface CategoryRepository extends JpaRepository<CategoryEntity, Long> {
	CategoryEntity findOneById(Long id);
}
