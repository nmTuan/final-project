package app.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import app.entity.NotifiCustEntity;

public interface NotifiCustRepository extends JpaRepository<NotifiCustEntity, Long> {

	@Query(value = "SELECT * FROM cust_notification n "
			+ "INNER JOIN notification_customer c ON n.id = c.notification_id "
			+ "WHERE c.cust_id = ?1 AND n.id = ?2", nativeQuery = true)
	NotifiCustEntity findOneById(Long custId, Long notifiId);

	@Query(value = "SELECT * FROM cust_notification n "
			+ "INNER JOIN notification_customer c ON n.id = c.notification_id " + "WHERE c.cust_id = ?1 "
			+ "ORDER BY n.createddate DESC", nativeQuery = true)
	List<NotifiCustEntity> findByCustId(Long custId, Pageable pages);

	@Modifying
	@Transactional
	@Query(value = "UPDATE cust_notification n " + "INNER JOIN notification_customer c ON n.id = c.notification_id "
			+ "SET status = ?1 " + "WHERE c.cust_id = ?2 AND n.id = ?3", nativeQuery = true)
	void updateStatusNotifi(int status, Long custId, Long notifiId);
	
	@Query(value = "SElECT COUNT(*) FROM cust_notification n " + 
			"INNER JOIN notification_customer c ON n.id = c.notification_id " + 
			"WHERE c.cust_id = ?1 AND n.status = ?2", nativeQuery = true)
	int countNotificationByStatus(Long custId, int status);
}
