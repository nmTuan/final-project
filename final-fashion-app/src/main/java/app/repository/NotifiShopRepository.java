package app.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import app.entity.NotifiShopEntity;

public interface NotifiShopRepository extends JpaRepository<NotifiShopEntity, Long> {

	@Query(value = "SELECT * FROM shop_notification n " + "INNER JOIN notification_shop s ON n.id = s.notification_id "
			+ "WHERE s.shop_id = ?1 AND n.id = ?2", nativeQuery = true)
	NotifiShopEntity findOneById(Long shopId, Long notifiId);

	@Query(value = "SELECT * FROM shop_notification n " + "INNER JOIN notification_shop s ON n.id = s.notification_id "
			+ "WHERE s.shop_id = ?1 " + "ORDER BY n.createddate DESC", nativeQuery = true)
	List<NotifiShopEntity> findNotifiByShopId(Long shopId, Pageable pages);

	@Modifying
	@Transactional
	@Query(value = "UPDATE shop_notification n " + "INNER JOIN notification_shop s ON n.id = s.notification_id "
			+ "SET status = ?1 " + "WHERE s.shop_id = ?2 AND n.id = ?3", nativeQuery = true)
	void updateStatusNotifi(int status, Long shopId, Long notifiId);

	@Query(value = "SElECT COUNT(*) FROM shop_notification n " + 
			"INNER JOIN notification_shop s ON n.id = s.notification_id " + 
			"WHERE s.shop_id = ?1 AND n.status = ?2", nativeQuery = true)
	int countNotificationByStatus(Long shopId, int status);
}
