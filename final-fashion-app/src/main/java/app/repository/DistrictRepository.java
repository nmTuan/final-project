package app.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import app.entity.DistrictEntity;

public interface DistrictRepository extends JpaRepository<DistrictEntity, Integer> {

	@Query(value = "SELECT * FROM district WHERE citycode = ?1 AND district LIKE CONCAT('%',?2,'%')", nativeQuery = true)
	List<DistrictEntity> findByCitycode(int code, String name, Pageable page);
}
