package app.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import app.entity.AccountEntity;

public interface AccountRepository extends JpaRepository<AccountEntity, Long> {

	AccountEntity findOneById(Long id);

	AccountEntity findOneByUsername(String usename);

	@Modifying
	@Transactional
	@Query(value = "UPDATE account a " + "INNER JOIN account_cust c ON a.id = c.acc_id "
			+ "INNER JOIN account_shop s ON a.id = s.acc_id " + "SET isShop = ?2 "
			+ "WHERE a.id = ?1", nativeQuery = true)
	void updateStatusShop(Long accId, Boolean isShop);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE account a " + "INNER JOIN account_cust c ON a.id = c.acc_id "
			+ "INNER JOIN account_shop s ON a.id = s.acc_id " + "SET password = ?2 "
			+ "WHERE a.id = ?1", nativeQuery = true)
	void updatePassword(Long accId, String password);
}
