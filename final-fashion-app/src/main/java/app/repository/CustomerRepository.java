package app.repository;

import java.sql.Timestamp;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import app.entity.CustomerEntity;

public interface CustomerRepository extends JpaRepository<CustomerEntity, Long> {

	CustomerEntity findOneById(Long id);

	@Query(value = "SELECT * FROM customer c " + "INNER JOIN account_cust m ON m.cust_id = c.id "
			+ "INNER JOIN account a ON a.id = m.acc_id " + "WHERE a.id = ?1", nativeQuery = true)
	CustomerEntity findOneByAccId(Long accId);

	CustomerEntity findOneByPhoneNumber(String phonenumber);

	@Modifying
	@Transactional
	@Query(value = "UPDATE customer "
			+ "SET address = IF(?1 = '' OR ?1 is null, address,?1), avatar = IF(?2 = '' OR ?2 is null, avatar, ?2),"
			+ "coverava = IF(?3 = '' OR ?3 is null, coverava, ?3), "
			+ "dob = IF(?4 = '' OR ?4 is null, dob, ?4), gender = IF(?5 = '' OR ?5 is null, gender, ?5), "
			+ "city = IF(?6 = '' OR ?6 is null, city, ?6), district = IF(?7 = '' OR ?7 is null, district, ?7), "
			+ "village = IF(?8 = '' OR ?8 is null, village, ?8)" + "WHERE id = ?9", nativeQuery = true)
	void updateUserInfo(String address, String avatar, String coverava, Timestamp dob, String gender, String city,
			String district, String village, Long id);

	@Modifying
	@Transactional
	@Query(value = "UPDATE customer " + "SET phonenumber = IF(?1 = '' OR ?1 is null, phonenumber,?1)"
			+ "WHERE id = ?2", nativeQuery = true)
	void updatePhoneNumber(String phoneNumber, Long id);

	@Query(value = "SELECT COUNT(*) FROM room r " + "INNER JOIN room_cust c ON c.room_id = r.id "
			+ "INNER JOIN room_shop s ON s.room_id = r.id "
			+ "WHERE c.cust_id = ?1 AND r.cust_status = ?2", nativeQuery = true)
	Integer countChatByStatus(Long custId, int status);
}
