package app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import app.entity.GenderEntity;

public interface GenderRepository extends JpaRepository<GenderEntity, Long>{
	GenderEntity findOneById(Long id);
}
