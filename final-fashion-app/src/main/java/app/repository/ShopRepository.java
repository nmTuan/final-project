package app.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import app.entity.ShopEntity;

public interface ShopRepository extends JpaRepository<ShopEntity, Long> {
	ShopEntity findOneById(Long id);

	List<ShopEntity> findByShopName(String name, Pageable page);

	ShopEntity findOneByPhoneNumber(String phoneNumber);
	
	@Query(value = "SELECT * FROM shop s " + "INNER JOIN account_shop a ON a.shop_id = s.id "
			+ "WHERE a.acc_id = ?1", nativeQuery = true)
	ShopEntity findOneByAccId(Long accId);

	@Query(value = "SELECT COUNT(*) FROM shop WHERE shopname = ?1", nativeQuery = true)
	int countShopByName(String name);

	@Modifying
	@Transactional
	@Query(value = "UPDATE shop "
			+ "SET address = IF(?1 = '' OR ?1 is null, address,?1), avatar = IF(?2 = '' OR ?2 is null, avatar, ?2),"
			+ "coverava = IF(?3 = '' OR ?3 is null, coverava, ?3), shopname = IF(?4 = '' OR ?4 is null, shopname, ?4),"
			+ "phonenumber = IF(?5 = '' OR ?5 is null, phonenumber, ?5), description = IF(?6 = '' OR ?6 is null, description, ?6) "
			+ "WHERE id = ?7", nativeQuery = true)
	void updateUserInfo(String address, String avatar, String coverava, String shopname, String phonenumber,
			String description, Long id);

	@Modifying
	@Transactional
	@Query(value = "UPDATE shop " + "SET vote = IF(?1 = '' OR ?1 is null, vote,?1) "
			+ "WHERE id = ?2", nativeQuery = true)
	void updateVoteShop(double vote, Long id);
	
	@Query(value = "SELECT COUNT(*) FROM room r " + 
			"INNER JOIN room_cust c ON c.room_id = r.id " + 
			"INNER JOIN room_shop s ON s.room_id = r.id " + 
			"WHERE s.shop_id = ?1 AND r.shop_status = ?2", nativeQuery = true)
	Integer countChatByStatus(Long shopId, int status);
}
