package app.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import app.entity.BankShopEntity;

public interface BankShopRepository extends JpaRepository<BankShopEntity, Integer> {
	
	BankShopEntity findOneById(int Id);

	@Query(value = "SELECT * from shop_bank b " + "INNER JOIN shop_to_bank t ON t.bank_id = b.id "
			+ "INNER JOIN shop s ON t.shop_id = s.id " + "WHERE s.id = ?1", nativeQuery = true)
	List<BankShopEntity> getListBankByShopId(Long shopId);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE shop_bank SET balance = ?2 WHERE id = ?1", nativeQuery = true)
	void updateBalanceShop(int bankId, int balance);
	
	@Modifying
	@Transactional
	@Query(value = "DELETE c, b FROM bank_cust c INNER JOIN bank_to_cust b ON c.id = b.bank_id WHERE c.id = ?1", nativeQuery = true)
	void deleteBank(int bankId);
}
