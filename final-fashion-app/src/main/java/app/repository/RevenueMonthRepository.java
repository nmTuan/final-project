package app.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import app.entity.RevenueMonthEntity;

public interface RevenueMonthRepository extends JpaRepository<RevenueMonthEntity, Long> {

	@Query(value = "SELECT * FROM revenue_month m " + 
			"INNER JOIN revenue_year_month r ON m.id = r.month_id " + 
			"INNER JOIN revenue_year y ON y.id = r.year_id " + 
			"INNER JOIN revenue_year_shop s ON s.revenue_year_id = r.year_id " + 
			"WHERE m.month LIKE CONCAT('%Tháng%',?1) AND y.year = ?2 AND s.shop_id = ?3 " , nativeQuery = true)
	RevenueMonthEntity findOneByMonth(String month, String year, Long shopId);

	@Modifying
	@Transactional
	@Query(value = "UPDATE revenue_month m " + "INNER JOIN revenue_year_month r ON m.id = r.month_id "
			+ "INNER JOIN revenue_year y ON y.id = r.year_id "
			+ "INNER JOIN revenue_year_shop s ON s.revenue_year_id = r.year_id " + "SET m.revenue = ?4 "
			+ "WHERE m.month LIKE CONCAT('%Tháng%', ?1) AND y.year = ?2 AND s.shop_id = ?3", nativeQuery = true)
	void updateRevenue(String month, String year, Long shopId, int revenue);
}
