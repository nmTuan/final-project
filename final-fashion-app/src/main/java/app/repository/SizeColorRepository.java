package app.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import app.entity.SizeColorEntity;

public interface SizeColorRepository extends JpaRepository<SizeColorEntity, Long> {
	SizeColorEntity findOneById(Long id);

	@Modifying
	@Transactional
	@Query(value = "UPDATE size_color SET color = IF(?2 = '' OR ?2 is null, color, ?2), price = IF(?3 = '' OR ?3 is null, price, ?3),"
			+ "quantity = IF(?4 = '' OR ?4 is null, quantity, ?4),size = IF(?5 = '' OR ?5 is null, size, ?5),"
			+ "rent_due_id = IF(?6 = '' OR ?6 is null, rent_due_id, ?6),"
			+ "rentprice =  IF(?7 = '' OR ?7 is null, rentprice, ?7),"
			+ "deposit =  IF(?8 = '' OR ?8 is null, deposit, ?8) " + "WHERE id = ?1", nativeQuery = true)
	void updateImageProduct(Long id, String color, Integer price, Integer quantity, String size, Integer rentDueId,
			Integer rentPrice, Integer deposit);
}
