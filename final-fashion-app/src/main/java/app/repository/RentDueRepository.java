package app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import app.entity.RentDueEntity;

public interface RentDueRepository extends JpaRepository<RentDueEntity, Integer>{
	RentDueEntity findOneById(Integer id);
}
