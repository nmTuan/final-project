const express = require('express');
const Route = express.Router();
const connection = require('../connection');
const moment = require('moment');

Route.get("/", function (req, res) {
    connection.query('SELECT * FROM shop', function (err, result, fields) {
        if (err) {
            // console.log("connect error " + err.stack);
            return;
        }
        // console.log("result ", JSON.stringify(result));
    })
    return res.send("success");
})

// Route.post("/sendMess", function (req, res) {
//     var { custId, shopId } = req.body;
//     checkCustShopId(custId, shopId)
//         .then(resultCustShop => {
//             if (resultCustShop != null) {
//                 findRoomByCustShopId(custId, shopId).then(resultRoom => {
//                     if (resultRoom.length > 0) {
//                         // console.log("have room ", JSON.stringify(resultRoom));
//                         createNewMess(req.body, resultRoom[0].id);
//                     } else {
//                         const roomName = `${resultCustShop[0][0].fullname}, ${resultCustShop[1][0].shopname}`
//                         createRoom(custId, shopId, roomName).then((roomId) => {
//                             createNewMess(req.body, roomId);
//                         }).catch(err => {
//                             // console.log(err);
//                         })
//                     }
//                 })
//                 return res.send("success");
//             }
//             return res.send("Customer or shop does not found");
//         }).catch(err => {
//             // console.log('err', err);
//         })
// })

function sendMess(body) {
    var { custId, shopId } = body;
    var message = {
        message: '',
        roomId: 0,
    };
    return new Promise(function(resolve, reject) {
        checkCustShopId(custId, shopId)
        .then(resultCustShop => {
            if (resultCustShop.length > 0) {
                findRoomByCustShopId(custId, shopId).then(resultRoom => {
                    if (resultRoom.length > 0) {
                        createNewMess(body, resultRoom[0].id);
                        message.message = 'success';
                        message.roomId = resultRoom[0].id;
                        resolve(message);
                    } else {
                        const roomName = `${resultCustShop[0][0].fullname}, ${resultCustShop[1][0].shopname}`
                        createRoom(custId, shopId, roomName).then((roomId) => {
                            createNewMess(body, roomId);
                            message.message = 'success';
                            message.roomId = roomId;
                            resolve(message);
                        }).catch(err => {
                            reject(err);
                        })
                    }
                })
            }
        }).catch(err => {
            reject(err);
        })
    })
}

function checkCustShopId(custId, shopId) {
    return new Promise(function (resolve, reject) {
        const queryCust = `SELECT * FROM customer WHERE id = ${custId}`
        const queryShop = `SELECT * FROM shop WHERE id = ${shopId}`
        connection.query(`${queryCust}; ${queryShop}`, [1, 2], function (err, results) {
            if (err) reject(err);
            var custData = results[0][0];
            var shopData = results[1][0];
            if (shopData && custData) {
                resolve(results);
            }
            resolve(null);
        })
    })
}

function findRoomByCustShopId(custId, shopId) {
    return new Promise(function (resolve, reject) {
        const query = `SELECT * FROM room r
        INNER JOIN room_cust c ON r.id = c.room_id
        INNER JOIN room_shop s ON r.id = s.room_id
        WHERE c.cust_id = ${custId} AND s.shop_id = ${shopId}`;
        connection.query(query, function (err, results) {
            if (err) reject(err);
            resolve(results);
        })
    })
}

function findRoomByCustId(custId) {
    return new Promise(function (resolve, reject) {
        const query = `SELECT * FROM room r INNER JOIN room_cust c ON c.room_id = r.id
        WHERE c.cust_id = ${custId}`
        connection.query(query, function (err, result){
            if (err) reject(err);
            resolve(result);
        })
    })

}

function findRoomByShopId(shopId) {
    return new Promise(function (resolve, reject) {
        const query = `SELECT * FROM room r INNER JOIN room_shop s ON s.room_id = r.id
        WHERE s.shop_id = ${shopId}`
        connection.query(query, function (err, result){
            if (err) reject(err);
            resolve(result);
        })
    })

}

function createRoom(custId, shopId, roomName) {
    return new Promise(function (resolve, reject) {
        const queryCreateRoom = `INSERT INTO room (name) VALUES ('${roomName}')`
        connection.query(queryCreateRoom, function (err, results) {
            if (err) reject(err);
            const queryTableCustRoom = `INSERT INTO room_cust (cust_id, room_id) VALUES ('${custId}','${results.insertId}')`
            const queryTableShopRoom = `INSERT INTO room_shop (shop_id, room_id) VALUES ('${shopId}','${results.insertId}')`
            connection.query(`${queryTableCustRoom};${queryTableShopRoom}`, [1, 2], function (err, resultJoinTable) {
                if (err) reject(err)
                resolve(results.insertId)
            })
        })
    })
}

function createNewMess(req, roomId) {
    return new Promise(function (resolve, reject) {
        var { content, receiver, receiverava, sender, senderava } = req;
        var createdate = moment().format('YYYY-MM-DD hh:mm:ss', Date.now());
        var query = `INSERT INTO chat_content(content, createdate, receiver, receiverava, sender, senderava) 
        VALUES ('${content}', '${createdate}', '${receiver}', '${receiverava}', '${sender}', '${senderava}')`;
        var updateRoom = `UPDATE room r SET modifieddate = '${createdate}', last_content = '${content}', 
                            cust_status = 0, shop_status = 0 WHERE id = ${roomId}`;
        connection.query(query, function (err, result) {
            if (err) {
                // console.log("connect error " + err.stack);
                return res.send("error");
            }
            connection.query(updateRoom, function (err, result) { });
            const queryJoinTableContentRoom = `INSERT INTO room_content (content_id, room_id) VALUES ('${result.insertId}','${roomId}')`
            connection.query(queryJoinTableContentRoom, function (err, resultJoinTable) {
                if (err) reject(err);
                resolve(resultJoinTable);
            })
        })
    })
}

// module.exports = Route;
module.exports = {
    sendMess,
    findRoomByCustShopId,
    findRoomByCustId,
    findRoomByShopId
}