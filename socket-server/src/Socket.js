const values = require('./values');
const database = require('./Database');
const chat = require('./Chat');
module.exports = function (server) {
    var io = require("socket.io")(server);
    const value = values.socket;
    io.on('connection', function (socket) {
        // console.log('connected ', socket.id);
        socket.on('run', function (result) {
            console.log(result);
            socket.to(result.socketReceive).emit('receive', result);
        })
        socket.on('joinRoom', function(roomId) {
            socket.join(roomId);
        })
        socket.on(value.login, function (data) {
            if (data.type === value.customer) {
                updateSocketCust(socket.id, data.custId);
                chat.findRoomByCustId(data.custId).then(result => {
                    result.map(item => {
                        socket.join(item.id);
                    })
                })
            } else {
                updateSocketShop(socket.id, data.shopId);
                chat.findRoomByShopId(data.shopId).then(result => {
                    result.map(item => {
                        socket.join(item.id);
                    })
                })
            }
        })
        socket.on('sendMess', function (data) {
            chat.findRoomByCustShopId(data.custId, data.shopId).then(result => {
                chat.sendMess(data).then(roomInfo => {
                    if (result.length > 0) {
                        socket.to(result[0].id).emit('receiveMess', data);
                    } else {
                        socket.to(data.socketReceive).emit('receiveMess', data);
                        socket.to(data.socketReceive).emit('firtJoin', roomInfo.roomId);
                        socket.join(roomInfo.roonId);
                    }
                }).catch(err => {
                    console.log('run hereeee', err);
                })
            }).catch(e => {
                console.log('run here', e);
            })
        })
        socket.on("disconnect", function () {
            console.log("soket disconnect");
        });
    });
}

function updateSocketCust(socketId, custId) {
    const sql = `UPDATE customer SET socket_id = CONCAT('${socketId}') WHERE id = ${custId}`;
    database.query(sql, function (err, result) { });
}

function updateSocketShop(socketId, shopId) {
    const sql = `UPDATE shop SET socket_id = CONCAT('${socketId}') WHERE id = ${shopId}`;
    database.query(sql, function (err, result) { });
}