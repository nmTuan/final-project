const connection = require('../connection');

function query(sql, args) {
    return new Promise((resovel, reject) => {
        connection.query(sql, args, function (err, results) {
            if (err) {
                return reject(err);
            }
            resovel(results);
        })
    })
}

function close() {
    return new Promise((resovel, reject) => {
        connection.end(err => {
            if (err) {
                return reject(err)
            }
            resovel();
        })
    })
}

// class DatabaseBase {


// }
module.exports = {
    query,
    close
};