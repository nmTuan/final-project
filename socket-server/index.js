const express = require('express');
const bodyparse = require('body-parser');

const connection = require('./connection');
// const testRoute = require('./src/Chat');

const Socket = require('./src/Socket');
const values = require('./src/values');

var app = express();

var server = require('http').Server(app);


Socket(server);

module.exports = server;

app.use(bodyparse.json());
// app.use('/chat', testRoute);

server.listen(3000);


