/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React from 'react';
import {MainApp, NotLogin, MainShop} from './src/navigation/Navigation';
import {View, Text, Platform, StyleSheet} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {inject, observer} from 'mobx-react';
import {values, color} from './src/constant';
import {shopService} from './src/services/ShopService';
import {UserService} from './src/services/UserService';
import {openSocket} from './src/config/Socket';

@inject('UserStore', 'ShopHomeStore')
@observer
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  connectToSocket() {
    openSocket();
  }

  async componentDidMount() {
    this.connectToSocket();
    const {UserStore, ShopHomeStore} = this.props;
    const key = values.keyAsynStorage;
    const userInfo = await AsyncStorage.getItem(key.userInfo);
    const username = await AsyncStorage.getItem(key.username);
    const accountId = await AsyncStorage.getItem(key.accountId);
    const userInfoJS = JSON.parse(userInfo);
    try {
      setTimeout(async () => {
        if (userInfo !== null) {
          if (userInfoJS.shopCode) {
            const res = await shopService.getShopInfo(userInfoJS.id);
            if (res) {
              ShopHomeStore.setShopInfo(res);
              ShopHomeStore.setUsername(username);
            }
            MainShop();
          } else {
            const NewUserInfo = await UserService.getUserInfo(accountId);
            if (NewUserInfo) {
              UserStore.userInfo = NewUserInfo.result;
              UserStore.username = username;
            }
            MainApp();
          }
        } else {
          NotLogin();
        }
      }, 1000);
    } catch (e) {
      console.log('e', e);
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Fashion App</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.agree,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: '#fff',
    fontSize: 30,
  },
});

export default App;
