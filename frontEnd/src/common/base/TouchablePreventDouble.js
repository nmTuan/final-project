import React from 'react';
import {observable} from 'mobx';
import {observer} from 'mobx-react';
import {TouchableOpacity} from 'react-native';

type Props = {
  activeOpacity: Number,
};

@observer
class TouchablePreventDouble extends React.Component<Props> {
  @observable debounce = true;

  static defaultProps = {
    activeOpacity: 1,
  };

  componentWillUnmount() {
    clearInterval(this.intervalId);
    this.intervalId = false;
  }

  handlePress = () => {
    if (!this.debounce) {
      return;
    }

    this.debounce = false;
    this.props.onPress && this.props.onPress();

    this.intervalId = setTimeout(() => {
      this.debounce = true;
    }, 500);
  };

  render() {
    const {children, activeOpacity} = this.props;
    return (
      <TouchableOpacity
        activeOpacity={activeOpacity}
        {...this.props}
        onPress={this.handlePress}>
        {children}
      </TouchableOpacity>
    );
  }
}

export default TouchablePreventDouble;
