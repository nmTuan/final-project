import React, {Component} from 'react';
import {View, Text, StyleSheet, StyleProp, ViewStyle} from 'react-native';
import {TouchablePreventDouble} from '.';
import {color} from '../../constant';

type Props = {
  textBtn?: String,
  onPress: Function,
  backgroundColor?: String,
  textColor?: String,
  styleBtn?: StyleProp<ViewStyle>,
};

class BtnBase extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static defaultProps = {
    textBtn: 'Text',
    backgroundColor: color.agree,
    textColor: color.white,
  };

  render() {
    const {textBtn, onPress, backgroundColor, textColor, styleBtn} = this.props;
    return (
      <View style={[styleBtn]}>
        <TouchablePreventDouble activeOpacity={0.3} onPress={onPress}>
          <View style={[styles.container, {backgroundColor}]}>
            <Text style={{color: textColor}}>{textBtn}</Text>
          </View>
        </TouchablePreventDouble>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewContainer: {flex: 1},
  container: {
    padding: 5,
    borderRadius: 6,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default BtnBase;
