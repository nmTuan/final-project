import React, {Component} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {TouchablePreventDouble, TopBar} from '.';
import {inject, observer} from 'mobx-react';
import {pushScreen} from '../../config/NavigationConfig';
import {values} from '../../constant';

type Props = {
  childLeft?: React.ElementType,
  childCenter?: React.ElementType,
  onPressCart?: Function,
  onPressChat?: Function,
  transparent?: Boolean,
  showCart?: Boolean,
};

@inject('OrderStore', 'UserStore', 'ShopHomeStore')
@observer
class TopBarChatCart extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static defaultProps = {
    transparent: false,
    showCart: true,
  };

  numberProduct = () => {
    const {OrderStore} = this.props;
    var number = 0;
    for (let i = 0; i < OrderStore.listProductInCart.length; i++) {
      number += OrderStore.listProductInCart[i].listProduct.length;
    }
    return number;
  };

  quantityChat = () => {
    const {ShopHomeStore, UserStore} = this.props;
    if (ShopHomeStore.shopInfo) {
      return ShopHomeStore.quantityChat;
    }
    if (UserStore.userInfo) {
      return UserStore.quantityChat;
    }
  };

  onPressCart = () => {
    const {UserStore} = this.props;
    if (UserStore.userInfo) {
      this.props.onPressCart();
    } else {
      pushScreen(this.props.componentId, 'LoginForm');
    }
  };

  onPressChat = () => {
    const {UserStore, ShopHomeStore} = this.props;
    if (UserStore.userInfo || ShopHomeStore.shopInfo) {
      this.props.onPressChat();
    } else {
      pushScreen(this.props.componentId, 'LoginForm');
    }
  };

  renderRight() {
    const {showCart} = this.props;
    return (
      <View style={styles.contaierRight}>
        {showCart && (
          <TouchablePreventDouble
            style={{paddingRight: 15}}
            activeOpacity={0.5}
            onPress={this.onPressCart}>
            <View>
              <Image
                source={require('@assets/icons/cart.png')}
                style={styles.stylesImage}
              />
              {this.numberProduct() > 0 && (
                <View style={styles.viewNumber}>
                  <Text style={styles.number}>{this.numberProduct()}</Text>
                </View>
              )}
            </View>
          </TouchablePreventDouble>
        )}
        <TouchablePreventDouble activeOpacity={0.5} onPress={this.onPressChat}>
          <View>
            <Image
              source={require('@assets/icons/chat.png')}
              style={styles.stylesImage}
            />
            {this.quantityChat() > 0 && (
              <View style={styles.viewNumber}>
                <Text style={styles.number}>{this.quantityChat()}</Text>
              </View>
            )}
          </View>
        </TouchablePreventDouble>
      </View>
    );
  }

  render() {
    const {transparent} = this.props;
    return (
      <View
        style={[
          styles.container,
          {backgroundColor: transparent ? 'transparent' : '#fff'},
        ]}>
        <TopBar
          childLeft={this.props.childLeft}
          childCenter={this.props.childCenter}
          childRight={this.renderRight()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    shadowColor: values.shadowColor,
    shadowOffset: values.shadowOffset,
    shadowOpacity: values.shadowOpacity,
    shadowRadius: values.shadowRadius,
    elevation: values.elevation,
  },
  stylesImage: {
    height: 30,
    width: 30,
    resizeMode: 'contain',
    // tintColor: Color.primary,
  },
  contaierRight: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingRight: 10,
  },
  viewNumber: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: 'red',
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    top: -7,
    right: -7,
  },
  number: {color: '#fff'},
});

export default TopBarChatCart;
