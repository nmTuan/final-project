import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {color} from '../../constant';
import Icon from 'react-native-vector-icons/FontAwesome';

type Props = {
  text: String,
};
class TxtError extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static defaultProps = {
    text: 'Error',
  };

  render() {
    return (
      <View style={styles.container}>
        <Icon name="warning" color={color.red} />
        <Text style={styles.txtError}>{this.props.text}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    paddingVertical: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtError: {color: color.red, marginLeft: 10},
});

export default TxtError;
