import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {TopBarChatCart, TopBar} from '.';
import {observer, PropTypes} from 'mobx-react';
import {values} from '../../constant';

const TopBarTitle = observer(
  ({
    title = '',
    onPressBack = () => {},
    onPressCart = () => {},
    onPressChat = () => {},
    childRight,
  }) => {
    const renderLeft = () => {
      return (
        <TouchableOpacity style={styles.btnBack} onPress={onPressBack}>
          <Icon name="angle-left" size={40} color="#000000" />
        </TouchableOpacity>
      );
    };
    const renderCenter = () => {
      return (
        <View style={styles.viewTitleTopBar}>
          <Text style={styles.titleTopBar} numberOfLines={1}>
            {title}
          </Text>
        </View>
      );
    };

    return childRight ? (
      <View style={styles.container}>
        <TopBar
          childLeft={renderLeft()}
          childCenter={renderCenter()}
          childRight={childRight}
        />
      </View>
    ) : (
      <TopBarChatCart
        childLeft={renderLeft()}
        childCenter={renderCenter()}
        onPressCart={onPressCart}
        onPressChat={onPressChat}
      />
    );
  },
);

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    shadowColor: values.shadowColor,
    shadowOffset: values.shadowOffset,
    shadowOpacity: values.shadowOpacity,
    shadowRadius: values.shadowRadius,
    elevation: values.elevation,
  },
  btnBack: {
    width: '100%',
    height: '100%',
    paddingLeft: 10,
    justifyContent: 'center',
  },
  viewTitleTopBar: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleTopBar: {fontSize: 18},
});

export default TopBarTitle;
