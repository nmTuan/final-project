import React, {Component} from 'react';
import {View, Text, Image, StyleSheet, Animated} from 'react-native';
import {color} from '../../constant';
import {TouchablePreventDouble} from '.';
import {ScreenConfig} from '../../config';

var timeoutSms = null;

type Props = {
  message?: String,
  title?: 'Fashion App',
  image?: '',
  onPress?: () => {},
};
class ScreenSms extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      animated: new Animated.Value(0),
    };
  }

  slideDown = () => {
    Animated.timing(this.state.animated, {
      toValue: 1,
      duration: 500,
      useNativeDriver: true,
    }).start();
    clearTimeout(timeoutSms);
    timeoutSms = setTimeout(() => {
      this.slideUp();
    }, 3000);
  };

  slideUp = () => {
    Animated.timing(this.state.animated, {
      toValue: 0,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  };

  render() {
    const {message, title, image, onPress} = this.props;
    const translateY = this.state.animated.interpolate({
      inputRange: [0, 1],
      outputRange: [-90 - ScreenConfig.marginTopScreen, 0],
    });
    return (
      <Animated.View
        style={[
          styles.container,
          {
            transform: [{translateY}],
          },
        ]}>
        <TouchablePreventDouble style={styles.viewContent} onPress={onPress}>
          <View style={styles.viewTitle}>
            <View style={styles.viewImageLogo}>
              <Image
                source={
                  image !== ''
                    ? {uri: image}
                    : require('@assets/logo/logo1.png')
                }
                style={styles.logo}
              />
            </View>
            <Text style={styles.title}>{title}</Text>
          </View>
          <Text numberOfLines={1}>{message}</Text>
        </TouchablePreventDouble>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    position: 'absolute',
    paddingHorizontal: 20,
    paddingTop: 10 + ScreenConfig.marginTopScreen,
  },
  viewContent: {
    width: '100%',
    height: 70,
    backgroundColor: color.agree,
    borderRadius: 10,
    padding: 10,
  },
  viewTitle: {width: '100%', flexDirection: 'row'},
  viewImageLogo: {
    width: 25,
    height: 25,
    backgroundColor: '#fff',
    padding: 4,
    borderRadius: 6,
    marginBottom: 5,
  },
  logo: {width: 20, height: 20, resizeMode: 'contain'},
  title: {color: '#fff', fontSize: 14, fontWeight: '700', marginLeft: 10},
});

export default ScreenSms;
