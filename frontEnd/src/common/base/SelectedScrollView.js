import React, {Component} from 'react';
import {View, Text, StyleSheet, FlatList} from 'react-native';
import {SearchBar} from 'react-native-elements';
import {color} from '../../constant';
import TouchablePreventDouble from './TouchablePreventDouble';

type Props = {
  keyItem?: String,
  title?: String,
  data: Array,
  onScrollEnd?: Function,
  valueSearch?: String,
  onChagneTextSearch?: (v: value) => void,
  onPressBack?: Function,
  onPressItem?: (v: value) => void,
};

class SelectedScrollView extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
    };
  }

  static defaultProps = {
    keyItem: 'name',
    valueSearch: '',
    title: 'Title',
  };

  renderItem = ({item, index}) => {
    const {keyItem, onPressItem} = this.props;
    return (
      <TouchablePreventDouble
        style={styles.viewItem}
        onPress={() => onPressItem(item)}>
        <Text>{item[`${keyItem}`]}</Text>
      </TouchablePreventDouble>
    );
  };

  onScrollEnd = () => {
    this.props.onScrollEnd();
  };

  updateSearch = value => {
    this.props.onChagneTextSearch(value);
  };

  render() {
    const {data, valueSearch, title, onPressBack, onScrollEnd} = this.props;
    return (
      <View style={styles.container}>
        <TouchablePreventDouble style={{flex: 1}} onPress={onPressBack} />
        <View style={styles.viewChild}>
          <Text style={styles.txtTitle}>{title}</Text>
          <View style={{width: '100%', paddingHorizontal: 10}}>
            <SearchBar
              containerStyle={styles.styleContainerSearch}
              lightTheme
              inputContainerStyle={styles.styleInputContainer}
              inputStyle={styles.styleInputSearch}
              placeholder="Tìm kiếm..."
              onChangeText={this.updateSearch}
              value={valueSearch}
            />
          </View>
          <FlatList
            style={{flex: 1, width: '100%', paddingHorizontal: 20}}
            data={data}
            keyExtractor={(item, index) => `${index}`}
            renderItem={this.renderItem}
            onEndReached={onScrollEnd}
            onEndReachedThreshold={0.1}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: '#00000050'},
  viewChild: {
    width: '100%',
    height: '80%',
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    position: 'absolute',
    bottom: 0,
    backgroundColor: '#fff',
    alignItems: 'center',
    paddingVertical: 10,
  },
  txtTitle: {fontSize: 18},
  styleContainerSearch: {
    backgroundColor: 'transparent',
    width: '100%',
    borderBottomColor: 'transparent',
    borderTopColor: 'transparent',
  },
  styleInputContainer: {
    backgroundColor: color.lightGrayColor,
    borderRadius: 20,
    height: 30,
    borderColor: 'transparent',
  },
  styleInputSearch: {fontSize: 15, color: '#000'},
  viewItem: {
    width: '100%',
    borderBottomWidth: 1,
    borderColor: 'gray',
    paddingBottom: 5,
    paddingTop: 10,
  },
});

export default SelectedScrollView;
