import React, {Component} from 'react';
import {
  View,
  Text,
  Modal,
  StyleSheet,
  StyleProp,
  ViewStyle,
  TouchableOpacity,
} from 'react-native';

type Props = {
  visible: Boolean,
  styleContainerModal?: StyleProp<ViewStyle>,
  styleBackground?: StyleProp<ViewStyle>,
  onPressCancel?: Function,
};

class ModalBase extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static defaultProps = {
    visible: false,
  };

  render() {
    const {visible} = this.props;
    return (
      <Modal animationType="slide" transparent visible={visible}>
        <TouchableOpacity
          style={[styles.backgroundModal, this.props.styleBackground]}
          activeOpacity={1}
          onPress={this.props.onPressCancel}>
          <View style={[styles.containerModal, this.props.styleContainerModal]}>
            {visible && this.props.children}
          </View>
        </TouchableOpacity>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  backgroundModal: {
    width: '100%',
    height: '100%',
    backgroundColor: '#00000030',
  },
  containerModal: {
    width: '70%',
    // padding: 10,
    backgroundColor: '#fff',
    borderRadius: 8,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});

export default ModalBase;
