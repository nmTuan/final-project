import React, {Component} from 'react';
import {View, Text, SafeAreaView, StyleSheet} from 'react-native';
import {ScreenOTP, ScreenSms, TouchablePreventDouble} from '.';
import ChatService from '../../services/ChatService';

var timeoutSms = null;

type Props = {
  content?: '',
  title?: '',
  image?: '',
  style?: React.ElementType,
  data?: Object,
};
class ViewNotification extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  getNotification() {
    timeoutSms = setTimeout(() => {
      this.sms.slideDown();
      clearTimeout(timeoutSms);
    }, 500);
  }

  onPressMessage = async () => {
    const {data} = this.props;
    // console.log(JSON.stringify(data));
    const room = await ChatService.getRoomByCustShop(data.custId, data.shopId);
    // if (room) {
      
    // }
  };

  render() {
    const {content, title, image, style, data} = this.props;
    return (
      <SafeAreaView style={{...style, flex: 1}}>
        {this.props.children}
        <ScreenSms
          ref={ref => (this.sms = ref)}
          message={content}
          title={title}
          image={image}
          onPress={this.onPressMessage}
        />
      </SafeAreaView>
    );
  }
}

export default ViewNotification;
