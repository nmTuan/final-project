import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';

type Props = {
  viewTop?: React.ReactElement,
  ViewMiddle?: React.ReactElement,
  ViewBottom?: React.ReactElement,
};
class Notification extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {viewTop, ViewMiddle, ViewBottom} = this.props;
    return (
      <View style={styles.container}>
        <View>{viewTop}</View>
        <View style={styles.styleContent}>{ViewMiddle}</View>
        <View style={styles.viewBottom}>{ViewBottom}</View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  styleContent: {paddingVertical: 30, width: '100%'},
  viewBottom: {width: '100%'},
});

export default Notification;
