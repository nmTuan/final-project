import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Dimensions,
  Platform,
  ViewPropTypes,
} from 'react-native';
import Props from 'prop-types';
import _ from 'lodash';

const deviceWidth = Dimensions.get('window').width;

type TypesProp = {
  getSelected?: index => void,
  value?: String,
};
class PickerScrollView extends Component<TypesProp> {
  static Props = {
    style: ViewPropTypes.style,
    dataSource: Props.array.isRequired,
    selectedIndex: Props.number,
    onValueChange: Props.func,
    renderItem: Props.func,
    highlightColor: Props.string,
    itemHeight: Props.number,
    wrapperHeight: Props.number,
    isScrollBottom: Props.func,
  };

  constructor(props) {
    super(props);

    this.itemHeight = this.props.itemHeight || 30;
    this.isScrollBottom = _.debounce(this.isScrollBottom, 500).bind();
    this.wrapperHeight =
      this.props.wrapperHeight ||
      (this.props.style ? this.props.style.height : 0) ||
      this.itemHeight * 5;

    this.state = {
      selectedIndex: this.props.selectedIndex || 0,
      // aisY: 70,
    };
  }

  static defaultProps = {
    value: '',
  };

  componentDidMount() {
    if (this.props.selectedIndex) {
      setTimeout(() => {
        this.scrollToIndex(this.props.selectedIndex);
      }, 0);
    }
  }

  componentWillUnmount() {
    this.timer && clearTimeout(this.timer);
  }

  isScrollBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const {isScrollBottom} = this.props;
    const paddingToBottom = 50;
    let checkBottom =
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom;
    if (isScrollBottom) {
      if (checkBottom) {
        isScrollBottom(checkBottom);
      }
    }
  };

  render() {
    const {header, footer} = this._renderPlaceHolder();
    const wrapperStyle = {
      height: this.wrapperHeight,
      // flex: 1,
      // backgroundColor: '#EAEAEA',
      overflow: 'hidden',
    };

    const highlightStyle = {
      position: 'absolute',
      // top: (this.wrapperHeight - this.itemHeight) / 2.5,
      // left: 5,
      flex: 1,
      height: 105,
      width: '100%',
      // borderRadius: 40,
      // borderColor: '#EAEAEA',
      // borderWidth: 3,
      // // backgroundColor: '#ED3439',
      // backgroundColor: 'green',
    };

    return (
      <View style={wrapperStyle}>
        <View />

        {/* <View style={{ position: 'absolute', right: 16, top: this.itemHeight, alignSelf: 'center', color: '#7E7E7E' }}>
                    <Text style={{ fontSize: 11 }}>{this.props.des}</Text>
                </View> */}

        <ScrollView
          style={highlightStyle}
          ref={sview => {
            this.sview = sview;
          }}
          bounces={false}
          showsVerticalScrollIndicator={false}
          onMomentumScrollBegin={this._onMomentumScrollBegin.bind(this)}
          onMomentumScrollEnd={this._onMomentumScrollEnd.bind(this)}
          onScrollBeginDrag={this._onScrollBeginDrag.bind(this)}
          onScrollEndDrag={this._onScrollEndDrag.bind(this)}
          nestedScrollEnabled
          onScroll={({nativeEvent}) => {
            this.isScrollBottom(nativeEvent);
          }}
          scrollEventThrottle={100}>
          {header}
          {this.props.dataSource.map(this._renderItem.bind(this))}
          {footer}
        </ScrollView>
      </View>
    );
  }

  // _onScroll = event => {
  //   this.setState({
  //     axisY: event.nativeEvent.contentOffset.y,
  //   })
  // }

  _renderPlaceHolder() {
    const h = (this.wrapperHeight - this.itemHeight) / 2;
    const header = <View style={{height: h, flex: 1}} />;
    const footer = <View style={{height: h, flex: 1}} />;
    return {header, footer};
  }

  _renderItem(data, index) {
    const {value} = this.props;
    const isSelected = index === this.state.selectedIndex;
    let item = (
      <Text
        style={
          isSelected
            ? [styles.itemText, styles.itemTextSelected]
            : styles.itemText
        }>
        {value && value !== '' ? data[`${value}`] : data}
      </Text>
    );

    if (this.props.renderItem) {
      item = this.props.renderItem(data, index, isSelected);
    }

    return (
      <View style={[styles.itemWrapper, {height: this.itemHeight}]} key={index}>
        {item}
      </View>
    );
  }

  _scrollFix(e) {
    let y = 0;
    const h = this.itemHeight;
    // let _y = 0;
    if (e.nativeEvent.contentOffset) {
      y = e.nativeEvent.contentOffset.y;
    }
    let selectedIndex = Math.round(y / h);
    if (selectedIndex === this.props.dataSource.length) {
      selectedIndex--;
    }
    this.scrollToIndex(selectedIndex);
  }

  _onScrollBeginDrag() {
    this.dragStarted = true;
    if (Platform.OS === 'ios') {
      this.isScrollTo = false;
    }
    this.timer && clearTimeout(this.timer);
  }

  _onScrollEndDrag(e) {
    this.dragStarted = false;
    // if not used, event will be garbaged
    const _e = {
      nativeEvent: {
        contentOffset: {
          y: e.nativeEvent.contentOffset.y,
        },
      },
    };
    this.timer && clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      if (!this.momentumStarted && !this.dragStarted) {
        this._scrollFix(_e, 'timeout');
      }
    }, 10);
  }

  _onMomentumScrollBegin(e) {
    this.momentumStarted = true;
    this.timer && clearTimeout(this.timer);
  }

  _onMomentumScrollEnd(e) {
    this.momentumStarted = false;
    if (!this.isScrollTo && !this.momentumStarted && !this.dragStarted) {
      this._scrollFix(e);
    }
  }

  scrollToIndex(ind) {
    this.setState({
      selectedIndex: ind,
    });
    const y = this.itemHeight * ind;
    this.sview.scrollTo({y});
    this.props.getSelected(ind);
  }

  onClickUp = () => {
    let {selectedIndex} = this.state;
    selectedIndex++;
    if (selectedIndex === this.props.dataSource.length) {
      selectedIndex = this.props.dataSource.length - 1;
    }
    this.setState({
      selectedIndex,
    });
    this.scrollToIndex(selectedIndex);
  };

  onClickDown = () => {
    let {selectedIndex} = this.state;
    if (selectedIndex === 0) {
      selectedIndex = 0;
    } else {
      selectedIndex--;
    }
    this.setState({
      selectedIndex,
    });
    this.scrollToIndex(selectedIndex);
  };
}

let styles = StyleSheet.create({
  itemWrapper: {
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  itemText: {
    color: 'gray',
    fontSize: 14,
  },
  itemTextSelected: {
    color: '#000',
    fontSize: 22,
    fontWeight: '500',
  },
});

export default PickerScrollView;
