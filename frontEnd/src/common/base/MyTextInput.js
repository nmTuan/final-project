/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Animated,
  TextInput,
  Image,
  ImageSourcePropType,
  StyleProp,
  ImageStyle,
} from 'react-native';
import {color} from '../../constant';

type Props = {
  lable?: String,
  iconLeft?: ImageSourcePropType,
  styleLeftIcon?: StyleProp<ImageStyle>,
  onTextChange?: text => {},
};

export default class MyTextInput extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      isValid: null,
    };
    this.animatedValue = new Animated.Value(0);
  }
  componentDidMount() {
    this.inputValue = '';
  }

  _handleTextLabel = ref => (this.lable = ref);

  onChangeText(text) {
    this.props.onTextChange(text);
    this.inputValue = text;
    this.setState({isValid: this.inputValue.length > 0});
  }
  selectField() {
    Animated.timing(this.animatedValue, {
      toValue: 100,
      duration: 150,
      useNativeDriver: false,
    }).start();
  }
  deselectField() {
    if (this.inputValue.length > 0) {
      return;
    }
    Animated.timing(this.animatedValue, {
      toValue: 0,
      duration: 200,
      useNativeDriver: false,
    }).start();
  }
  render() {
    const {iconLeft, styleLeftIcon} = this.props;
    let that = this;
    let interpolatedLabelPosition = that.animatedValue.interpolate({
      inputRange: [0, 100],
      outputRange: [25, 0],
    });
    let interpolatedLabelSize = that.animatedValue.interpolate({
      inputRange: [0, 100],
      outputRange: [12, 13],
    });
    let colorLabel = that.animatedValue.interpolate({
      inputRange: [0, 100],
      outputRange: ['#000', '#ada5a5'],
    });
    return (
      <View style={styles.container}>
        <View
          style={[styles.myInputStyle, {borderColor: color.lightGrayColor}]}>
          <Animated.Text
            ref={this._handleTextLabel}
            style={{
              fontSize: interpolatedLabelSize,
              position: 'absolute',
              top: interpolatedLabelPosition,
              color: colorLabel,
              marginLeft: 3,
            }}>
            {this.props.lable}
          </Animated.Text>
          <View style={styles.viewTextInput}>
            <TextInput
              {...this.props}
              style={styles.textInput}
              onFocus={() => {
                this.selectField();
              }}
              onBlur={() => {
                this.deselectField();
              }}
              onChangeText={text => {
                this.onChangeText(text);
              }}
            />
            {iconLeft && (
              <Image
                source={iconLeft}
                style={[{marginLeft: 5}, styleLeftIcon]}
              />
            )}
          </View>
        </View>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    width: '100%',
    paddingHorizontal: 10,
    alignItems: 'center',
  },
  myInputStyle: {
    borderBottomWidth: 1,
    width: '100%',
    height: 50,
  },
  textInput: {
    height: 35,
    fontSize: 14,
    color: '#000',
    width: '90%',
  },
  viewTextInput: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
});
