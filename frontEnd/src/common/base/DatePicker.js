import React from 'react';
import {TouchableOpacity, Platform, Text, StyleSheet, View} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';

type Props = {
  date?: Date,
  onClose?: Function,
  onChange?: (e: Event, d: date) => void,
  onConfirm?: Function,
};

class DatePicker extends React.Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      dateSelected: new Date(),
    };
  }

  onChange = (event, dateSelected) => {
    this.props.onChange(event, dateSelected);
  };

  getDateSelected = () => {
    return this.state.date;
  };

  render() {
    const {onClose, date, onConfirm} = this.props;
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={onClose}
        activeOpacity={1}>
        {Platform.OS === 'ios' && (
          <View style={styles.header}>
            <TouchableOpacity onPress={onClose}>
              <Text>Huỷ bỏ</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={onConfirm}>
              <Text>Xác nhận</Text>
            </TouchableOpacity>
          </View>
        )}
        <DateTimePicker
          value={date}
          mode="date"
          display="default"
          onChange={this.onChange}
          style={{backgroundColor: 'white'}}
        />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Platform.OS === 'ios' ? '#00000066' : 'transparent',
    position: 'absolute',
    justifyContent: 'flex-end',
    width: '100%',
    height: '100%',
  },
  header: {
    width: '100%',
    padding: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderColor: 'gray',
  },
});

export default DatePicker;
