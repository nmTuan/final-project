import React, {Component} from 'react';
import {SafeAreaView, StyleProp, ViewStyle} from 'react-native';

type Props = {
  viewStyle?: StyleProp<ViewStyle>,
};
class ViewSafeArea extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <SafeAreaView style={[{flex: 1}, this.props.viewStyle]}>
        {this.props.children}
      </SafeAreaView>
    );
  }
}

export default ViewSafeArea;
