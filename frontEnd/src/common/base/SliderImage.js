import React, {PureComponent} from 'react';
import {View, StyleSheet, Image, TouchableWithoutFeedback} from 'react-native';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import {ScreenConfig} from '../../config';
import {values} from '../../constant';
import {TouchablePreventDouble} from '.';

type Props = {
  keyItem?: String,
  onPressItem?: (v: value, i: index) => {},
};
class SliderImage extends PureComponent<Props> {
  constructor(props) {
    super(props);
    this.state = {
      activeSlide: 0,
    };
  }

  static defaultProps = {
    keyItem: '',
  };
  renderItem = ({item, index}) => {
    const {keyItem, onPressItem} = this.props;
    const uri = keyItem ? item[`${keyItem}`] : item.img;
    return (
      <TouchablePreventDouble
        style={{height: 200}}
        onPress={() => onPressItem(item, index - 3)}>
        <Image
          source={{uri: `${values.domain}${uri}`}}
          style={styles.imageProduct}
        />
      </TouchablePreventDouble>
    );
  };

  pagination() {
    const {activeSlide} = this.state;
    return (
      <Pagination
        dotsLength={this.props.arrImage.length}
        activeDotIndex={activeSlide}
        containerStyle={styles.containerDot}
        dotStyle={styles.dotstyle}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
    );
  }

  render() {
    return (
      <View style={styles.containerCarousel}>
        <Carousel
          ref={c => {
            this._carousel = c;
          }}
          loop
          inactiveSlideOpacity={1}
          autoplay
          autoplayDelay={500}
          data={this.props.arrImage}
          renderItem={this.renderItem}
          sliderWidth={ScreenConfig.deviceWidth}
          itemWidth={ScreenConfig.deviceWidth}
          onSnapToItem={index => {
            this.setState({activeSlide: index});
          }}
        />
        {this.pagination()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  dotstyle: {
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: 'rgba(255, 255, 255, 255)',
  },
  containerDot: {
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: 0,
  },
  containerCarousel: {alignItems: 'center'},
  imageProduct: {resizeMode: 'contain', width: '100%', height: '100%'},
});

export default SliderImage;
