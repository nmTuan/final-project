import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, Modal, View} from 'react-native';

type Props = {
  visible: Boolean,
  onPressCancel?: Function,
};
class PopupPicker extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static defaultProps = {
    visible: false,
  };

  render() {
    const {visible, children, onPressCancel} = this.props;
    return (
      <Modal visible={visible} transparent animationType="slide">
        <TouchableOpacity
          style={styles.backgroundModal}
          activeOpacity={1}
          onPress={onPressCancel}
        />
        <View style={styles.containerModal}>{visible && children}</View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  backgroundModal: {
    flex: 1,
    backgroundColor: '#00000040',
  },
  containerModal: {
    width: '100%',
    backgroundColor: '#fff',
    position: 'absolute',
    bottom: 0,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
  },
});

export default PopupPicker;
