import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageSourcePropType,
  StyleProp,
  TextStyle,
  Image,
  ImageStyle,
  TouchableOpacity,
} from 'react-native';
import {color} from '../../constant';

type Props = {
  textLeft?: String,
  textRight?: String,
  icon?: ImageSourcePropType,
  styleTextLeft?: StyleProp<TextStyle>,
  styleTextRight?: StyleProp<TextStyle>,
  styleIcon?: StyleProp<ImageStyle>,
  touchable?: Boolean,
  onPress?: Function,
};

class TextView extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static defaultProps = {
    textLeft: '',
    textRight: '',
    touchable: false,
  };

  _onTouch = () => {
    this.props.onPress();
  };

  _handleUnTochable = () => {};

  render() {
    const {
      textLeft,
      textRight,
      icon,
      styleTextLeft,
      styleTextRight,
      touchable,
      onPress,
    } = this.props;
    return (
      <TouchableOpacity
        style={styles.container}
        activeOpacity={touchable ? 0.5 : 1}
        onPress={touchable ? this._onTouch : this._handleUnTochable}>
        <View style={styles.viewText}>
          <Text style={styleTextLeft}> {textLeft} </Text>
          <View style={styles.viewRight}>
            <Text style={styleTextRight}> {textRight} </Text>
            {icon && <Image source={icon} style={styles.iconRight} />}
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 50,
    paddingHorizontal: 10,
    alignItems: 'center',
  },
  viewText: {
    marginTop: 10,
    flexDirection: 'row',
    width: '100%',
    borderBottomWidth: 1,
    paddingBottom: 5,
    borderColor: color.lightGrayColor,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  viewRight: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconRight: {
    width: 25,
    height: 25,
    resizeMode: 'center',
  },
});

export default TextView;
