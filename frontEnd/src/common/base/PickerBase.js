/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { Animated, StyleSheet, View, Easing, TouchableOpacity, } from 'react-native';
import { values } from '../../constant';
import { ScreenConfig } from '../../config';

type Props = {
  isShowModal?: Boolean,
  hideModal?: Function,
}
class PickerBase extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      viewHeight: -ScreenConfig.deviceHeight,
      animationHeight: -ScreenConfig.deviceHeight,
    };
    this.slideUpValue = new Animated.Value(0);
  }

  static defaultProps = {
    isShowModal: false,
  }

  componentDidMount() {
    // this.slideUp();
  }

  slideUp = () => {
    Animated.timing(
      this.slideUpValue,
      {
        toValue: 1,
        duration: values.duaration,
        easing: Easing.ease,
        useNativeDriver: true,
      }
    ).start();
  }

  slideDown = () => {
    Animated.timing(
      this.slideUpValue,
      {
        toValue: 0,
        duration: values.duaration,
        easing: Easing.ease,
        useNativeDriver: true,
      }
    ).start();
  }

  onPressCancel = () => {
    this.props.hideModal();
    this.slideDown();

  }

  onLayout = e => {
    this.setState({
      viewHeight: e.nativeEvent.layout.height,
    });
  }

  onLayoutAnimation = e => {
    const height = (this.state.viewHeight * 80) / 100;
    if (e.nativeEvent.layout.height > 20) {
      if (e.nativeEvent.layout.height >= height && height > 0) {
        this.setState({
          animationHeight: height,
        });
      } else {
        this.setState({
          animationHeight: e.nativeEvent.layout.height,
        });
      }
    }
  }

  render() {
    const { viewHeight, animationHeight } = this.state;
    const { isShowModal } = this.props;
    const maxHeight = (viewHeight * 80) / 100;
    return (
      <View onLayout={this.onLayout}
        style={[styles.container, { bottom: isShowModal ? 0 : -viewHeight }]}>
        <TouchableOpacity style={{ flex: 1 }} activeOpacity={1}
          onPress={this.onPressCancel} />
        <Animated.View onLayout={this.onLayoutAnimation}
          style={[styles.viewPopup,
          {
            maxHeight, bottom: isShowModal ? 0 : -animationHeight,
            transform: [
              {
                translateY: this.slideUpValue.interpolate({
                  inputRange: [0, 1],
                  outputRange: [animationHeight, 0],
                }),
              },
            ],
          }]} >
          {isShowModal ? this.props.children : <></>}
        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewPopup: {
    flex: 1,
    width: '100%',
    backgroundColor: 'white',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    padding: 10,
    position: 'absolute',
  },
  container: { position: 'absolute', width: '100%', height: '100%', backgroundColor: '#00000080', justifyContent: 'flex-end' },
});

export default PickerBase;
