import React, {Component} from 'react';
import {View, Text, StyleSheet, TextInput} from 'react-native';
import BtnBase from './BtnBase';
import TouchablePreventDouble from './TouchablePreventDouble';
import {color} from '../../constant';
import {TxtError} from '.';

type Props = {
  reSend?: Function,
  send?: Function,
  onChangeText?: (value: String) => void,
  cancleModal?: Function,
  textError?: String,
};
class ScreenOTP extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static defaultProps = {
    textError: '',
  };

  render() {
    const {reSend, send, onChangeText, cancleModal, textError} = this.props;
    return (
      <View style={styles.backgroundModal}>
        <TouchablePreventDouble
          style={{flex: 1}}
          activeOpacity={1}
          onPress={cancleModal}
        />
        <View style={styles.containerModal}>
          <Text style={styles.txtTitle}>Nhập mã OTP</Text>
          <View style={styles.viewTxtInput}>
            <TextInput
              style={styles.txtInput}
              onChangeText={onChangeText}
              autoFocus
            />
            <TouchablePreventDouble style={styles.btnReSend} onPress={reSend}>
              <Text style={{color: color.white}}>Gửi lại mã</Text>
            </TouchablePreventDouble>
          </View>
          {textError !== '' && <TxtError text={textError} />}
          <BtnBase styleBtn={styles.btnSend} textBtn="Gửi mã" onPress={send} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  backgroundModal: {
    width: '100%',
    height: '100%',
    backgroundColor: '#00000066',
  },
  containerModal: {
    width: '100%',
    position: 'absolute',
    bottom: 0,
    backgroundColor: '#fff',
    alignItems: 'center',
    borderTopRightRadius: 6,
    borderTopLeftRadius: 6,
    paddingTop: 10,
    paddingBottom: 20,
    paddingHorizontal: 20,
  },
  txtTitle: {fontSize: 18},
  viewTxtInput: {
    height: 35,
    width: '100%',
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
  },
  txtInput: {
    width: '80%',
    height: '100%',
    borderTopLeftRadius: 6,
    borderBottomLeftRadius: 6,
    borderWidth: 1,
    borderColor: 'gray',
  },
  btnReSend: {
    height: '100%',
    backgroundColor: color.agree,
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
    borderWidth: 1,
    borderColor: color.agree,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
  },
  btnSend: {height: 40, width: '100%', marginTop: 20},
});

export default ScreenOTP;
