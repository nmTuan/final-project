import React, {Component} from 'react';
import {View, Animated, StyleSheet} from 'react-native';
import {values} from '../../constant';
import {ScreenConfig} from '../../config';

type Props = {
  topBar?: React.ElementType,
};
class ViewAnimationTopBar extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const height = Animated.diffClamp(
      this.props.scrollY,
      0,
      values.topBarHeight,
    ).interpolate({
      inputRange: [0, values.topBarHeight],
      outputRange: [0, -values.topBarHeight],
      extrapolate: 'clamp',
    });
    const opacityHeader = Animated.diffClamp(
      this.props.scrollY,
      0,
      values.topBarHeight,
    ).interpolate({
      inputRange: [0, values.topBarHeight],
      outputRange: [1, 0],
    });
    return (
      <View style={{flex: 1}}>
        {this.props.children}
        <Animated.View
          style={[
            styles.viewTopBar,
            {
              transform: [
                {
                  translateY: height,
                },
              ],
              opacity: opacityHeader,
            },
          ]}>
          {this.props.topBar}
        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewTopBar: {
    // flex: 1,
    height: 40,
    width: '100%',
    position: 'absolute',
    top: 0,
  },
});

export default ViewAnimationTopBar;
