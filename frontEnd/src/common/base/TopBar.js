import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {values} from '../../constant';

type Props = {
  childLeft?: React.ElementType,
  childCenter?: React.ElementType,
  childRight?: React.ElementType,
};

class TopBar extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {childLeft, childCenter, childRight} = this.props;
    return childCenter ? (
      <View style={styles.container}>
        <View style={styles.viewChild}>{childLeft}</View>
        <View style={styles.viewChild}>{childCenter}</View>
        <View style={styles.viewChild}>{childRight}</View>
      </View>
    ) : (
      <View style={styles.container}>
        <View style={{flex: 2}}>{childLeft}</View>
        <View style={{flex: 1}}>{childRight}</View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: values.topBarHeight,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    alignItems: 'center',
  },
  viewChild: {
    flex: 1,
  },
});

export default TopBar;
