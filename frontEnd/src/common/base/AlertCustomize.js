import React from 'react';
import {ModalBase, BtnBase} from '.';
import {Text, View, StyleSheet, Modal, TouchableOpacity} from 'react-native';
import {color} from '../../constant';

const AlertCustomize = ({
  visible,
  title,
  content,
  onCancel,
  onPressLeft,
  onPressRight,
  textLeft = 'Huỷ bỏ',
  textRight = 'Đồng ý',
  backgroundColorRight = color.agree,
  colorRight = '#fff',
}) => {
  return (
    <Modal animationType="slide" transparent visible={visible}>
      <TouchableOpacity
        style={styles.backgroundModal}
        activeOpacity={1}
        onPress={onCancel}>
        <View style={styles.containerModal}>
          {title && <Text style={styles.titleModal}>{title}</Text>}
          {content && <Text style={styles.contentModal}>{content}</Text>}
          <View style={styles.containerBtnModal}>
            <TouchableOpacity
              style={[styles.btnModal, styles.btnLeft]}
              onPress={onPressLeft}>
              <Text>{textLeft}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                ...styles.btnModal,
                ...styles.btnRight,
                backgroundColor: backgroundColorRight,
              }}
              onPress={onPressRight}>
              <Text style={{color: colorRight}}>{textRight}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </TouchableOpacity>
    </Modal>
  );
};

const styles = StyleSheet.create({
  titleModal: {fontSize: 18, color: 'red'},
  contentModal: {
    fontSize: 15,
    textAlign: 'center',
    marginVertical: 10,
    marginHorizontal: 10,
  },
  containerBtnModal: {
    width: '100%',
    height: 40,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  btnModal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnLeft: {
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderColor: color.cancel,
  },
  btnRight: {
    borderTopWidth: 1,
    borderLeftWidth: 1,
    borderColor: color.agree,
    borderBottomRightRadius: 6,
  },
  backgroundModal: {
    width: '100%',
    height: '100%',
    backgroundColor: '#00000030',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerModal: {
    width: '70%',
    backgroundColor: '#fff',
    borderRadius: 8,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});

export default AlertCustomize;
