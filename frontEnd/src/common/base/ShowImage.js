/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import {values} from '../../constant';
import Carousel from 'react-native-snap-carousel';
import {ScreenConfig} from '../../config';

var timeout = null;

type ShowModalProps = {
  onPressCancel?: () => {},
  arrImage: [],
  keyItem: '',
  firstItem: 0,
};

class ShowImage extends React.PureComponent<ShowModalProps> {
  constructor(props) {
    super(props);
    this.state = {
      showTopBar: false,
    };
  }

  onPressImage = () => {
    this.setState({
      showTopBar: !this.state.showTopBar,
    });
    clearTimeout(timeout);
    timeout = setTimeout(() => {
      this.setState({
        showTopBar: false,
      });
    }, 3000);
  };

  renderItem = ({item, index}) => {
    const {keyItem} = this.props;
    const image = keyItem && keyItem !== '' ? item[`${keyItem}`] : item;
    return (
      <TouchableWithoutFeedback activeOpacity={1} onPress={this.onPressImage}>
        <Image
          source={{uri: `${values.domain}${image}`}}
          style={styles.image}
        />
      </TouchableWithoutFeedback>
    );
  };

  render() {
    const {showTopBar} = this.state;
    const {firstItem, keyItem} = this.props;
    return (
      <SafeAreaView style={styles.inner}>
        <Carousel
          style={{flex: 1}}
          inactiveSlideOpacity={1}
          data={this.props.arrImage}
          renderItem={this.renderItem}
          sliderWidth={ScreenConfig.deviceWidth}
          itemWidth={ScreenConfig.deviceWidth}
          firstItem={firstItem}
        />
        {showTopBar && (
          <TouchableOpacity
            style={{
              ...styles.viewTopBar,
              top: keyItem ? ScreenConfig.marginTopScreen : 0,
            }}>
            <TouchableWithoutFeedback onPress={this.props.onPressCancel}>
              <Text style={styles.textDone}>Xong</Text>
            </TouchableWithoutFeedback>
          </TouchableOpacity>
        )}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  inner: {
    flex: 1,
    ...StyleSheet.absoluteFillObject,
    backgroundColor: '#000000',
  },
  image: {
    ...StyleSheet.absoluteFillObject,
    width: null,
    height: null,
    resizeMode: 'contain',
  },
  viewTopBar: {
    width: '100%',
    height: 40,
    backgroundColor: '#737373',
    position: 'absolute',
    shadowColor: values.shadowColor,
    shadowOffset: values.shadowOffset,
    shadowOpacity: values.shadowOpacity,
    shadowRadius: values.shadowRadius,
    elevation: values.elevation,
    justifyContent: 'center',
    paddingLeft: 10,
  },
  textDone: {
    color: '#fff',
    fontSize: 16,
  },
});

export default ShowImage;
