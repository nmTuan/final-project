import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { color } from '../../constant';
import { ScreenConfig } from '../../config';

const OptionBase = ({
  title = '',
  textBtn1 = '',
  textBtn2 = '',
  textCancel = 'Huỷ bỏ',
  onPress1 = () => { },
  onPress2 = () => { },
  onPressCancel = () => { },
}) => {
  return (
    <View style={styles.backgroundModal}>
      <TouchableOpacity
        style={{ flex: 1 }}
        onPress={onPressCancel}
      />
      <View style={styles.containerModal}>
        <View style={styles.viewChild}>
          <View style={styles.viewText}>
            <Text>{title}</Text>
          </View>
          <View style={styles.line} />
          <TouchableOpacity
            style={styles.btnLogoutModal}
            activeOpacity={0.5}
            onPress={onPress1}>
            <Text style={styles.txtLogout}>{textBtn1}</Text>
          </TouchableOpacity>
          <View style={styles.line} />
          {
            textBtn2 !== '' && (
              <TouchableOpacity
                style={styles.btnLogoutModal}
                activeOpacity={0.5}
                onPress={onPress2}>
                <Text style={styles.txtLogout}>{textBtn2}</Text>
              </TouchableOpacity>
            )}
        </View>
        <TouchableOpacity
          style={{ ...styles.viewChild, paddingVertical: 10 }}
          onPress={onPressCancel}>
          <Text style={styles.txtCancel}>{textCancel}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  backgroundModal: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    backgroundColor: '#00000066',
  },
  containerModal: {
    width: '100%',
    position: 'absolute',
    bottom: 0,
    paddingHorizontal: 10,
    paddingBottom: ScreenConfig.bottomIphoneX + 10,
  },
  viewChild: {
    width: '100%',
    borderRadius: 8,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
  },
  viewText: {
    width: '100%',
    paddingVertical: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  line: { width: '100%', height: 1, backgroundColor: color.lightGrayColor },
  btnLogoutModal: {
    paddingTop: 10,
    paddingBottom: 5,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtLogout: { color: 'red', fontSize: 16 },
  txtCancel: { color: color.agree, fontSize: 16 },
})

export default OptionBase;
