import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Modal,
  Platform,
  KeyboardAvoidingView,
  Keyboard,
} from 'react-native';
import {Navigation} from 'react-native-navigation';
import {
  TopBar,
  TouchablePreventDouble,
  PickerScrollView,
  BtnBase,
  ScreenSms,
  ScreenOTP,
} from '../../common/base';
import Icon from 'react-native-vector-icons/FontAwesome';
import {color, values} from '../../constant';
import moment from 'moment';
import DatePicker from '../../common/base/DatePicker';
import {inject, observer} from 'mobx-react';
import Address from '../address';
import {UserService} from '../../services/UserService';
import {notifyMessage} from '../../config/Func';
import {AccountService} from '../../services/AccountService';

var timeoutSms = null;

@inject('AddressStore', 'UserStore')
@observer
class UpdatePersonalInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gender: ['Nam', 'Nữ', 'khác'],
      genderSelected: 0,
      showModalGender: false,
      showModalAddress: false,
      addressSelected: values.address.city,
      showDatepicker: false,
      dateIos: new Date(),
      personalInfo: null,
      phoneNumber: '',
      otpSms: '',
      showOtp: false,
      otpInput: '',
    };
  }

  UNSAFE_componentWillMount() {
    const {userInfo} = this.props;
    this.setState({
      personalInfo: userInfo,
      phoneNumber: userInfo.phoneNumber,
    });
    this.state.gender.map((item, index) => {
      if (userInfo.gender.toLocaleLowerCase() === item.toLocaleLowerCase()) {
        this.setState({
          genderSelected: index,
        });
      }
    });
  }

  componentDidMount() {
    this.getDataAddress();
  }

  getDataAddress = async () => {
    const {AddressStore} = this.props;
    const {personalInfo} = this.state;
    const city = await UserService.getCity(personalInfo.city);
    const district = await UserService.getDistrict(
      city[0].code,
      personalInfo.district,
    );
    const village = await UserService.getVillage(
      district[0].code,
      personalInfo.village,
    );
    AddressStore.setCity(city[0]);
    AddressStore.setDistrict(district[0]);
    AddressStore.setVillage(village[0]);
  };

  onPressBack = () => {
    Navigation.dismissModal(this.props.componentId);
    this.props.AddressStore.resetData();
  };

  onPressAddress = type => {
    const {AddressStore} = this.props;
    const address = values.address;
    switch (type) {
      case address.city:
        this.setState({
          addressSelected: address.city,
          showModalAddress: true,
        });
        break;
      case address.district:
        if (AddressStore.citySelected) {
          this.setState({
            addressSelected: address.district,
            showModalAddress: true,
          });
        } else {
          console.log('have to pick city - updateInfo');
        }
        break;
      case address.village:
        if (AddressStore.districtSelected) {
          this.setState({
            addressSelected: address.village,
            showModalAddress: true,
          });
        } else {
          console.log('have to pick district - updateInfo');
        }
        break;
      default:
        break;
    }
  };

  dissmissAddress = () => {
    this.setState({
      showModalAddress: false,
    });
  };

  onSelecteAddress = item => {
    const address = values.address;
    const personalInfo = this.state.personalInfo;
    switch (item.type) {
      case address.city:
        personalInfo.city = item.name;
        personalInfo.district = '';
        personalInfo.village = '';
        personalInfo.address = '';
        break;
      case address.district:
        personalInfo.district = item.name;
        personalInfo.village = '';
        personalInfo.address = '';
        break;
      case address.village:
        personalInfo.village = item.name;
        personalInfo.address = '';
        break;
      default:
        break;
    }
    this.setState({
      personalInfo,
      showModalAddress: false,
    });
  };

  handleValueChange = index => {
    this.setState({
      genderSelected: index,
    });
  };

  dissmissGender = () => {
    this.setState({
      showModalGender: false,
    });
  };

  onPressGender = () => {
    this.setState({
      showModalGender: true,
    });
  };

  onPressGetOtp = async () => {
    const otpSms = await AccountService.getOtpWithToken(
      this.state.personalInfo.phoneNumber,
    );
    this.setState({
      showOtp: true,
      otpSms,
    });
    timeoutSms = setTimeout(() => {
      this.sms.slideDown();
    }, 1000);
  };

  onPressReSendOtp = async () => {
    this.sms.slideUp();
    clearTimeout(timeoutSms);
    this.onPressGetOtp();
  };

  handleInputOtp = value => {
    this.setState({
      otpInput: value,
    });
  };

  onPressUpdatePhoneNumber = async () => {
    const {personalInfo, otpInput} = this.state;
    const res = await UserService.updatePhoneNumber(
      personalInfo.id,
      personalInfo.phoneNumber,
      otpInput,
    );
    if (res.message === 'update success') {
      this.setState({
        showOtp: false,
      });
      notifyMessage(res.message);
    } else if (res) {
      Keyboard.dismiss();
      notifyMessage(res.message);
    } else {
      notifyMessage('Error!!');
    }
  };

  onPressDissmissOtp = () => {
    this.setState({
      showOtp: false,
    });
  };

  onPressDate = () => {
    this.setState({
      showDatepicker: true,
    });
  };

  onChageDate = (event, dateSelected) => {
    const personalInfo = this.state.personalInfo;
    if (Platform.OS === 'ios') {
      personalInfo.dob = dateSelected;
    } else {
      if (event.type === 'set') {
        personalInfo.dob = event.nativeEvent.timestamp;
        this.setState({
          personalInfo,
          showDatepicker: false,
        });
      }
    }
  };

  onCloseDatePicker = () => {
    const personalInfo = this.state.personalInfo;
    if (Platform.OS === 'ios') {
      personalInfo.dob = this.state.dateIos;
      this.setState({
        personalInfo,
      });
    }
    this.setState({
      showDatepicker: false,
    });
  };

  onConfirmDate = () => {
    this.setState({
      dateIos: this.state.personalInfo.dob,
      showDatepicker: false,
    });
  };

  onChangePhoneNumber = value => {
    const personalInfo = this.state.personalInfo;
    personalInfo.phoneNumber = value;
    this.setState({
      personalInfo,
    });
  };

  onChangeAddress = value => {
    const personalInfo = this.state.personalInfo;
    personalInfo.address = value;
    this.setState({
      personalInfo,
    });
  };

  onPressUpdate = async () => {
    const {personalInfo, phoneNumber} = this.state;
    if (
      personalInfo.fullName !== '' &&
      personalInfo.city !== '' &&
      personalInfo.district !== '' &&
      personalInfo.village !== '' &&
      personalInfo.phoneNumber !== '' &&
      personalInfo.address !== ''
    ) {
      if (phoneNumber !== personalInfo.phoneNumber) {
        notifyMessage('Bạn phải gửi OTP cho số điện thoại mới');
      } else {
        const {
          id,
          address,
          dob,
          gender,
          city,
          district,
          village,
        } = personalInfo;
        const res = await UserService.updateUserInfo(
          id,
          dob,
          gender,
          '',
          '',
          city,
          district,
          village,
          address,
        );
        if (res.message === 'update success') {
          const userInfo = await UserService.getUserInfo(id);
          this.props.UserStore.setUserInfo(userInfo.result);
        }
        notifyMessage(res.message);
      }
    } else {
      notifyMessage('Bạn phải điền đầy đủ thông tin');
    }
  };

  renderInfor = () => {
    const {address} = values;
    const {gender, genderSelected, personalInfo} = this.state;
    return (
      <View style={styles.viewContent}>
        <View>
          <Text>Họ và tên</Text>
          <TextInput
            value={personalInfo.fullName && personalInfo.fullName}
            style={styles.textInput}
          />
        </View>
        <View style={styles.viewPicker}>
          <Text>Ngày sinh</Text>
          <TouchablePreventDouble
            style={styles.viewSelected}
            onPress={this.onPressDate}>
            <Text>{moment(personalInfo.dob).format('DD/MM/YYYY')}</Text>
            <View style={styles.viewIconSelected}>
              <Icon name="calendar" size={12} style={styles.iconSelected} />
            </View>
          </TouchablePreventDouble>
        </View>
        <View style={styles.viewPicker}>
          <Text>Giới tính</Text>
          <TouchablePreventDouble
            style={styles.viewSelected}
            onPress={this.onPressGender}>
            <Text>{gender[genderSelected]}</Text>
            <View style={styles.viewIconSelected}>
              <Icon name="sort-down" size={20} style={styles.iconSelected} />
            </View>
          </TouchablePreventDouble>
        </View>
        <View style={styles.containerTextInput}>
          <Text>Số điện thoại</Text>
          <View style={styles.viewPicker}>
            <TextInput
              style={styles.textInput}
              placeholder="Số điện thoại"
              value={personalInfo.phoneNumber}
              onChangeText={this.onChangePhoneNumber}
            />
            <TouchableOpacity
              style={styles.btnGetOTP}
              onPress={this.onPressReSendOtp}>
              <Text>Lấy OTP</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.viewPicker}>
          <Text>Tỉnh/Thành phố</Text>
          <TouchablePreventDouble
            style={styles.viewSelected}
            onPress={() => this.onPressAddress(address.city)}>
            <View style={{flex: 1}}>
              <Text numberOfLines={1}>
                {personalInfo.city && personalInfo.city !== ''
                  ? personalInfo.city
                  : 'Chọn thành phố'}
              </Text>
            </View>

            <View style={styles.viewIconSelected}>
              <Icon name="sort-down" size={20} style={styles.iconSelected} />
            </View>
          </TouchablePreventDouble>
        </View>
        <View style={styles.viewPicker}>
          <Text>Quận/Huyện</Text>
          <TouchablePreventDouble
            style={styles.viewSelected}
            onPress={() => this.onPressAddress(address.district)}>
            <View style={{flex: 1}}>
              <Text numberOfLines={1}>
                {personalInfo.district && personalInfo.district !== ''
                  ? personalInfo.district
                  : 'Chọn Quận/Huyện'}
              </Text>
            </View>

            <View style={styles.viewIconSelected}>
              <Icon name="sort-down" size={20} style={styles.iconSelected} />
            </View>
          </TouchablePreventDouble>
        </View>
        <View style={styles.viewPicker}>
          <Text>Xã/Phường</Text>
          <TouchablePreventDouble
            style={styles.viewSelected}
            onPress={() => this.onPressAddress(address.village)}>
            <View style={{flex: 1}}>
              <Text numberOfLines={1}>
                {personalInfo.village && personalInfo.village !== ''
                  ? personalInfo.village
                  : 'Chọn Xã/Phường'}
              </Text>
            </View>

            <View style={styles.viewIconSelected}>
              <Icon name="sort-down" size={20} style={styles.iconSelected} />
            </View>
          </TouchablePreventDouble>
        </View>
        <View style={styles.containerTextInput}>
          <Text>Nhập địa chỉ chi tiết</Text>
          <TextInput
            style={styles.textInput}
            placeholder="Địa chỉ chi tiết"
            value={personalInfo.address}
            onChangeText={this.onChangeAddress}
          />
        </View>
      </View>
    );
  };

  render() {
    const {
      showModalGender,
      gender,
      genderSelected,
      showDatepicker,
      personalInfo,
      addressSelected,
      showModalAddress,
      showOtp,
      otpSms,
    } = this.state;
    const {AddressStore} = this.props;
    return (
      <SafeAreaView style={styles.container}>
        <TouchableOpacity
          activeOpacity={1}
          onPress={Keyboard.dismiss}
          style={{flex: 1}}>
          <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
            keyboardVerticalOffset={-20}
            style={{flex: 1}}>
            <View style={styles.viewTopBar}>
              <TopBar
                childLeft={
                  <TouchableOpacity onPress={this.onPressBack}>
                    <Icon name="angle-left" size={40} style={styles.btnBack} />
                  </TouchableOpacity>
                }
                childCenter={
                  <Text style={styles.title}>Cập nhật thông tin</Text>
                }
              />
            </View>
            {this.renderInfor()}
            <View style={styles.viewBtnUpdate}>
              <BtnBase textBtn="Cập nhật" onPress={this.onPressUpdate} />
            </View>
          </KeyboardAvoidingView>
        </TouchableOpacity>
        {showModalAddress && (
          <Address
            type={addressSelected}
            showModalAddress={showModalAddress}
            onPressBack={this.dissmissAddress}
            AddressStore={AddressStore}
            onPressItem={this.onSelecteAddress}
          />
        )}
        <Modal visible={showModalGender} transparent animationType="slide">
          {showModalGender && (
            <View style={styles.backgroundModalGender}>
              <TouchablePreventDouble
                style={{flex: 1}}
                onPress={this.dissmissGender}
              />
              <View style={styles.viewChildGender}>
                <View>
                  <Text style={styles.txtTitleGender}>Giới tính</Text>
                  <TouchablePreventDouble
                    style={styles.btnSelecteGender}
                    onPress={this.dissmissGender}>
                    <Text style={styles.txtSelected}>Chọn</Text>
                  </TouchablePreventDouble>
                </View>
                <View style={styles.line} />
                <PickerScrollView
                  ref={ref => (this.pickerScroll = ref)}
                  dataSource={gender}
                  selectedIndex={genderSelected}
                  itemHeight={105 / 3}
                  wrapperHeight={105}
                  getSelected={this.handleValueChange}
                />
              </View>
            </View>
          )}
        </Modal>
        <Modal visible={showDatepicker} transparent animationType="slide">
          {showDatepicker && (
            <DatePicker
              date={moment(personalInfo.dob).toDate()}
              onChange={this.onChageDate}
              onClose={this.onCloseDatePicker}
              onConfirm={this.onConfirmDate}
            />
          )}
        </Modal>
        {showOtp ? (
          <View style={styles.containerOtp}>
            <ScreenOTP
              cancleModal={this.onPressDissmissOtp}
              reSend={this.onPressReSendOtp}
              send={this.onPressUpdatePhoneNumber}
              onChangeText={this.handleInputOtp}
            />
          </View>
        ) : (
          <></>
        )}
        <ScreenSms
          ref={ref => (this.sms = ref)}
          message={`Mã OTP của bạn dùng cho app Fashion là: ${otpSms}`}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: color.lightGrayColor},
  btnBack: {marginLeft: 10},
  title: {fontSize: 16, textAlign: 'center'},
  viewTopBar: {backgroundColor: '#fff'},
  viewContent: {
    backgroundColor: '#fff',
    padding: 10,
    marginTop: 10,
  },
  textInput: {
    width: '100%',
    height: 40,
    borderBottomWidth: 1,
    borderColor: color.lightGrayColor,
  },
  containerTextInput: {width: '100%', paddingTop: 10},
  viewBack: {
    width: '100%',
    paddingTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  txtBack: {textDecorationLine: 'underline', color: '#2c829e'},
  btn: {height: 40, width: '90%', marginTop: 30},
  viewPicker: {
    width: '100%',
    height: 40,
    paddingVertical: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  viewSelected: {
    height: '100%',
    width: 130,
    borderWidth: 1,
    borderColor: 'gray',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingLeft: 5,
  },
  viewIconSelected: {
    height: '100%',
    width: 25,
    borderLeftWidth: 1,
    borderColor: 'gray',
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconSelected: {marginBottom: 5},
  backgroundModalGender: {flex: 1, backgroundColor: '#00000060'},
  viewChildGender: {
    width: '100%',
    padding: 10,
    backgroundColor: '#fff',
    position: 'absolute',
    bottom: 0,
  },
  txtTitleGender: {fontSize: 18, textAlign: 'center'},
  line: {width: '100%', height: 1, backgroundColor: 'gray', marginVertical: 10},
  btnSelecteGender: {
    position: 'absolute',
    right: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtSelected: {color: 'blue'},
  viewBtnUpdate: {
    width: '100%',
    paddingHorizontal: 20,
    height: 40,
    position: 'absolute',
    bottom: 20,
  },
  btnGetOTP: {position: 'absolute', right: 0},
  containerOtp: {position: 'absolute', width: '100%', height: '100%'},
});

export default UpdatePersonalInfo;
