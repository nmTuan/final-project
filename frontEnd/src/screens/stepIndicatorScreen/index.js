import React, {Component} from 'react';
import {StepIndicator} from '../../common/base';
import {values} from '../../constant';
import {View} from 'react-native';

const customStyles = {
  stepIndicatorSize: 25,
  currentStepIndicatorSize: 30,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: '#fe7013',
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: '#fe7013',
  stepStrokeUnFinishedColor: '#aaaaaa',
  separatorFinishedColor: '#fe7013',
  separatorUnFinishedColor: '#aaaaaa',
  stepIndicatorFinishedColor: '#fe7013',
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: '#fe7013',
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: '#aaaaaa',
  labelColor: '#999999',
  labelSize: 13,
  currentStepLabelColor: '#fe7013',
};

class StepIndicatorScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPosition: 0,
      heightView: 0,
    };
  }

  _handleStepIndicator = ref => (this.stepIndicator = ref);

  _onNextStep = () => {
    const step = this.state.currentPosition + 1;
    this.stepIndicator.onCurrentPositionChanged(this.state.step);
    this.setState({
      currentPosition: step,
    });
  };

  _onBackStep = () => {
    const step = this.state.currentPosition - 1;
    this.stepIndicator.onCurrentPositionChanged(this.state.step);
    this.setState({
      currentPosition: step,
    });
  };

  render() {
    return (
      <View style={{width: '100%'}}>
        <StepIndicator
          customStyles={customStyles}
          currentPosition={this.state.currentPosition}
          labels={values.labels}
          ref={this._handleStepIndicator}
        />
      </View>
    );
  }
}

export default StepIndicatorScreen;
