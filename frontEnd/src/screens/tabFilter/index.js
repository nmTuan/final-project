import React, {Component} from 'react';
import {View, Text, StyleSheet, ActivityIndicator, Modal} from 'react-native';
import {TouchablePreventDouble} from '../../common/base';
import Icon from 'react-native-vector-icons/FontAwesome';
import {color, values} from '../../constant';
import {observer} from 'mobx-react';
import HomeService from '../../services/HomeService';
import ScreenConfig from '../../config/ScreenConfig';

@observer
class TabFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
    };
  }

  getDataProduct = async filter => {
    const {HomeStore, filterStore} = this.props;
    this.setState({
      isLoading: true,
    });
    filterStore.sort = filter;
    const dataProduct = await HomeService.getProduct(
      filterStore.sort,
      0,
      values.sizePage,
    );
    if (dataProduct) {
      this.setState({
        isLoading: false,
      });
      HomeStore.setProduct([], dataProduct.result);
    }
  };

  _onPressSelectedType = async type => {
    const {filterStore} = this.props;
    filterStore.onPressSelectedType(type);
    filterStore.currentStateSort = values.sortState.normal;
    var sort = [];
    switch (type) {
      case values.selected.mostSell:
        sort = [
          {
            direction: 'DESC',
            property: 'sold',
          },
        ];
        filterStore.sort.listSort = sort;
        break;
      case values.selected.mostSell:
        filterStore.reset();
        break;
      case values.selected.rent:
        filterStore.sort.isRent = 1;
        break;
      default:
        break;
    }
    this.getDataProduct(filterStore.sort);
  };

  _onPressSortVote = async () => {
    const {filterStore} = this.props;
    var sort = [
      {
        direction: 'DESC',
        property: 'vote',
      },
    ];
    if (filterStore.currentTypeSelected === values.selected.mostSell) {
      sort.push({
        direction: 'DESC',
        property: 'sold',
      });
    }
    filterStore.sort.listSort = sort;
    this.getDataProduct(filterStore.sort);
  };

  _onPressSortPrice = currentSortState => {
    const {filterStore} = this.props;
    filterStore.priceSort(currentSortState);
    var sort = [
      {
        direction: 'DESC',
        property: 'maxPrice',
      },
    ];
    switch (currentSortState) {
      case values.sortState.up:
        sort = [
          {
            direction: 'DESC',
            property: 'maxPrice',
          },
        ];
        break;
      case values.sortState.down:
        sort = [
          {
            direction: 'ASC',
            property: 'maxPrice',
          },
        ];
        break;
      default:
        break;
    }
    if (filterStore.currentTypeSelected === values.selected.mostSell) {
      sort.push({
        direction: 'DESC',
        property: 'sold',
      });
    }
    filterStore.sort.listSort = sort;
    this.getDataProduct(filterStore.sort);
  };

  _onPressShowFilterOption = () => {
    const {filterStore} = this.props;
    filterStore.isShowFilter = true;
  };

  renderSortIcon(currentStateSort) {
    const sort = values.sortState;
    return currentStateSort === sort.up ? (
      <Icon name="sort-up" size={20} style={{marginTop: 7}} />
    ) : currentStateSort === sort.down ? (
      <Icon name="sort-down" size={20} style={{marginBottom: 7}} />
    ) : (
      <Icon name="sort" size={20} />
    );
  }

  render() {
    const {filterStore} = this.props;
    const {sortState, selected} = values;
    const {isLoading} = this.state;
    return (
      <View style={[styles.viewFilter, styles.shadowBox]}>
        <TouchablePreventDouble
          style={[
            styles.btnOptionFilter,
            filterStore.currentTypeSelected === selected.all &&
              styles.underline,
          ]}
          onPress={() => this._onPressSelectedType(selected.all)}
          activeOpacity={0.6}>
          <Text>Tất cả</Text>
          <View style={styles.verticalLine} />
        </TouchablePreventDouble>
        {/* <TouchablePreventDouble
          style={[
            styles.btnOptionFilter,
            filterStore.currentTypeSelected === selected.rent &&
              styles.underline,
          ]}
          onPress={() => this._onPressSelectedType(selected.rent)}
          activeOpacity={0.6}>
          <Text>Thuê</Text>
          <View style={styles.verticalLine} />
        </TouchablePreventDouble> */}
        <TouchablePreventDouble
          style={[
            styles.btnOptionFilter,
            filterStore.currentTypeSelected === selected.mostSell &&
              styles.underline,
          ]}
          onPress={() => this._onPressSelectedType(selected.mostSell)}
          activeOpacity={0.6}>
          <Text>Bán chạy</Text>
          <View style={styles.verticalLine} />
        </TouchablePreventDouble>
        <TouchablePreventDouble
          style={[styles.btnOptionFilter]}
          onPress={this._onPressSortVote}
          activeOpacity={0.6}>
          <Text style={{marginRight: 5}}>Đánh giá</Text>
          {this.renderSortIcon(sortState.down)}
          <View style={styles.verticalLine} />
        </TouchablePreventDouble>
        <TouchablePreventDouble
          style={styles.btnOptionFilter}
          onPress={() => this._onPressSortPrice(filterStore.currentStateSort)}
          activeOpacity={0.6}>
          <Text style={{marginRight: 5}}>Giá</Text>
          {this.renderSortIcon(filterStore.currentStateSort)}
          <View style={styles.verticalLine} />
        </TouchablePreventDouble>
        <TouchablePreventDouble
          style={styles.btnFilter}
          activeOpacity={0.6}
          onPress={this._onPressShowFilterOption}>
          <Icon name="filter" size={26} color={color.primary} />
        </TouchablePreventDouble>
        <Modal visible={isLoading} transparent>
          <View style={styles.viewActivityIndicator}>
            <ActivityIndicator size="large" color="#00ff00" />
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  shadowBox: {
    shadowColor: values.shadowColor,
    shadowOffset: values.shadowOffset,
    shadowOpacity: values.shadowOpacity,
    shadowRadius: values.shadowRadius,
    // elevation: values.elevation,
  },
  viewFilter: {
    width: '100%',
    height: 40,
    backgroundColor: '#ffffff',
    marginVertical: 10,
    flexDirection: 'row',
    // top: ScreenConfig.marginTopScreen,
  },
  btnOptionFilter: {
    // paddingHorizontal: 15,
    flex: 2,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  underline: {
    borderBottomWidth: 1,
    borderColor: color.primary,
  },
  btnFilter: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  verticalLine: {
    height: '50%',
    borderColor: color.lightGrayColor,
    borderRightWidth: 1,
    position: 'absolute',
    right: 0,
  },
  viewActivityIndicator: {
    position: 'absolute',
    bottom: 10,
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default TabFilter;
