import React, {Component} from 'react';
import {View, Image, StyleSheet, Modal, SafeAreaView} from 'react-native';
import {BtnBase, ScreenSms} from '../../common/base';
import LoginForm from '../loginForm';
import SignUpForm from '../signUpForm';
import {inject, observer} from 'mobx-react';
import {Navigation} from 'react-native-navigation';
import { pushScreen } from '../../config/NavigationConfig';

@inject('SignUpStore', 'UserStore', 'AddressStore')
@observer
class NotLogged extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onPressLogin = () => {
    pushScreen(this.props.componentId, 'LoginForm');
  };

  onPressSignup = () => {
    pushScreen(this.props.componentId, 'SignUpForm');
  };

  render() {
    return (
      <View style={styles.cotainer}>
        <Image source={require('@assets/logo/logo1.png')} style={styles.logo} />
        <BtnBase
          styleBtn={styles.btn}
          textBtn="Đăng nhập"
          onPress={this.onPressLogin}
        />
        <BtnBase
          styleBtn={styles.btn}
          textBtn="Đăng Ký"
          backgroundColor="gray"
          onPress={this.onPressSignup}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cotainer: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  logo: {width: 150, height: 150, resizeMode: 'contain'},
  btn: {height: 40, width: '90%', marginTop: 10},
  viewModal: {
    width: '100%',
    height: '100%',
  },
});

export default NotLogged;
