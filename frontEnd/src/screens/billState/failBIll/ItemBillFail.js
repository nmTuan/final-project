import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import BaseBill from '../BaseBill';
import {color} from '../../../constant';
import {Navigation} from 'react-native-navigation';
import TouchablePreventDouble from '../../../common/base/TouchablePreventDouble';
import moment from 'moment';
import {showModal} from '../../../config/NavigationConfig';

class ItemBillFail extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onPressBill = () => {
    const {item} = this.props;
    const passProps = {
      data: item,
    };
    showModal('BillDetailFail', passProps);
  };

  render() {
    const {item, index} = this.props;
    return (
      <TouchablePreventDouble
        style={styles.container}
        key={`${index}`}
        activeOpacity={1}
        onPress={this.onPressBill}>
        <BaseBill item={item} index={index} />
        <View style={styles.viewBottom}>
          <Text style={styles.txtDateCreate}>
            {`Ngày giao hàng thất bại: ${moment(item.actionDate).format(
              'DD/MM/YYYY',
            )}`}
          </Text>
        </View>
      </TouchablePreventDouble>
    );
  }
}

const styles = StyleSheet.create({
  container: {backgroundColor: '#fff', marginTop: 20, paddingBottom: 10},
  viewBottom: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
  },
  txtDateCreate: {color: color.agree},
});

export default ItemBillFail;
