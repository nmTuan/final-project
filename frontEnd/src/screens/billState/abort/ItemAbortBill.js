import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import BaseBill from '../BaseBill';
import {color} from '../../../constant';
import {TouchablePreventDouble} from '../../../common/base';
import {Navigation} from 'react-native-navigation';
import moment from 'moment';
import {statusProduct} from '../../../constant/Format';
import { showModal } from '../../../config/NavigationConfig';

class ItemAbortBill extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onPressBill = () => {
    const {item, UserStore} = this.props;
    const passProps = {
      data: item,
      UserStore: UserStore,
    };
    showModal('BillDetailAbort', passProps);
  };

  render() {
    const {item, index} = this.props;
    return (
      <TouchablePreventDouble
        style={styles.container}
        key={`${index}`}
        activeOpacity={1}
        onPress={this.onPressBill}>
        <BaseBill item={item} index={index} />
        <View style={styles.viewBottom}>
          <Text style={styles.txtDateCreate}>
            {`Ngày huỷ đơn: ${moment(item.actionDate).format('DD/MM/YYYY')}`}
          </Text>
          <Text style={styles.txtDateCreate}>{statusProduct(item.status)}</Text>
        </View>
      </TouchablePreventDouble>
    );
  }
}

const styles = StyleSheet.create({
  container: {backgroundColor: '#fff', marginTop: 20, paddingBottom: 10},
  viewBottom: {
    width: '100%',
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  txtDateCreate: {color: color.agree, marginTop: 5},
});

export default ItemAbortBill;
