import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  RefreshControl,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import ItemBillConfirmed from './ItemBillConfirmed';
import {values, color} from '../../../constant';
import {UserService} from '../../../services/UserService';
import {observer} from 'mobx-react';

@observer
class ConfirmedBill extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      isLoadData: true,
    };
  }

  componentDidMount() {
    this.getData();
  }

  async getData() {
    const {UserStore} = this.props;
    var bills = [];
    const billConfirm = await UserService.getBill(
      UserStore.userInfo.id,
      values.bill.confirmed,
    );
    if (
      billConfirm.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      bills = [...billConfirm.result.result];
    }
    UserStore.setBillConfirm(bills);
    this.setState({
      isLoadData: false,
    });
  }

  onRefresh = () => {
    this.setState({
      refreshing: true,
    });
    this.getData();
    this.setState({
      refreshing: false,
    });
  };

  render() {
    const {UserStore, componentId} = this.props;
    const {refreshing, isLoadData} = this.state;
    return (
      <View>
        {!isLoadData ? (
          UserStore.billConfirm.length > 0 ? (
            <FlatList
              data={UserStore.billConfirm}
              refreshing={refreshing}
              onRefresh={this.onRefresh}
              renderItem={({item, index}) => (
                <ItemBillConfirmed
                  item={item}
                  index={index}
                  componentId={componentId}
                />
              )}
            />
          ) : (
            <View style={styles.containerEmpty}>
              <ScrollView
                style={{flex: 1}}
                contentContainerStyle={styles.scrollView}
                refreshControl={
                  <RefreshControl
                    refreshing={refreshing}
                    onRefresh={this.onRefresh}
                  />
                }>
                <Text style={styles.txtNoneProduct}>Không có sản phẩm</Text>
              </ScrollView>
            </View>
          )
        ) : (
          <View style={styles.loading}>
            <ActivityIndicator size="large" color={color.agree} />
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  txtNoneProduct: {textAlign: 'center', marginTop: 10},
  scrollView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerEmpty: {width: '100%', height: '100%'},
  loading: {
    width: '100%',
    height: '100%',
    alignContent: 'center',
    justifyContent: 'center',
  },
});

export default ConfirmedBill;
