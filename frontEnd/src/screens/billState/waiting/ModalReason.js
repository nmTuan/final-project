import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {CheckBox} from 'react-native-elements';
import {color} from '../../../constant';
import { notifyMessage } from '../../../config/Func';

class ModalReason extends Component {
  constructor(props) {
    super(props);
    this.state = {
      arr: [
        {id: 1, name: 'Có chỗ mua khác rẻ hơn'},
        {id: 2, name: 'Không thích sản phẩm này nữa'},
        {id: 3, name: 'Đã mua chỗ khác'},
      ],
      indexCheck: -1,
    };
  }

  onPressCheck = check => {
    this.setState({
      indexCheck: check,
    });
  };

  renderItem = (item, index) => {
    const {indexCheck} = this.state;
    return (
      <View style={styles.containerItem}>
        <CheckBox
          title={item.name}
          checkedIcon="dot-circle-o"
          uncheckedIcon="circle-o"
          checked={indexCheck === index ? true : false}
          onPress={() => this.onPressCheck(index)}
          containerStyle={styles.containerCheckBox}
        />
      </View>
    );
  };

  onPressSend = () => {
    const {indexCheck, arr} = this.state;
    if (indexCheck >= 0) {
      this.props.onPressSend(arr[indexCheck].name);
    } else {
      notifyMessage('Vui lòng chọn lý do hủy đơn hàng');
    }
  }

  render() {
    return (
      <View style={styles.background}>
        <View style={styles.container}>
          <View style={{padding: 10}}>
            <Text style={styles.title}>Lý do bạn muốn huỷ đơn hàng</Text>
            {this.state.arr.map((item, index) => this.renderItem(item, index))}
          </View>
          <View style={styles.containerBtn}>
            <TouchableOpacity
              style={styles.btnCancel}
              onPress={this.props.onPressCancel}>
              <Text style={styles.txtCancel}>Huỷ</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btnSend} onPress={this.onPressSend}>
              <Text>Gửi</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    width: '100%',
    height: '100%',
    backgroundColor: '#00000066',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    width: ' 70%',
    borderRadius: 8,
    backgroundColor: '#fff',
  },
  title: {fontSize: 18, color: 'gray'},
  containerItem: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 10,
  },
  containerCheckBox: {
    width: '100%',
    borderColor: 'transparent',
    padding: 0,
    backgroundColor: 'transparent',
  },
  containerBtn: {
    width: '100%',
    flexDirection: 'row',
    borderTopWidth: 1,
    borderColor: color.lightGrayColor,
    height: 30,
  },
  btnCancel: {
    flex: 1,
    borderBottomLeftRadius: 8,
    backgroundColor: color.agree,
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtCancel: {color: '#fff'},
  btnSend: {
    flex: 1,
    borderBottomRightRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default ModalReason;
