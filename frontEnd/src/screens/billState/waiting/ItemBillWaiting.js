import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import BaseBill from '../BaseBill';
import {color} from '../../../constant';
import {BtnBase, TouchablePreventDouble} from '../../../common/base';
import {Navigation} from 'react-native-navigation';
import moment from 'moment';
import { showModal } from '../../../config/NavigationConfig';

class ItemBillWaiting extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onPressAbortBill = (item, index) => {
    this.props.abortBill(item, index);
  };

  onPressBill = () => {
    const {item} = this.props;
    const passProps = {
      data: item,
    };
    showModal('BillDetailWaiting', passProps);
  };

  render() {
    const {item, index} = this.props;
    return (
      <TouchablePreventDouble
        key={`${index}`}
        style={styles.container}
        activeOpacity={1}
        onPress={this.onPressBill}>
        <BaseBill item={item} index={index} />
        <View style={styles.viewBottom}>
          <Text style={styles.txtDateCreate}>
            {`Ngày tạo đơn: ${moment(item.createdDate).format('DD/MM/yyyy')}`}
          </Text>
          <BtnBase
            styleBtn={styles.btnAbort}
            textBtn="Huỷ đơn hàng"
            onPress={() => this.onPressAbortBill(item, index)}
          />
        </View>
      </TouchablePreventDouble>
    );
  }
}

const styles = StyleSheet.create({
  container: {backgroundColor: '#fff', marginTop: 20, paddingBottom: 10},
  viewBottom: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
  },
  txtDateCreate: {color: color.agree},
  btnAbort: {height: 30},
});

export default ItemBillWaiting;
