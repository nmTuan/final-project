import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Modal,
  FlatList,
  ScrollView,
  RefreshControl,
  ActivityIndicator,
} from 'react-native';
import ItemBillWaiting from './ItemBillWaiting';
import ModalReason from './ModalReason';
import {UserService} from '../../../services/UserService';
import {values, color} from '../../../constant';
import {observer} from 'mobx-react';
import {notifyMessage} from '../../../config/Func';

@observer
class BillWaiting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showReason: false,
      item: null,
      index: -1,
      refreshing: false,
      isLoadData: true,
    };
  }

  componentDidMount() {
    this.getData();
  }

  async getData() {
    const {UserStore} = this.props;
    var bills = [];
    const billWaiting = await UserService.getBill(
      UserStore.userInfo.id,
      values.bill.waiting,
    );
    if (
      billWaiting.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      bills = [...billWaiting.result.result];
    }
    UserStore.setBillWaiting(bills);
    this.setState({
      isLoadData: false,
    });
  }

  onRefresh = () => {
    this.setState({
      refreshing: true,
    });
    this.getData();
    this.setState({
      refreshing: false,
    });
  };

  onAbortBill = (item, index) => {
    this.setState({
      showReason: true,
      item,
      index,
    });
  };

  onPressCancel = () => {
    this.setState({
      showReason: false,
    });
  };

  onPressSend = async reason => {
    const {UserStore} = this.props;
    const {item, index} = this.state;
    const res = await UserService.abortBill(item.id, 2, reason);
    if (
      res.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      UserStore.billWaiting.splice(index, 1);
      item.status = 2;
      UserStore.billAbort.push(item);
    }
    notifyMessage(res.message);
    this.setState({
      showReason: false,
    });
  };

  render() {
    const {componentId, UserStore} = this.props;
    const {showReason, refreshing, isLoadData} = this.state;
    return (
      <View>
        {!isLoadData ? (
          UserStore.billWaiting.length > 0 ? (
            <FlatList
              data={UserStore.billWaiting}
              refreshing={refreshing}
              onRefresh={this.onRefresh}
              renderItem={({item, index}) => (
                <ItemBillWaiting
                  item={item}
                  index={index}
                  componentId={componentId}
                  abortBill={this.onAbortBill}
                />
              )}
            />
          ) : (
            <View style={styles.containerEmpty}>
              <ScrollView
                style={{flex: 1}}
                contentContainerStyle={styles.scrollView}
                refreshControl={
                  <RefreshControl
                    refreshing={refreshing}
                    onRefresh={this.onRefresh}
                  />
                }>
                <Text style={styles.txtNoneProduct}>Không có sản phẩm</Text>
              </ScrollView>
            </View>
          )
        ) : (
          <View style={styles.loading}>
            <ActivityIndicator size="large" color={color.agree} />
          </View>
        )}
        <Modal visible={showReason} transparent>
          <ModalReason
            onPressCancel={this.onPressCancel}
            onPressSend={this.onPressSend}
          />
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  txtNoneProduct: {textAlign: 'center', marginTop: 10},
  scrollView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerEmpty: {width: '100%', height: '100%'},
  loading: {
    width: '100%',
    height: '100%',
    alignContent: 'center',
    justifyContent: 'center',
  },
});

export default BillWaiting;
