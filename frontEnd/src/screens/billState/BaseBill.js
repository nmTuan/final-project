import React, {Component} from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Rating, colors} from 'react-native-elements';
import {color, values} from '../../constant';
import {statusProduct} from '../../constant/Format';

class BaseBill extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {item} = this.props;
    return (
      <View style={styles.containerItem}>
        <View style={styles.viewShopInfo}>
          <View style={styles.viewShopName}>
            <Text style={styles.shopName} numberOfLines={2}>
              {item.shopName}
            </Text>
            <Text style={styles.statusBill} numberOfLines={1}>
              {statusProduct(item.status)}
            </Text>
          </View>
          <View style={styles.viewShopAddress}>
            <Icon name="map-marker" size={15} />
            <Text style={styles.shopAddress}>{item.shopAddress}</Text>
          </View>
        </View>
        <View style={styles.containerListProduct}>
          <Image
            source={{uri: `${values.domain}${item.thumbnail}`}}
            style={styles.productImg}
          />
          <View style={styles.containerInfoProduct}>
            <View style={styles.viewProductName}>
              <Text style={styles.productName} numberOfLines={1}>
                {item.productName}
              </Text>
            </View>
            <View style={styles.viewPrice}>
              <Text style={styles.oldPrice}>Gia cu</Text>
              <Text>Gia Moi</Text>
            </View>
            <View style={styles.viewVote}>
              <Rating startingValue={item.vote} imageSize={15} />
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerListProduct: {
    width: '100%',
    borderWidth: 1,
    borderColor: 'gray',
    paddingHorizontal: 10,
    paddingVertical: 5,
    flexDirection: 'row',
    marginTop: 10,
  },
  productImg: {width: 100, height: 80, resizeMode: 'stretch'},
  containerInfoProduct: {flex: 1, marginHorizontal: 10},
  viewProductName: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  productName: {fontSize: 17},
  viewPrice: {flexDirection: 'row'},
  oldPrice: {
    fontSize: 14,
    textDecorationLine: 'line-through',
    color: 'gray',
    marginRight: 10,
  },
  viewVote: {paddingTop: 10, alignItems: 'flex-start'},
  containerItem: {
    backgroundColor: '#fff',
    width: '100%',
    marginVertical: 5,
    paddingBottom: 10,
  },
  viewShopInfo: {
    padding: 10,
  },
  viewShopName: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  shopName: {flex: 2, fontSize: 20, color: '#000'},
  statusBill: {flex: 1, fontSize: 16, color: color.agree},
  viewShopAddress: {flexDirection: 'row', alignItems: 'center', paddingTop: 5},
  shopAddress: {marginLeft: 5, color: '#000'},
  titleModal: {fontSize: 14, color: 'red'},
  contentModal: {fontSize: 14, textAlign: 'center', marginVertical: 10},
  containerBtnModal: {
    width: '100%',
    height: 30,
    flexDirection: 'row',
    marginVertical: 10,
    justifyContent: 'space-between',
  },
  btnModal: {width: '45%'},
  btnTrash: {
    width: '100%',
    height: '100%',
    paddingRight: 10,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  btnBuy: {
    width: '100%',
    height: 40,
    paddingHorizontal: 20,
    position: 'absolute',
    bottom: 20,
  },
});

export default BaseBill;
