import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import {TouchablePreventDouble} from '../../common/base';
import {color, values} from '../../constant';
import {Rating} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {pushScreen} from '../../config/NavigationConfig';
import {formatCurrency} from '../../constant/Format';

class Product extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  _onPressProduct = (item, index) => {
    const passProps = {
      item,
      index,
    };
    pushScreen(this.props.componentId, 'ProductDetail', passProps);
  };

  render() {
    const {item, index} = this.props;
    return (
      <TouchablePreventDouble
        style={[styles.viewProduct]}
        activeOpacity={0.6}
        onPress={() => this._onPressProduct(item, index)}>
        <View style={[styles.containerProduct, styles.shadowBox]}>
          <Image
            style={styles.imageProduct}
            source={{uri: `${values.domain}${item.thumbnail}`}}
          />
          <View style={styles.containerInfor}>
            <Text style={styles.productName} numberOfLines={1}>
              {item.productName}
            </Text>
            <View style={styles.viewRow}>
              {item.oldPrice > 0 && (
                <Text style={styles.oldPrice}>
                  {formatCurrency(item.oldPrice)}
                </Text>
              )}
              <Text>
                {`${
                  item.minPrice ? formatCurrency(item.minPrice) + ' - ' : ''
                }${formatCurrency(item.maxPrice)} `}
              </Text>
            </View>
            <View style={[styles.viewRow, styles.viewRating]}>
              <Text>{`Da ban: ${item.sold}`}</Text>
              <Rating imageSize={15} startingValue={item.vote} readonly />
            </View>
            <View style={[styles.viewRow, styles.viewLocation]}>
              <Icon name="map-marker" />
              <Text style={styles.txtLocation} numberOfLines={1}>
                {item.shopAddress}
              </Text>
            </View>
          </View>
        </View>
      </TouchablePreventDouble>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.background,
  },
  viewProduct: {
    padding: 5,
    paddingTop: 5,
    width: '50%',
  },
  shadowBox: {
    shadowColor: values.shadowColor,
    shadowOffset: values.shadowOffset,
    shadowOpacity: values.shadowOpacity,
    shadowRadius: values.shadowRadius,
    // elevation: values.elevation,
  },
  containerProduct: {
    backgroundColor: '#fff',
  },
  imageProduct: {
    height: 130,
    width: '100%',
    resizeMode: 'contain',
  },
  containerInfor: {padding: 5},
  productName: {fontSize: 16, marginVertical: 5},
  viewRow: {flexDirection: 'row'},
  oldPrice: {
    fontSize: 13,
    color: 'gray',
    textDecorationLine: 'line-through',
    marginRight: 10,
  },
  newPrice: {marginLeft: 10},
  viewRating: {
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingBottom: 5,
  },
  viewLocation: {
    width: '100%',
    paddingLeft: 6,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  txtLocation: {marginLeft: 10, fontSize: 13},
});

export default Product;
