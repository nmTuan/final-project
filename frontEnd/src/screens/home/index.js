import React from 'react';
import {
  View,
  StyleSheet,
  Animated,
  RefreshControl,
  ActivityIndicator,
  Modal,
} from 'react-native';
import {values, color} from '../../constant';
import {ViewSafeArea, ViewNotification} from '../../common/base';
import SearchTopBar from '../searchTopBar';
import Filter from '../filter';
import Product from '../product';
import TabFilter from '../tabFilter';
import {inject, observer} from 'mobx-react';
import {pushScreen} from '../../config/NavigationConfig';
import ViewAnimationTopBar from '../../common/base/ViewAnimationTopBar';
import HomeService from '../../services/HomeService';
import {isCloseToBottom} from '../../config/Func';
import Search from './Search';
import {UserService} from '../../services/UserService';
import {getSocket} from '../../config/Socket';

@inject('FilterStore', 'OrderStore', 'HomeStore', 'UserStore')
@observer
class Home extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentPosition: 0,
      currentStateSort: values.sortState.normal,
      currentSelected: values.selected.all,
      scrollY: new Animated.Value(0),
      isShowFilter: false,
      scrolling: 0,
      page: 0,
      refresh: false,
      isLoadmore: false,
      isShowSearch: false,
      title: '',
      message: '',
      image: '',
      dataMessage: null,
    };
  }

  async UNSAFE_componentWillMount() {
    const {FilterStore} = this.props;
    this.getListProduct(FilterStore.sort);
    FilterStore.categories = await HomeService.getListCategory();
    FilterStore.age = await HomeService.getListAge();
    FilterStore.gender = await HomeService.getListGender();
    this.getQuantityChat();
  }

  async componentDidMount() {
    const {OrderStore, UserStore} = this.props;
    var cart = [];
    if (UserStore.userInfo) {
      this.updateSocket(UserStore.userInfo.id);
      cart = await UserService.getCart(UserStore.userInfo.id);
      OrderStore.listProductInCart = cart;
    }
    getSocket().on('receiveMess', result => {
      this.setState({
        message: result.content,
        title: result.sender,
        image: result.senderava,
        dataMessage: result,
      });
      this.notification.getNotification();
      getSocket().on('firtJoin', roomId => {
        getSocket().emit('joinRoom', roomId);
      });
    });
  }

  getQuantityChat = async () => {
    const {UserStore} = this.props;
    if (UserStore.userInfo) {
      const count = await UserService.getQuantityChat(UserStore.userInfo.id, 0);
      UserStore.quantityChat = count;
    }
  };

  updateSocket(id) {
    const {login, customer} = values.socket;
    const data = {
      type: customer, // customer
      custId: id,
    };
    getSocket().emit(login, data);
  }

  async getListProduct(sort) {
    const {HomeStore} = this.props;
    const dataProduct = await HomeService.getProduct(sort, 0, values.sizePage);
    if (dataProduct) {
      HomeStore.setProduct([], dataProduct.result);
    }
  }

  _onPressAccept = async () => {
    const {FilterStore, HomeStore} = this.props;
    FilterStore.currentTypeSelected = -1;
    FilterStore.sort.productName = '';
    const dataProduct = await HomeService.getProduct(
      FilterStore.sort,
      0,
      values.sizePage,
    );
    if (dataProduct) {
      HomeStore.setProduct([], dataProduct.result);
    }
  };

  onPressCart = () => {
    pushScreen(this.props.componentId, 'Cart');
  };

  onPressChat = () => {
    pushScreen(this.props.componentId, 'ListRoomCust');
  };

  onFocus = () => {
    console.log('onfocus');
    this.search.blur();
    this.setState({
      isShowSearch: true,
    });
  };

  dissmissSearch = () => {
    this.setState({
      isShowSearch: false,
    });
  };

  renderTopBar() {
    return (
      <SearchTopBar
        ref={ref => (this.search = ref)}
        animationSearch={this.state.scrollY}
        onPressCart={this.onPressCart}
        onPressChat={this.onPressChat}
        onFocus={this.onFocus}
        componentId={this.props.componentId}
      />
    );
  }

  onScrollEnd = async ({nativeEvent}) => {
    const {HomeStore, FilterStore} = this.props;
    if (isCloseToBottom(nativeEvent)) {
      const page = this.state.page + 1;
      this.setState({
        isLoadmore: true,
      });
      const dataProduct = await HomeService.getProduct(
        FilterStore.sort,
        page,
        values.sizePage,
      );
      if (dataProduct) {
        HomeStore.setProduct(HomeStore.listProducts, dataProduct.result);
        this.setState({
          isLoadmore: false,
        });
        if (dataProduct.result.length > 0) {
          this.setState({
            page,
          });
        }
      }
    }
  };

  onScrollEndDrag = ({nativeEvent}) => {
    if (nativeEvent.contentOffset.y > 25) {
      Animated.timing(this.state.scrollY, {
        toValue: 50,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(this.state.scrollY, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  onRefresh = () => {
    const {FilterStore} = this.props;
    this.setState({
      refresh: true,
    });
    this.tabFilter._onPressSelectedType(FilterStore.currentTypeSelected);
    this.setState({
      refresh: false,
      page: 0,
    });
  };

  render() {
    const {FilterStore, componentId, HomeStore} = this.props;
    const {
      refresh,
      isLoadmore,
      isShowSearch,
      dataMessage,
      title,
      message,
      image,
    } = this.state;
    const top = Animated.diffClamp(
      this.state.scrollY,
      0,
      values.topBarHeight,
    ).interpolate({
      inputRange: [0, values.topBarHeight],
      outputRange: [0, -values.topBarHeight],
      extrapolate: 'clamp',
    });
    return (
      <ViewNotification
        style={styles.container}
        ref={ref => (this.notification = ref)}
        data={dataMessage}
        title={title}
        content={message}
        image={image}>
        <ViewAnimationTopBar
          topBar={this.renderTopBar()}
          scrollY={this.state.scrollY}>
          <Animated.ScrollView
            style={{flex: 1, marginTop: 60}}
            scrollEventThrottle={16}
            bounces={false}
            onMomentumScrollEnd={this.onScrollEnd}
            onScrollEndDrag={this.onScrollEndDrag}
            refreshControl={
              <RefreshControl
                refreshing={refresh}
                onRefresh={this.onRefresh}
                tintColor="#00ff00"
              />
            }
            onScroll={Animated.event(
              [
                {
                  nativeEvent: {
                    contentOffset: {y: this.state.scrollY},
                  },
                },
              ],
              {
                useNativeDriver: true,
              },
            )}>
            <View style={{paddingTop: values.topBarHeight}}>
              <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                {HomeStore.listProducts.map((item, index) => {
                  return (
                    <Product
                      item={item}
                      index={index}
                      componentId={componentId}
                    />
                  );
                })}
              </View>
            </View>
          </Animated.ScrollView>
          <Animated.View
            style={[
              styles.containerFilter,
              {
                transform: [
                  {
                    translateY: top,
                  },
                ],
              },
            ]}>
            <TabFilter
              ref={ref => (this.tabFilter = ref)}
              filterStore={FilterStore}
              HomeStore={HomeStore}
            />
          </Animated.View>
        </ViewAnimationTopBar>
        {FilterStore.isShowFilter ? (
          <Filter
            ref={ref => (this.filter = ref)}
            onPressAccept={this._onPressAccept}
          />
        ) : (
          <></>
        )}
        {isLoadmore && (
          <View style={styles.viewActivityIndicator}>
            <ActivityIndicator size="large" color="#00ff00" />
          </View>
        )}
        <Modal visible={isShowSearch}>
          {isShowSearch && (
            <ViewSafeArea>
              <Search
                cancel={this.dissmissSearch}
                HomeStore={HomeStore}
                FilterStore={FilterStore}
              />
            </ViewSafeArea>
          )}
        </Modal>
      </ViewNotification>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.background,
  },
  viewActivityIndicator: {
    position: 'absolute',
    bottom: 10,
    width: '100%',
    alignItems: 'center',
  },
  containerFilter: {
    position: 'absolute',
    top: values.topBarHeight,
    flex: 1,
    width: '100%',
  },
});

export default Home;
