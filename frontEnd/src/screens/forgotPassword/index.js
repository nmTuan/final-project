import React, {Component} from 'react';
import {View, Text, StyleSheet, TextInput, Modal} from 'react-native';
import Color from '../../constant/Color';
import TouchablePreventDouble from '../../common/base/TouchablePreventDouble';
import BtnBase from '../../common/base/BtnBase';
import {color} from '../../constant';
import {ScreenOTP} from '../../common/base';
import ViewSafeArea from '../../common/base/ViewSafeArea';

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showOtp: false,
    };
  }

  onPressBack = () => {
    this.props.onPressBackForgotPass();
  };

  handleOTP = value => {
    console.log('asdf ', value);
  };

  reSendOtp = () => {
    console.log('resend');
  };

  sendOtp = () => {
    console.log('send');
  };

  onPressGetOtp = () => {
    this.setState({
      showOtp: true,
    });
  };

  cancleModal = () => {
    this.setState({
      showOtp: false,
    });
  };

  render() {
    const {showOtp} = this.state;
    return (
      <ViewSafeArea>
        <View style={{paddingHorizontal: 20, paddingTop: 30}}>
          <Text style={{color: Color.agree}}>
            Để lấy lại mật khẩu, bạn cần phải nhập số điện thoại đã đăng ký tài
            khoản đó.
          </Text>
          <View style={styles.containerTextInput}>
            <Text>Số điện thoại</Text>
            <TextInput style={styles.textInput} placeholder="vd: 0962486601" />
          </View>
          <View style={styles.viewBack}>
            <TouchablePreventDouble onPress={this.onPressBack}>
              <Text style={styles.txtBack}>Quay lại</Text>
            </TouchablePreventDouble>
          </View>
          <BtnBase
            styleBtn={styles.btn}
            textBtn="Gửi OTP"
            onPress={this.onPressGetOtp}
          />
          <Modal visible={showOtp} transparent animationType="slide">
            {showOtp && (
              <ScreenOTP
                onChangeText={this.handleOTP}
                reSend={this.reSendOtp}
                send={this.sendOtp}
                cancleModal={this.cancleModal}
              />
            )}
          </Modal>
        </View>
      </ViewSafeArea>
    );
  }
}

const styles = StyleSheet.create({
  containerTextInput: {width: '100%', paddingTop: 10},
  textInput: {
    borderBottomWidth: 1,
    borderColor: 'gray',
    height: 35,
    width: '100%',
  },
  viewBack: {
    width: '100%',
    paddingTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  txtBack: {textDecorationLine: 'underline', color: '#2c829e'},
  btn: {height: 40, width: '100%', marginTop: 20},
});
export default ForgotPassword;
