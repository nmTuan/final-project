import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Modal,
  FlatList,
  SafeAreaView,
} from 'react-native';
import {
  TopBar,
  ModalBase,
  BtnBase,
  AlertCustomize,
  ScreenOTP,
  ScreenSms,
} from '../../common/base';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Navigation} from 'react-native-navigation';
import {color, values} from '../../constant';
import MainBankScreen from '../common/bank/MainBankScreen';
import {inject, observer} from 'mobx-react';
import {notifyMessage} from '../../config/Func';
import {UserService} from '../../services/UserService';
import {AccountService} from '../../services/AccountService';
var timeoutSms = null;

@inject('UserStore', 'OrderStore')
@observer
class BankCustInformation extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showBankInfo: false,
      isShowDelete: false,
      bankSelected: null,
      indexSelected: -1,
      showOtp: false,
      errorOtp: '',
      otpSms: '',
    };
  }

  onPressBack = () => {
    Navigation.dismissModal(this.props.componentId);
  };

  addBank = () => {
    this.setState({
      showBankInfo: true,
    });
  };

  dissMissModal = () => {
    this.setState({
      showBankInfo: false,
    });
  };

  onPressGetOtp = async () => {
    const otpSms = await AccountService.getOtpWithToken('0123456');
    this.setState({
      showOtp: true,
      otpSms,
    });
    timeoutSms = setTimeout(() => {
      this.sms.slideDown();
    }, 1000);
  };

  onPressReSendOtp = async () => {
    this.sms.slideUp();
    clearTimeout(timeoutSms);
    this.onPressGetOtp();
  };

  handleInputOtp = value => {
    this.setState({
      otpInput: value,
    });
  };

  onPressDissmissOtp = () => {
    this.setState({
      showOtp: false,
    });
  };

  onPressNext = async bankInfo => {
    const {UserStore} = this.props;
    const body = {custId: UserStore.userInfo.id, ...bankInfo};
    const res = await UserService.addBank(body);
    if (
      res.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      UserStore.listBank.push(body);
    }
    notifyMessage(res.message);
    this.setState({
      showBankInfo: false,
    });
  };

  onSelecteBank = (item, index) => {
    this.setState({
      bankSelected: item,
      isShowDelete: true,
      indexSelected: index,
    });
  };

  onCancelDelete = () => {
    this.setState({
      isShowDelete: false,
    });
  };

  onAgreeDelete = () => {
    this.onPressGetOtp();
    this.setState({
      isShowDelete: false,
    });
  };

  onPressSendOtp = async () => {
    const {UserStore} = this.props;
    const {otpSms, otpInput, bankSelected, indexSelected} = this.state;
    if (otpSms == otpInput) {
      const res = await UserService.delteteBank(bankSelected.id);
      if (res.errorCode == 200) {
        UserStore.listBank.splice(indexSelected, 1);
      } else {
        alert(res.message);
      }
      this.setState({
        showOtp: false,
      });
    } else {
      this.setState({
        errorOtp: 'Otp không đúng',
      });
    }
  };

  renderItem = ({item, index}) => {
    return (
      <View style={styles.inner}>
        <Text style={styles.text}>{item.bankName}</Text>
        <Text style={styles.text}>{item.creditName}</Text>
        <Text style={styles.text}>{item.creditNumber}</Text>
        <Text style={styles.text}>{`${item.month}/${item.year}`}</Text>
        <TouchableOpacity
          style={styles.btnDeleteBank}
          onPress={() => this.onSelecteBank(item, index)}>
          <Text>Huỷ liên kết</Text>
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    const {UserStore} = this.props;
    const {showBankInfo, isShowDelete, showOtp, errorOtp, otpSms} = this.state;
    return (
      <SafeAreaView viewStyle={styles.container}>
        <View style={styles.content}>
          <View style={styles.viewTopBar}>
            <TopBar
              childLeft={
                <TouchableOpacity onPress={this.onPressBack}>
                  <Icon name="angle-left" size={40} style={{marginLeft: 10}} />
                </TouchableOpacity>
              }
              childCenter={
                <Text style={{fontSize: 16, textAlign: 'center'}}>
                  Thông tin ngân hàng
                </Text>
              }
            />
          </View>
          <View>
            <FlatList
              data={UserStore.listBank}
              keyExtractor={(item, index) => `${index}`}
              renderItem={this.renderItem}
            />
          </View>
          {UserStore.listBank.length < 3 && (
            <View>
              <TouchableOpacity
                style={{...styles.inner, ...styles.btn}}
                onPress={this.addBank}>
                <Icon name="plus-square" size={20} color={color.agree} />
                <Text style={styles.textAdd}>Liên kết thêm ngân hàng</Text>
              </TouchableOpacity>
              <Modal visible={showBankInfo} animationType="slide">
                {showBankInfo && (
                  <MainBankScreen
                    onPressBack={this.dissMissModal}
                    onPressNext={this.onPressNext}
                  />
                )}
              </Modal>
            </View>
          )}
          <AlertCustomize
            visible={isShowDelete}
            content="Bạn có chắc chắn muốn huỷ bỏ liên kết với thẻ này không ?"
            backgroundColorRight="#fff"
            colorRight="#000"
            onPressLeft={this.onCancelDelete}
            onPressRight={this.onAgreeDelete}
          />
          {showOtp ? (
            <View style={styles.containerOtp}>
              <ScreenOTP
                cancleModal={this.onPressDissmissOtp}
                reSend={this.onPressReSendOtp}
                send={this.onPressSendOtp}
                textError={errorOtp}
                onChangeText={this.handleInputOtp}
              />
            </View>
          ) : (
            <></>
          )}
          <ScreenSms
            ref={ref => (this.sms = ref)}
            message={`Mã OTP huỷ liên kết của bạn là: ${otpSms}`}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: '100%',
    backgroundColor: color.lightGrayColor,
  },
  content: {
    width: '100%',
    height: '100%',
  },
  viewTopBar: {
    backgroundColor: '#fff',
  },
  inner: {
    backgroundColor: '#fff',
    padding: 10,
    marginTop: 10,
  },
  text: {
    fontSize: 16,
    color: color.agree,
    marginRight: 95,
  },
  btn: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textAdd: {
    marginLeft: 10,
    color: color.agree,
  },
  viewIconTick: {
    position: 'absolute',
    right: 10,
    top: 5,
  },
  btnDeleteBank: {
    borderWidth: 1,
    borderColor: color.lightGrayColor,
    borderRadius: 6,
    width: 90,
    marginTop: 10,
    paddingVertical: 5,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: 5,
    top: 5,
  },
  containerOtp: {
    position: 'absolute',
    ...StyleSheet.absoluteFillObject,
  },
});

export default BankCustInformation;
