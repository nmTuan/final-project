import React, { useEffect, useRef } from 'react';
import { View, StyleSheet, Text, ScrollView } from "react-native";
import { observer } from 'mobx-react-lite';
import { TouchablePreventDouble } from '../../common/base';
import { color, values } from '../../constant';

const RenderTabBar = ({ UserStore }) => {
    const tabSelected = UserStore.tabBillSelected;
    const { bill } = values;

    const handleSelected = value => {
        const { confirmed, delivering, succes, fail, abort, waiting } = values.bill;
        switch (value) {
            case waiting:
                UserStore.setTabBillSelected(waiting);
                break;
            case confirmed:
                UserStore.setTabBillSelected(confirmed);
                break;
            case delivering:
                UserStore.setTabBillSelected(delivering);
                break;
            case succes:
                UserStore.setTabBillSelected(succes);
                break;
            case fail:
                UserStore.setTabBillSelected(fail);
                break;
            case abort:
                UserStore.setTabBillSelected(abort);
                break;
            default:
                break;
        }
    };

    const renderWaitingBill = (tabSelected, bill) => {
        return (
            <TouchablePreventDouble
                style={
                    tabSelected === bill.waiting
                        ? styles.btnSelected
                        : styles.btnUnSelected
                }
                onPress={() => handleSelected(bill.waiting)}>
                <Text
                    style={
                        tabSelected === bill.waiting
                            ? styles.txtselected
                            : styles.txtUnselected
                    }>
                    Chờ xác nhận
            </Text>
            </TouchablePreventDouble>
        );
    };

    const renderCornfirmedBill = (tabSelected, bill) => {
        return (
            <TouchablePreventDouble
                style={
                    tabSelected === bill.confirmed
                        ? styles.btnSelected
                        : styles.btnUnSelected
                }
                onPress={() => handleSelected(bill.confirmed)}>
                <Text
                    style={
                        tabSelected === bill.confirmed
                            ? styles.txtselected
                            : styles.txtUnselected
                    }>
                    Chờ giao hàng
            </Text>
            </TouchablePreventDouble>
        );
    };

    const renderDeliveringBill = (tabSelected, bill) => {
        return (
            <TouchablePreventDouble
                style={
                    tabSelected === bill.delivering
                        ? styles.btnSelected
                        : styles.btnUnSelected
                }
                onPress={() => handleSelected(bill.delivering)}>
                <Text
                    style={
                        tabSelected === bill.delivering
                            ? styles.txtselected
                            : styles.txtUnselected
                    }>
                    Đang giao hàng
            </Text>
            </TouchablePreventDouble>
        );
    };

    const renderSuccessBill = (tabSelected, bill) => {
        return (
            <TouchablePreventDouble
                style={
                    tabSelected === bill.succes
                        ? styles.btnSelected
                        : styles.btnUnSelected
                }
                onPress={() => handleSelected(bill.succes)}>
                <Text
                    style={
                        tabSelected === bill.succes
                            ? styles.txtselected
                            : styles.txtUnselected
                    }>
                    Giao hàng thành công
            </Text>
            </TouchablePreventDouble>
        );
    };

    const renderFailBill = (tabSelected, bill) => {
        return (
            <TouchablePreventDouble
                style={
                    tabSelected === bill.fail
                        ? styles.btnSelected
                        : styles.btnUnSelected
                }
                onPress={() => handleSelected(bill.fail)}>
                <Text
                    style={
                        tabSelected === bill.fail
                            ? styles.txtselected
                            : styles.txtUnselected
                    }>
                    Giao hàng thất bại
            </Text>
            </TouchablePreventDouble>
        );
    };

    const renderAbortBill = (tabSelected, bill) => {
        return (
            <TouchablePreventDouble
                style={
                    tabSelected === bill.abort ? styles.btnSelected : styles.btnUnSelected
                }
                onPress={() => handleSelected(bill.abort)}>
                <Text
                    style={
                        tabSelected === bill.abort
                            ? styles.txtselected
                            : styles.txtUnselected
                    }>
                    Đã huỷ
            </Text>
            </TouchablePreventDouble>
        );
    };
    return (
        <ScrollView 
            horizontal 
            showsHorizontalScrollIndicator={false}
        >
            <View style={[styles.viewSelectedBar]}>
                {renderWaitingBill(tabSelected, bill)}
                {renderCornfirmedBill(tabSelected, bill)}
                {renderDeliveringBill(tabSelected, bill)}
                {renderSuccessBill(tabSelected, bill)}
                {renderFailBill(tabSelected, bill)}
                {renderAbortBill(tabSelected, bill)}
            </View>
        </ScrollView>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: color.background,
    },
    txtTopBar: { marginLeft: 10, fontSize: 18, fontWeight: 'bold' },
    viewSelectedBar: {
        flexDirection: 'row',
        width: '100%',
        height: 40,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: 'gray',
        borderLeftWidth: 1,
        backgroundColor: '#fff',
    },
    btnUnSelected: {
        flex: 1,
        borderRightWidth: 1,
        padding: 10,
        borderColor: '#000000',
        alignItems: 'center',
        justifyContent: 'center',
    },
    btnSelected: {
        flex: 1,
        borderRightWidth: 1,
        padding: 10,
        borderColor: color.agree,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: color.agree,
    },
    txtUnselected: {
        color: '#000000',
    },
    txtselected: {
        color: '#fff',
    },
});

export default RenderTabBar;
