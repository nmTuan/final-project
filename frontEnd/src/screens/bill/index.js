import React from 'react';
import {View, Text, StyleSheet, Animated, ScrollView} from 'react-native';
import ViewPager from '@react-native-community/viewpager';
import {color, values} from '../../constant';
import {
  ViewSafeArea,
  TopBarChatCart,
  TouchablePreventDouble,
} from '../../common/base';
import {observer, inject} from 'mobx-react';
import BillWaiting from '../billState/waiting/BillWaiting';
import ConfirmedBill from '../billState/confirmed/ConfirmedBill';
import AbortBill from '../billState/abort/AbortBill';
import {pushScreen} from '../../config/NavigationConfig';
import RenderTabBar from './RenderTabBar';
import DeliveringBill from '../billState/delivering';
import SuccessBill from '../billState/successBill';
import FailBill from '../billState/failBIll';

@inject('OrderStore', 'UserStore')
@observer
class Bill extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      scrollY: new Animated.Value(0),
      selected: values.bill.waiting,
    };
  }

  UNSAFE_componentWillUpdate() {
    const {UserStore} = this.props;
    this.viewPager.setPage(UserStore.tabBillSelected);
  }

  handlePageSeleted = e => {
    const {UserStore} = this.props;
    UserStore.setTabBillSelected(e.nativeEvent.position);
  };

  onPressCart = () => {
    pushScreen(this.props.componentId, 'Cart');
  };

  onPressChat = () => {
    pushScreen(this.props.componentId, 'ListRoomCust');
  };

  render() {
    const {OrderStore, componentId, UserStore} = this.props;
    const selected = UserStore.tabBillSelected;
    const {waiting, confirmed, delivering, succes, fail, abort} = values.bill;
    return (
      <ViewSafeArea viewStyle={styles.container}>
        <TopBarChatCart
          childLeft={<Text style={styles.txtTopBar}>Đơn hàng</Text>}
          onPressCart={this.onPressCart}
          onPressChat={this.onPressChat}
        />
        <View>
          <RenderTabBar UserStore={UserStore} />
        </View>
        <ViewPager
          style={{flex: 1}}
          ref={ref => (this.viewPager = ref)}
          initialPage={selected}
          onPageSelected={this.handlePageSeleted}>
          <View>
            {selected <= confirmed && (
              <BillWaiting
                OrderStore={OrderStore}
                UserStore={UserStore}
                componentId={componentId}
              />
            )}
          </View>
          <View>
            {waiting <= selected <= delivering && (
              <ConfirmedBill
                OrderStore={OrderStore}
                UserStore={UserStore}
                componentId={componentId}
              />
            )}
          </View>
          <View>
            {confirmed <= selected <= succes && (
              <DeliveringBill
                OrderStore={OrderStore}
                UserStore={UserStore}
                componentId={componentId}
              />
            )}
          </View>
          <View>
            {delivering <= selected <= fail && (
              <SuccessBill
                OrderStore={OrderStore}
                UserStore={UserStore}
                componentId={componentId}
              />
            )}
          </View>
          <View>
            {succes <= selected <= abort && (
              <FailBill
                OrderStore={OrderStore}
                UserStore={UserStore}
                componentId={componentId}
              />
            )}
          </View>
          <View>
            {selected >= fail && (
              <AbortBill
                OrderStore={OrderStore}
                UserStore={UserStore}
                componentId={componentId}
              />
            )}
          </View>
        </ViewPager>
      </ViewSafeArea>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.background,
  },
  txtTopBar: {marginLeft: 10, fontSize: 18, fontWeight: 'bold'},
  viewSelectedBar: {
    flexDirection: 'row',
    width: '100%',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: 'gray',
    borderLeftWidth: 1,
    backgroundColor: '#fff',
  },
  btnUnSelected: {
    flex: 1,
    borderRightWidth: 1,
    padding: 10,
    borderColor: '#000000',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnSelected: {
    flex: 1,
    borderRightWidth: 1,
    padding: 10,
    borderColor: color.agree,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: color.agree,
  },
  txtUnselected: {
    color: '#000000',
  },
  txtselected: {
    color: '#fff',
  },
});

export default Bill;
