import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Keyboard,
} from 'react-native';
import {TopBar, BtnBase, TxtError} from '../../common/base';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Navigation} from 'react-native-navigation';
import {color} from '../../constant';
import {AccountService} from '../../services/AccountService';
import {notifyMessage} from '../../config/Func';

class UpdatePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPass: '',
      newPass: '',
      rePass: '',
      error: '',
    };
  }

  onPressBack = () => {
    Navigation.dismissModal(this.props.componentId);
  };

  onPressNext = async () => {
    const {userInfo} = this.props;
    const {currentPass, newPass, rePass} = this.state;
    if (currentPass !== '' && newPass !== '' && rePass !== '') {
      if (newPass === rePass) {
        const res = await AccountService.changePass(
          userInfo.id,
          currentPass,
          newPass,
        );
        if (res.message === 'update success') {
          Keyboard.dismiss();
          notifyMessage(res.message);
          Navigation.dismissModal(this.props.componentId);
        } else {
          this.setState({
            error: res.message,
          });
        }
      } else {
        this.setState({
          error: 'Mật khẩu không trùng',
        });
      }
    } else {
      this.setState({
        error: 'Thiếu thông tin',
      });
    }
  };

  handleCurrentPassword = value => {
    this.setState({
      currentPass: value,
    });
  };

  handleNewPassword = value => {
    this.setState({
      newPass: value,
    });
  };

  handleRePassword = value => {
    this.setState({
      rePass: value,
    });
  };

  render() {
    const {error} = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.viewTopBar}>
          <TopBar
            childLeft={
              <TouchableOpacity onPress={this.onPressBack}>
                <Icon name="angle-left" size={40} style={styles.btnBack} />
              </TouchableOpacity>
            }
            childCenter={
              <Text style={styles.title}>{'Cập nhật Mật khẩu'}</Text>
            }
          />
        </View>
        <TextInput
          style={styles.textInput}
          placeholder="Nhập mật khẩu hiện tại"
          secureTextEntry
          onChangeText={this.handleCurrentPassword}
        />
        <TextInput
          style={styles.textInput}
          placeholder="Nhập mật khẩu mới"
          secureTextEntry
          onChangeText={this.handleNewPassword}
        />
        <TextInput
          style={styles.textInputBottom}
          placeholder="Nhập lại mật khẩu"
          secureTextEntry
          onChangeText={this.handleRePassword}
        />
        {error !== '' && <TxtError text={error} />}
        <View style={styles.btnNext}>
          <BtnBase textBtn={'Thay đổi'} onPress={this.onPressNext} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: color.lightGrayColor},
  viewTopBar: {backgroundColor: '#fff'},
  viewContent: {
    backgroundColor: '#fff',
    padding: 10,
    marginTop: 10,
  },
  btnBack: {marginLeft: 10},
  title: {fontSize: 16, textAlign: 'center'},
  textInput: {width: '100%', backgroundColor: '#fff', marginTop: 30},
  textInputBottom: {
    width: '100%',
    backgroundColor: '#fff',
    borderTopWidth: 1,
    borderColor: color.lightGrayColor,
  },
  btnNext: {width: '100%', paddingHorizontal: 30, height: 40, marginTop: 30},
});

export default UpdatePassword;
