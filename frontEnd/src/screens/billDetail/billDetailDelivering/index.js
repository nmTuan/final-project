import React from 'react';
import {Text, ScrollView, TouchableOpacity} from 'react-native';
import BillDetailBase from '../BillDetailBase';
import {Navigation} from 'react-native-navigation';
import {SafeAreaView} from 'react-native';
import {TopBar} from '../../../common/base';
import Icon from 'react-native-vector-icons/FontAwesome';

class BillDetailDelivering extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onPressBack = () => {
    Navigation.dismissModal(this.props.componentId);
  };

  render() {
    const {data} = this.props;
    return (
      <SafeAreaView style={{flex: 1}}>
        <TopBar
          childLeft={
            <TouchableOpacity onPress={this.onPressBack}>
              <Icon name="angle-left" size={40} style={{marginLeft: 10}} />
            </TouchableOpacity>
          }
          childCenter={
            <Text style={{fontSize: 16, textAlign: 'center'}}>
              Đang giao hàng
            </Text>
          }
        />
        <ScrollView style={{flex: 1}}>
          <BillDetailBase data={data} />
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default BillDetailDelivering;
