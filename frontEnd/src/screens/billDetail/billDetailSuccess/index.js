import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TextInput,
  Modal,
  TouchableOpacity,
  Keyboard,
  SafeAreaView,
} from 'react-native';
import BillDetailBase from '../BillDetailBase';
import {Navigation} from 'react-native-navigation';
import BtnBase from '../../../common/base/BtnBase';
import {AirbnbRating} from 'react-native-elements';
import {inject, observer} from 'mobx-react';
import {pushScreen} from '../../../config/NavigationConfig';
import {TopBar} from '../../../common/base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { UserService } from '../../../services/UserService';
import {notifyMessage} from '../../../config/Func';

@inject('OrderStore', 'UserStore')
@observer
class BillDetailSuccess extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowRating: false,
      rating: 0,
      value: '',
    };
  }

  ratingCompleted = rating => {
    this.setState({
      rating,
    });
  };

  onChangText = value => {
    this.setState({
      value,
    });
  };

  onPressSendRating = async () => {
    const {value, rating} = this.state;
    const {data, UserStore} = this.props;
    const body = {
      productBillId: data.id,
      custId: UserStore.userInfo.id,
      content: value,
      vote: rating,
      avatar: UserStore.userInfo.avatar,
      username: UserStore.username,
    };
    if (value !== '') {
      const res = await UserService.sendFeedback(body);
      console.log(res);
      this.setState({
        isShowRating: false,
      });
    } else {
      notifyMessage('Bạn phải nhập nội dung');
    }
  };

  onPressDissmissModal = () => {
    this.setState({
      isShowRating: false,
    });
    Keyboard.dismiss();
  };

  onPressRating = () => {
    this.setState({
      isShowRating: true,
    });
  };

  onPressBack = () => {
    Navigation.dismissModal(this.props.componentId);
  };

  onPressBuy = () => {
    const {componentId, data} = this.props;
    var productBill = [];
    const product = {
      shopId: data.shopId,
      shopName: data.shopName,
      shopAddress: data.shopAddress,
      totalPrice: 0,
      deliveryId: data.deliveryId,
      note: data.note,
      listProduct: data.productBillDTOs,
    };
    productBill.push(product);
    pushScreen(componentId, 'Buy', {data: productBill});
  };

  renderVote() {
    const {value} = this.state;
    return (
      <TouchableOpacity
        style={styles.backgroundRating}
        activeOpacity={1}
        onPress={this.onPressDissmissModal}>
        <View style={styles.containerRating}>
          <Text style={styles.titleRating}>Đánh giá sản phẩm</Text>
          <View style={styles.viewRating}>
            <AirbnbRating
              showRating={false}
              defaultRating={0}
              size={30}
              onFinishRating={this.ratingCompleted}
            />
          </View>
          <TextInput
            style={styles.textInputRating}
            value={value}
            onChangeText={this.onChangText}
            placeholder="Nội dung..."
            multiline
          />
          <View style={styles.viewBtnRating}>
            <BtnBase
              styleBtn={styles.btnRating}
              backgroundColor="gray"
              textBtn="Huỷ bỏ"
              onPress={this.onPressDissmissModal}
            />
            <BtnBase
              styleBtn={styles.btnRating}
              textBtn="Gửi"
              onPress={this.onPressSendRating}
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    const {isShowRating} = this.state;
    const {data} = this.props;
    return (
      <SafeAreaView style={{flex: 1}}>
        <TopBar
          childLeft={
            <TouchableOpacity onPress={this.onPressBack}>
              <Icon name="angle-left" size={40} style={{marginLeft: 10}} />
            </TouchableOpacity>
          }
          childCenter={
            <Text style={{fontSize: 16, textAlign: 'center'}}>
              Giao hàng thành công
            </Text>
          }
        />
        <ScrollView style={{flex: 1}}>
          <BillDetailBase data={data} />
          <View style={styles.containerBtn}>
            <BtnBase
              styleBtn={styles.btn}
              textBtn="Đánh giá"
              onPress={this.onPressRating}
            />
            <BtnBase
              styleBtn={styles.btn}
              textBtn="Mua lại"
              backgroundColor="red"
              onPress={this.onPressBuy}
            />
          </View>
          <Modal visible={isShowRating} transparent animationType="slide">
            {this.renderVote()}
          </Modal>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  containerBtn: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
    alignItems: 'center',
  },
  btn: {height: 40, width: '45%'},
  backgroundRating: {
    width: '100%',
    height: '100%',
    backgroundColor: '#00000050',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerRating: {
    width: '80%',
    backgroundColor: '#fff',
    borderRadius: 10,
    padding: 10,
    alignItems: 'center',
  },
  titleRating: {fontSize: 18},
  viewRating: {paddingTop: 10},
  textInputRating: {
    height: 80,
    width: '100%',
    borderWidth: 1,
    borderColor: 'gray',
    textAlignVertical: 'top',
    borderRadius: 6,
    marginTop: 10,
  },
  viewBtnRating: {
    width: '100%',
    flexDirection: 'row',
    paddingTop: 20,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  btnRating: {width: '45%', height: 30},
});
export default BillDetailSuccess;
