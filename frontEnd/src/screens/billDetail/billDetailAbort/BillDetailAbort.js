import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import {Navigation} from 'react-native-navigation';
import BillDetailBase from '../BillDetailBase';
import {BtnBase, TopBar} from '../../../common/base';
import {pushScreen} from '../../../config/NavigationConfig';
import Icon from 'react-native-vector-icons/FontAwesome';
import { observer } from 'mobx-react';

@observer
class BillDetailAbort extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onPressBack = () => {
    Navigation.dismissModal(this.props.componentId);
  };

  onPressBuy = () => {
    const {componentId, data, UserStore} = this.props;
    UserStore.productInBill = [];
    UserStore.setProductBill(data);
    const passProps = {
      data: UserStore.productInBill,
      reBuy: true,
    };
    pushScreen(componentId, 'Buy', passProps);
  };

  render() {
    const {data} = this.props;
    return (
      <SafeAreaView style={{flex: 1}}>
        <TopBar
          childLeft={
            <TouchableOpacity onPress={this.onPressBack}>
              <Icon name="angle-left" size={40} style={{marginLeft: 10}} />
            </TouchableOpacity>
          }
          childCenter={
            <Text style={{fontSize: 16, textAlign: 'center'}}>Đã Huỷ Đơn</Text>
          }
        />
        <ScrollView style={{flex: 1}}>
          <BillDetailBase data={data} />
          <View style={styles.containerBtn}>
            <BtnBase
              styleBtn={styles.btn}
              textBtn="Mua lại"
              onPress={this.onPressBuy}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  containerBtn: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
    alignItems: 'center',
  },
  btn: {height: 40, width: '100%'},
});

export default BillDetailAbort;
