import React from 'react';
import {View} from 'react-native';
import ListRoom from '../../common/chat/listRoom';
import {inject, observer} from 'mobx-react';
import {UserService} from '../../../services/UserService';
import {values} from '../../../constant';
import { showModal } from '../../../config/NavigationConfig';
import { shopService } from '../../../services/ShopService';

@inject('UserStore', 'UserChatStore')
@observer
class ListRoomCust extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }

  async UNSAFE_componentWillMount() {
    const {item, socketId, shopId} = this.props;
    if (item || socketId || shopId) {
      var shopInfo = null;
      if (shopId) {
        shopInfo = await shopService.getShopInfo(shopId);
      }
      const passProps = {
        item,
        socketId,
        shopInfo,
      };
      showModal('RoomDetailCust', passProps);
    }
  }

  async componentDidMount() {
    const {UserStore, UserChatStore} = this.props;
    var arr = [];
    const data = await UserService.getListRoomChat(UserStore.userInfo.id);
    if (
      data.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      arr = data.result.roomChats;
    }
    UserChatStore.setListRoomChat(arr);
  }

  onPressItem = async (item, index) => {
    const {UserStore} = this.props;
    const shopInfo = await shopService.getShopInfo(item.shopId);
    if (shopInfo) {
      await UserService.updateStatusRoomChat(item.id, 1);
      UserStore.quantityChat = UserStore.quantityChat - 1;
      const passProps = {
        item,
        index,
        shopInfo,
      };
      showModal('RoomDetailCust', passProps);
    }
  };

  onRefresh = () => {
    this.setState({
      refreshing: true,
    });
    setTimeout(() => {
      this.setState({
        refreshing: false,
      });
    }, 1000);
  };

  onLoadMore = () => {
    // console.log('load more');
  };

  render() {
    const {UserChatStore} = this.props;
    const {refreshing} = this.state;
    return (
      <View style={{flex: 1}}>
        <ListRoom
          data={UserChatStore.listRoomChat}
          componentId={this.props.componentId}
          onPressItem={this.onPressItem}
          refreshing={refreshing}
          onLoadMore={this.onLoadMore}
          onRefeshing={this.onRefresh}
          keyStatus="custStatus"
        />
      </View>
    );
  }
}

export default ListRoomCust;
