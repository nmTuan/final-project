/* eslint-disable react/no-did-mount-set-state */
import React, {Component} from 'react';
import {View} from 'react-native';
import RoomDetail from '../../common/chat/RoomDetail';
import {values} from '../../../constant';
import {getSocket} from '../../../config/Socket';
import ChatService from '../../../services/ChatService';
import {inject, observer} from 'mobx-react';
import {UserService} from '../../../services/UserService';
import {OptionBase} from '../../../common/base';
import {
  resizeImage,
  openLibSingleImage,
  openCamera,
} from '../../../config/ImagePicker';

@inject('UserStore', 'UserChatStore')
@observer
class RoomDetailCust extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: '',
      data: [],
      page: 0,
      showImagePicker: false,
    };
  }

  async UNSAFE_componentWillMount() {
    const {item} = this.props;
    if (item) {
      const data = await ChatService.getListMessByRoom(item.id);
      if (
        data.message.toLocaleLowerCase() ===
        values.messageSuccess.toLocaleLowerCase()
      ) {
        this.setState({
          data: data.result.result,
        });
      }
    }
  }

  componentDidMount() {
    getSocket().on('receiveMess', result => {
      const newData = this.state.data;
      newData.unshift(result);
      this.setState({
        data: newData,
      });
    });
  }

  onLoadMore = async () => {
    const {page, data} = this.state;
    const {item} = this.props;
    const pageMess = page + 1;
    const newMess = await ChatService.getListMessByRoom(item.id, pageMess, 10);
    if (newMess.errorCode === 200) {
      if (newMess.result.result.length > 0) {
        this.setState({
          data: [...data, ...newMess.result.result],
          page: pageMess,
        });
      }
    }
  };

  onPressSend = () => {
    const {shopInfo, UserStore} = this.props;
    const {content} = this.state;
    const body = {
      custId: UserStore.userInfo.id,
      shopId: shopInfo.id,
      content: content,
      receiver: shopInfo.shopName,
      receiverava: shopInfo.avatar,
      sender: UserStore.username,
      senderava: UserStore.userInfo.avatar,
      socketReceive: shopInfo.socketId,
    };
    getSocket().emit('sendMess', body);
    this.state.data.unshift(body);
    this.setState({
      content: '',
      data: this.state.data,
    });
  };

  onPressTakePic = async () => {
    const {item} = this.props;
    const result = await openCamera();
    this.setState({
      showImagePicker: false,
    });
    if (result) {
      resizeImage(
        result.image,
        result.width / 5,
        result.height / 5,
        'JPEG',
        100,
      )
        .then(async uri => {
          const data = await ChatService.uploadSingleFile(uri, item.id);
          if (data) {
            this.setState({
              content: data,
            });
            this.onPressSend();
          }
        })
        .catch(e => console.log(e));
    }
  };

  onPressLibs = async () => {
    const {item} = this.props;
    const result = await openLibSingleImage();
    this.setState({
      showImagePicker: false,
    });
    if (result) {
      resizeImage(
        result.image,
        result.width / 5,
        result.height / 5,
        'JPEG',
        100,
      )
        .then(async uri => {
          console.log(uri);
          const data = await ChatService.uploadSingleFile(uri, item.id);
          if (data) {
            this.setState({
              content: data,
            });
            this.onPressSend();
          }
        })
        .catch(e => console.log(e));
    }

    // results.map(item => {
    //   resizeImage(item.image, item.width, item.height, 'JPEG', 50)
    //     .then(uri => {
    //       arrImage.push(uri);
    //       console.log(arrImage);
    //     })
    //     .catch(err => {
    //       console.log(err);
    //     });
    // });
  };

  onPressCamera = () => {
    this.setState({
      showImagePicker: true,
    });
  };

  onPressCancel = () => {
    this.setState({
      showImagePicker: false,
    });
  };

  onPressBack = async () => {
    const {UserStore, UserChatStore} = this.props;
    var arr = [];
    const data = await UserService.getListRoomChat(UserStore.userInfo.id);
    if (
      data.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      arr = data.result.roomChats;
    }
    UserChatStore.setListRoomChat(arr);
  };

  render() {
    const {item, index, UserStore} = this.props;
    const {content, data, showImagePicker} = this.state;
    const isImage = content.search('.jpg') > 0 ? true : false;
    return (
      <View style={{flex: 1}}>
        <RoomDetail
          item={item}
          index={index}
          componentId={this.props.componentId}
          content={isImage ? '' : content}
          data={data}
          onChangeText={value => this.setState({content: value})}
          onPressSend={this.onPressSend}
          username={UserStore.username}
          onPressBack={this.onPressBack}
          onPressCamera={this.onPressCamera}
          onLoadMore={this.onLoadMore}
        />
        {showImagePicker && (
          <OptionBase
            title="Chọn ảnh"
            textBtn1="Chụp ảnh"
            textBtn2="Chọn ảnh từ thư viện"
            onPress1={this.onPressTakePic}
            onPress2={this.onPressLibs}
            onPressCancel={this.onPressCancel}
          />
        )}
      </View>
    );
  }
}

export default RoomDetailCust;
