import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {PickerScrollView, PopupPicker} from '../../common/base';
import {formatCurrency} from '../../constant/Format';
import {observer} from 'mobx-react';
import {color, values} from '../../constant';
var deliveryPrice = 0;
@observer
class ItemProduct extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isShowModal: false,
      deliverySelected: 1,
      currentDelivery: 1,
      totlePriceProduct: 0,
      totalPrice: 0,
      note: '',
    };
  }

  componentDidMount() {
    const {item} = this.props;
    item.deliveryId = this.state.deliverySelected;
    this.caculatePrice(this.state.deliverySelected);
  }

  UNSAFE_componentWillMount() {
    const {item} = this.props;
    if (item.deliveryId >= 0) {
      this.setState({
        deliverySelected: item.deliveryId,
      });
    }
    var total = 0;
    item.listProduct.map((product, index) => {
      if (product.isRent) {
        total += product.rentPrice * product.quantity * product.due;
      } else {
        total += product.price * product.quantity;
      }
      if (product.priceDelivery > deliveryPrice) {
        deliveryPrice = product.priceDelivery;
      }
    });
    total = total + deliveryPrice;
    this.setState({
      totlePriceProduct: total,
    });
  }

  handleValueChange = index => {
    this.setState({
      deliverySelected: index,
    });
  };

  onPressDelivery = () => {
    this.setState({
      isShowModal: true,
      deliverySelected: this.state.currentDelivery,
    });
  };

  onPressCancel = () => {
    this.setState({
      isShowModal: false,
    });
  };

  _handleNote = value => {
    const {item} = this.props;
    item.note = value;
  };

  // _onBlurNote = () => {
  //   const {item} = this.props;
  //   item.note = this.state.note;
  // };

  onPressSelectDelivery = () => {
    const {item} = this.props;
    this.setState({
      currentDelivery: this.state.deliverySelected,
      isShowModal: false,
    });
    this.caculatePrice(this.state.deliverySelected);
    item.deliveryId = this.state.deliverySelected;
  };

  caculatePrice = deliverySelected => {
    const {item} = this.props;
    const total = this.state.totlePriceProduct; //dataDelivery[deliverySelected].price
    this.setState({
      totalPrice: total,
    });
    item.totalPrice = total;
  };

  _renderListProduct = (itemProduct, indexProduct, index) => {
    return (
      <View style={styles.containerListProduct}>
        <Image
          source={{uri: `${values.domain}${itemProduct.productImg}`}}
          style={styles.productImg}
        />
        <View style={styles.containerInfoProduct}>
          <View style={styles.viewProductName}>
            <Text style={styles.productName} numberOfLines={1}>
              {itemProduct.productName}
            </Text>
            <Text>{`x${itemProduct.quantity}`}</Text>
          </View>
          <View style={styles.viewPrice}>
            {itemProduct.oldPrice > 0 && (
              <Text style={styles.oldPrice}>
                {formatCurrency(itemProduct.oldPrice)}
              </Text>
            )}
            <Text>{formatCurrency(itemProduct.price)}</Text>
          </View>
          {itemProduct.isRent && (
            <View>
              <View>
                <Text>{`Giá thuê: ${formatCurrency(
                  itemProduct.rentPrice,
                )}/ngày`}</Text>
              </View>
              <View>
                <Text>{`Thời gian: ${itemProduct.due} ngày`}</Text>
              </View>
            </View>
          )}
        </View>
        {itemProduct.isRent && (
          <View style={styles.viewRent}>
            <Text style={styles.textRent}>Thuê</Text>
          </View>
        )}
      </View>
    );
  };

  renderModal() {
    const {deliveryName, item} = this.props;
    const {isShowModal, deliverySelected} = this.state;
    return (
      <PopupPicker visible={isShowModal} onPressCancel={this.onPressCancel}>
        <View>
          <View style={styles.containerTitleModal}>
            <View style={styles.viewTitleModal}>
              <TouchableOpacity onPress={this.onPressCancel}>
                <Text style={[styles.txtTitleModal, {color: 'gray'}]}>
                  Huỷ bỏ
                </Text>
              </TouchableOpacity>
              <Text style={styles.txtTitleModal}>Phương thức vận chuyển</Text>
              <TouchableOpacity onPress={this.onPressSelectDelivery}>
                <Text style={[styles.txtTitleModal, {color: 'green'}]}>
                  Chọn
                </Text>
              </TouchableOpacity>
            </View>
            <View style={styles.line} />
          </View>
          <PickerScrollView
            ref={ref => (this.pickerScroll = ref)}
            dataSource={deliveryName}
            selectedIndex={
              item.deliveryId >= 0 ? item.deliveryId : deliverySelected
            }
            itemHeight={105 / 3}
            wrapperHeight={105}
            getSelected={this.handleValueChange}
          />
        </View>
      </PopupPicker>
    );
  }

  render() {
    const {item, index} = this.props;
    const {totalPrice, note} = this.state;
    return (
      <View style={styles.containerInfor}>
        <Text style={{fontSize: 16, marginBottom: 5}}>{item.shopName}</Text>
        <View style={styles.containerTitleInfor}>
          <Icon name="map-marker" size={13} />
          <Text style={{fontSize: 13, marginLeft: 10}}>{item.shopAddress}</Text>
        </View>
        {item.listProduct.map((itemProduct, indexProduct) =>
          this._renderListProduct(itemProduct, indexProduct, index),
        )}
        <View>
          <View style={styles.containerTitleDelivery}>
            <Text>Phí vận chuyển</Text>
            <Text>{`${formatCurrency(deliveryPrice)} VNĐ`}</Text>
            {/* <TouchableOpacity onPress={this.onPressDelivery}>
              <Icon name="edit" size={18} />
            </TouchableOpacity> */}
          </View>
          {/* {dataDelivery[item.deliveryId] && (
            <View style={styles.containerTitleDelivery}>
              <Text>{dataDelivery[item.deliveryId].name}</Text>
              <Text>{`${formatCurrency(
                dataDelivery[item.deliveryId].price,
              )} VNĐ`}</Text>
            </View>
          )} */}
          <View style={styles.line} />
          <View style={styles.containerTitleDelivery}>
            <Text style={styles.txtDelevery}>Thành tiền</Text>
            <Text style={styles.txtDelevery}>
              {`${formatCurrency(totalPrice)} VNĐ`}
            </Text>
          </View>
        </View>
        <TextInput
          style={styles.inputNote}
          placeholder="Để lại lưu ý..."
          value={item.note}
          onChangeText={this._handleNote}
        />
        {this.renderModal()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerInfor: {
    width: '100%',
    padding: 10,
    backgroundColor: '#fff',
    marginTop: 10,
  },
  containerTitleInfor: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  containerListProduct: {
    width: '100%',
    borderWidth: 1,
    borderRadius: 4,
    borderColor: 'gray',
    paddingHorizontal: 10,
    paddingVertical: 5,
    flexDirection: 'row',
    marginTop: 10,
  },
  productImg: {width: 100, height: 80, resizeMode: 'stretch'},
  containerInfoProduct: {flex: 1, marginHorizontal: 10},
  viewProductName: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  productName: {fontSize: 17},
  viewPrice: {flexDirection: 'row', paddingVertical: 6},
  oldPrice: {
    fontSize: 14,
    textDecorationLine: 'line-through',
    color: 'gray',
    marginRight: 10,
  },
  containerTitleDelivery: {
    width: '100%',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingTop: 10,
  },
  txtDelevery: {color: 'green'},
  inputNote: {
    width: '100%',
    height: 40,
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
  },
  line: {
    width: '100%',
    height: 1,
    backgroundColor: 'gray',
    marginVertical: 10,
  },
  backgroundModal: {
    flex: 1,
    backgroundColor: '#00000040',
  },
  containerModal: {
    width: '100%',
    backgroundColor: '#fff',
    position: 'absolute',
    bottom: 0,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
  },
  containerTitleModal: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
  },
  viewTitleModal: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 5,
  },
  txtTitleModal: {fontSize: 16},
  viewRent: {
    paddingHorizontal: 10,
    paddingVertical: 2,
    backgroundColor: color.agree,
    position: 'absolute',
    top: 13,
    borderRadius: 3,
  },
  textRent: {color: '#fff'},
});

export default ItemProduct;
