import React, {Component} from 'react';
import {View, Text, StyleSheet, TextInput} from 'react-native';
import BtnBase from '../../common/base/BtnBase';
import {formatCurrency} from '../../constant/Format';
import {observer} from 'mobx-react';
import InformationBill from '../InformationBill';

@observer
class Delivery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otherPlace: '',
      totalPrice: 0,
    };
  }

  _onChangeText = value => {
    this.setState({
      otherPlace: value,
    });
  };

  onPressNext = () => {
    const {OrderStore, viewPager, UserStore} = this.props;
    if (this.state.otherPlace !== '') {
      OrderStore.bill.orderPlace = this.state.otherPlace;
    } else {
      OrderStore.bill.orderPlace = UserStore.userInfo.address;
    }
    OrderStore.stepIndicatorRef._onNextStep();
    OrderStore.pageSelected = 1;
    viewPager.setPage(1);
  };

  caculateTotalBill = () => {
    const {OrderStore} = this.props;
    var total = 0;
    OrderStore.bill.productBillDTOs.map(item => {
      total += item.totalPrice;
    });
    OrderStore.setBill(total);
    return total;
  };

  render() {
    const {OrderStore, UserStore} = this.props;
    const {otherPlace} = this.state;
    return (
      <View style={{flex: 1}}>
        <InformationBill
          OrderStore={OrderStore}
          UserStore={UserStore}
          otherPlace={otherPlace}
          onChangeText={this._onChangeText}
        />
        <View style={styles.containerBtnNext}>
          <Text style={styles.txtTotalPrice}>
            {`Tổng thanh toán: ${formatCurrency(this.caculateTotalBill())} VNĐ`}
          </Text>
          <BtnBase
            textBtn="Tiếp theo"
            styleBtn={styles.btnNext}
            onPress={this.onPressNext}
          />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  txtInfor: {color: 'green', fontSize: 15, marginTop: 5, flexWrap: 'wrap'},
  textInputAddress: {
    width: '100%',
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    height: 40,
    marginTop: 10,
  },
  txtAttention: {fontSize: 13, color: 'gray'},
  containerInfor: {
    width: '100%',
    padding: 10,
    backgroundColor: '#fff',
    marginTop: 10,
  },
  containerTitleInfor: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  line: {
    width: '100%',
    height: 1,
    backgroundColor: 'gray',
    marginVertical: 10,
  },
  titleAddress: {fontSize: 16, marginLeft: 10},
  containerBtnNext: {
    marginTop: 20,
    paddingVertical: 10,
    backgroundColor: '#fff',
    paddingHorizontal: 20,
  },
  txtTotalPrice: {textAlign: 'right'},
  btnNext: {height: 40, marginTop: 10},
});

export default Delivery;
