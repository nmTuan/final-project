import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Notification, BtnBase} from '../../common/base';
import Icon from 'react-native-vector-icons/FontAwesome';
import {color, values} from '../../constant';
import {MainApp} from '../../navigation/Navigation';
import {UserService} from '../../services/UserService';

class BuyNotification extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  async componentWillUnmount() {
    const {UserStore} = this.props;
    var bills = [];
    const billWaiting = await UserService.getBill(
      UserStore.userInfo.id,
      values.bill.waiting,
    );
    if (
      billWaiting.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      bills = [...billWaiting.result.result];
    }
    UserStore.setBillWaiting(bills);
    this.props.OrderStore.resetBill;
    UserStore.setTabBillSelected(values.bill.waiting);
  }

  onPressClose = () => {
    this.props.onPressClose();
  };

  onPressBill = () => {
    MainApp(1);
  };

  renderIcon() {
    return (
      <View style={styles.containerIcon}>
        <Icon name="check" size={50} color="green" />
      </View>
    );
  }

  renderContent() {
    return (
      <View style={styles.containerContent}>
        <Text style={styles.title}>Đặt hàng thành công</Text>
        <Text style={styles.content}>
          Đơn hàng của bạn đang chờ cửa hàng xác nhận
        </Text>
      </View>
    );
  }

  renderButton() {
    return (
      <View style={styles.containerBtn}>
        <BtnBase
          textBtn="Đóng"
          backgroundColor="gray"
          styleBtn={styles.btn}
          onPress={this.onPressClose}
        />
        <BtnBase
          textBtn="Xem đơn hàng"
          styleBtn={styles.btn}
          onPress={this.onPressBill}
        />
      </View>
    );
  }

  render() {
    return (
      <View>
        <Notification
          viewTop={this.renderIcon()}
          ViewMiddle={this.renderContent()}
          ViewBottom={this.renderButton()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerIcon: {
    width: 80,
    height: 80,
    borderRadius: 40,
    borderWidth: 1,
    borderColor: color.agree,
    backgroundColor: '#14c45170',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerContent: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  title: {color: 'green', fontSize: 20, fontWeight: 'bold', marginBottom: 10},
  content: {fontSize: 16},
  containerBtn: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 40,
  },
  btn: {height: 40, width: '40%'},
});

export default BuyNotification;
