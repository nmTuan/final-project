import React, {Component} from 'react';
import {View, Text, FlatList, TouchableOpacity, StyleSheet} from 'react-native';
import {ViewSafeArea, TopBarTitle, BtnBase, ModalBase} from '../../common/base';
import {color} from '../../constant';
import {inject, observer} from 'mobx-react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Navigation} from 'react-native-navigation';
import {pushScreen} from '../../config/NavigationConfig';
import RenderListProduct from './RenderListProduct';
import { UserService } from '../../services/UserService';

@inject('OrderStore')
@observer
class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productName: '',
      isShowModal: false,
      contentModal: '',
      indexProduct: -1,
      indexShop: -1,
      itemProduct: null,
    };
  }

  onPressBack = () => {
    Navigation.pop(this.props.componentId);
  };

  _onPressCancel = () => {
    this.setState({
      isShowModal: false,
    });
  };

  _onPressTrash = (itemProduct, indexProduct, index) => {
    const contentModal = `Bạn có muốn xoá sản phẩm ${
      itemProduct.productName
    } ra khỏi giỏ hàng`;
    this.setState({
      isShowModal: true,
      contentModal,
      indexProduct,
      indexShop: index,
      itemProduct,
    });
  };

  _onPressTrashAll = () => {
    const contentModal = 'Bạn có muốn xoá toàn bộ sản phẩm có trong giỏ hàng';
    this.setState({
      isShowModal: true,
      contentModal,
    });
  };

  onPressCancel = () => {
    this.setState({
      isShowModal: false,
    });
  };

  _onPressBuy = () => {
    const passProps = {
      data: this.props.OrderStore.listProductInCart,
    };
    pushScreen(this.props.componentId, 'Buy', passProps);
  };

  _handleRemoveProduct = async () => {
    const {indexProduct, indexShop, itemProduct} = this.state;
    const {OrderStore} = this.props;
    const ids = [];
    if (indexProduct >= 0 && indexShop >= 0) {
      if (OrderStore.listProductInCart[indexShop].listProduct.length > 1) {
        OrderStore.listProductInCart[indexShop].listProduct.splice(
          indexProduct,
          1,
        );
      } else {
        OrderStore.listProductInCart.splice(indexShop, 1);
      }
      ids.push(itemProduct.id);
    } else {
      OrderStore.listProductInCart.map(itemShop => {
        itemShop.listProduct.map(item => {
          ids.push(item.id);
        });
      });
      OrderStore.listProductInCart = [];
    }
    await UserService.removeProductFromCart(ids);
    this.setState({
      isShowModal: false,
      indexProduct: -1,
      indexShop: -1,
      itemProduct: null,
    });
  };

  _renderItem = ({item, index}) => {
    const lengthData = this.props.OrderStore.listProductInCart.length - 1;
    return (
      <View
        style={[
          styles.containerItem,
          {paddingBottom: index === lengthData ? 75 : 0},
        ]}>
        <View style={styles.viewShopInfo}>
          <Text style={styles.shopName} numberOfLines={2}>
            {item.shopName}
          </Text>
          <View style={styles.viewShopAddress}>
            <Icon name="map-marker" size={15} />
            <Text style={styles.shopAddress}>{item.shopAddress}</Text>
          </View>
        </View>
        {item.listProduct.map((itemProduct, indexProduct) => (
          <RenderListProduct
            itemProduct={itemProduct}
            indexProduct={indexProduct}
            index={index}
            _onPressTrash={this._onPressTrash}
          />
        ))}
      </View>
    );
  };

  _renderChildRight = () => {
    return (
      <TouchableOpacity style={styles.btnTrash} onPress={this._onPressTrashAll}>
        <Icon name="trash" size={30} color="#000000" color={color.agree} />
      </TouchableOpacity>
    );
  };

  render() {
    const {OrderStore} = this.props;
    const {isShowModal, contentModal} = this.state;
    // console.log(" list product - cart", JSON.stringify(OrderStore.listProductInCart));
    return (
      <ViewSafeArea viewStyle={{backgroundColor: color.lightGrayColor}}>
        <TopBarTitle
          title="Giỏ hàng"
          onPressBack={this.onPressBack}
          childRight={this._renderChildRight()}
        />
        {OrderStore.listProductInCart.length > 0 ? (
          <FlatList
            style={{flex: 1}}
            data={OrderStore.listProductInCart}
            keyExtractor={(item, index) => `${index}`}
            renderItem={this._renderItem}
          />
        ) : (
          <Text style={styles.txtNoneProduct}>Không có sản phẩm</Text>
        )}
        <BtnBase
          textBtn="Mua sản phẩm"
          styleBtn={styles.btnBuy}
          onPress={this._onPressBuy}
        />
        <ModalBase
          visible={isShowModal}
          styleContainerModal={{width: '70%', padding: 10}}
          styleBackground={{alignItems: 'center', justifyContent: 'center'}}
          onPressCancel={this.onPressCancel}>
          <Text style={styles.titleModal}>Xoá sản phẩm khỏi giỏ hàng</Text>
          <Text style={styles.contentModal}>{contentModal}</Text>
          <View style={styles.containerBtnModal}>
            <BtnBase
              textBtn="Huỷ bỏ"
              backgroundColor={color.cancel}
              textColor="#000"
              styleBtn={styles.btnModal}
              onPress={this._onPressCancel}
            />
            <BtnBase
              textBtn="Đồng ý"
              styleBtn={styles.btnModal}
              onPress={this._handleRemoveProduct}
            />
          </View>
        </ModalBase>
      </ViewSafeArea>
    );
  }
}

const styles = StyleSheet.create({
  containerListProduct: {
    width: '100%',
    borderWidth: 1,
    borderColor: 'gray',
    paddingHorizontal: 10,
    paddingVertical: 5,
    flexDirection: 'row',
    marginTop: 10,
  },
  productImg: {width: 100, height: 80, resizeMode: 'stretch'},
  containerInfoProduct: {flex: 1, marginHorizontal: 10},
  viewProductName: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  productName: {fontSize: 17},
  viewPrice: {flexDirection: 'row'},
  oldPrice: {
    fontSize: 14,
    textDecorationLine: 'line-through',
    color: 'gray',
    marginRight: 10,
  },
  viewVote: {paddingTop: 10, alignItems: 'flex-start'},
  containerItem: {
    flex: 1,
    backgroundColor: '#fff',
    width: '100%',
    marginVertical: 5,
    paddingBottom: 10,
  },
  viewShopInfo: {padding: 10},
  shopName: {fontSize: 20, color: '#000'},
  viewShopAddress: {flexDirection: 'row', alignItems: 'center', paddingTop: 5},
  shopAddress: {marginLeft: 5, color: '#000'},
  titleModal: {fontSize: 14, color: 'red'},
  contentModal: {fontSize: 14, textAlign: 'center', marginVertical: 10},
  containerBtnModal: {
    width: '100%',
    height: 30,
    flexDirection: 'row',
    marginVertical: 10,
    justifyContent: 'space-between',
  },
  btnModal: {width: '45%'},
  btnTrash: {
    width: '100%',
    height: '100%',
    paddingRight: 10,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  txtNoneProduct: {textAlign: 'center', marginTop: 10},
  btnBuy: {
    width: '100%',
    height: 40,
    paddingHorizontal: 20,
    position: 'absolute',
    bottom: 20,
  },
  viewRent: {
    paddingHorizontal: 10,
    paddingVertical: 2,
    backgroundColor: color.agree,
    position: 'absolute',
    top: 13,
    borderRadius: 3,
  },
  textRent: {color: '#fff'},
});

export default Cart;
