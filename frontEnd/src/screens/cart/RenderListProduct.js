import React from 'react';
import {View, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {Rating} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {formatCurrency} from '../../constant/Format';
import {values} from '../../constant';

class RenderListProduct extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {itemProduct, indexProduct, index} = this.props;
    return (
      <View style={styles.containerListProduct}>
        <Image
          source={{uri: `${values.domain}${itemProduct.productImg}`}}
          style={styles.productImg}
        />
        <View style={styles.containerInfoProduct}>
          <View style={styles.viewProductName}>
            <Text style={styles.productName} numberOfLines={1}>
              {`${itemProduct.productName}`}
            </Text>
            <TouchableOpacity
              onPress={() =>
                this.props._onPressTrash(itemProduct, indexProduct, index)
              }>
              <Icon name="trash" size={20} />
            </TouchableOpacity>
          </View>
          <View style={styles.viewPrice}>
            {itemProduct.oldPrice > 0 && (
              <Text style={styles.oldPrice}>
                {formatCurrency(itemProduct.oldPrice)}
              </Text>
            )}

            <Text>{`${formatCurrency(itemProduct.price)}   x${
              itemProduct.quantity
            }`}</Text>
          </View>
          <View style={styles.viewVote}>
            <Rating startingValue={itemProduct.vote} readonly imageSize={15} />
          </View>
          {itemProduct.rent && (
            <View>
              <Text>{`Thời gian: ${itemProduct.due} ngày`}</Text>
            </View>
          )}
        </View>
        {itemProduct.rent && (
          <View style={styles.viewRent}>
            <Text style={styles.textRent}>Thuê</Text>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerListProduct: {
    width: '100%',
    borderWidth: 1,
    borderColor: 'gray',
    paddingHorizontal: 10,
    paddingVertical: 5,
    flexDirection: 'row',
    marginTop: 10,
  },
  productImg: {width: 100, height: 80, resizeMode: 'stretch'},
  containerInfoProduct: {flex: 1, marginHorizontal: 10},
  viewProductName: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  productName: {fontSize: 17},
  viewPrice: {flexDirection: 'row'},
  oldPrice: {
    fontSize: 14,
    textDecorationLine: 'line-through',
    color: 'gray',
    marginRight: 10,
  },
  viewVote: {paddingTop: 10, alignItems: 'flex-start'},
});

export default RenderListProduct;
