import React, {Component} from 'react';
import {View, Text, StyleSheet, Animated, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';

class DescribeProduce extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animatedDescribe: new Animated.Value(0),
      heightDescribe: 0,
      isShowMore: false,
    };
  }
  onLayout = e => {
    this.setState({
      heightDescribe: e.nativeEvent.layout.height,
    });
  };

  _onPressMore = () => {
    if (this.state.isShowMore) {
      this.setState({
        isShowMore: false,
      });
      Animated.timing(this.state.animatedDescribe, {
        toValue: 0,
        duration: 500,
        useNativeDriver: false,
      }).start();
    } else {
      this.setState({
        isShowMore: true,
      });
      Animated.timing(this.state.animatedDescribe, {
        toValue: 1,
        duration: 500,
        useNativeDriver: false,
      }).start();
    }
  };

  render() {
    const {isShowMore, heightDescribe} = this.state;
    const {data} = this.props;
    const height = this.state.animatedDescribe.interpolate({
      inputRange: [0, 1],
      outputRange: [90, this.state.heightDescribe + 50],
    });
    const showmore = heightDescribe >= 50 ? true : false;
    return (
      <View style={[styles.viewChild, {paddingBottom: 10}]}>
        <Text style={styles.txtNameProduct}>Chi tiết sản phẩm</Text>
        <View>
          <View style={styles.containerKeyValue}>
            <Text style={styles.keyStyle}>Thương hiệu</Text>
            <Text style={styles.valueStyle} numberOfLines={1}>
              {data && data.brand}
            </Text>
          </View>
          <View style={styles.containerKeyValue}>
            <Text style={styles.keyStyle}>Thể loại</Text>
            <Text style={styles.valueStyle} numberOfLines={1}>
              {data && data.category}
            </Text>
          </View>
          <View style={styles.containerKeyValue}>
            <Text style={styles.keyStyle}>Độ tuổi phù hợp</Text>
            <Text style={styles.valueStyle} numberOfLines={1}>
              {data && data.age}
            </Text>
          </View>
        </View>
        <View style={[styles.containerKeyValue, {paddingTop: 10}]}>
          <Icon name="map-marker" size={15} />
          <Text style={styles.valueStyle} numberOfLines={2}>
            {data && data.shopAddress}
          </Text>
        </View>
        <View style={{paddingTop: 10}}>
          <Animated.View
            style={{
              ...styles.viewDescription,
              ...{height: showmore ? height : null},
            }}>
            <Text>Mô tả</Text>
            <Text onLayout={this.onLayout} style={{flexWrap: 'wrap'}}>
              {data && data.description}
            </Text>
          </Animated.View>
          {showmore && (
            <LinearGradient
              style={styles.viewMore}
              colors={['#ffffff30', '#ffffff', '#ffffff']}>
              <TouchableOpacity
                style={styles.btnMore}
                activeOpacity={0.7}
                onPress={this._onPressMore}>
                <Text style={styles.txtMore}>
                  {!isShowMore ? 'Xem thêm' : 'Rút gọn'}
                </Text>
                <Icon
                  name={!isShowMore ? 'angle-down' : 'angle-up'}
                  size={15}
                  color="#000"
                />
              </TouchableOpacity>
            </LinearGradient>
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewChild: {backgroundColor: '#ffffff', padding: 5, marginVertical: 5},
  txtNameProduct: {fontSize: 14, fontWeight: '600'},
  keyStyle: {flex: 1, fontSize: 13, color: 'gray'},
  valueStyle: {flex: 3, fontSize: 13, marginLeft: 5},
  containerKeyValue: {width: '100%', flexDirection: 'row', paddingTop: 5},
  viewMore: {
    width: '100%',
    height: 30,
    position: 'absolute',
    bottom: 0,
  },
  btnMore: {
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  txtMore: {color: '#000', marginRight: 10},
  viewDescription: {
    width: '100%',
    overflow: 'hidden',
  },
  // btnMore: {
  //   width: '100%',
  //   height: 30,
  //   position: 'absolute',
  //   bottom: 0,
  //   backgroundColor: '#fff',
  //   opacity: 0.7,
  //   flexDirection: 'row',
  //   justifyContent: 'center',
  //   alignItems: 'flex-end',
  // },
  // txtMore: {color: '#000', marginRight: 10},
});

export default DescribeProduce;
