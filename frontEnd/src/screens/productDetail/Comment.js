/* eslint-disable react/no-did-update-set-state */
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Image,
} from 'react-native';
import {Rating} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {UserService} from '../../services/UserService';
import {values, color} from '../../constant';
import moment from 'moment';

class Comment extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      size: 0,
    };
  }

  async componentDidUpdate() {
    const {data} = this.props;
    const res = await UserService.getListFeedback(data.id, '');
    if (res.message === values.messageSuccess) {
      this.setState({
        data: res.result.result,
        size: res.result.size,
      });
    }
  }

  _renderItem(item, index) {
    return (
      <View style={styles.containerItem} key={`${index}`}>
        <View style={{flex: 1}}>
          <View style={styles.viewAvatar}>
            {item.avatar !== '' ? (
              <Image source={{uri: `${values.domain}${item.avatar}`}} />
            ) : (
              <Icon name="user" size={30} />
            )}
          </View>
        </View>
        <View style={{flex: 4}}>
          <View style={styles.viewUsername}>
            <Text>{item.username}</Text>
            <Rating startingValue={item.vote} imageSize={15} />
          </View>
          <View>
            <Text style={{flexWrap: 'wrap', fontSize: 14}}>{item.content}</Text>
            <Text style={{color: 'gray'}}>
              {moment(item.createdDate).format('DD/MM/YYYY')}
            </Text>
          </View>
        </View>
      </View>
    );
  }

  render() {
    const {data, size} = this.state;
    return (
      <View style={[styles.viewChild, {paddingBottom: 90}]}>
        <View style={styles.viewTitle}>
          <Text style={styles.txtNameProduct}>Đánh giá</Text>
          <Text style={styles.txtNameProduct}>{`${size} lượt Đánh giá`}</Text>
        </View>

        {data.length > 0 ? (
          data.map((item, index) => this._renderItem(item, index))
        ) : (
          <Text style={styles.noComment}>Không có bình luận</Text>
        )}
        {/* <TouchableOpacity
          activeOpacity={0.7}
          style={styles.btnMore}
          onPress={this._onPressMore}>
          <Text style={styles.txtMore}>Xem thêm</Text>
          <Icon name={'angle-right'} size={15} color="#000" />
        </TouchableOpacity> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewChild: {backgroundColor: '#ffffff', padding: 5, marginTop: 5},
  txtNameProduct: {fontSize: 14, fontWeight: '600'},
  viewTitle: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: 10,
  },
  btnMore: {
    width: '100%',
    height: 30,
    position: 'absolute',
    bottom: 70,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginBottom: 5,
  },
  txtMore: {color: '#000', marginRight: 10},
  containerItem: {width: '100%', flexDirection: 'row', paddingVertical: 5},
  viewAvatar: {
    width: 50,
    height: 50,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: color.lightGrayColor,
    borderWidth: 1,
    overflow: 'hidden',
  },
  viewUsername: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  noComment: {
    color: 'gray',
    textAlign: 'center',
    marginTop: 10,
  },
});

export default Comment;
