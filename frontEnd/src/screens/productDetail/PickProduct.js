import React, {Component} from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {color, values} from '../../constant';
import {observer} from 'mobx-react';
import {formatCurrency} from '../../constant/Format';
import BtnBase from '../../common/base/BtnBase';
import BuyProduct from './BuyProduct';
import RentPorduct from './RentPorduct';
import {UserService} from '../../services/UserService';
import {pushScreen} from '../../config/NavigationConfig';

@observer
class PickProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itemSelected: 0,
      btnSelected: 0,
    };
  }

  _onPressOrder = type => {
    const {orderStore, data, onCancelModal} = this.props;
    const {btnSelected} = this.state;
    var isRent = false;
    var due = 0;
    if (orderStore.quantity > 0) {
      const productOrder = {
        shopId: data.shopId,
        shopName: data.shopName,
        shopAddress: data.shopAddress,
        listProduct: [],
      };
      var itemSelected = -1;
      if (btnSelected === 0) {
        itemSelected = this.buy.getCurrentItem();
      } else {
        itemSelected = this.rent.getCurrentItem();
        isRent = true;
        due = orderStore.rentDue;
      }
      const item = data.sizeColors[itemSelected];
      var product = {
        sizeId: item.id,
        productImg: data.thumbnail,
        productName: data.productName,
        quantity: orderStore.quantity,
        oldPrice: data.oldPrice,
        price: item.price,
        rentPrice: item.rentPrice,
        deposit: item.deposit,
        vote: data.vote,
        rent: isRent,
        due,
        priceDelivery: data.priceDelivery,
      };
      productOrder.listProduct.push(product);
      if (type === values.addToCart) {
        this.addToCart(productOrder);
      } else {
        const passProps = {
          data: [productOrder],
          reBuy: true,
        };
        pushScreen(this.props.componentId, 'Buy', passProps);
        onCancelModal();
      }
    } else {
      alert('Số lượng sản phẩm phải lớn hơn 0');
    }
  };

  addToCart = async product => {
    const {orderStore, onCancelModal, UserStore} = this.props;
    const {sizeId, quantity, rent, due} = product.listProduct[0];
    if (UserStore.userInfo) {
      const res = await UserService.addToCart(
        UserStore.userInfo.id,
        sizeId,
        quantity,
        rent,
        due,
      );
      if (
        res.message.toLocaleLowerCase() ===
        values.messageSuccess.toLocaleLowerCase()
      ) {
        product.listProduct[0] = res.result;
        var dataCart = orderStore.listProductInCart;
        if (dataCart.length === 0) {
          dataCart.push(product);
        } else {
          var shopIndex = -1;
          dataCart.map((item, index) => {
            if (item.shopId === product.shopId) {
              shopIndex = index;
            }
          });
          if (shopIndex >= 0) {
            dataCart[shopIndex].listProduct.push(product.listProduct[0]);
          } else {
            dataCart.push(product);
          }
        }
        orderStore.listProductInCart = dataCart;
        onCancelModal();
      } else {
        alert(res);
      }
    }
  };

  handleOption = option => {
    this.setState({
      btnSelected: option,
    });
  };

  render() {
    const {img, data, orderStore, typeBtn} = this.props;
    const {btnSelected} = this.state;
    return (
      <View style={{width: '100%'}}>
        {data && data.isRent === 1 && (
          <View style={styles.topBtn}>
            <TouchableOpacity
              style={[
                styles.btnBuy,
                {
                  backgroundColor:
                    btnSelected === 0 ? color.agree : 'transparent',
                },
              ]}
              onPress={() => this.handleOption(0)}>
              <Text style={{color: btnSelected === 0 ? '#fff' : '#000'}}>
                Mua
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.btnRent,
                {
                  backgroundColor:
                    btnSelected === 1 ? color.agree : 'transparent',
                },
              ]}
              onPress={() => this.handleOption(1)}>
              <Text style={{color: btnSelected === 1 ? '#fff' : '#000'}}>
                Thuê
              </Text>
            </TouchableOpacity>
          </View>
        )}
        <View style={{padding: 10}}>
          {data && data.isRent && btnSelected === 1 ? (
            <RentPorduct
              ref={ref => (this.rent = ref)}
              data={data}
              img={img}
              orderStore={orderStore}
            />
          ) : (
            <BuyProduct
              ref={ref => (this.buy = ref)}
              data={data}
              img={img}
              orderStore={orderStore}
            />
          )}
          <BtnBase
            textBtn={
              typeBtn === values.addToCart ? 'Thêm vào giỏ hàng' : 'Mua ngay'
            }
            styleBtn={{marginTop: 20, height: 40}}
            onPress={() => this._onPressOrder(typeBtn)}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  txtItem: {textAlign: 'center'},
  itemSelected: {backgroundColor: color.agree},
  img: {resizeMode: 'stretch', width: 80, height: 50},
  topBtn: {
    width: '100%',
    borderBottomWidth: 1,
    borderColor: color.lightGrayColor,
    flexDirection: 'row',
    height: 40,
  },
  btnBuy: {
    flex: 1,
    borderTopLeftRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtTopBtn: {color: '#fff'},
  btnRent: {
    flex: 1,
    borderTopRightRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default PickProduct;
