import React, {Component} from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {BtnBase} from '../../common/base';
import {color} from '../../constant';
import {formatCurrency} from '../../constant/Format';
import {observer} from 'mobx-react';

@observer
class RentPorduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itemSelected: 0,
    };
  }

  getCurrentItem() {
    return this.state.itemSelected;
  }

  _onPressItem = index => {
    // this.props.orderStore.resetQuantity();
    this.setState({
      itemSelected: index,
    });
  };

  _onPressDecrease = () => {
    const {orderStore} = this.props;
    if (orderStore.quantity > 0) {
      orderStore.decreaseQuantity();
    }
  };

  _onPressIncrease = () => {
    this.props.orderStore.increaseQuantity();
  };

  _onPressDecreaseRentDue = () => {
    const {orderStore} = this.props;
    if (orderStore.rentDue > 0) {
      orderStore.decreaseRentDue();
    }
  };

  _onPressIncreaseRentDue = () => {
    this.props.orderStore.increaseRentDue();
  };

  caculatePrice = () => {
    const {orderStore, data} = this.props;
    const {itemSelected} = this.state;
    const sumPrice =
      data.sizeColors[itemSelected].rentPrice *
      orderStore.quantity *
      orderStore.rentDue;
    return sumPrice;
  };

  caculateDeposit = () => {
    const {orderStore, data} = this.props;
    const {itemSelected} = this.state;
    const sumPrice =
      data.sizeColors[itemSelected].deposit *
      orderStore.quantity *
      orderStore.rentDue;
    return sumPrice;
  };

  _renderItem(item, index) {
    const {itemSelected} = this.state;
    return (
      <TouchableOpacity
        key={index.toString()}
        activeOpacity={0.6}
        style={[
          styles.btnCategory,
          itemSelected === index && styles.itemSelected,
        ]}
        onPress={() => this._onPressItem(index)}>
        <Text
          numberOfLines={2}
          style={
            itemSelected === index ? styles.txtItemSelected : styles.txtItem
          }>
          {`${item.size} - ${item.color}`}
        </Text>
      </TouchableOpacity>
    );
  }

  renderQuantity() {
    const {orderStore} = this.props;
    return (
      <View style={styles.viewRowSpace}>
        <Text>Số lượng</Text>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            style={[styles.btnDeIncrease, styles.btnDecrease]}
            activeOpacity={0.6}
            onPress={this._onPressDecrease}>
            <Text>-</Text>
          </TouchableOpacity>
          <View style={styles.viewQuantity}>
            <Text>{orderStore.quantity}</Text>
          </View>
          <TouchableOpacity
            style={[styles.btnDeIncrease, styles.btnIncrease]}
            activeOpacity={0.6}
            onPress={this._onPressIncrease}>
            <Text>+</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  renderRentDue() {
    const {orderStore} = this.props;
    return (
      <View style={styles.viewRowSpace}>
        <Text>Số ngày</Text>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            style={[styles.btnDeIncrease, styles.btnDecrease]}
            activeOpacity={0.6}
            onPress={this._onPressDecreaseRentDue}>
            <Text>-</Text>
          </TouchableOpacity>
          <View style={styles.viewQuantity}>
            <Text>{orderStore.rentDue}</Text>
          </View>
          <TouchableOpacity
            style={[styles.btnDeIncrease, styles.btnIncrease]}
            activeOpacity={0.6}
            onPress={this._onPressIncreaseRentDue}>
            <Text>+</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  render() {
    const {data, img} = this.props;
    const {itemSelected} = this.state;
    return (
      <View>
        <View style={styles.containerTitle}>
          <Image source={{uri: img}} style={styles.img} />
          <View style={styles.containerNamePrice}>
            <Text style={styles.nameProduct}>{data && data.productName}</Text>
            <Text>{`Giá thuê (VNĐ): ${formatCurrency(
              data && data.sizeColors[itemSelected].rentPrice,
            )}/${'ngày'}`}</Text>
            <Text>{`Đặt cọc (VNĐ): ${formatCurrency(
              data && data.sizeColors[itemSelected].deposit,
            )}/${'ngày'}`}</Text>
          </View>
        </View>
        <View style={styles.viewItemCategory}>
          {data &&
            data.sizeColors.map((item, index) => this._renderItem(item, index))}
        </View>
        {this.renderQuantity()}
        {this.renderRentDue()}
        <View style={styles.viewTotalPrice}>
          <View style={[styles.viewRowSpace]}>
            <Text>Giá thuê</Text>
            <Text>{`${formatCurrency(this.caculatePrice())} VNĐ`}</Text>
          </View>
          <View style={[styles.viewRowSpace]}>
            <Text>Đặt cọc</Text>
            <Text>{`${formatCurrency(this.caculateDeposit())} VNĐ`}</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerTitle: {width: '100%', flexDirection: 'row'},
  containerNamePrice: {paddingLeft: 10},
  nameProduct: {fontWeight: '700'},
  viewItemCategory: {flexDirection: 'row', flexWrap: 'wrap', paddingTop: 10},
  btnCategory: {
    padding: 10,
    marginRight: 10,
    marginBottom: 10,
    maxWidth: 150,
    borderWidth: 1,
    borderColor: color.lightGrayColor,
    borderRadius: 4,
  },
  txtItem: {textAlign: 'center'},
  txtItemSelected: {textAlign: 'center', color: '#fff'},
  itemSelected: {backgroundColor: color.agree},
  img: {resizeMode: 'contain', width: 80, height: '100%'},
  btnDeIncrease: {
    width: 30,
    height: 25,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: color.agree,
  },
  btnDecrease: {
    borderTopLeftRadius: 3,
    borderBottomLeftRadius: 3,
  },
  btnIncrease: {
    borderTopRightRadius: 3,
    borderBottomRightRadius: 3,
  },
  viewQuantity: {
    paddingHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: color.agree,
  },
  viewRowSpace: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 5,
  },
  viewTotalPrice: {
    paddingTop: 10,
    borderTopWidth: 1,
    borderColor: 'gray',
    marginTop: 20,
  },
  topBtn: {
    width: '100%',
    borderBottomWidth: 1,
    borderColor: color.lightGrayColor,
    flexDirection: 'row',
    height: 40,
  },
});

export default RentPorduct;
