import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {color, values} from '../../constant';
import {Navigation} from 'react-native-navigation';
import {Rating} from 'react-native-elements';
import DescribeProduce from './DescribeProduce';
import Comment from './Comment';
import {
  TopBarTitle,
  ViewSafeArea,
  ModalBase,
  SliderImage,
  TouchablePreventDouble,
  ShowImage,
} from '../../common/base';
import PickProduct from './PickProduct';
import {inject, observer} from 'mobx-react';
import {pushScreen} from '../../config/NavigationConfig';
import HomeService from '../../services/HomeService';
import {formatCurrency} from '../../constant/Format';
import Icon from 'react-native-vector-icons/FontAwesome';
import {MainApp} from '../../navigation/Navigation';
import ChatService from '../../services/ChatService';

@inject('OrderStore', 'UserStore')
@observer
class ProductDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      isShowOrder: false,
      typeBtn: 0,
      firstLogin: false,
      showImage: false,
      indexImage: 0,
    };
  }

  async UNSAFE_componentWillMount() {
    const {item} = this.props;
    const data = await HomeService.detailProduct(item.id);
    this.setState({
      data,
    });
  }

  onPressBack = async () => {
    const {firstLogin} = this.state;
    const {OrderStore, UserStore} = this.props;
    if (UserStore.userInfo) {
      if (firstLogin) {
        MainApp();
      } else {
        Navigation.pop(this.props.componentId);
      }
      OrderStore.resetQuantity();
      OrderStore.resetRentDue();
    } else {
      Navigation.pop(this.props.componentId);
    }
  };

  _onPressShowModal = type => {
    const {UserStore} = this.props;
    if (UserStore.userInfo) {
      this.setState({
        isShowOrder: true,
        typeBtn: type,
      });
    } else {
      this.setState({
        firstLogin: true,
      });
      console.log(this.state.firstLogin);
      const passProps = {
        firstLogin: true,
      };
      pushScreen(this.props.componentId, 'LoginForm', passProps);
    }
  };

  onPressCart = () => {
    pushScreen(this.props.componentId, 'Cart');
  };

  onPressChat = () => {
    pushScreen(this.props.componentId, 'ListRoomCust');
  };

  onPressChatToShop = async () => {
    const {item, UserStore} = this.props;
    var passProps = {};
    const custId = UserStore.userInfo.id;
    const room = await ChatService.getRoomByCustShop(custId, item.shopId);
    if (room) {
      passProps = {
        shopId: item.shopId,
        item: room.result,
      };
    } else {
      passProps = {
        shopId: item.shopId,
      };
    }
    pushScreen(this.props.componentId, 'ListRoomCust', passProps);
  };

  _cancelModal = () => {
    this.setState({
      isShowOrder: false,
    });
  };

  onPressImage = (item, index) => {
    this.setState({
      showImage: true,
      indexImage: index,
    });
  };

  dismissImage = () => {
    this.setState({
      showImage: false,
    });
  };

  renderNameProduct() {
    const {data} = this.state;
    return (
      <View style={styles.viewChild}>
        <Text style={styles.txtNameProduct}>{data && data.productName}</Text>
        <Text style={styles.currentPrice}>
          {data &&
            `${
              data.minPrice ? formatCurrency(data.minPrice) + ' - ' : ''
            }${formatCurrency(data.maxPrice)} `}
        </Text>
        {data && data.oldPrice > 0 && (
          <Text style={styles.oldPrice}>{formatCurrency(data.oldPrice)}</Text>
        )}
        <View style={styles.viewVote}>
          <Rating startingValue={data ? data.vote : 0} imageSize={15} />
          <Text style={styles.txtSold}>{`Đã bán: ${data && data.sold}`}</Text>
          <TouchablePreventDouble
            style={styles.btnChat}
            onPress={this.onPressChatToShop}>
            <Icon name="comment" size={30} color={color.agree} />
          </TouchablePreventDouble>
        </View>
      </View>
    );
  }

  render() {
    const {isShowOrder, data, typeBtn, showImage, indexImage} = this.state;
    const {OrderStore, UserStore, componentId} = this.props;
    return (
      <ViewSafeArea viewStyle={{backgroundColor: color.lightGrayColor}}>
        {!showImage && (
          <TopBarTitle
            title={data && data.productName}
            onPressBack={this.onPressBack}
            onPressCart={this.onPressCart}
            onPressChat={this.onPressChat}
          />
        )}
        <View style={{flex: 1}}>
          <ScrollView style={{flex: 1}}>
            <View
              style={{backgroundColor: data ? '#fff' : color.lightGrayColor}}>
              {data && (
                <SliderImage
                  arrImage={data.images}
                  keyItem={'image'}
                  onPressItem={this.onPressImage}
                />
              )}
            </View>
            {this.renderNameProduct()}
            <DescribeProduce data={data} />
            <Comment data={data} />
          </ScrollView>
          <View style={styles.btnOrder}>
            <TouchableOpacity
              style={[styles.baseBtnBottom, styles.BtnAdd]}
              onPress={() => this._onPressShowModal(values.addToCart)}>
              <Icon name="cart-plus" size={20} color="#ba7256" />
              <Text style={styles.txtBtnAdd}>Thêm giỏ hàng</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.baseBtnBottom, styles.btnBuy]}
              onPress={() => this._onPressShowModal(values.buy)}>
              <Text style={styles.txtBtnBuy}>Mua hàng</Text>
            </TouchableOpacity>
          </View>
          <ModalBase
            visible={isShowOrder}
            styleContainerModal={{width: '100%'}}
            styleBackground={{justifyContent: 'flex-end'}}
            onPressCancel={this._cancelModal}>
            {isShowOrder && (
              <PickProduct
                img={`${values.domain}${data.images[1].image}`}
                orderStore={OrderStore}
                UserStore={UserStore}
                data={data}
                componentId={componentId}
                typeBtn={typeBtn}
                onCancelModal={this._cancelModal}
              />
            )}
          </ModalBase>
        </View>
        {showImage && (
          <ShowImage
            arrImage={data.images}
            onPressCancel={this.dismissImage}
            keyItem="image"
            firstItem={indexImage}
          />
        )}
      </ViewSafeArea>
    );
  }
}

const styles = StyleSheet.create({
  imageProduct: {resizeMode: 'stretch', width: '100%', height: '100%'},
  viewChild: {backgroundColor: '#ffffff', padding: 5, marginVertical: 5},
  txtNameProduct: {fontSize: 18, fontWeight: '600'},
  currentPrice: {fontSize: 16, color: color.agree, marginVertical: 5},
  oldPrice: {fontSize: 13, color: 'gray', textDecorationLine: 'line-through'},
  viewVote: {
    flexDirection: 'row',
    paddingVertical: 10,
    alignItems: 'center',
    width: '100%',
  },
  txtSold: {marginLeft: 25},
  btnOrder: {
    height: 40,
    width: '100%',
    position: 'absolute',
    bottom: 0,
    backgroundColor: '#fff',
    flexDirection: 'row',
  },
  baseBtnBottom: {
    flexDirection: 'row',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  BtnAdd: {backgroundColor: '#42f5da', flex: 1, borderTopLeftRadius: 6},
  btnBuy: {backgroundColor: color.agree, flex: 2, borderTopRightRadius: 6},
  txtBtnBuy: {color: '#fff'},
  txtBtnAdd: {color: '#ba7256', marginLeft: 5},
  btnChat: {
    position: 'absolute',
    right: 10,
  },
});

export default ProductDetail;
