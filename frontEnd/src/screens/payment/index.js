import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {color, values} from '../../constant';
import {BtnBase} from '../../common/base';
import {showModal} from '../../config/NavigationConfig';
import { Modal } from 'react-native';
import { MainBankScreen } from '..';
import { UserService } from '../../services/UserService';

class Payment extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      paymentSelected: 1,
      bankSelected: 0,
      showBankInfo: false,
    };
  }

  componentDidMount() {
    const {OrderStore} = this.props;
    OrderStore.bill.paymentId = this.state.paymentSelected;
    OrderStore.bill.bankId = '';
    OrderStore.bill.bankName = '';
    OrderStore.bill.creditNumber = '';
  }

  onPressNext = () => {
    const {OrderStore, viewPager} = this.props;
    OrderStore.stepIndicatorRef._onNextStep();
    OrderStore.pageSelected = 2;
    viewPager.setPage(2);
  };

  onPressCod = () => {
    const {OrderStore} = this.props;
    this.setState({
      paymentSelected: 1,
    });
    OrderStore.bill.paymentId = 1;
    OrderStore.bill.bankId = '';
    OrderStore.bill.bankName = '';
    OrderStore.bill.creditNumber = '';
  };

  onPressBank = () => {
    this.setState({
      showBankInfo: true,
    });
  };

  onSelectBank = (item, index) => {
    const {OrderStore} = this.props;
    this.setState({
      paymentSelected: 2,
      bankSelected: index,
    });
    OrderStore.bill.paymentId = 2;
    OrderStore.bill.bankId = item.id;
    OrderStore.bill.bankName = item.bankName;
    OrderStore.bill.creditNumber = item.creditNumber;
  };

  dissMissModal = () => {
    this.setState({
      showBankInfo: false,
    });
  };

  onPressFinish = async bankInfo => {
    const {UserStore, OrderStore} = this.props;
    const body = {custId: UserStore.userInfo.id, ...bankInfo};
    const res = await UserService.addBank(body);
    if (
      res.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      UserStore.listBank.push(body);
      this.setState({
        paymentSelected: 2,
        bankSelected: UserStore.listBank.length - 1,
      });
      OrderStore.bill.paymentId = 2;
    }
    this.setState({
      showBankInfo: false,
    });
  };

  render() {
    const {paymentSelected, bankSelected, showBankInfo} = this.state;
    const {UserStore} = this.props;
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={{...styles.btn, paddingTop: 20, paddingBottom: 20}}
          activeOpacity={0.6}
          onPress={this.onPressCod}>
          <Text>Thanh toán khi nhận hàng</Text>
          {paymentSelected === 1 && (
            <Icon name="check" color={color.agree} size={20} />
          )}
        </TouchableOpacity>
        <TouchableOpacity style={styles.btn} activeOpacity={0.6}>
          <Text style={styles.txtBank}>Thanh toán qua ngân hàng</Text>
        </TouchableOpacity>
        {UserStore.listBank.length > 0 ? (
          UserStore.listBank.map((item, index) => {
            return (
              <TouchableOpacity
                style={styles.bankInfor}
                onPress={() => this.onSelectBank(item, index)}>
                <View>
                  <Text style={styles.text}>{item.bankName}</Text>
                  <Text style={styles.text}>{`STK: ${item.creditNumber}`}</Text>
                </View>
                {paymentSelected == 2 && bankSelected == index && (
                  <Icon name="check" color={color.agree} size={20} />
                )}
              </TouchableOpacity>
            );
          })
        ) : (
          <></>
        )}
        {UserStore.listBank.length < 3 && (
          <TouchableOpacity style={styles.inner} onPress={this.onPressBank}>
            <Icon name="plus-square" size={20} color={color.agree} />
            <Text style={styles.textAdd}>Liên kết thêm ngân hàng</Text>
          </TouchableOpacity>
        )}
        <BtnBase
          textBtn="Tiếp theo"
          styleBtn={styles.btnNext}
          onPress={this.onPressNext}
        />
        <Modal visible={showBankInfo} animationType="slide">
          {showBankInfo && (
            <MainBankScreen
              onPressBack={this.dissMissModal}
              onPressNext={this.onPressFinish}
            />
          )}
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {flex: 1, paddingTop: 20},
  btn: {
    backgroundColor: '#fff',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    alignItems: 'center',
    paddingTop: 10,
    paddingHorizontal: 10,
    borderTopWidth: 1,
    borderColor: 'gray',
  },
  btnNext: {
    width: '100%',
    height: 40,
    marginTop: 10,
    paddingHorizontal: 20,
    position: 'absolute',
    bottom: 10,
  },
  text: {
    fontSize: 16,
  },
  bankInfor: {
    backgroundColor: '#fff',
    paddingHorizontal: 10,
    paddingBottom: 10,
    paddingTop: 5,
    borderTopWidth: 1,
    borderColor: color.lightGrayColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  txtBank: {
    marginBottom: 10,
  },
  inner: {
    backgroundColor: '#fff',
    padding: 10,
    flexDirection: 'row',
    borderTopWidth: 1,
    borderColor: color.lightGrayColor,
  },
  textAdd: {
    marginLeft: 10,
    color: color.agree,
  },
});
export default Payment;
