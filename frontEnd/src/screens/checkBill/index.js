import React, {Component} from 'react';
import {View, Text, StyleSheet, Modal} from 'react-native';
import InformationBill from '../InformationBill';
import {BtnBase} from '../../common/base';
import {formatCurrency} from '../../constant/Format';
import {observer} from 'mobx-react';
import {Navigation} from 'react-native-navigation';
import BuyNotification from '../buyNotification';
import {UserService} from '../../services/UserService';
import {values} from '../../constant';
import {notifyMessage} from '../../config/Func';

@observer
class CheckBill extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      heightTextInput: 0,
      value: '',
      editable: false,
      isShowNotification: false,
    };
  }

  UNSAFE_componentWillMount() {
    const {OrderStore} = this.props;
    this.setState({
      value: OrderStore.bill.orderPlace,
    });
  }

  _onChangText = value => {
    this.setState({
      value,
    });
  };

  _handleChangeAddress = () => {
    const {OrderStore} = this.props;
    const {editable, value} = this.state;
    if (!editable) {
      this.setState({
        editable: true,
      });
    } else {
      if (value === '') {
        this.setState({
          value: OrderStore.bill.orderPlace,
        });
      } else {
        this.setState({
          value,
        });
        OrderStore.bill.orderPlace = value;
      }
      this.setState({
        editable: false,
      });
    }
  };

  caculateTotalBill = () => {
    const {OrderStore} = this.props;
    var total = 0;
    if (OrderStore.bill) {
      OrderStore.bill.productBillDTOs.map(item => {
        total += item.totalPrice;
      });
      OrderStore.bill.totalBill = total;
    }
  };

  onPressNext = async () => {
    const {OrderStore, reBuy} = this.props;
    const {
      custId,
      orderPlace,
      paymentId,
      bankName,
      bankId,
      creditNumber,
      totalBill,
    } = OrderStore.bill;
    const productBillDTOs = [];
    OrderStore.bill.productBillDTOs.map(item => {
      item.listProduct.map(itemProduct => {
        const product = {
          sizeId: itemProduct.sizeId,
          quantity: itemProduct.quantity,
          deliveryId: item.deliveryId > 0 ? item.deliveryId : 1,
          note: item.note ? item.note : '',
          isRent: itemProduct.rent,
        };
        productBillDTOs.push(product);
      });
    });
    const data = {
      customerId: custId,
      orderPlace,
      paymentId,
      bankId,
      bankName,
      creditNumber,
      totalBill,
      productBillDTOs,
    };
    const res = await UserService.createBill(data);
    if (
      res.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      if (!reBuy) {
        const ids = [];
        OrderStore.listProductInCart.map(itemShop => {
          itemShop.listProduct.map(item => {
            ids.push(item.id);
          });
        });
        await UserService.removeProductFromCart(ids);
        OrderStore.listProductInCart = [];
      }
      this.setState({
        isShowNotification: true,
      });
    } else {
      console.log(res);
      alert(res.message);
    }
  };

  _onPressEditPayment = () => {
    const {OrderStore, viewPager} = this.props;
    OrderStore.stepIndicatorRef._onBackStep();
    OrderStore.pageSelected = 1;
    viewPager.setPage(1);
  };

  onPressCloseModal = () => {
    Navigation.pop(this.props.componentId);
    this.setState({
      isShowNotification: false,
    });
  };

  render() {
    const {OrderStore, UserStore} = this.props;
    const {value, heightTextInput, editable, isShowNotification} = this.state;
    return (
      <View style={{flex: 1}}>
        <InformationBill
          styleTextInput={[
            editable
              ? styles.textInputStyleEditable
              : styles.textInputStyleNonEditable,
            {height: Math.max(35, heightTextInput)},
          ]}
          OrderStore={OrderStore}
          UserStore={UserStore}
          editable={editable}
          isShowAll={false}
          otherPlace={value}
          onChangeText={this._onChangText}
          onContentSizeChange={event => {
            this.setState({
              heightTextInput: event.nativeEvent.contentSize.height,
            });
          }}
          onPressEditAddress={this._handleChangeAddress}
          onPressEditPayment={this._onPressEditPayment}
        />
        <View style={styles.containerBtnNext}>
          <Text style={styles.txtTotalPrice}>
            {`Tổng thanh toán: ${formatCurrency(
              OrderStore.bill.totalBill,
            )} VNĐ`}
          </Text>
          <BtnBase
            textBtn="Thanh toán"
            styleBtn={styles.btnNext}
            onPress={this.onPressNext}
          />
        </View>
        <Modal visible={isShowNotification} animationType="slide">
          {isShowNotification && (
            <BuyNotification
              OrderStore={OrderStore}
              UserStore={UserStore}
              onPressClose={this.onPressCloseModal}
            />
          )}
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textInputStyleNonEditable: {
    color: 'green',
    fontSize: 16,
    flexWrap: 'wrap',
  },
  textInputStyleEditable: {
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 6,
    color: '#000',
    paddingLeft: 5,
  },
  containerBtnNext: {
    marginTop: 20,
    paddingVertical: 10,
    backgroundColor: '#fff',
    paddingHorizontal: 20,
  },
  txtTotalPrice: {textAlign: 'right'},
  btnNext: {height: 40, marginTop: 10},
});

export default CheckBill;
