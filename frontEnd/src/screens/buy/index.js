import React, {createRef} from 'react';
import {View, Animated, StyleSheet} from 'react-native';
import {ViewSafeArea, TopBarTitle} from '../../common/base';
import StepIndicatorScreen from '../stepIndicatorScreen';
import {color, values} from '../../constant';
import {inject, observer} from 'mobx-react';
import Delivery from '../delivery';
import {Navigation} from 'react-native-navigation';
import ViewPager from '@react-native-community/viewpager';
import {ScreenConfig} from '../../config';
import Payment from '../payment';
import CheckBill from '../checkBill';

@inject('OrderStore', 'UserStore')
@observer
class index extends React.Component {
  scrollViewInfor = createRef();
  scrollViewCheckBill = createRef();
  isScroll = false;

  constructor(props) {
    super(props);
    this.state = {
      scrollY: new Animated.Value(0),
      heightView: 0,
    };
  }

  UNSAFE_componentWillMount() {
    const {OrderStore, data} = this.props;
    if (data) {
      OrderStore.setProductInBill(data);
    }
  }

  componentDidMount() {
    const {OrderStore, UserStore} = this.props;
    OrderStore.bill.custId = UserStore.userInfo.id;
  }

  _handleStepIndicator = ref => {
    const {OrderStore} = this.props;
    OrderStore.stepIndicatorRef = ref;
  };

  renderChildRight() {
    return <></>;
  }

  onPressBack = () => {
    const {OrderStore} = this.props;
    OrderStore.stepIndicatorRef._onBackStep();
    if (OrderStore.pageSelected === 0) {
      Navigation.pop(this.props.componentId);
    } else if (OrderStore.pageSelected === 1) {
      if (this.isScroll) {
        this.state.scrollY.setValue(this.state.heightView);
      }
      OrderStore.pageSelected = 0;
      this.viewPager.setPage(0);
    } else if (OrderStore.pageSelected === 2) {
      this.state.scrollY.setValue(0);
      OrderStore.pageSelected = 1;
      this.viewPager.setPage(1);
    }
  };

  _onLayout = e => {
    this.setState({
      heightView: e.nativeEvent.layout.height,
    });
  };

  handleScroll = e => {
    if (e.nativeEvent.contentOffset.y >= this.state.heightView) {
      this.isScroll = true;
    }
  };

  _onPageSelected = e => {
    const position = e.nativeEvent.position;
    if (position === 0) {
      if (this.isScroll) {
        this.scrollViewInfor.current.scrollTo({x: 0, y: 0, animated: true});
      } else {
        this.state.scrollY.setValue(0);
      }
    } else {
      this.state.scrollY.setValue(0);
    }
  };

  renderViewInfor() {
    const {OrderStore, data, UserStore} = this.props;
    const {heightView} = this.state;
    return (
      <View style={{flex: 1}}>
        <Animated.ScrollView
          ref={this.scrollViewInfor}
          style={styles.containerDelivery}
          scrollEventThrottle={1}
          scrollToOverflowEnabled={true}
          onScroll={Animated.event(
            [
              {
                nativeEvent: {
                  contentOffset: {y: this.state.scrollY},
                },
              },
            ],
            {useNativeDriver: true, listener: this.handleScroll},
          )}>
          <View style={[styles.viewDelivery, {paddingTop: heightView}]}>
            <Delivery
              OrderStore={OrderStore}
              UserStore={UserStore}
              viewPager={this.viewPager}
              data={data}
            />
          </View>
        </Animated.ScrollView>
      </View>
    );
  }

  renderCheckBill() {
    const {OrderStore, componentId, UserStore, reBuy} = this.props;
    const {heightView} = this.state;
    return (
      <View>
        {OrderStore.pageSelected === 2 && (
          <Animated.ScrollView
            ref={this.scrollViewCheckBill}
            style={styles.containerDelivery}
            scrollEventThrottle={1}
            scrollToOverflowEnabled={true}
            onScroll={Animated.event(
              [
                {
                  nativeEvent: {
                    contentOffset: {y: this.state.scrollY},
                  },
                },
              ],
              {useNativeDriver: true, listener: this.handleScroll},
            )}>
            <View style={[styles.viewDelivery, {paddingTop: heightView}]}>
              {OrderStore.pageSelected == 2 && (
                <CheckBill
                  OrderStore={OrderStore}
                  viewPager={this.viewPager}
                  componentId={componentId}
                  UserStore={UserStore}
                  reBuy={reBuy}
                />
              )}
            </View>
          </Animated.ScrollView>
        )}
      </View>
    );
  }

  render() {
    const {OrderStore, UserStore} = this.props;
    const {heightView, scrollY} = this.state;
    const height = scrollY.interpolate({
      inputRange: [0, heightView],
      outputRange: [0, -heightView + values.topBarHeight],
      extrapolate: 'clamp',
    });
    const opacity = scrollY.interpolate({
      inputRange: [0, heightView],
      outputRange: [1, 0],
      extrapolate: 'clamp',
    });
    return (
      <ViewSafeArea viewStyle={{backgroundColor: color.lightGrayColor}}>
        <TopBarTitle
          title="Đặt hàng"
          childRight={this.renderChildRight()}
          onPressBack={this.onPressBack}
        />
        <ViewPager
          ref={ref => (this.viewPager = ref)}
          style={styles.viewPager}
          scrollEnabled={false}
          initialPage={0}
          onPageSelected={this._onPageSelected}>
          {this.renderViewInfor()}
          <View style={{paddingTop: heightView}}>
            {OrderStore.pageSelected == 1 && (
              <Payment
                OrderStore={OrderStore}
                UserStore={UserStore}
                viewPager={this.viewPager}
                scrollY={scrollY}
                heightView={heightView}
                isScroll={this.isScroll}
              />
            )}
          </View>
          {this.renderCheckBill()}
        </ViewPager>
        <Animated.View
          onLayout={this._onLayout}
          style={[
            styles.containerProgessBar,
            {
              opacity,
              transform: [{translateY: height}],
            },
          ]}>
          <StepIndicatorScreen ref={this._handleStepIndicator} />
        </Animated.View>
      </ViewSafeArea>
    );
  }
}

const styles = StyleSheet.create({
  containerProgessBar: {
    width: '100%',
    backgroundColor: 'transparent',
    top: values.topBarHeight + ScreenConfig.marginTopScreen + 10,
    position: 'absolute',
  },
  viewPager: {flex: 1},
  containerDelivery: {flex: 1},
  viewDelivery: {flex: 1},
});
export default index;
