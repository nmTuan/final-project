import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  Modal,
  Animated,
  TouchableOpacity,
  Image,
} from 'react-native';
import {
  ViewSafeArea,
  TopBarChatCart,
  TouchablePreventDouble,
  BtnBase,
  OptionBase,
} from '../../common/base';
import {color, values} from '../../constant';
import Icon from 'react-native-vector-icons/FontAwesome';
import {ScreenConfig} from '../../config';
import {inject, observer} from 'mobx-react';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';
import {NotLogin} from '../../navigation/Navigation';
import {Navigation} from 'react-native-navigation';
import {FileService} from '../../services/FileService';
import Values from '../../constant/Values';
import {pushScreen, showModal} from '../../config/NavigationConfig';
import BankingInfo from './BankingInfo';

@inject('UserStore', 'OrderStore')
@observer
class Personal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      scrollY: new Animated.Value(0),
      showLogout: false,
      url: '',
    };
  }

  onPressRegisteShop = () => {
    const passProps = {
      userInfo: this.props.UserStore.userInfo,
    };
    showModal('ShopRegister', passProps);
  };

  onPressUpdate = () => {
    const passProps = {
      userInfo: this.props.UserStore.userInfo,
    };
    showModal('UpdatePersonalInfo', passProps);
  };

  onPressUpdatePass = () => {
    const passProps = {
      userInfo: this.props.UserStore.userInfo,
    };
    showModal('UpdatePassword', passProps);
  };

  showLogout = () => {
    this.setState({
      showLogout: true,
    });
  };

  onCancelLogout = () => {
    this.setState({
      showLogout: false,
    });
  };

  onPressLogout = () => {
    const {UserStore, OrderStore} = this.props;
    NotLogin();
    UserStore.reset();
    OrderStore.listProductInCart = [];
    AsyncStorage.clear();
  };

  onPressCart = () => {
    pushScreen(this.props.componentId, 'Cart');
  };

  onPressChat = () => {
    pushScreen(this.props.componentId, 'ListRoomCust');
  };

  renderInforPerson = () => {
    const {UserStore} = this.props;
    const userInfo = UserStore.userInfo;
    return (
      userInfo && (
        <View style={[styles.viewInfo, {marginTop: 70}]}>
          <View style={styles.titlePersonalInfo}>
            <Text style={{color: color.agree}}>Thông tin cá nhân</Text>
            <TouchableOpacity onPress={this.onPressUpdate}>
              <Icon name="edit" size={20} />
            </TouchableOpacity>
          </View>
          <View style={styles.infor}>
            <Text>Họ và tên</Text>
            <Text>{userInfo.fullName}</Text>
          </View>
          <View style={styles.infor}>
            <Text>Ngày sinh</Text>
            <Text>{moment(userInfo.dob).format('DD/MM/YYYY')}</Text>
          </View>
          <View style={styles.infor}>
            <Text>Giới tính</Text>
            <Text>{userInfo.gender}</Text>
          </View>
          <View style={styles.infor}>
            <Text>Số điện thoại</Text>
            <Text>{userInfo.phoneNumber}</Text>
          </View>
          <View style={styles.infor}>
            <Text>Tỉnh/Thành phố</Text>
            <Text>{userInfo.city}</Text>
          </View>
          <View style={styles.infor}>
            <Text>Quận/Huyện</Text>
            <Text>{userInfo.district}</Text>
          </View>
          <View style={styles.infor}>
            <Text>Xã/Phường</Text>
            <Text>{userInfo.village}</Text>
          </View>
          <View style={{paddingTop: 10}}>
            <Text>Địa chỉ chi tiết</Text>
            <Text style={{marginTop: 5}}>{userInfo.address}</Text>
          </View>
        </View>
      )
    );
  };

  renderInforAccount = () => {
    const {UserStore} = this.props;
    return (
      <View style={[styles.viewInfo]}>
        <Text style={{color: color.agree}}>Thông tin tài khoản</Text>
        <View style={styles.infor}>
          <Text>Tên đăng nhập</Text>
          <Text>{UserStore.username}</Text>
        </View>
        <TouchablePreventDouble
          style={styles.infor}
          activeOpacity={0.5}
          onPress={this.onPressUpdatePass}>
          <Text>Đổi mật khẩu</Text>
          <Icon name="angle-right" size={20} />
        </TouchablePreventDouble>
      </View>
    );
  };

  render() {
    const {UserStore, OrderStore} = this.props;
    const {showLogout} = this.state;
    const interpolate = this.state.scrollY.interpolate({
      inputRange: [0, values.topBarHeight],
      outputRange: ['transparent', '#ffffff'],
    });
    return (
      <ViewSafeArea viewStyle={styles.container}>
        {/* <NotLogged /> */}
        <Animated.ScrollView
          style={styles.scrollView}
          onScroll={Animated.event(
            [
              {
                nativeEvent: {
                  contentOffset: {y: this.state.scrollY},
                },
              },
            ],
            {
              useNativeDriver: false,
            },
          )}>
          <View style={styles.containerAva}>
            <View style={styles.coverava} />
            <View style={styles.viewAvata}>
              <TouchableOpacity style={styles.avatar}>
                <Image
                  source={{
                    uri: `${Values.domain}${'images/1591152016255.jpg'}`,
                  }}
                  style={styles.imgAvatar}
                />
              </TouchableOpacity>
              <Text>{UserStore.userInfo && UserStore.username}</Text>
            </View>
          </View>
          {this.renderInforPerson()}
          <BankingInfo UserStore={UserStore} />
          {this.renderInforAccount()}
          <View
            style={[styles.viewInfo, {flexDirection: 'row', flexWrap: 'wrap'}]}>
            {UserStore.userInfo && !UserStore.userInfo.isShop ? (
              <Text>
                {
                  'Hãy đăng ký đại lý trở thành một người bán hàng trong hệ thống của chúng tôi. '
                }
                <TouchableWithoutFeedback onPress={this.onPressRegisteShop}>
                  <Text
                    style={{color: 'blue', textDecorationLine: 'underline'}}>
                    Đăng ký
                  </Text>
                </TouchableWithoutFeedback>
              </Text>
            ) : (
              <Text>
                {'Ban đang đã trở thành đại lý trong hệ thống của chúng tôi. '}
              </Text>
            )}
          </View>
          <View style={styles.btnLogout}>
            <BtnBase textBtn="Đăng xuất" onPress={this.showLogout} />
          </View>
        </Animated.ScrollView>
        <Animated.View
          style={[styles.viewTopBar, {backgroundColor: interpolate}]}>
          <TopBarChatCart
            transparent
            onPressCart={this.onPressCart}
            onPressChat={this.onPressChat}
          />
        </Animated.View>
        <Modal visible={showLogout} transparent>
        {showLogout && (
            <OptionBase
              title="Bạn có chắc chắn muốn đăng xuất không?"
              textBtn1="Đăng xuất"
              onPress1={this.onPressLogout}
              onPressCancel={this.onCancelLogout}
            />
          )}
        </Modal>
      </ViewSafeArea>
    );
  }
}

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: color.lightGrayColor},
  viewTopBar: {
    width: '100%',
    position: 'absolute',
    top: ScreenConfig.marginTopScreen,
  },
  containerAva: {width: '100%', alignItems: 'center'},
  coverava: {width: '100%', height: 150, backgroundColor: '#999897'},
  viewAvata: {
    position: 'absolute',
    bottom: -60,
    alignItems: 'center',
    justifyContent: 'center',
  },
  avatar: {
    width: 80,
    height: 80,
    borderRadius: 40,
    marginBottom: 5,
    overflow: 'hidden',
  },
  imgAvatar: {width: '100%', height: '100%', resizeMode: 'cover'},
  scrollView: {flex: 1, paddingBottom: 10},
  viewInfo: {
    width: '100%',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#fff',
    padding: 10,
    backgroundColor: '#fff',
    marginTop: 20,
  },
  infor: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    borderBottomWidth: 1,
    borderColor: 'gray',
    paddingVertical: 10,
  },
  btnLogout: {
    width: '100%',
    height: 40,
    paddingHorizontal: 20,
    marginVertical: 10,
  },
  titlePersonalInfo: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default Personal;
