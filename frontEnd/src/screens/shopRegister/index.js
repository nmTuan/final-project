import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  TouchableWithoutFeedback,
  Modal,
} from 'react-native';
import {TopBar} from '../../common/base';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Navigation} from 'react-native-navigation';
import {color} from '../../constant';
import BtnBase from '../../common/base/BtnBase';
import NotificationShop from './NotificationShop';
import {inject, observer} from 'mobx-react';
import {CheckBox} from 'react-native-elements';

@inject('UserStore')
@observer
class ShopRegister extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showNotification: false,
    };
  }

  onPressBack = () => {
    Navigation.dismissModal(this.props.componentId);
  };

  onPressContract = () => {
    alert('asdf');
  };

  onPressRegister = () => {
    this.setState({
      showNotification: true,
    });
  };

  onChangeShopname = value => {
    const {UserStore} = this.props;
    const shopInfo = UserStore.shopInfo;
    shopInfo.shopName = value;
  };

  onChangePhoneNumber = value => {
    const {UserStore} = this.props;
    const shopInfo = UserStore.shopInfo;
    shopInfo.phoneNumber = value;
  };

  onChangeAddress = value => {
    const {UserStore} = this.props;
    const shopInfo = UserStore.shopInfo;
    shopInfo.address = value;
  };

  onChangeDescription = value => {
    const {UserStore} = this.props;
    const shopInfo = UserStore.shopInfo;
    shopInfo.description = value;
  };

  render() {
    const {showNotification} = this.state;
    const {UserStore} = this.props;
    const shopInfo = UserStore.shopInfo;
    return (
      <View style={styles.container}>
        <View style={styles.viewTopBar}>
          <TopBar
            childLeft={
              <TouchableOpacity onPress={this.onPressBack}>
                <Icon name="angle-left" size={40} style={styles.btnBack} />
              </TouchableOpacity>
            }
            childCenter={<Text style={styles.title}>Đăng ký cửa hàng</Text>}
          />
        </View>
        <View style={styles.inner}>
          <View style={styles.viewInput}>
            <Text>Tên cửa hàng</Text>
            <TextInput
              placeholder="Tên cửa hàng"
              value={shopInfo.shopName}
              style={styles.textInput}
              onChangeText={this.onChangeShopname}
            />
          </View>
          <View style={styles.viewInput}>
            <Text>Số điện thoại</Text>
            <TextInput
              placeholder="Số điện thoại"
              value={shopInfo.phoneNumber}
              style={styles.textInput}
              onChangeText={this.onChangePhoneNumber}
            />
          </View>
          <View style={styles.viewInput}>
            <Text>Địa chỉ cửa hàng</Text>
            <TextInput
              placeholder="Địa chỉ cửa hàng"
              value={shopInfo.address}
              style={styles.textInput}
              onChangeText={this.onChangeAddress}
            />
          </View>
          <View style={styles.viewInput}>
            <Text>Mô tả</Text>
            <TextInput
              style={styles.textInputRating}
              value={shopInfo.description}
              placeholder="Nội dung..."
              multiline
              onChangeText={this.onChangeDescription}
            />
          </View>
          <View
            style={{
              ...styles.viewInput,
              ...{flexDirection: 'row', alignItems: 'center'},
            }}>
            <CheckBox checked />
            <Text>
              Tôi đồng ý với các{' '}
              <TouchableWithoutFeedback onPress={this.onPressContract}>
                <Text style={styles.txtContract}>điều khoản</Text>
              </TouchableWithoutFeedback>
            </Text>
          </View>
          <View style={styles.btnRegister}>
            <BtnBase textBtn="Đăng ký" onPress={this.onPressRegister} />
          </View>
        </View>
        <Modal visible={showNotification} animationType="slide">
          <NotificationShop onPressClose={this.onPressBack} />
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: '#fff'},
  btnBack: {marginLeft: 10},
  title: {fontSize: 16, textAlign: 'center'},
  viewTopBar: {backgroundColor: '#fff'},
  textInput: {
    width: '100%',
    height: 40,
    borderBottomWidth: 1,
    borderColor: color.lightGrayColor,
  },
  inner: {paddingTop: 30, paddingHorizontal: 20},
  viewInput: {paddingTop: 15},
  textInputRating: {
    height: 90,
    width: '100%',
    borderWidth: 1,
    borderColor: 'gray',
    textAlignVertical: 'top',
    borderRadius: 6,
    marginTop: 10,
  },
  txtContract: {color: 'blue', textDecorationLine: 'underline'},
  btnRegister: {
    width: '100%',
    paddingHorizontal: 20,
    height: 40,
    marginTop: 30,
  },
});

export default ShopRegister;
