import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {color} from '../../constant';
import {Notification, BtnBase} from '../../common/base';

class NotificationShop extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  renderIcon() {
    return (
      <View style={styles.containerIcon}>
        <Icon name="check" size={50} color="green" />
      </View>
    );
  }

  renderContent() {
    return (
      <View style={styles.containerContent}>
        <Text style={styles.title}>Chúc mừng</Text>
        <Text style={styles.content}>Bạn đã trở thành một đại lý</Text>
        <Text style={styles.content}>Mã shop: FA0110</Text>
      </View>
    );
  }

  renderButton() {
    return (
      <View style={styles.containerBtn}>
        <BtnBase
          textBtn="Đóng"
          backgroundColor="gray"
          styleBtn={styles.btn}
          onPress={this.props.onPressClose}
        />
      </View>
    );
  }

  render() {
    return (
      <View>
        <Notification
          viewTop={this.renderIcon()}
          ViewMiddle={this.renderContent()}
          ViewBottom={this.renderButton()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerIcon: {
    width: 80,
    height: 80,
    borderRadius: 40,
    borderWidth: 1,
    borderColor: color.agree,
    backgroundColor: '#14c45170',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerContent: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  title: {color: 'green', fontSize: 20, fontWeight: 'bold', marginBottom: 10},
  content: {fontSize: 16},
  containerBtn: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 60,
  },
  btn: {height: 40, width: '100%'},
});

export default NotificationShop;
