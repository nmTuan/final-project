import React, {Component} from 'react';
import EditProduct from '../editProduct';
import {inject, observer} from 'mobx-react';
import ProductShopService from '../../../services/ProductShopService';
import {Navigation} from 'react-native-navigation';
import {notifyMessage} from '../../../config/Func';
import AsyncStorage from '@react-native-community/async-storage';
import {values} from '../../../constant';

@inject('ShopProductStore', 'ShopHomeStore')
@observer
class AddNewProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productInfo: {
        productName: '',
        thumbnail: '',
        shortDescription: '',
        minPrice: 0,
        maxPrice: 0,
        oldPrice: '',
        description: '',
        brand: '',
        sold: 0,
        vote: 0,
        material: '',
        category_id: '',
        gender_id: '',
        age_id: '',
        isRent: false,
        bankId: 0,
        priceDelivery: 0,
        images: [],
        sizeColors: [
          {
            size: '',
            color: '',
            quantity: 0,
            price: 0,
            rentPrice: 0,
            rentDueId: '1',
            deposit: 0,
          },
        ],
      },
    };
  }

  async UNSAFE_componentWillMount() {
    const {ShopProductStore} = this.props;
    ShopProductStore.productInfo = this.state.productInfo;
  }

  onPressAdd = async product => {
    const {ShopHomeStore} = this.props;
    const newProduct = {
      shopId: ShopHomeStore.shopInfo.id,
      detailId: 0,
      ...product,
    };
    const res = await ProductShopService.addNewProduct(newProduct);
    if (res === 401) {
      notifyMessage('401: Authentication error');
    } else {
      if (res.message === values.messageSuccess) {
        newProduct.detailId = res.result.id;
        const listNewProduct = [newProduct];
        ShopHomeStore.setListProduct(ShopHomeStore.listProduct, listNewProduct);
        notifyMessage('Thêm thành công');
        Navigation.dismissModal(this.props.componentId);
      } else {
        notifyMessage(res.message);
      }
    }
  };

  onPressDeleteSizeColor = index => {
    const {ShopProductStore} = this.props;
    const productInfo = ShopProductStore.productInfo;
    productInfo.sizeColors.splice(index, 1);
  };

  render() {
    const {ShopProductStore, ShopHomeStore, componentId} = this.props;
    return (
      ShopProductStore.productInfo && (
        <EditProduct
          ShopProductStore={ShopProductStore}
          ShopHomeStore={ShopHomeStore}
          componentId={componentId}
          onPressSave={this.onPressAdd}
          txtBtn="Thêm sản phẩm"
          onPressDeleteSizeColor={this.onPressDeleteSizeColor}
        />
      )
    );
  }
}

export default AddNewProduct;
