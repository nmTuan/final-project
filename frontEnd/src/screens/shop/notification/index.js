import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  ActivityIndicator,
  Platform,
} from 'react-native';
import {ViewSafeArea, TopBarChatCart} from '../../../common/base';
import {color, values} from '../../../constant';
import {shopService} from '../../../services/ShopService';
import {inject, observer} from 'mobx-react';
import {Navigation} from 'react-native-navigation';
import {notifyMessage} from '../../../config/Func';
import RenderItem from './RenderItem';
import { pushScreen } from '../../../config/NavigationConfig';

@inject('ShopHomeStore')
@observer
class Notification extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      refreshing: false,
      isLoadding: false,
      countNotifi: 0,
    };
  }

  componentDidMount() {
    this.initData();
    this.getBadgeNotifi();
  }

  async getBadgeNotifi() {
    const {ShopHomeStore} = this.props;
    const id = ShopHomeStore.shopInfo.id;
    const res = await shopService.countNotification(id, 0);
    if (res.result && res.result > 0) {
      this.setState({
        countNotifi: res.result,
      });
      Navigation.mergeOptions(this.props.componentId, {
        bottomTab: {
          badge: `${res.result}`,
        },
      });
    }
  }

  async initData() {
    const {ShopHomeStore} = this.props;
    const data = await shopService.getListNotifiShop(ShopHomeStore.shopInfo.id);
    if (
      data.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      ShopHomeStore.setNotification([], data.result.notificationShopDTOs);
    }
  }

  onPressItem = async (item, index) => {
    const {ShopHomeStore} = this.props;
    const shopId = ShopHomeStore.shopInfo.id;
    var badge = this.state.countNotifi;
    const bill = item.title.search('đơn hàng');
    if (item.status === 0) {
      item.status = 1;
      const count = this.state.countNotifi - 1;
      badge = count;
      this.setState({
        countNotifi: count,
      });
      await shopService.changeStatusNotify(item.id, shopId);
    }
    badge = badge > 0 ? badge : '';
    Navigation.mergeOptions(this.props.componentId, {
      bottomTab: {
        badge: `${badge}`,
      },
    });
    if (bill >= 0) {
      this.redirectToBill(item);
    }
  };

  redirectToBill = item => {
    const {ShopHomeStore} = this.props;
    const {abortBill, waitingBill} = values.typeNotificationShop;
    setTimeout(() => {
      switch (item.title) {
        case abortBill:
          ShopHomeStore.setTabBillSelected(values.bill.abort);
          break;
        case waitingBill:
          ShopHomeStore.setTabBillSelected(values.bill.waiting);
          break;
        default:
          break;
      }
    }, 200);
    Navigation.mergeOptions(this.props.componentId, {
      bottomTabs: {
        currentTabIndex: 2,
      },
    });
  };

  onRefresh = () => {
    this.setState({
      refreshing: true,
    });
    this.initData();
    this.setState({
      refreshing: false,
    });
  };

  onPressChat = () => {
    pushScreen(this.props.componentId, 'ListRoomShop');
  };

  onLoadMore = async () => {
    const {page} = this.state;
    const {ShopHomeStore} = this.props;
    this.setState({
      isLoadding: true,
    });
    const currentPage = page + 1;
    const id = ShopHomeStore.shopInfo.id;
    const data = await shopService.getListNotifiShop(id, currentPage, 10);
    if (
      data.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      if (data.result.notificationShopDTOs.length > 0) {
        this.setState({
          page: currentPage,
        });
        ShopHomeStore.setNotification(
          ShopHomeStore.notification,
          data.result.notificationShopDTOs,
        );
      }
      this.setState({
        isLoadding: false,
      });
    } else {
      this.setState({
        isLoadding: false,
      });
      notifyMessage(data.message);
    }
  };

  render() {
    const {ShopHomeStore} = this.props;
    const {refreshing, isLoadding} = this.state;
    return (
      <ViewSafeArea viewStyle={styles.container}>
        <TopBarChatCart
          childLeft={<Text style={styles.txtTopBar}>Thông báo</Text>}
          showCart={false}
          onPressChat={this.onPressChat}
        />
        <FlatList
          data={ShopHomeStore.notification}
          keyExtractor={(item, index) => `${index}`}
          renderItem={({item, index}) => (
            <RenderItem
              item={item}
              index={index}
              onPressItem={this.onPressItem}
            />
          )}
          refreshing={refreshing}
          onRefresh={this.onRefresh}
          onMomentumScrollEnd={this.onLoadMore}
        />
        {isLoadding && (
          <View style={styles.loading}>
            <ActivityIndicator size="large" color={color.agree} />
          </View>
        )}
      </ViewSafeArea>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.background,
  },
  txtTopBar: {marginLeft: 10, fontSize: 18, fontWeight: 'bold'},
  loading: {
    width: '100%',
    position: 'absolute',
    bottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
export default Notification;
