import React, {Component} from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import {
  ViewSafeArea,
  TopBar,
  SliderImage,
  TouchablePreventDouble,
  AlertCustomize,
  ShowImage,
} from '../../../common/base';
import {TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Navigation} from 'react-native-navigation';
import {color, values} from '../../../constant';
import GeneralInfo from './GeneralInfo';
import SizeColors from './SizeColors';
import DetailInfo from './DetailInfo';
import Comment from './Comment';
import {inject, observer} from 'mobx-react';
import ProductShopService from '../../../services/ProductShopService';
import {showModal} from '../../../config/NavigationConfig';

@inject('ShopProductStore', 'ShopHomeStore')
@observer
class ShopProductDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      productDetail: null,
      showImage: false,
      indexImage: 0,
    };
  }

  async UNSAFE_componentWillMount() {
    const {item, ShopHomeStore} = this.props;
    const product = await ProductShopService.getDetailProduct(item.id);
    if (product.message === values.messageSuccess) {
      ShopHomeStore.setProduct(product.result);
      this.setState({
        productDetail: product.result,
      });
    }
  }

  onPressBack = () => {
    Navigation.pop(this.props.componentId);
  };

  onPressEdit = () => {
    const {productDetail} = this.state;
    const {ShopProductStore} = this.props;
    ShopProductStore.productInfo = productDetail;
    const passProps = {
      productDetail: productDetail,
    };
    showModal('UpdateProductInfo', passProps);
  };

  onPressDelete = () => {
    this.setState({
      showModal: true,
    });
  };

  onPressCanel = () => {
    this.setState({
      showModal: false,
    });
  };

  onAgreeDelete = async () => {
    const {item, index, ShopHomeStore} = this.props;
    const res = await ProductShopService.deleteProduct(item.detailId);
    if (
      res.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      ShopHomeStore.listProduct.splice(index, 1);
      Navigation.pop(this.props.componentId);
    }
  };

  onPressItem = (item, index) => {
    this.setState({
      indexImage: index,
      showImage: true,
    });
  };

  dismissImage = () => {
    this.setState({
      showImage: false,
    });
  };

  renderRightTopbar = () => {
    return (
      <View style={styles.containerRightTobbar}>
        <TouchableOpacity style={styles.btnTopBar} onPress={this.onPressDelete}>
          <Icon name="trash" size={25} color="red" />
        </TouchableOpacity>
        <TouchablePreventDouble
          style={styles.btnTopBar}
          onPress={this.onPressEdit}>
          <Icon name="edit" size={25} color={color.agree} />
        </TouchablePreventDouble>
      </View>
    );
  };

  render() {
    const {ShopHomeStore} = this.props;
    const {showModal, showImage, indexImage} = this.state;
    const productDetail = ShopHomeStore.productDetail;
    return (
      <ViewSafeArea>
        <View style={styles.viewTopBar}>
          {!showImage && (
            <TopBar
              childLeft={
                <TouchableOpacity onPress={this.onPressBack}>
                  <Icon name="angle-left" size={40} style={styles.btnBack} />
                </TouchableOpacity>
              }
              childCenter={<Text style={styles.title}>Ten san pham</Text>}
              childRight={this.renderRightTopbar()}
            />
          )}
        </View>
        {productDetail && (
          <ScrollView style={{flex: 1}}>
            <SliderImage
              arrImage={productDetail.images}
              keyItem={'image'}
              onPressItem={this.onPressItem}
            />
            <GeneralInfo productInfo={productDetail} />
            <SizeColors productInfo={productDetail} />
            <DetailInfo productInfo={productDetail} />
            <Comment productInfo={productDetail} />
          </ScrollView>
        )}
        <AlertCustomize
          visible={showModal}
          title="Xoá sản phẩm"
          content="Bạn có muốn xoá sản phẩm này khỏi kho hàng"
          onCancel={this.onPressCanel}
          onPressLeft={this.onPressCanel}
          onPressRight={this.onAgreeDelete}
        />
        {showImage && (
          <ShowImage
            arrImage={productDetail.images}
            onPressCancel={this.dismissImage}
            keyItem="image"
            firstItem={indexImage}
          />
        )}
      </ViewSafeArea>
    );
  }
}

const styles = StyleSheet.create({
  btnBack: {marginLeft: 10},
  title: {fontSize: 16, textAlign: 'center'},
  viewTopBar: {
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: color.lightGrayColor,
  },
  containerRightTobbar: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingRight: 10,
  },
  btnTopBar: {alignItems: 'center', justifyContent: 'center', marginLeft: 15},
});
export default ShopProductDetail;
