import React, { useState } from 'react';
import { View, Text, StyleSheet, Animated, TouchableOpacity } from 'react-native';
import { color } from '../../../constant';
import { formatCurrency } from '../../../constant/Format';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';

class RenderItem extends React.PureComponent {
    render() {
        const { item, index, productInfo } = this.props;
        const length = productInfo.sizeColors.length;
        return (
            <View style={{ ...styles.containerItem, borderBottomWidth: index === length - 1 ? 0 : 1 }}>
                <View style={styles.viewContent}>
                    <Text style={styles.key}>Kích thước</Text>
                    <Text style={styles.value}>{item.size}</Text>
                </View>
                <View style={styles.viewContent}>
                    <Text style={styles.key}>Màu</Text>
                    <Text style={styles.value}>{item.color}</Text>
                </View>
                <View style={styles.viewContent}>
                    <Text style={styles.key}>Số lượng</Text>
                    <Text style={styles.value}>{item.quantity}</Text>
                </View>
                <View style={styles.viewContent}>
                    <Text style={styles.key}>Giá mua</Text>
                    <Text style={styles.value}>{`${formatCurrency(item.price)} VNĐ`}</Text>
                </View>
                {/* <View style={styles.viewContent}>
                    <Text style={styles.key}>Giá thuê</Text>
                    <Text style={styles.value}>{`${formatCurrency(item.rentPrice)}/ngày`}</Text>
                </View>
                <View style={styles.viewContent}>
                    <Text style={styles.key}>Đặt cọc</Text>
                    <Text style={styles.value}>{`${formatCurrency(item.deposit)} VNĐ`}</Text>
                </View> */}
            </View>
        );
    }
}

const SizeColors = ({ productInfo }) => {
    const length = productInfo.sizeColors.length;
    const [heightDescribe, setHeightDescribe] = useState(0);
    const [isShowMore, setShowMore] = useState(false);
    const [animatedDescribe] = useState(new Animated.Value(0));
    const showmore = heightDescribe >= 350 ? true : false;

    const _onPressMore = () => {
        if (isShowMore) {
            setShowMore(false)
            Animated.timing(animatedDescribe, {
                toValue: 0,
                duration: 500,
                useNativeDriver: false,
            }).start();
        } else {
            setShowMore(true)
            Animated.timing(animatedDescribe, {
                toValue: 1,
                duration: 500,
                useNativeDriver: false,
            }).start();
        }
    };

    const onLayout = e => {
        setHeightDescribe(e.nativeEvent.layout.height);
    };

    const height = animatedDescribe.interpolate({
        inputRange: [0, 1],
        outputRange: [350, heightDescribe + 50],
    });

    return (
        <View style={styles.container}>
            <Animated.View
                style={{
                    ...styles.viewDescription,
                    ...{ height: showmore ? height : null },
                }}>
                <Text style={styles.title}>Màu và kích thước</Text>
                <View onLayout={onLayout}>
                    {productInfo.sizeColors.map((item, index) =>
                        <RenderItem item={item} index={index} productInfo={productInfo} />
                    )}
                </View>
            </Animated.View>
            {showmore && (
                <LinearGradient
                    style={styles.viewMore}
                    colors={['#ffffff30', '#ffffff', '#ffffff']}>
                    <TouchableOpacity
                        style={styles.btnMore}
                        activeOpacity={0.7}
                        onPress={_onPressMore}>
                        <Text style={styles.txtMore}>
                            {!isShowMore ? 'Xem thêm' : 'Rút gọn'}
                        </Text>
                        <Icon
                            name={!isShowMore ? 'angle-down' : 'angle-up'}
                            size={15}
                            color="#000"
                        />
                    </TouchableOpacity>
                </LinearGradient>
            )}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        borderTopWidth: 0.5,
        borderBottomWidth: 1,
        marginTop: 10,
        borderColor: color.lightGrayColor,
        paddingHorizontal: 10,
        paddingVertical: 5,
        backgroundColor: '#fff'
    },
    title: {
        fontSize: 18,
        color: color.agree,
    },
    viewContent: { width: '100%', flexDirection: 'row' },
    key: { color: '#999999', flex: 1 },
    value: { flex: 2 },
    containerItem: { paddingVertical: 10, borderColor: color.lightGrayColor },
    txtTitle: { fontSize: 16, color: color.agree },
    viewTitleInfo: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    viewMore: {
        width: '100%',
        height: 30,
        position: 'absolute',
        bottom: 0,
    },
    btnMore: {
        height: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    txtMore: { color: '#000', marginRight: 10 },
    viewDescription: {
        width: '100%',
        overflow: 'hidden',
    },
})

export default SizeColors;