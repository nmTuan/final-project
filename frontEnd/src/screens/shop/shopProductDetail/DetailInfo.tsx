import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { color } from '../../../constant';
import { Animated } from 'react-native';
import { useState } from 'react';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';

const DetailInfo = ({ productInfo }) => {
    const [heightDescribe, setHeightDescribe] = useState(0);
    const [isShowMore, setShowMore] = useState(false);
    const [animatedDescribe] = useState(new Animated.Value(0));
    const showmore = heightDescribe >= 50 ? true : false;

    const _onPressMore = () => {
        if (isShowMore) {
            setShowMore(false)
            Animated.timing(animatedDescribe, {
                toValue: 0,
                duration: 500,
                useNativeDriver: false,
            }).start();
        } else {
            setShowMore(true)
            Animated.timing(animatedDescribe, {
                toValue: 1,
                duration: 500,
                useNativeDriver: false,
            }).start();
        }
    };

    const onLayout = e => {
        setHeightDescribe(e.nativeEvent.layout.height);
    };

    const height = animatedDescribe.interpolate({
        inputRange: [0, 1],
        outputRange: [90, heightDescribe + 50],
    });
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Chi tiết sản phẩm</Text>
            <View style={styles.viewContent}>
                <Text style={styles.key}>Thương hiệu</Text>
                <Text style={styles.value} numberOfLines={1}>{productInfo.brand}</Text>
            </View>
            <View style={styles.viewContent}>
                <Text style={styles.key}>Thể loại</Text>
                <Text style={styles.value} numberOfLines={1}>{productInfo.category}</Text>
            </View>
            <View style={styles.viewContent}>
                <Text style={styles.key}>Độ tuổi phù hợp</Text>
                <Text style={styles.value} numberOfLines={1}>{productInfo.age}</Text>
            </View>
            <View style={styles.viewContent}>
                <Text style={styles.key}>Giới tinh</Text>
                <Text style={styles.value} numberOfLines={1}>{productInfo.gender}</Text>
            </View>
            <View style={styles.containerDescrestion}>
                <Animated.View
                    style={{
                        ...styles.viewDescription,
                        ...{ height: showmore ? height : null },
                    }}>
                    <Text>Mô tả</Text>
                    <Text onLayout={onLayout} style={{ flexWrap: 'wrap' }}>
                        {productInfo.description}
                    </Text>
                </Animated.View>
                {showmore && (
                    <LinearGradient
                        style={styles.viewMore}
                        colors={['#ffffff30', '#ffffff', '#ffffff']}>
                        <TouchableOpacity
                            style={styles.btnMore}
                            activeOpacity={0.7}
                            onPress={_onPressMore}>
                            <Text style={styles.txtMore}>
                                {!isShowMore ? 'Xem thêm' : 'Rút gọn'}
                            </Text>
                            <Icon
                                name={!isShowMore ? 'angle-down' : 'angle-up'}
                                size={15}
                                color="#000"
                            />
                        </TouchableOpacity>
                    </LinearGradient>
                )}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        borderTopWidth: 0.5,
        borderBottomWidth: 1,
        marginTop: 10,
        borderColor: color.lightGrayColor,
        paddingHorizontal: 10,
        paddingVertical: 5,
        backgroundColor: '#fff'
    },
    title: {
        fontSize: 18,
        color: color.agree,
    },
    viewContent: { width: '100%', flexDirection: 'row', paddingTop: 5 },
    key: { color: '#999999', flex: 1 },
    value: { flex: 2 },
    containerDescrestion: { paddingTop: 10 },
    txtTitle: { fontSize: 16, color: color.agree },
    viewTitleInfo: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    viewMore: {
        width: '100%',
        height: 30,
        position: 'absolute',
        bottom: 0,
    },
    btnMore: {
        height: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    txtMore: { color: '#000', marginRight: 10 },
    viewDescription: {
        width: '100%',
        overflow: 'hidden',
    },
})

export default DetailInfo;