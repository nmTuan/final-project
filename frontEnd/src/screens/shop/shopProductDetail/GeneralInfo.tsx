import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { color } from '../../../constant';
import { Rating } from 'react-native-elements';
import { formatCurrency } from '../../../constant/Format';

const GeneralInfo = ({ productInfo }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.productName}>{productInfo.productName}</Text>
            <Text style={styles.price}>
                {productInfo.minPrice > 0 ?
                    `${formatCurrency(productInfo.minPrice)} - ${formatCurrency(productInfo.maxPrice)}`
                    :
                    `${formatCurrency(productInfo.maxPrice)}`
                }
            </Text>
            {
                productInfo.oldPrice ? (
                    <Text style={styles.oldPrice}>{formatCurrency(productInfo.oldPrice)}</Text>
                )
                : (<></>)
            }
            <View style={styles.viewRating}>
                <Rating startingValue={productInfo.vote} imageSize={20} />
                <Text style={{ marginLeft: 40 }}>{`Đã bán: ${productInfo.sold}`}</Text>
            </View>
        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        borderTopWidth: 0.5,
        borderBottomWidth: 1,
        marginTop: 10,
        borderColor: color.lightGrayColor,
        paddingHorizontal: 10,
        paddingVertical: 5,
        backgroundColor: '#fff'
    },
    productName: {
        fontSize: 18
    },
    price: {
        fontSize: 16,
        color: color.agree,
        marginTop: 5,
    },
    oldPrice: {
        color: 'gray',
        textDecorationLine: 'line-through',
    },
    viewRating: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 10
    }
})

export default GeneralInfo;