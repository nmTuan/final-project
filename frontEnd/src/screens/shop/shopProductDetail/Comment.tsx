import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Rating } from 'react-native-elements';
import { color, values } from '../../../constant';
import { UserService } from '../../../services/UserService';
import moment from 'moment';

class _RenderItem extends React.PureComponent {
    render() {
        const { item, index } = this.props;
        return (
            <View style={styles.containerItem} key={`${index}`}>
                <View style={{ flex: 1 }}>
                    <View style={styles.viewAvatar}>
                        {
                            item && item.avatar !== '' ?
                                <Image source={{ uri: `${values.domain}${item.avatar}` }} />
                                :
                                <Icon name="user" size={30} />
                        }
                    </View>
                </View>
                <View style={{ flex: 4 }}>
                    <View style={styles.viewUsername}>
                        <Text>{item.username}</Text>
                        <Rating startingValue={item.vote} imageSize={15} />
                    </View>
                    <View>
                        <Text style={{ flexWrap: 'wrap', fontSize: 14 }}>{item.content}</Text>
                        <Text style={{ color: 'gray' }}>
                            {moment(item.createdDate).format('DD/MM/YYYY')}
                        </Text>
                    </View>
                </View>
            </View>
        )
    }
}

const Comment = ({ productInfo }) => {
    const [data, setData] = useState([]);
    const [size, setSize] = useState(0);

    useEffect(() => {
        getData();
    }, []);

    const getData = async () => {
        const res = await UserService.getListFeedback(productInfo.id, '');
        if (res.message === values.messageSuccess) {
            setData(res.result.result);
            setSize(res.result.size);
        }
    }
    return (
        <View style={styles.container}>
            <View style={styles.viewTitle}>
                <Text style={styles.txtNameProduct}>Đánh giá</Text>
                <Text style={styles.txtNameProduct}>{`${size} lượt Đánh giá`}</Text>
            </View>
            {data.map((item, index) => <_RenderItem item={item} index={index} />)}
            {/* <TouchableOpacity
                activeOpacity={0.7}
                style={styles.btnMore}>
                <Text style={styles.txtMore}>Xem thêm</Text>
                <Icon name={'angle-right'} size={15} color="#000" />
            </TouchableOpacity> */}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        borderTopWidth: 0.5,
        borderBottomWidth: 1,
        marginTop: 10,
        borderColor: color.lightGrayColor,
        paddingHorizontal: 10,
        paddingVertical: 5,
        backgroundColor: '#fff'
    },
    txtNameProduct: { fontSize: 14, fontWeight: '600' },
    viewTitle: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: 10,
    },
    btnMore: {
        width: '100%',
        height: 30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'flex-end',
        marginBottom: 5,
    },
    txtMore: { color: '#000', marginRight: 10 },
    containerItem: { width: '100%', flexDirection: 'row', paddingVertical: 5 },
    viewAvatar: {
        width: 50,
        height: 50,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: color.lightGrayColor,
        borderWidth: 1,
        overflow: 'hidden',
    },
    viewUsername: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
});

export default Comment;