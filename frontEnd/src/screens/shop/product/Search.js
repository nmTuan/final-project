import React, {Component} from 'react';
import {View, Text, StyleSheet, FlatList, TouchableOpacity} from 'react-native';
import SearchTopBar from '../../searchTopBar';
import {color} from '../../../constant';

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      search: '',
    };
  }

  onChangeText = async value => {
    this.setState({
      search: value,
    });
  };

  _renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        style={styles.viewItem}
        onPress={() => this.onPressItem(item)}>
        <Text>{item.productName}</Text>
      </TouchableOpacity>
    );
  };

  render() {
    const {search, data} = this.state;
    return (
      <View style={styles.container}>
        <SearchTopBar
          searchOnly
          cancel={this.props.cancel}
          search={search}
          onChangeText={this.onChangeText}
        />
        <View style={styles.containerItem}>
          <FlatList
            data={data}
            keyExtractor={(item, index) => `${index}`}
            renderItem={this._renderItem}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: color.lightGrayColor},
  viewItem: {
    padding: 10,
    borderBottomColor: color.lightGrayColor,
    borderBottomWidth: 1,
  },
  containerItem: {backgroundColor: '#fff', marginTop: 5},
});
export default Search;
