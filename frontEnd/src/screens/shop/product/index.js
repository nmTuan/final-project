import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Animated,
  RefreshControl,
  ActivityIndicator,
  Modal,
  Text,
} from 'react-native';
import {values, color} from '../../../constant';
import {ViewSafeArea, TouchablePreventDouble} from '../../../common/base';
import SearchTopBar from '../../searchTopBar';
import RenderItem from './RenderItem';
import {pushScreen, showModal} from '../../../config/NavigationConfig';
import ViewAnimationTopBar from '../../../common/base/ViewAnimationTopBar';
import Search from './Search';
import Icon from 'react-native-vector-icons/FontAwesome';
import {shopService} from '../../../services/ShopService';
import {inject, observer} from 'mobx-react';

@inject('ShopHomeStore')
@observer
class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPosition: 0,
      currentStateSort: values.sortState.normal,
      currentSelected: values.selected.all,
      scrollY: new Animated.Value(0),
      isShowFilter: false,
      scrolling: 0,
      page: 0,
      refresh: false,
      isLoadmore: false,
      isShowSearch: false,
      isLoadding: false,
    };
  }

  componentDidMount() {
    this.getListProduct();
  }

  async getListProduct() {
    const {ShopHomeStore} = this.props;
    const shopInfo = ShopHomeStore.shopInfo;
    const data = await shopService.getProductByShopId(shopInfo.id);
    if (data.result.result) {
      ShopHomeStore.setListProduct([], data.result.result);
    }
  }

  onFocus = () => {
    this.search.blur();
    this.setState({
      isShowSearch: true,
    });
  };

  dissmissSearch = () => {
    this.setState({
      isShowSearch: false,
    });
  };

  onPressChat = () => {
    pushScreen(this.props.componentId, 'ListRoomShop');
  };

  renderTopBar() {
    return (
      <SearchTopBar
        ref={ref => (this.search = ref)}
        animationSearch={this.state.scrollY}
        onPressChat={this.onPressChat}
        onFocus={this.onFocus}
        showCart={false}
      />
    );
  }

  onScrollEndDrag = ({nativeEvent}) => {
    if (nativeEvent.contentOffset.y > 25) {
      Animated.timing(this.state.scrollY, {
        toValue: values.topBarHeight,
        duration: 500,
        useNativeDriver: false,
      }).start();
    } else {
      Animated.timing(this.state.scrollY, {
        toValue: 0,
        duration: 500,
        useNativeDriver: false,
      }).start();
    }
  };

  onRefresh = () => {
    this.setState({
      refresh: true,
    });
    this.getListProduct();
    this.setState({
      refresh: false,
      page: 0,
    });
  };

  onPressProduct = (item, index) => {
    this.setState({
      isLoadding: true,
    });
    const passProps = {
      item,
      index,
    };
    pushScreen(this.props.componentId, 'ShopProductDetail', passProps);
    setTimeout(() => {
      this.setState({
        isLoadding: false,
      });
    }, 500);
  };

  onPressAdd = () => {
    showModal('AddNewProduct');
  };

  render() {
    const {ShopHomeStore} = this.props;
    const {refresh, isLoadmore, isShowSearch, isLoadding} = this.state;
    const top = Animated.diffClamp(
      this.state.scrollY,
      0,
      values.topBarHeight,
    ).interpolate({
      inputRange: [0, values.topBarHeight],
      outputRange: [0, -values.topBarHeight],
      extrapolate: 'clamp',
    });
    const backgroundColor = Animated.diffClamp(
      this.state.scrollY,
      0,
      values.topBarHeight,
    ).interpolate({
      inputRange: [0, values.topBarHeight],
      outputRange: ['#fff', 'transparent'],
    });
    return (
      <ViewSafeArea viewStyle={styles.container}>
        <ViewAnimationTopBar
          topBar={this.renderTopBar()}
          scrollY={this.state.scrollY}>
          <Animated.ScrollView
            style={{flex: 1}}
            bounces={false}
            scrollEventThrottle={16}
            onMomentumScrollEnd={this.onScrollEnd}
            onScrollEndDrag={this.onScrollEndDrag}
            refreshControl={
              <RefreshControl
                refreshing={refresh}
                onRefresh={this.onRefresh}
                tintColor="#00ff00"
              />
            }
            onScroll={Animated.event(
              [
                {
                  nativeEvent: {
                    contentOffset: {y: this.state.scrollY},
                  },
                },
              ],
              {
                useNativeDriver: false,
              },
            )}>
            <View style={{paddingTop: values.topBarHeight + 60}}>
              <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                {ShopHomeStore.listProduct.map((item, index) => {
                  return (
                    <RenderItem
                      item={item}
                      index={index}
                      componentId={this.props.componentId}
                      onPressProduct={this.onPressProduct}
                    />
                  );
                })}
              </View>
            </View>
          </Animated.ScrollView>
          <Animated.View
            style={{
              ...styles.containerFilter,
              backgroundColor,
              transform: [
                {
                  translateY: top,
                },
              ],
            }}>
            <View style={styles.viewBtnAdd}>
              <TouchablePreventDouble
                style={styles.btnAdd}
                onPress={this.onPressAdd}>
                <Icon name="plus" size={20} color="#fff" />
                <Text style={styles.txtAdd}>Thêm sản phẩm</Text>
              </TouchablePreventDouble>
            </View>
          </Animated.View>
        </ViewAnimationTopBar>
        {isLoadmore && (
          <View style={styles.viewActivityIndicator}>
            <ActivityIndicator size="large" color="#00ff00" />
          </View>
        )}
        <Modal visible={isShowSearch}>
          {isShowSearch && (
            <ViewSafeArea>
              <Search cancel={this.dissmissSearch} />
            </ViewSafeArea>
          )}
        </Modal>
        {isLoadding && (
          <View style={styles.viewLoadding}>
            <ActivityIndicator size="large" color={color.agree} />
          </View>
        )}
      </ViewSafeArea>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.background,
  },
  viewActivityIndicator: {
    position: 'absolute',
    bottom: 10,
    width: '100%',
    alignItems: 'center',
  },
  containerFilter: {
    position: 'absolute',
    top: values.topBarHeight,
    flex: 1,
    width: '100%',
  },
  viewBtnAdd: {
    width: '100%',
    height: 45,
    backgroundColor: 'transparent',
    marginTop: 10,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: 10,
  },
  btnAdd: {
    height: 40,
    paddingHorizontal: 10,
    backgroundColor: color.agree,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 6,
  },
  txtAdd: {
    color: '#fff',
    marginLeft: 5,
  },
  viewLoadding: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    alignContent: 'center',
    justifyContent: 'center',
  },
});

export default Product;
