import React, {Component} from 'react';
import EditProduct from '../editProduct';
import {inject, observer} from 'mobx-react';
import {notifyMessage} from '../../../config/Func';
import ProductShopService from '../../../services/ProductShopService';
import {values} from '../../../constant';
import {View} from 'react-native';
import {AlertCustomize} from '../../../common/base';

@inject('ShopProductStore', 'ShopHomeStore')
@observer
class UpdateProductInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      indexSizeColor: -1,
    };
  }

  onPressUpdate = product => {
    const updateInfo = this.updateInfoGeneral(product);
    const updateImage = this.updateImage(product);
    const updateSizeColor = this.updateSizeColor(product);
    if (!updateInfo && !updateImage && !updateSizeColor) {
      notifyMessage('Không có thông tin nào thay đổi');
    } else {
      alert('Cập nhật thành công');
    }
  };

  updateInfoGeneral = async product => {
    const {productDetail, ShopHomeStore} = this.props;
    const {
      id,
      thumbnail,
      productName,
      shortDescription,
      isRent,
      age_id,
      category_id,
      gender_id,
      minPrice,
      maxPrice,
      oldPrice,
      quantity,
      description,
      brand,
      material,
      bankId,
    } = product;
    if (
      thumbnail !== productDetail.thumbnail ||
      productName !== productDetail.productName ||
      shortDescription !== productDetail.shortDescription ||
      isRent !== productDetail.isRent ||
      age_id !== productDetail.age_id ||
      category_id !== productDetail.category_id ||
      gender_id !== productDetail.gender_id ||
      minPrice !== productDetail.minPrice ||
      maxPrice !== productDetail.maxPrice ||
      oldPrice !== productDetail.oldPrice ||
      quantity !== productDetail.quantity ||
      description !== productDetail.description ||
      brand !== productDetail.brand ||
      material !== productDetail.material ||
      bankId > 0
    ) {
      const body = {
        productName,
        thumbnail,
        shortDescription,
        minPrice,
        maxPrice,
        oldPrice,
        quantity,
        description,
        brand,
        category_id,
        gender_id,
        age_id,
        isRent,
        bankId,
      };
      const res = await ProductShopService.updateProductInfo(id, body);
      if (
        res.message.toLocaleLowerCase() ===
        values.messageSuccess.toLocaleLowerCase()
      ) {
        ShopHomeStore.setProduct(product);
        return true;
      } else {
        alert(res.message);
      }
    } else {
      return false;
    }
  };

  updateImage = async product => {
    const {productDetail, ShopHomeStore} = this.props;
    const images = product.images;
    var update = false;
    const arrImage = [];
    for (let i = 0; i < images.length; i++) {
      if (productDetail.images[i]) {
        if (images[i].name !== productDetail.images[i].name) {
          const res = await ProductShopService.updateImage(
            productDetail.images[i].id,
            images[i].image,
            images[i].name,
          );
          if (
            res.message.toLocaleLowerCase() ===
            values.messageSuccess.toLocaleLowerCase()
          ) {
            ShopHomeStore.setProduct(product);
            update = true;
          }
        }
      } else {
        const image = {
          name: images[i].name,
          image: images[i].image,
        };
        arrImage.push(image);
      }
    }
    if (arrImage.length > 0) {
      const res = await ProductShopService.addNewImage(
        productDetail.id,
        arrImage,
      );
      if (
        res.message.toLocaleLowerCase() ===
        values.messageSuccess.toLocaleLowerCase()
      ) {
        ShopHomeStore.setProduct(product);
        update = true;
      }
    }
    return update;
  };

  updateSizeColor = async product => {
    const {ShopHomeStore, productDetail} = this.props;
    const {sizeColors} = productDetail;
    const newSizeColors = product.sizeColors;
    var update = false;
    const arrSizeColor = [];
    for (let i = 0; i < newSizeColors.length; i++) {
      const sizeColor = {
        size: newSizeColors[i].size,
        color: newSizeColors[i].color,
        quantity: newSizeColors[i].quantity,
        price: newSizeColors[i].price,
        rendueId: '1',
        rentPrice: newSizeColors[i].rentPrice,
        Deposit: newSizeColors[i].deposit,
      };
      if (sizeColors[i]) {
        const {size, color, rentPrice, deposit, quantity, price} = sizeColors[
          i
        ];
        if (
          newSizeColors[i].size !== size ||
          newSizeColors[i].color !== color ||
          newSizeColors[i].rentPrice !== rentPrice ||
          newSizeColors[i].deposit !== deposit ||
          newSizeColors[i].quantity !== quantity ||
          newSizeColors[i].price !== price
        ) {
          const res = await ProductShopService.updateSizeColor(
            sizeColors[i].id,
            sizeColor,
          );
          if (
            res.message.toLocaleLowerCase() ===
            values.messageSuccess.toLocaleLowerCase()
          ) {
            ShopHomeStore.setProduct(product);
            update = true;
          }
        }
      } else {
        arrSizeColor.push(sizeColor);
      }
    }
    if (arrSizeColor.length > 0) {
      const res = await ProductShopService.addNewSizeColor(
        productDetail.id,
        arrSizeColor,
      );
      if (
        res.message.toLocaleLowerCase() ===
        values.messageSuccess.toLocaleLowerCase()
      ) {
        ShopHomeStore.setProduct(product);
        update = true;
      }
    }
    return update;
  };

  onPressDeleteSizeColor = index => {
    this.setState({
      indexSizeColor: index,
      showModal: true,
    });
  };

  dissmissModal = () => {
    this.setState({
      showModal: false,
    });
  };

  onDeleteSizeColor = async () => {
    const {ShopProductStore, ShopHomeStore} = this.props;
    const {indexSizeColor} = this.state;
    const sizeColorId =
      ShopProductStore.productInfo.sizeColors[indexSizeColor].id;
    const res = await ProductShopService.deleteSizeColor(sizeColorId);
    if (
      res.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      const productInfo = ShopProductStore.productInfo;
      productInfo.sizeColors.splice(indexSizeColor, 1);
      ShopHomeStore.setProduct(productInfo);
      this.dissmissModal();
      notifyMessage('Xoá thành công');
    }
  };

  render() {
    const {showModal, indexSizeColor} = this.state;
    const {ShopProductStore, ShopHomeStore, componentId} = this.props;
    const sizeColor = ShopProductStore.productInfo.sizeColors;
    const content =
      indexSizeColor >= 0
        ? `Bạn có muốn xoá size ${sizeColor[indexSizeColor].size} màu ${
            sizeColor[indexSizeColor].color
          }?`
        : '';
    return (
      <View style={{flex: 1}}>
        <EditProduct
          ShopProductStore={ShopProductStore}
          ShopHomeStore={ShopHomeStore}
          componentId={componentId}
          onPressSave={this.onPressUpdate}
          txtBtn="Cập nhật"
          onPressDeleteSizeColor={this.onPressDeleteSizeColor}
        />
        <AlertCustomize
          visible={showModal}
          title="Xoá màu và kích cỡ"
          content={content}
          onCancel={this.dissmissModal}
          onPressLeft={this.dissmissModal}
          onPressRight={this.onDeleteSizeColor}
        />
      </View>
    );
  }
}

export default UpdateProductInfo;
