import React, {Component} from 'react';
import {View, Text, Animated, TouchableOpacity, StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import {color} from '../../../constant';
import {Navigation} from 'react-native-navigation';
import {observer} from 'mobx-react';
import { showModal } from '../../../config/NavigationConfig';

@observer
class ShopInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animatedDescribe: new Animated.Value(0),
      heightDescribe: 0,
      isShowMore: false,
    };
  }

  _onPressMore = () => {
    if (this.state.isShowMore) {
      this.setState({
        isShowMore: false,
      });
      Animated.timing(this.state.animatedDescribe, {
        toValue: 0,
        duration: 500,
        useNativeDriver: false,
      }).start();
    } else {
      this.setState({
        isShowMore: true,
      });
      Animated.timing(this.state.animatedDescribe, {
        toValue: 1,
        duration: 500,
        useNativeDriver: false,
      }).start();
    }
  };

  onLayout = e => {
    this.setState({
      heightDescribe: e.nativeEvent.layout.height,
    });
  };

  onPressEdit = () => {
    showModal('UpdateShopInfo');
  };

  render() {
    const {sizeAva, ShopHomeStore} = this.props;
    const {heightDescribe, isShowMore} = this.state;
    const shopInfo = ShopHomeStore.shopInfo;
    const height = this.state.animatedDescribe.interpolate({
      inputRange: [0, 1],
      outputRange: [90, this.state.heightDescribe + 50],
    });
    const showmore = heightDescribe >= 50 ? true : false;
    return (
      <View style={{...styles.inner, ...{marginTop: sizeAva}}}>
        <View style={styles.viewTitleInfo}>
          <Text style={styles.txtTitle}>Thông tin cửa hàng</Text>
          <TouchableOpacity onPress={this.onPressEdit}>
            <Icon name="edit" size={20} />
          </TouchableOpacity>
        </View>
        <View>
          <View style={styles.containerTextInput}>
            <View>
              <View style={styles.infor}>
                <Text>Số điện thoại</Text>
                <Text>{shopInfo.phoneNumber}</Text>
              </View>
              <View style={styles.viewShopAddress}>
                <Text>Địa chỉ chi tiết</Text>
                <Text style={styles.txtShopAddress}>{shopInfo.address}</Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.containerDescrestion}>
          <Animated.View
            style={{
              ...styles.viewDescription,
              ...{height: showmore ? height : null},
            }}>
            <Text>Mô tả</Text>
            <Text onLayout={this.onLayout} style={{flexWrap: 'wrap'}}>
              {shopInfo.description}
            </Text>
          </Animated.View>
          {showmore && (
            <LinearGradient
              style={styles.viewMore}
              colors={['#ffffff30', '#ffffff', '#ffffff']}>
              <TouchableOpacity
                style={styles.btnMore}
                activeOpacity={0.7}
                onPress={this._onPressMore}>
                <Text style={styles.txtMore}>
                  {!isShowMore ? 'Xem thêm' : 'Rút gọn'}
                </Text>
                <Icon
                  name={!isShowMore ? 'angle-down' : 'angle-up'}
                  size={15}
                  color="#000"
                />
              </TouchableOpacity>
            </LinearGradient>
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  shopName: {marginBottom: 15, marginLeft: 10, fontSize: 16},
  btnSignout: {paddingBottom: 12},
  inner: {
    width: '100%',
    backgroundColor: '#fff',
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginTop: 20,
  },
  infor: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    borderBottomWidth: 1,
    borderColor: color.lightGrayColor,
    paddingTop: 10,
  },
  viewShopAddress: {
    paddingTop: 10,
    borderBottomWidth: 1,
    borderColor: color.lightGrayColor,
  },
  txtShopAddress: {marginTop: 5},
  viewMore: {
    width: '100%',
    height: 30,
    position: 'absolute',
    bottom: 0,
  },
  btnMore: {
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  txtMore: {color: '#000', marginRight: 10},
  viewDescription: {
    width: '100%',
    overflow: 'hidden',
  },
  containerDescrestion: {paddingTop: 10},
  txtTitle: {fontSize: 16, color: color.agree},
  viewTitleInfo: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

export default ShopInfo;
