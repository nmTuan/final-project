import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image,
  Platform,
  Alert,
} from 'react-native';
import {
  ViewSafeArea,
  TouchablePreventDouble,
  PickerScrollView,
  OptionBase,
} from '../../../common/base';
import {color, values} from '../../../constant';
import Icon from 'react-native-vector-icons/FontAwesome';
import BarChart from './Barchart';
import {inject, observer} from 'mobx-react';
import moment from 'moment';
import {Modal} from 'react-native';
import ShopInfo from './ShopInfo';
import {openLibSingleImage, openCamera} from '../../../config/ImagePicker';
import {FileService} from '../../../services/FileService';
import ImageResizer from 'react-native-image-resizer';
import {shopService} from '../../../services/ShopService';
import {ScreenConfig} from '../../../config';
import AsyncStorage from '@react-native-community/async-storage';
import {NotLogin} from '../../../navigation/Navigation';
import {getSocket} from '../../../config/Socket';
import ViewNotification from '../../../common/base/ViewNotification';
import BankingInfo from './BankingInfo';
const sizeAva = 60;
const typeIamge = {
  coverAva: 0,
  ava: 1,
};

@inject('ShopHomeStore')
@observer
class ShopHome extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      arr: [2018, 2019, 2020],
      indexSelected: 1,
      isScroll: false,
      showPikerYear: false,
      yearSelected: 2,
      showLogout: false,
      title: '',
      message: '',
      image: '',
      dataMessage: null,
    };
  }

  UNSAFE_componentWillMount() {
    this.getQuantityChat();
  }

  componentDidMount() {
    const {ShopHomeStore} = this.props;
    this.updateSocket(ShopHomeStore.shopInfo.id);
    getSocket().on('receiveMess', result => {
      this.setState({
        message: result.content,
        title: result.sender,
        image: result.senderava,
        dataMessage: result,
      });
      this.notification.getNotification();
      getSocket().on('firtJoin', roomId => {
        getSocket().emit('joinRoom', roomId);
      });
    });
  }

  getQuantityChat = async () => {
    const {ShopHomeStore} = this.props;
    const count = await shopService.countChatByShopStatus(
      ShopHomeStore.shopInfo.id,
      0,
    );
    ShopHomeStore.quantityChat = count;
  };

  updateSocket(id) {
    const {login, shop} = values.socket;
    const data = {
      type: shop, // customer
      shopId: id,
    };
    getSocket().emit(login, data);
  }

  onPressImage = (title, callback) => {
    Alert.alert(
      '',
      title,
      [
        {
          text: 'Huỷ bỏ',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'chọn ảnh thư viện',
          onPress: async () => {
            const result = await openLibSingleImage();
            callback(result);
          },
        },
        {
          text: 'Camera',
          onPress: async () => {
            const result = await openCamera();
            callback(result);
          },
        },
      ],
      {cancelable: false},
    );
  };

  onPressYear = () => {
    this.setState({
      showPikerYear: true,
    });
  };

  dismissPicker = () => {
    this.setState({
      showPikerYear: false,
    });
  };

  handleValueChange = value => {
    this.setState({
      yearSelected: value,
    });
  };

  onCancelLogout = () => {
    this.setState({
      showLogout: false,
    });
  };

  onPressLogout = () => {
    const {ShopHomeStore} = this.props;
    NotLogin();
    AsyncStorage.clear();
    setTimeout(() => {
      ShopHomeStore.resetData();
    }, 500);
  };

  onPressChangeImage = async type => {
    const {ShopHomeStore} = this.props;
    const shopInfo = ShopHomeStore.shopInfo;
    const isCoverAva = type === typeIamge.coverAva ? true : false;
    const title =
      type === typeIamge.coverAva ? 'Chọn ảnh bìa' : 'Chọn ảnh đại diện';
    this.onPressImage(title, coverAva => {
      if (coverAva) {
        ImageResizer.createResizedImage(coverAva.image, 300, 300, 'JPEG', 50)
          .then(async response => {
            const uri =
              Platform.OS === 'ios'
                ? response.uri.replace('file://', '')
                : response.uri;
            const data = await FileService.uploadFile(uri);
            if (data) {
              await shopService.updateInfoShop(
                shopInfo.id,
                '',
                '',
                !isCoverAva ? data : '',
                isCoverAva ? data : '',
                '',
                '',
              );
            }
            if (isCoverAva) {
              shopInfo.coverave = data;
            } else {
              shopInfo.avatar = data;
            }
          })
          .catch(e => {
            console.log(e);
          });
      }
    });
  };

  render() {
    const {ShopHomeStore} = this.props;
    const shopInfo = ShopHomeStore.shopInfo;
    const {
      isScroll,
      showPikerYear,
      yearSelected,
      arr,
      showLogout,
      title,
      message,
      image,
      dataMessage,
    } = this.state;
    return (
      <ViewNotification
        ref={ref => (this.notification = ref)}
        style={{backgroundColor: color.lightGrayColor}}
        data={dataMessage}
        title={title}
        content={message}
        image={image}>
        <ScrollView style={{flex: 1}} showsVerticalScrollIndicator={false}>
          <View>
            <TouchablePreventDouble
              style={styles.viewCoverava}
              onPress={() => this.onPressChangeImage(typeIamge.coverAva)}>
              {shopInfo.coverave && (
                <Image
                  source={{
                    uri: `${values.domain}${shopInfo.coverave}`,
                  }}
                  style={styles.image}
                />
              )}
            </TouchablePreventDouble>
            <View style={styles.contentTitle}>
              <View style={styles.containerNameAva}>
                <TouchablePreventDouble
                  style={styles.viewAva}
                  onPress={() => this.onPressChangeImage(typeIamge.ava)}>
                  {shopInfo.avatar ? (
                    <Image
                      source={{uri: `${values.domain}${shopInfo.avatar}`}}
                      style={styles.image}
                    />
                  ) : (
                    <Icon name="user" size={40} />
                  )}
                </TouchablePreventDouble>
                <Text style={styles.shopName}>{shopInfo.shopName}</Text>
              </View>
              <TouchableOpacity
                style={styles.btnSignout}
                onPress={() => this.setState({showLogout: true})}>
                <Icon name="sign-out" size={25} />
              </TouchableOpacity>
            </View>
          </View>
          <ShopInfo sizeAva={sizeAva} ShopHomeStore={ShopHomeStore} />
          <BankingInfo ShopHomeStore={ShopHomeStore} />
          <View style={styles.inner}>
            <Text style={styles.txtTitle}>Top 3 sản phẩm bán nhiều nhất</Text>
            {this.state.arr.map((item, index) => {
              return (
                <Text
                  style={{marginTop: 5, marginLeft: 5 + index * 5}}>{`${index +
                  1}. ${'Tên sản phẩm'}`}</Text>
              );
            })}
          </View>
          <View style={styles.inner}>
            <View style={styles.viewTitleRevenue}>
              <Text style={styles.txtTitle}>Doanh thu của năm</Text>
              <TouchablePreventDouble
                style={styles.viewSelected}
                onPress={this.onPressYear}>
                <Text>{arr[yearSelected]}</Text>
                <View style={styles.viewIconSelected}>
                  <Icon
                    name="sort-down"
                    size={20}
                    style={styles.iconSelected}
                  />
                </View>
              </TouchablePreventDouble>
            </View>
            <BarChart isScroll={isScroll} />
            <Text style={{marginTop: 10, fontSize: 16}}>
              {`Tổng doanh thu năm ${arr[yearSelected]}: 60.000.000 VNĐ`}
            </Text>
          </View>
        </ScrollView>
        <Modal visible={showPikerYear} transparent animationType="slide">
          {showPikerYear && (
            <View style={styles.backgroundModalGender}>
              <TouchablePreventDouble
                style={{flex: 1}}
                onPress={this.dismissPicker}
              />
              <View style={styles.viewChildGender}>
                <View>
                  <Text style={styles.txtTitleGender}>Giới tính</Text>
                  <TouchablePreventDouble
                    style={styles.btnSelecteGender}
                    onPress={this.dismissPicker}>
                    <Text style={styles.txtSelected}>Chọn</Text>
                  </TouchablePreventDouble>
                </View>
                <View style={styles.line} />
                <PickerScrollView
                  dataSource={this.state.arr}
                  selectedIndex={yearSelected}
                  itemHeight={105 / 3}
                  wrapperHeight={105}
                  getSelected={this.handleValueChange}
                />
              </View>
            </View>
          )}
        </Modal>
        <Modal visible={showLogout} transparent>
          {showLogout && (
            <OptionBase
              title="Bạn có chắc chắn muốn đăng xuất không?"
              textBtn1="Đăng xuất"
              onPress1={this.onPressLogout}
              onPressCancel={this.onCancelLogout}
            />
          )}
        </Modal>
      </ViewNotification>
    );
  }
}

const styles = StyleSheet.create({
  viewCoverava: {width: '100%', height: 200, backgroundColor: '#fff'},
  contentTitle: {
    width: '100%',
    height: sizeAva,
    position: 'absolute',
    bottom: 20 - sizeAva,
    paddingLeft: 20,
    paddingRight: 10,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
  },
  viewAva: {
    width: sizeAva,
    height: sizeAva,
    borderRadius: sizeAva / 2,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    overflow: 'hidden',
  },
  containerNameAva: {flexDirection: 'row', alignItems: 'flex-end'},
  shopName: {marginBottom: 15, marginLeft: 10, fontSize: 16},
  btnSignout: {paddingBottom: 12},
  inner: {
    width: '100%',
    backgroundColor: '#fff',
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginTop: 20,
  },
  txtTitle: {fontSize: 16, color: color.agree},
  viewSelected: {
    height: 30,
    width: 80,
    borderWidth: 1,
    borderColor: 'gray',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingLeft: 5,
  },
  viewIconSelected: {
    height: '100%',
    width: 25,
    borderLeftWidth: 1,
    borderColor: 'gray',
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconSelected: {marginBottom: 5},
  viewTitleRevenue: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  backgroundModalGender: {flex: 1, backgroundColor: '#00000060'},
  viewChildGender: {
    width: '100%',
    padding: 10,
    backgroundColor: '#fff',
    position: 'absolute',
    bottom: 0,
  },
  txtTitleGender: {fontSize: 18, textAlign: 'center'},
  btnSelecteGender: {
    position: 'absolute',
    right: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtSelected: {color: 'blue'},
  image: {width: '100%', height: '100%', resizeMode: 'cover'},
});

export default ShopHome;
