import React, { useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
import { color, values } from '../../../constant';
import { TouchablePreventDouble } from '../../../common/base';
import { showModal } from '../../../config/NavigationConfig';
import { shopService } from '../../../services/ShopService';

const BankingInfo = ({ ShopHomeStore }) => {
    const onPressBankInfo = () => {
        showModal('BankInformation');
    }

    useEffect(() => {
        getListBank();
    }, []);

    const getListBank = async () => {
        const res = await shopService.getListBankByShopId(
          ShopHomeStore.shopInfo.id,
        );
        if (
          res.message.toLocaleLowerCase() ===
          values.messageSuccess.toLocaleLowerCase()
        ) {
          ShopHomeStore.setListBank(res.result.result);
        }
      };

    return (
        <TouchablePreventDouble style={styles.inner} onPress={onPressBankInfo}>
            <View style={styles.viewTitleInfo}>
                <Text style={styles.txtTitle}>Thông tin thẻ ngân hàng</Text>
                <Icon name="angle-right" size={20} />
            </View>
            {/* <View>
                <Text>Tên ngân hàng</Text>
                <Text>Tên chủ thẻ</Text>
                <Text>Số thẻ</Text>
                <Text>Tháng - năm</Text>
            </View> */}
        </TouchablePreventDouble>
    );
}

const styles = StyleSheet.create({
    inner: {
        width: '100%',
        backgroundColor: '#fff',
        paddingHorizontal: 20,
        paddingVertical: 10,
        marginTop: 20,
    },
    txtTitle: { fontSize: 16, color: color.agree },
    viewTitleInfo: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
})

export default BankingInfo;