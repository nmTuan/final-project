import React from 'react';
import {Grid, BarChart, XAxis} from 'react-native-svg-charts';
import {Text} from 'react-native-svg';
import {View, ScrollView, StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {color} from '../../../constant';
import {observer} from 'mobx-react';
import {TouchablePreventDouble, PickerScrollView} from '../../../common/base';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Modal} from 'react-native';
import moment from 'moment';

@observer
class Barchart extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      arr: [2018, 2019, 2020],
      dataRevenue: [
        {
          month: 'Tháng 1',
          revennue: 1000000,
        },
        {
          month: 'Tháng 2',
          revennue: 1000000,
        },
        {
          month: 'Tháng 3',
          revennue: 2000000,
        },
        {
          month: 'Tháng 4',
          revennue: 3000000,
        },
        {
          month: 'Tháng 5',
          revennue: 2000000,
        },
        {
          month: 'Tháng 6',
          revennue: 1400000,
        },
        {
          month: 'Tháng 7',
          revennue: 0,
        },
        {
          month: 'Tháng 8',
          revennue: 0,
        },
        {
          month: 'Tháng 9',
          revennue: 0,
        },
        {
          month: 'Tháng 10',
          revennue: 0,
        },
        {
          month: 'Tháng 11',
          revennue: 0,
        },
        {
          month: 'Tháng 12',
          revennue: 0,
        },
      ],
      data: [],
      indexSelected: 1,
      isScroll: false,
    };
  }

  UNSAFE_componentWillMount() {
    const month = moment(new Date()).format('M');
    if (month > 6) {
      this.setState({
        isScroll: true,
      });
    }
    const {dataRevenue} = this.state;
    var data = [];
    for (var i = 0; i < dataRevenue.length; i++) {
      data.push(dataRevenue[i].revennue / 1000000);
    }
    this.setState({
      data,
    });
  }

  componentDidMount() {
    const {isScroll} = this.state;
    if (isScroll) {
      setTimeout(() => {
        this.scrollRef.scrollToEnd();
      }, 200);
    }
  }

  handleValueChange = index => {
    this.setState({
      genderSelected: index,
    });
  };

  render() {
    const {data} = this.state;
    const CUT_OFF = 3;
    const Labels = ({x, y, bandwidth, data}) =>
      data.map(
        (value, index) =>
          value > 0 && (
            <Text
              key={index}
              x={x(index) + bandwidth / 2}
              y={value < CUT_OFF ? y(value) - 10 : y(value) + 15}
              fontSize={14}
              fill={value >= CUT_OFF ? 'white' : 'black'}
              alignmentBaseline={'middle'}
              textAnchor={'middle'}>
              {`${value} tr`}
            </Text>
          ),
      );

    return (
      <ScrollView
        ref={ref => (this.scrollRef = ref)}
        style={{flex: 1}}
        scrollEventThrottle={200}
        decelerationRate="fast"
        horizontal
        showsHorizontalScrollIndicator={false}>
        <View style={{height: 200, width: 700, paddingVertical: 16}}>
          <BarChart
            style={{width: '100%', height: '100%'}}
            data={data}
            svg={{fill: color.agree}}
            contentInset={{top: 10, bottom: 10}}
            spacing={0.5}
            gridMin={0}>
            <Grid direction={Grid.Direction.HORIZONTAL} />
            <Labels />
          </BarChart>
          <XAxis
            data={data}
            formatLabel={(value, index) => `Tháng ${index + 1}`}
            contentInset={{left: 30, right: 30}}
            svg={{fontSize: 10, fill: 'black'}}
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({});

export default Barchart;
