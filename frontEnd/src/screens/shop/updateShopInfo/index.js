import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import ViewSafeArea from '../../../common/base/ViewSafeArea';
import {TopBar, ScreenOTP, ScreenSms, BtnBase} from '../../../common/base';
import Icon from 'react-native-vector-icons/FontAwesome';
import {StyleSheet} from 'react-native';
import {Navigation} from 'react-native-navigation';
import {color, values} from '../../../constant';
import {AccountService} from '../../../services/AccountService';
import {inject, observer} from 'mobx-react';
import {notifyMessage} from '../../../config/Func';
import {shopService} from '../../../services/ShopService';

var timeoutSms = null;

@inject('ShopHomeStore')
@observer
class UpdateShopInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showOtp: false,
      otpSms: '',
      phoneNumber: '',
      shopName: '',
      shopAddress: '',
      description: '',
      inputOtp: '',
      errorOtp: '',
    };
  }

  UNSAFE_componentWillMount() {
    const {ShopHomeStore} = this.props;
    const shopInfo = ShopHomeStore.shopInfo;
    this.setState({
      phoneNumber: shopInfo.phoneNumber,
      shopName: shopInfo.shopName,
      description: shopInfo.description,
      shopAddress: shopInfo.address,
    });
  }

  onPressBack = () => {
    Navigation.dismissModal(this.props.componentId);
  };

  onPressGetOtp = async () => {
    const otpSms = await AccountService.getOtpWithToken(this.state.phoneNumber);
    this.setState({
      showOtp: true,
      otpSms,
    });
    timeoutSms = setTimeout(() => {
      this.sms.slideDown();
    }, 1000);
  };

  onPressReSendOtp = async () => {
    const shopInfo = this.props.ShopHomeStore.shopInfo;
    if (this.state.phoneNumber !== shopInfo.phoneNumber) {
      this.sms.slideUp();
      clearTimeout(timeoutSms);
      this.onPressGetOtp();
    } else {
      notifyMessage('Số điện thoại không đổi');
    }
  };

  onPressDissmissOtp = () => {
    this.setState({
      showOtp: false,
    });
  };

  onPressUpdate = async () => {
    const {ShopHomeStore} = this.props;
    const {shopName, description, shopAddress} = this.state;
    const shopInfo = ShopHomeStore.shopInfo;
    Keyboard.dismiss();
    if (
      shopName !== shopInfo.shopName ||
      description !== shopInfo.description ||
      shopAddress !== shopInfo.address
    ) {
      if (shopName !== '' && description !== '' && shopAddress !== '') {
        const update = await shopService.updateInfoShop(
          shopInfo.id,
          shopName,
          description,
          '',
          '',
          '',
          shopAddress,
        );
        if (update.message == 'update success') {
          shopInfo.shopName = shopName;
          shopInfo.description = description;
          shopInfo.address = shopAddress;
          notifyMessage('Cập nhật thành công');
        } else {
          notifyMessage(update.message);
        }
      } else {
        notifyMessage('Thiếu thông tin');
      }
    } else {
      console.log('can not update');
    }
  };

  onPressUpdatePhoneNumber = async () => {
    const {ShopHomeStore} = this.props;
    const {otpSms, inputOtp, phoneNumber} = this.state;
    const shopInfo = ShopHomeStore.shopInfo;
    // console.log(otpSms, '-----', inputOtp);
    if (otpSms !== inputOtp) {
      this.setState({
        errorOtp: 'OTP sai',
      });
    } else {
      console.log('run here');
      const res = await shopService.checkPhoneNumber(phoneNumber, inputOtp);
      if (res.errorCode === 200) {
        const update = await shopService.updateInfoShop(
          shopInfo.id,
          '',
          '',
          '',
          '',
          phoneNumber,
          '',
        );
        if (update.message == 'update success') {
          shopInfo.phoneNumber = phoneNumber;
          this.setState({
            showOtp: false,
          });
        } else {
          this.setState({
            errorOtp: update.message,
          });
        }
      } else {
        this.setState({
          errorOtp: res.message,
        });
      }
    }
  };

  render() {
    const {
      phoneNumber,
      shopName,
      description,
      showOtp,
      otpSms,
      errorOtp,
      shopAddress,
    } = this.state;
    return (
      <TouchableOpacity
        activeOpacity={1}
        style={{flex: 1}}
        onPress={Keyboard.dismiss}>
        <ViewSafeArea viewStyle={styles.container}>
          <View style={styles.viewTopBar}>
            <TopBar
              childLeft={
                <TouchableOpacity onPress={this.onPressBack}>
                  <Icon name="angle-left" size={40} style={styles.btnBack} />
                </TouchableOpacity>
              }
              childCenter={<Text style={styles.title}>Cập nhật thông tin</Text>}
            />
          </View>
          <View style={styles.inner}>
            <View>
              <Text>Tên cửa hàng</Text>
              <TextInput
                value={shopName}
                style={styles.textInput}
                placeholder={'Tên cửa hàng'}
                onChangeText={value => this.setState({shopName: value})}
              />
            </View>
            <View style={styles.containerTextInput}>
              <Text>Địa chỉ của hàng</Text>
              <TextInput
                value={shopAddress}
                style={styles.textInput}
                placeholder={'Địa chỉ cửa hàng'}
                onChangeText={value => this.setState({shopAddress: value})}
              />
            </View>
            <View style={styles.containerTextInput}>
              <Text>Số điện thoại</Text>
              <View style={styles.viewPicker}>
                <TextInput
                  style={styles.textInput}
                  placeholder="Số điện thoại"
                  value={phoneNumber}
                  onChangeText={value => this.setState({phoneNumber: value})}
                  keyboardType="numeric"
                />
                <TouchableOpacity
                  style={styles.btnGetOTP}
                  onPress={this.onPressReSendOtp}>
                  <Text>Lấy OTP</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{marginTop: 10}}>
              <Text>Mô tả</Text>
              <TextInput
                value={description}
                style={styles.textInputRating}
                placeholder="Nội dung..."
                multiline
                onChangeText={value => this.setState({description: value})}
              />
            </View>
          </View>
          <View style={styles.btnUpdate}>
            <BtnBase textBtn="Cập nhật" onPress={this.onPressUpdate} />
          </View>
          {showOtp ? (
            <View style={styles.containerOtp}>
              <ScreenOTP
                cancleModal={this.onPressDissmissOtp}
                reSend={this.onPressReSendOtp}
                send={this.onPressUpdatePhoneNumber}
                textError={errorOtp}
                onChangeText={value => this.setState({inputOtp: value})}
              />
            </View>
          ) : (
            <></>
          )}
          <ScreenSms
            ref={ref => (this.sms = ref)}
            message={`Mã OTP của bạn dùng cho app Fashion là: ${otpSms}`}
          />
        </ViewSafeArea>
      </TouchableOpacity>
    );
  }
}
const styles = StyleSheet.create({
  container: {backgroundColor: color.lightGrayColor, alignItems: 'center'},
  btnBack: {marginLeft: 10},
  title: {fontSize: 16, textAlign: 'center'},
  viewTopBar: {backgroundColor: '#fff'},
  textInput: {
    width: '100%',
    height: 40,
    borderBottomWidth: 1,
    borderColor: color.lightGrayColor,
  },
  inner: {
    width: '100%',
    backgroundColor: '#fff',
    padding: 10,
    marginTop: 10,
  },
  containerTextInput: {width: '100%', paddingTop: 10},
  viewPicker: {
    width: '100%',
    height: 40,
    paddingVertical: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  btnGetOTP: {position: 'absolute', right: 0},
  containerOtp: {position: 'absolute', width: '100%', height: '100%'},
  textInputRating: {
    height: 80,
    width: '100%',
    borderWidth: 1,
    borderColor: 'gray',
    textAlignVertical: 'top',
    borderRadius: 6,
    marginTop: 10,
  },
  btnUpdate: {width: '80%', height: 40, marginTop: 20},
});
export default UpdateShopInfo;
