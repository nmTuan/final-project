import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import {TopBar, BtnBase} from '../../../common/base';
import {TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Navigation} from 'react-native-navigation';
import {color} from '../../../constant';
import InfoGeneral from './InfoGeneral';
import ViewSafeArea from '../../../common/base/ViewSafeArea';
import ProductImage from './ProductImage';
import ProductSizeColor from './ProductSizeColor';
import {notifyMessage} from '../../../config/Func';

class EditProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenHeight: 0,
    };
  }

  componentWillUnmount() {
    this.props.ShopProductStore.setProduct(null);
  }

  onPressBack = () => {
    Navigation.dismissModal(this.props.componentId);
  };

  onPressAdd = () => {
    const {ShopProductStore} = this.props;
    const productInfo = ShopProductStore.productInfo;
    if (
      productInfo.productName !== '' &&
      productInfo.maxPrice > 0 &&
      productInfo.thumbnail !== '' &&
      productInfo.description !== '' &&
      productInfo.shortDescription !== '' &&
      productInfo.brand !== '' &&
      productInfo.material !== '' &&
      productInfo.category_id !== '' &&
      productInfo.gender_id !== '' &&
      productInfo.age_id !== '' &&
      productInfo.images.length > 0 &&
      productInfo.sizeColors.length > 0 &&
      productInfo.bankId > 0
    ) {
      if (ShopProductStore.checkSizeColor()) {
        this.props.onPressSave(productInfo);
      } else {
        notifyMessage('Bạn vui lòng điền đầy đủ thông tin kích cỡ, màu');
      }
    } else {
      notifyMessage('Bạn vui lòng điền đầy đủ thông tin sản phẩm');
    }
  };

  handleScrollt = event => {
    this.scroll = event.nativeEvent.contentOffset.y;
  };

  handleSizet = (width, height) => {
    if (this.scroll) {
      // const position = this.scroll + height - this.height;
      this.sv.scrollTo({x: 0, y: this.scroll + 168, animated: true});
    }
    this.height = height;
  };

  render() {
    const {
      ShopProductStore,
      txtBtn,
      onPressDeleteSizeColor,
      ShopHomeStore,
    } = this.props;
    const {listAge, listGender, listCategories} = this.state;
    return (
      <ViewSafeArea>
        <View style={styles.viewTopBar}>
          <TopBar
            childLeft={
              <TouchableOpacity onPress={this.onPressBack}>
                <Icon name="angle-left" size={40} style={styles.btnBack} />
              </TouchableOpacity>
            }
            childCenter={<Text style={styles.title}>Thêm sản phẩm mới</Text>}
          />
        </View>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <ScrollView
            ref={ref => (this.sv = ref)}
            style={{flex: 1}}
            contentContainerStyle={{flexGrow: 1}}
            scrollEventThrottle={16}
            showsVerticalScrollIndicator={false}
            onScroll={this.handleScrollt}
            onContentSizeChange={this.handleSizet}>
            <View style={styles.inner}>
              <InfoGeneral
                ShopProductStore={ShopProductStore}
                listCategories={listCategories}
                listAge={listAge}
                listGender={listGender}
                ShopHomeStore={ShopHomeStore}
              />
              <ProductImage ShopProductStore={ShopProductStore} />
              <ProductSizeColor
                ShopProductStore={ShopProductStore}
                onPressDeleteSizeColor={onPressDeleteSizeColor}
              />
              <View style={styles.btnAdd}>
                <BtnBase textBtn={txtBtn} onPress={this.onPressAdd} />
              </View>
            </View>
          </ScrollView>
        </TouchableWithoutFeedback>
      </ViewSafeArea>
    );
  }
}

const styles = StyleSheet.create({
  container: {backgroundColor: color.lightGrayColor, alignItems: 'center'},
  btnBack: {marginLeft: 10},
  title: {fontSize: 16, textAlign: 'center'},
  viewTopBar: {
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: color.lightGrayColor,
  },
  textInput: {
    width: '100%',
    height: 40,
    borderBottomWidth: 1,
    borderColor: color.lightGrayColor,
  },
  inner: {
    flex: 1,
    width: '100%',
    backgroundColor: '#fff',
    padding: 10,
    marginTop: 10,
  },
  containerTextInput: {width: '100%', paddingTop: 10},
  viewPicker: {
    width: '100%',
    height: 40,
    paddingVertical: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  btnGetOTP: {position: 'absolute', right: 0},
  containerOtp: {position: 'absolute', width: '100%', height: '100%'},
  textInputRating: {
    height: 80,
    width: '100%',
    borderWidth: 1,
    borderColor: 'gray',
    textAlignVertical: 'top',
    borderRadius: 6,
    marginTop: 10,
  },
  btnUpdate: {width: '80%', height: 40, marginTop: 20},
  btnAdd: {
    width: '100%',
    height: 40,
    marginVertical: 20,
  },
});

export default EditProduct;
