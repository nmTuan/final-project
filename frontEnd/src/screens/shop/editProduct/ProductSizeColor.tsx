import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput } from "react-native";
import { observer } from "mobx-react";
import 'mobx-react-lite/batchingForReactNative'
import { color } from '../../../constant';
import Icon from 'react-native-vector-icons/FontAwesome';
import { notifyMessage } from '../../../config/Func';

export const RenderItem = observer(({item, index, ShopProductStore, onPressDeleteSizeColor}) => {
    const selection = {
        start: item.rentPrice <= 0 ? 0 : item.rentPrice.toString().length,
        end: item.rentPrice <= 0 ? 0 : item.rentPrice.toString().length,
    };
    const [edit, setEdit] = useState(false);
    const [startCurosr, setCursor] = useState(0);

    useEffect(() => {
        checkState();
    }, []);

    const checkState = () => {
        if (item.size === '' || item.color === '' || item.quantity <= 0 || item.price <= 0) {
            if (ShopProductStore.productInfo.isRent) {
                if (item.rentPrice <= 0 || item.rentDueId === '' || item.deposit <= 0) {
                    setEdit(true);
                }
            } else {
                setEdit(true);
            }
        }
    }

    const handleCheck = () => {
        setEdit(false);
    }

    const deleteSizeColor = () => {
        if (ShopProductStore.productInfo.sizeColors.length > 1) {
            onPressDeleteSizeColor(index);
        }  else {
            notifyMessage('Bạn không thể xoá hết thông tin');
        }
    }

    return (
        <View style={styles.containerItem}>
            <View style={styles.viewInfo}>
                <Text style={styles.titleInfo}>Cỡ</Text>
                <View style={styles.viewValue}>
                    <TextInput
                        style={styles.textInput}
                        value={item.size}
                        placeholder='Size'
                        editable={edit}
                        onChangeText={value => item.size = value}
                    />
                </View>
            </View>
            <View style={styles.viewInfo}>
                <Text style={styles.titleInfo}>Màu</Text>
                <View style={styles.viewValue}>
                    <TextInput
                        style={styles.textInput}
                        value={item.color}
                        placeholder='Màu'
                        editable={edit}
                        onChangeText={value => item.color = value}
                    />
                </View>
            </View>
            <View style={styles.viewInfo}>
                <Text style={styles.titleInfo}>Số lượng</Text>
                <View style={styles.viewValue}>
                    <TextInput
                        style={styles.textInput}
                        value={`${item.quantity <= 0 ? '' : item.quantity}`}
                        placeholder='Số lượng'
                        editable={edit}
                        onChangeText={value => item.quantity = value !== '' ? parseInt(value) : ''}
                        keyboardType="numeric"
                    />
                </View>
            </View>
            <View style={styles.viewInfo}>
                <Text style={styles.titleInfo}>Giá Mua</Text>
                <View style={styles.viewValue}>
                    <TextInput
                        style={styles.textInput}
                        value={`${item.price <= 0 ? '' : item.price}`}
                        placeholder='Giá mua'
                        editable={edit}
                        onChangeText={value => item.price = value !== '' ? parseInt(value) : ''}
                        keyboardType="numeric"
                    />
                </View>
            </View>
            {/* <View style={styles.viewInfo}>
                <Text style={styles.titleInfo}>Giá Thuê</Text>
                <View style={styles.viewValue}>
                    <TextInput
                        style={styles.textInput}
                        value={`${item.rentPrice <= 0 ? '' : item.rentPrice}/ngày`}
                        placeholder='Giá thuê (theo ngày)'
                        selection={startCurosr > item.rentPrice.toString().length ? selection : null}
                        onSelectionChange={e => setCursor(e.nativeEvent.selection.start)}
                        editable={edit}
                        onChangeText={value => {
                            item.rentPrice = value !== '/ngày' ? parseInt(value) : ''
                        }}
                        keyboardType="numeric"
                    />
                </View>
            </View>
            <View style={styles.viewInfo}>
                <Text style={styles.titleInfo}>Đặt cọc</Text>
                <View style={styles.viewValue}>
                    <TextInput
                        style={styles.textInput}
                        value={`${item.deposit <= 0 ? '' : item.deposit}`}
                        editable={edit}
                        placeholder='Giá đặt cọc'
                        onChangeText={value => item.deposit = value !== '' ? parseInt(value) : ''}
                        keyboardType="numeric"
                    />
                </View>
            </View> */}
            {edit ? <View style={styles.btnDelete}>
                <TouchableOpacity style={{ marginRight: 10 }} onPress={deleteSizeColor}>
                    <Icon name="minus-square" size={25} color={color.red} />
                </TouchableOpacity>
                <TouchableOpacity onPress={handleCheck}>
                    <Icon name="check-square" size={25} color={color.agree} />
                </TouchableOpacity>
            </View>
                :
                <View style={styles.btnDelete}>
                    <TouchableOpacity onPress={() => setEdit(true)}>
                        <Icon name="edit" size={25} />
                    </TouchableOpacity>
                </View>
            }
        </View>
    )
});

const ProductSizeColor = observer(({ ShopProductStore, onPressDeleteSizeColor }) => {
    const productInfo = ShopProductStore.productInfo;
    const addNewSizeColor = () => {
        if (ShopProductStore.checkSizeColor()) {
            const arr = productInfo.sizeColors;
            const sizeColors = {
                size: '',
                color: '',
                quantity: 0,
                price: 0,
                rentPrice: 0,
                rentDueId: '1',
                deposit: 0,
            };
            arr.unshift(sizeColors);
            ShopProductStore.setSizeColors(arr);
        } else {
            notifyMessage('Bạn chưa điền đầy đủ thông tin sản phẩm');
        }
    }

    return (
        <View>
            <View style={styles.containerTitle}>
                <Text style={styles.title}>
                    Kích cỡ và màu *
                </Text>
                <TouchableOpacity onPress={addNewSizeColor}>
                    <Icon name="plus-square" size={25} color={color.agree} />
                </TouchableOpacity>
            </View>
            {
                productInfo.sizeColors.map((item: any, index: any) => 
                    <RenderItem 
                        item={item} 
                        index={index} 
                        ShopProductStore={ShopProductStore} 
                        onPressDeleteSizeColor={onPressDeleteSizeColor} 
                    />
                )
            }
        </View>
    )
})

const styles = StyleSheet.create({
    containerTitle: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    title: {
        color: color.agree
    },
    containerItem: {
        borderWidth: 1,
        borderRadius: 6,
        padding: 10,
        marginTop: 10,
        borderColor: '#00000060'
    },
    viewInfo: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    titleInfo: { color: '#377ae6', flex: 1 },
    viewValue: { flex: 4, borderBottomWidth: 1, borderColor: color.lightGrayColor },
    textInput: { height: 40, fontSize: 14, backgroundColor: 'transparent', color: '#000' },
    btnDelete: { position: 'absolute', top: 5, right: 5, flexDirection: 'row' },
})

export default ProductSizeColor;