import React, { useState, Props } from 'react';
import { View, Text, TextInput, StyleSheet, TouchableOpacity, Switch, Modal } from "react-native";
import { color, values } from "../../../constant";
import Icon from 'react-native-vector-icons/FontAwesome';
import { PopupPicker, PickerScrollView, TouchablePreventDouble, ViewSafeArea } from '../../../common/base';
import { observer } from 'mobx-react';
import 'mobx-react-lite/batchingForReactNative'
import { formatCurrency } from '../../../constant/Format';
import { useEffect } from 'react';
import HomeService from '../../../services/HomeService';
import { CheckBox } from 'react-native-elements';
import MainBankScreen from '../../common/bank/MainBankScreen';
import { shopService } from '../../../services/ShopService';
import { notifyMessage } from '../../../config/Func';

const InfoGeneral = observer(({ ShopProductStore, ShopHomeStore }) => {
  const productInfo = ShopProductStore.productInfo;
  const listBank = ShopHomeStore.listBank;
  const pickerScroll = {
    age: 'age',
    gender: 'gender',
  }
  const [listCategories, setListCategories] = useState([]);
  const [listAge, setListAge] = useState([]);
  const [listGender, setListGender] = useState([]);
  const [typeModal, setTypeModal] = useState('');
  const [dataModal, setDataModal] = useState([]);
  const [titleModal, setTitleModal] = useState('');
  const [pickerSelected, setPickerSelected] = useState(0);
  const [showCategory, setCategoryModal] = useState(false);
  const [ageSelected, setAgeSelected] = useState(-1);
  const [genderSelected, setGenderSelected] = useState(-1);
  const [showModalPicker, setModal] = useState(false);
  const [category, setCategory] = useState('');
  const [showBankInfo, setShowBankInfo] = useState(false);

  useEffect(() => {
    getListType();
    if (productInfo.category) {
      setCategory(productInfo.category);
    }
    if (productInfo.gender_id > 0) {
      setGenderSelected(productInfo.gender_id - 1);
    }
    if (productInfo.age_id > 0) {
      setAgeSelected(productInfo.age_id - 1);
    }
    checkBank();
  }, []);

  const getListType = async () => {
    const listCategories = await HomeService.getListCategory();
    const listAge = await HomeService.getListAge();
    const listGender = await HomeService.getListGender();
    setListGender(listGender);
    setListAge(listAge);
    setListCategories(listCategories);
  }

  const checkBank = async () => {
    productInfo.bankId = listBank[0].id
  }

  const handleModal = () => setModal(showModalPicker => !showModalPicker);
  const handleShowCategory = () => setCategoryModal(showCategory => !showCategory);
  const toggleSwitch = () => productInfo.isRent = !productInfo.isRent;
  const changeNameProduct = (value: String) => productInfo.productName = value;
  const changeMinPrice = (value: string) => productInfo.minPrice = value !== '' ? parseInt(value) : 0;
  const changeMaxPrice = (value: string) => productInfo.maxPrice = value !== '' ? parseInt(value) : '';
  const changeOldPrice = (value: string) => productInfo.oldPrice = value !== '' ? parseInt(value) : 0;
  const changeBrand = (value: string) => productInfo.brand = value;
  const changeMeterial = (value: string) => productInfo.material = value;
  const onChangeShortDescription = (value: string) => productInfo.shortDescription = value;
  const onChagneDescription = (value: string) => productInfo.description = value;
  const onPickCategory = (value: any) => {
    productInfo.category_id = value.id;
    setCategory(value.name);
    handleShowCategory();
  };
  const handleValueChangeAge = (value: number) => {
    switch (typeModal) {
      case pickerScroll.age:
        setAgeSelected(value);
        productInfo.age = listAge[value].age;
        productInfo.age_id = listAge[value].id;
        break;
      case pickerScroll.gender:
        setGenderSelected(value);
        productInfo.gender = listGender[value].gender;
        productInfo.gender_id = listGender[value].id;
        break;
      default:
        break;
    }
  }

  const addBank = () => {
    setShowBankInfo(true);
  }

  const onPressNext = async bankInfo => {
    const body = {shopId: ShopHomeStore.shopInfo.id, ...bankInfo};
    const res = await shopService.addBank(body);
    console.log(JSON.stringify(res));
    if (
      res.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      ShopHomeStore.listBank.push(body);
    }
    notifyMessage(res.message);
    setShowBankInfo(false);
    setBankSelected(0);
    productInfo.bankId = res.result.id;
  };

  const onPressPicker = (type: string, data: Array<any>) => {
    switch (type) {
      case pickerScroll.age:
        setTitleModal('Độ tuổi');
        setAgeSelected(1);
        setPickerSelected(ageSelected >= 0 ? ageSelected : 1);
        setTypeModal(pickerScroll.age);
        setDataModal(data);
        break;
      case pickerScroll.gender:
        setTitleModal('Giới tính');
        setGenderSelected(1);
        setPickerSelected(genderSelected >= 0 ? genderSelected : 1);
        setTypeModal(pickerScroll.gender);
        setDataModal(data);
        break;
      default:
        setTitleModal('')
        break;
    }
    handleModal();
  }

  const renderModal = () => {
    return (
      <Modal visible={showModalPicker} transparent animationType="slide">
        {
          showModalPicker && (
            <ViewSafeArea>
              <View style={styles.backgroundModalGender}>
                <TouchablePreventDouble style={{ flex: 1 }} onPress={handleModal} />
                <View style={styles.viewChildGender}>
                  <View>
                    <Text style={styles.txtTitleGender}>{titleModal}</Text>
                    <TouchablePreventDouble style={styles.btnSelecteGender} onPress={handleModal}>
                      <Text style={styles.txtSelected}>Chọn</Text>
                    </TouchablePreventDouble>
                  </View>
                  <View style={styles.line} />
                  <PickerScrollView
                    dataSource={dataModal}
                    selectedIndex={pickerSelected}
                    value={typeModal}
                    itemHeight={105 / 3}
                    wrapperHeight={105}
                    getSelected={handleValueChangeAge}
                  />
                </View>
              </View>
            </ViewSafeArea>
          )}
      </Modal>
    );
  }

  const renderItem = (item: any, index: any) => {
    return (
      <TouchableOpacity style={styles.itemCategory} onPress={() => onPickCategory(item)}>
        <Text>{`${index + 1}. ${item.name}`}</Text>
      </TouchableOpacity>
    );
  }

  return (
    <View>
      <View>
        <Text style={styles.txtTitle}>Tên sản phẩm *</Text>
        <TextInput
          value={productInfo.productName}
          style={styles.textInput}
          placeholder={'Tên sản phẩm'}
          onChangeText={changeNameProduct}
        />
      </View>
      <View style={styles.containerTextInput}>
        <Text style={styles.txtTitle}>Giá thấp nhât</Text>
        <TextInput
          value={`${productInfo.minPrice}`}
          style={styles.textInput}
          placeholder={'Giá thấp nhất (Nếu có)'}
          onChangeText={changeMinPrice}
          keyboardType="numeric"
        />
      </View>
      <View style={styles.containerTextInput}>
        <Text style={styles.txtTitle}>Giá cao nhất *</Text>
        <TextInput
          value={`${productInfo.maxPrice}`}
          style={styles.textInput}
          placeholder={'Nhập giá cao nhất'}
          onChangeText={changeMaxPrice}
          keyboardType="numeric"
        />
      </View>
      <View style={styles.containerTextInput}>
        <Text style={styles.txtTitle}>Giá cũ </Text>
        <TextInput
          value={`${productInfo.oldPrice}`}
          style={styles.textInput}
          placeholder={'Giá cũ (Nếu có)'}
          onChangeText={changeOldPrice}
          keyboardType="numeric"
        />
      </View>
      {/* <View style={styles.containerTextInput}>
        <Text style={styles.txtTitle}>Địa chỉ sản xuất </Text>
        <TextInput
          value={`${productInfo.oldPrice}`}
          style={styles.textInput}
          placeholder={'Giá cũ (Nếu có)'}
          onChangeText={changeOldPrice}
        />
      </View> */}
      <View style={styles.containerTextInput}>
        <Text style={styles.txtTitle}>Thương hiệu *</Text>
        <TextInput
          value={productInfo.brand}
          style={styles.textInput}
          placeholder={'Có thể nhập (Không có)'}
          onChangeText={changeBrand}
        />
      </View>
      <View style={styles.containerTextInput}>
        <Text style={styles.txtTitle}>Chât liệu *</Text>
        <TextInput
          value={productInfo.material}
          style={styles.textInput}
          placeholder={'Chất liệu'}
          onChangeText={changeMeterial}
        />
      </View>
      <View style={styles.containerTextInput}>
        <Text style={styles.txtTitle}>Thể loại *</Text>
        <TouchableOpacity style={styles.viewSelection} onPress={handleShowCategory}>
          <Text>{category !== '' ? category : 'Chọn thể loại'}</Text>
          <Icon name="sort-down" size={25} />
        </TouchableOpacity>
      </View>
      <View style={styles.containerTextInput}>
        <Text style={styles.txtTitle}>Độ tuổi *</Text>
        <TouchableOpacity style={styles.viewSelection} onPress={() => onPressPicker(pickerScroll.age, listAge)}>
          <Text>{listAge.length > 0 && ageSelected >= 0 ? listAge[ageSelected].age : 'Chọn độ tuổi'}</Text>
          <Icon name="sort-down" size={25} />
        </TouchableOpacity>
      </View>
      <View style={styles.containerTextInput}>
        <Text style={styles.txtTitle}>Giới tính *</Text>
        <TouchableOpacity style={styles.viewSelection} onPress={() => onPressPicker(pickerScroll.gender, listGender)}>
          <Text>{listGender.length > 0 && genderSelected >= 0 ? listGender[genderSelected].gender : 'Chọn giới tính'}</Text>
          <Icon name="sort-down" size={25} />
        </TouchableOpacity>
      </View>
      <View style={styles.containerTextInput}>
        <Text style={styles.txtTitle}>Phí vận chuyển</Text>
        <TextInput 
          style={styles.viewSelection}
          placeholder="VNĐ"
          value={`${productInfo.priceDelivery <= 0 ? '' : productInfo.priceDelivery}`}
          onChangeText={value => productInfo.priceDelivery = value !== '' ? parseInt(value) : ''}
          keyboardType="numeric"
        />
      </View>
      <View style={styles.containerTextInput}>
        <Text style={styles.txtTitle}>Ngân hàng thụ hưởng</Text>
        {
          listBank.length > 0 ?
            listBank.map((item, index) => {
              return (
                <View>
                  <Text style={styles.txtValue}>{`${item.bankName} (STK: ${item.creditNumber})`}</Text>
                </View>
              );
            })
            :
            <TouchableOpacity
              style={{ ...styles.inner, ...styles.btn }}
              onPress={addBank}>
              <Icon name="plus-square" size={20} color={color.agree} />
              <Text style={styles.textAdd}>Liên kết thêm ngân hàng</Text>
            </TouchableOpacity>
        }
      </View>
      {/* <View style={styles.viewSwitch}>
        <Text>Thuê</Text>
        <Switch
          trackColor={{ false: "#767577", true: color.agree }}
          thumbColor={"#f4f3f4"}
          ios_backgroundColor="#3e3e3e"
          onValueChange={toggleSwitch}
          value={productInfo.isRent}
        />
      </View> */}
      <View style={styles.containerTextInput}>
        <Text>Mô tả ngắn *</Text>
        <TextInput
          value={productInfo.shortDescription}
          style={{ ...styles.textInputDesciber, ...{ height: 80 } }}
          placeholder="Mô tả ngắn..."
          multiline
          onChangeText={onChangeShortDescription}
        />
      </View>
      <View style={styles.containerTextInput}>
        <Text>Mô tả chi tiết *</Text>
        <TextInput
          value={productInfo.description}
          style={{ ...styles.textInputDesciber, ...{ height: 120 } }}
          placeholder="Mô tả chi tiết..."
          multiline
          onChangeText={onChagneDescription}
        />
      </View>
      <PopupPicker visible={showCategory} onPressCancel={handleShowCategory}>
        {
          showCategory && (
            <ViewSafeArea viewStyle={{ padding: 10 }}>
              <View style={styles.viewTitleModal}>
                <Text style={styles.title}>Thể loại</Text>
              </View>
              {listCategories.map((item, index) => renderItem(item, index))}
            </ViewSafeArea>
          )}

      </PopupPicker>
      {renderModal()}
      <Modal visible={showBankInfo} animationType="slide">
          {showBankInfo && (
            <MainBankScreen
              onPressBack={() => setShowBankInfo(false)}
              onPressNext={onPressNext}
            />
          )}
        </Modal>
    </View>
  );
})

const styles = StyleSheet.create({
  textInput: {
    width: '100%',
    height: 40,
    borderBottomWidth: 1,
    borderColor: color.lightGrayColor,
  },
  inner: {
    width: '100%',
    backgroundColor: '#fff',
    padding: 10,
    marginTop: 10,
  },
  containerTextInput: { width: '100%', paddingTop: 10 },
  viewPicker: {
    width: '100%',
    height: 40,
    paddingVertical: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  viewSelection: {
    width: '100%',
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: color.lightGrayColor,
    height: 40,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  viewSwitch: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 10
  },
  txtTitle: {
    color: color.agree
  },
  txtValue:{
    fontSize: 16,
  },
  textInputDesciber: {
    width: '100%',
    borderWidth: 1,
    borderColor: 'gray',
    textAlignVertical: 'top',
    borderRadius: 6,
    marginTop: 10,
  },
  itemCategory: { borderBottomWidth: 1, borderColor: color.lightGrayColor, paddingTop: 10 },
  viewTitleModal: { width: '100%', alignItems: 'center', justifyContent: 'center' },
  title: { fontSize: 18 },
  backgroundModalGender: { flex: 1, backgroundColor: '#00000060' },
  viewChildGender: {
    width: '100%',
    padding: 10,
    backgroundColor: '#fff',
    position: 'absolute',
    bottom: 0,
  },
  txtTitleGender: { fontSize: 18, textAlign: 'center' },
  line: { width: '100%', height: 1, backgroundColor: 'gray', marginVertical: 10 },
  btnSelecteGender: {
    position: 'absolute',
    right: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtSelected: { color: 'blue' },
  checkBox: {
    padding: 0,
    marginLeft: 0,
    backgroundColor: 'transparent',
    borderColor: 'transparent',
  },
  btn: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textAdd: {
    marginLeft: 10,
    color: color.agree,
  },
});

export default InfoGeneral;
