import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Linking, Alert, Image, StyleSheet, ScrollView, Platform } from 'react-native';
import { color, values } from '../../../constant';
import Icon from 'react-native-vector-icons/FontAwesome';
import { openCamera, openLibMultiImage, openLibSingleImage } from '../../../config/ImagePicker';
import { observer } from 'mobx-react';
import 'mobx-react-lite/batchingForReactNative'
import ImageResizer from 'react-native-image-resizer';
import { FileService } from '../../../services/FileService';

const resizeImage = async (image: string, width: number, height: number) => {
    var data = {
        name: '',
        image: '',
    };
    const widthImage = width > 300 ? width : 300;
    const heightImage = height > 300 ? height : 300;
    const response = await ImageResizer.createResizedImage(image, widthImage, heightImage, 'JPEG', 90)
    try {
        const uri =
            Platform.OS === 'ios'
                ? response.uri.replace('file://', '')
                : response.uri;
        data.name = response.name;
        data.image = await FileService.uploadFile(uri);
    } catch (e) {
        console.log(e);
    }
    return data;
}

const ProductImage = observer(({ ShopProductStore }) => {
    const productInfo = ShopProductStore.productInfo;
    const typeImage = {
        libs: 0,
        camera: 1,
    }
    const [thumbnail, setThumbnail] = useState('');
    const [images, setListImage] = useState([]);
    const [type, setType] = useState(-1);

    const onPressThumbnail = () => {
        Alert.alert(
            "",
            "Chọn ảnh thumbnail",
            [
                {
                    text: "Huỷ bỏ",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                {
                    text: "chọn ảnh thư viện",
                    onPress: async () => {
                        const result = await openLibSingleImage();
                        if (result) {
                            const data = await resizeImage(result.image, result.width/10, result.height/10);
                            if (data.image !== '') {
                                productInfo.thumbnail = data.image;
                            }
                        }
                        
                    }
                },
                {
                    text: "Camera",
                    onPress: async () => {
                        const result = await openCamera();
                        if (result) {
                            const data = await resizeImage(result.image, result.width/10, result.height/10);
                            if (data.image !== '') {
                                productInfo.thumbnail = data.image;
                            }
                        }
                        
                    }
                }
            ],
            { cancelable: false }
        );
    }

    const onPressListImage = (index:any) => {
        var newListImage = productInfo.images;
        Alert.alert(
            "",
            "Chọn ảnh sản phẩm",
            [
                {
                    text: "Huỷ bỏ",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                {
                    text: "chọn ảnh thư viện",
                    onPress: async () => {
                        if (index >= 0) {
                            onChangeSingleImage(newListImage, index);
                        } else {
                            onChangeMultipleImage(newListImage);
                        }
                    }
                },
                {
                    text: "Camera",
                    onPress: async () => {
                        const arr = [];
                        const image = await openCamera()
                        const data = await resizeImage(image.image, image.width/10, image.height/10);
                        if (data.image !== '') {
                            arr.push(data);
                        }
                        newListImage = [...newListImage, ...arr];
                        productInfo.images = newListImage;
                    }
                }
            ],
            { cancelable: false }
        );
    }

    const onChangeMultipleImage = async (newListImage: Array<any>) => {
        const listImages = await openLibMultiImage(newListImage.length)
        const imagesUri = [];
        for (const item of listImages) {
            const data = await resizeImage(item.image, item.width/10, item.height/10);
            if (data.image !== '') {
                imagesUri.push(data);
            }
        }
        newListImage = [...newListImage, ...imagesUri];
        productInfo.images = newListImage;
    }

    const onChangeSingleImage = async (listImage: Array<any>, index: any) => {
        const result = await openLibSingleImage();
        const data = await resizeImage(result.image, result.width/10, result.height/10);
        listImage[index] = data;
        productInfo.images = listImage;
    }

    return (
        <View style={{ paddingVertical: 10 }}>
            <View>
                <Text style={styles.title}>Ảnh thumbnail *</Text>
                <TouchableOpacity style={styles.btnCamera} onPress={onPressThumbnail}>
                    {
                        productInfo.thumbnail !== '' ?
                            <Image source={{ uri: `${values.domain}${productInfo.thumbnail}` }} style={{ width: '100%', height: '100%', resizeMode: 'cover' }} />
                            :
                            <Icon name="camera" size={25} />
                    }
                </TouchableOpacity>
            </View>
            <View style={styles.inner}>
                <Text style={styles.title}>Ảnh Sản phẩm *</Text>
                <ScrollView style={{ height: 100 }} horizontal={true}>
                    <View style={{ flexDirection: 'row', height: 90 }}>
                        {productInfo.images.map((item, index) => {
                            return (
                                <TouchableOpacity style={styles.btnCamera} onPress={() => onPressListImage(index)}>
                                    <Image source={{ uri: `${values.domain}${item.image}` }} style={{ width: '100%', height: '100%', resizeMode: 'cover' }} />
                                </TouchableOpacity>
                            );
                        })}
                        {productInfo.images.length < 6 &&
                            <TouchableOpacity style={styles.btnCamera} onPress={() => onPressListImage(-1)}>
                                <Icon name="plus-square" size={25} />
                            </TouchableOpacity>
                        }
                    </View>
                </ScrollView>
            </View>
        </View>
    );
})

const styles = StyleSheet.create({
    inner: { paddingTop: 10 },
    title: { color: color.agree },
    btnCamera: {
        width: 90,
        height: 90,
        borderWidth: 1,
        borderColor: color.lightGrayColor,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
        marginRight: 5,
    },
})

export default ProductImage;