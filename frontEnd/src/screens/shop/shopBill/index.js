import React, {Component} from 'react';
import {View, Text, Animated, StyleSheet, ScrollView} from 'react-native';
import {values, color} from '../../../constant';
import {
  TouchablePreventDouble,
  ViewSafeArea,
  TopBarChatCart,
} from '../../../common/base';
import ViewPager from '@react-native-community/viewpager';
import BillWaiting from '../shopBillState/billWaiting';
import BillConfirmed from '../shopBillState/billConfirmed';
import BillAborted from '../shopBillState/billAborted';
import {inject, observer} from 'mobx-react';
import {pushScreen} from '../../../config/NavigationConfig';
import RenderTabBar from './RenderTabBar';
import BillDelivering from '../shopBillState/billDelivering';
import BillSuccess from '../shopBillState/billSuccess';
import BillFail from '../shopBillState/billFail';

@inject('ShopHomeStore')
@observer
class ShopBill extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      scrollY: new Animated.Value(0),
    };
  }

  UNSAFE_componentWillUpdate() {
    const {ShopHomeStore} = this.props;
    const tabSelected = ShopHomeStore.tabBillSelected;
    this.viewPager.setPage(tabSelected);
  }

  handlePageSeleted = e => {
    const {ShopHomeStore} = this.props;
    ShopHomeStore.setTabBillSelected(e.nativeEvent.position);
  };

  onPressChat = () => {
    pushScreen(this.props.componentId, 'ListRoomShop');
  };

  onRefresh = () => {
    console.log('re');
  };

  render() {
    const {componentId, ShopHomeStore} = this.props;
    const tabSelected = ShopHomeStore.tabBillSelected;
    const {waiting, confirmed, delivering, succes, fail, abort} = values.bill;
    return (
      <ViewSafeArea viewStyle={styles.container}>
        <TopBarChatCart
          childLeft={<Text style={styles.txtTopBar}>Đơn hàng</Text>}
          showCart={false}
          onPressChat={this.onPressChat}
        />
        <View>
          <RenderTabBar
            viewPager={this.viewPager}
            ShopHomeStore={ShopHomeStore}
          />
        </View>
        <ViewPager
          style={{flex: 1}}
          ref={ref => (this.viewPager = ref)}
          initialPage={tabSelected}
          onPageSelected={this.handlePageSeleted}>
          <View>
            {waiting <= tabSelected <= confirmed && (
              <BillWaiting
                componentId={componentId}
                ShopHomeStore={ShopHomeStore}
              />
            )}
          </View>
          <View>
            {waiting <= tabSelected <= delivering && (
              <BillConfirmed
                componentId={componentId}
                ShopHomeStore={ShopHomeStore}
              />
            )}
          </View>
          <View>
            {confirmed <= tabSelected <= succes && (
              <BillDelivering
                componentId={componentId}
                ShopHomeStore={ShopHomeStore}
              />
            )}
          </View>
          <View>
            {delivering <= tabSelected <= fail ? (
              <BillSuccess
                componentId={componentId}
                ShopHomeStore={ShopHomeStore}
              />
            ) : (
              <></>
            )}
          </View>
          <View>
            {succes <= tabSelected <= abort && (
              <BillFail
                componentId={componentId}
                ShopHomeStore={ShopHomeStore}
              />
            )}
          </View>
          <View>
            {tabSelected >= fail && (
              <BillAborted
                componentId={componentId}
                ShopHomeStore={ShopHomeStore}
              />
            )}
          </View>
        </ViewPager>
      </ViewSafeArea>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.background,
  },
  txtTopBar: {marginLeft: 10, fontSize: 18, fontWeight: 'bold'},
});

export default ShopBill;
