import React from 'react';
import {View} from 'react-native';
import ListRoom from '../../../common/chat/listRoom';
import {showModal} from '../../../../config/NavigationConfig';
import {inject, observer} from 'mobx-react';
import {shopService} from '../../../../services/ShopService';
import { UserService } from '../../../../services/UserService';

@inject('ShopChatStore', 'ShopHomeStore')
@observer
class ListRoomShop extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }

  async UNSAFE_componentWillMount() {
    const {item, socketId, userId} = this.props;
    if (item || socketId || userId) {
      var userInfo = null;
      if (userId) {
        userInfo = await UserService.getUserInfo(userId);
      }
      const passProps = {
        item,
        socketId,
        userInfo,
      };
      showModal('RoomDetailShop', passProps);
    }
  }

  async componentDidMount() {
    const {ShopChatStore, ShopHomeStore} = this.props;
    const data = await shopService.getListRoom(ShopHomeStore.shopInfo.id);
    if (data) {
      ShopChatStore.setListRoomChat(data.result.roomChats);
    }
  }

  onPressItem = async (item, index) => {
    const {ShopHomeStore} = this.props;
    const userInfo = await UserService.getUserInfo(item.custId);
    if (userInfo) {
      await shopService.updateStatusRoomChat(item.id, 1);
      ShopHomeStore.quantityChat = ShopHomeStore.quantityChat - 1;
      const passProps = {
        item,
        index,
        userInfo: userInfo.result,
      };
      showModal('RoomDetailShop', passProps);
    }
  };

  onRefresh = () => {
    this.setState({
      refreshing: true,
    });
    setTimeout(() => {
      this.setState({
        refreshing: false,
      });
    }, 1000);
  };

  onLoadMore = () => {
    // console.log('load more');
  };

  render() {
    const {ShopChatStore} = this.props;
    const {refreshing} = this.state;
    return (
      <View style={{flex: 1}}>
        <ListRoom
          data={ShopChatStore.listRoomChat}
          componentId={this.props.componentId}
          onPressItem={this.onPressItem}
          refreshing={refreshing}
          onLoadMore={this.onLoadMore}
          onRefeshing={this.onRefresh}
          keyStatus="shopStatus"
        />
      </View>
    );
  }
}

export default ListRoomShop;
