/* eslint-disable react/no-did-mount-set-state */
import React, {Component} from 'react';
import {View, Platform} from 'react-native';
import RoomDetail from '../../../common/chat/RoomDetail';
import {inject, observer} from 'mobx-react';
import {getSocket} from '../../../../config/Socket';
import ChatService from '../../../../services/ChatService';
import {shopService} from '../../../../services/ShopService';
import {OptionBase} from '../../../../common/base';
import {
  openCamera,
  resizeImage,
  openLibMultiImage,
  openLibSingleImage,
} from '../../../../config/ImagePicker';
import {FileService} from '../../../../services/FileService';
import ImageResizer from 'react-native-image-resizer';

@inject('ShopHomeStore', 'ShopChatStore')
@observer
class RoomDetailShop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      showImagePicker: false,
      page: 0,
      content: '',
    };
  }

  async UNSAFE_componentWillMount() {
    const {item} = this.props;
    if (item) {
      const data = await ChatService.getListMessByRoom(item.id);
      if (data.errorCode === 200) {
        this.setState({
          data: data.result.result,
        });
      }
    }
  }

  componentDidMount() {
    getSocket().on('receiveMess', result => {
      const newData = this.state.data;
      newData.unshift(result);
      this.setState({
        data: newData,
      });
    });
  }

  onLoadMore = async () => {
    const {page, data} = this.state;
    const {item} = this.props;
    const pageMess = page + 1;
    const newMess = await ChatService.getListMessByRoom(item.id, pageMess, 10);
    if (newMess.errorCode === 200) {
      if (newMess.result.result.length > 0) {
        this.setState({
          data: [...data, ...newMess.result.result],
          page: pageMess,
        });
      }
    }
  };

  onPressSend = () => {
    const {userInfo, ShopHomeStore} = this.props;
    const {content} = this.state;
    if (content.trim() !== '') {
      const body = {
        custId: userInfo.id,
        shopId: ShopHomeStore.shopInfo.id,
        content: content,
        receiver: userInfo.fullName,
        receiverava: userInfo.avatar,
        sender: ShopHomeStore.username,
        senderava: ShopHomeStore.shopInfo.avatar,
        socketReceive: userInfo.socketId,
      };
      getSocket().emit('sendMess', body);
      this.state.data.unshift(body);
      this.setState({
        content: '',
        data: this.state.data,
      });
    }
  };

  onPressBack = async () => {
    const {ShopChatStore, ShopHomeStore} = this.props;
    const data = await shopService.getListRoom(ShopHomeStore.shopInfo.id);
    if (data) {
      ShopChatStore.setListRoomChat(data.result.roomChats);
    }
  };

  onPressTakePic = async () => {
    const {item} = this.props;
    const result = await openCamera();
    this.setState({
      showImagePicker: false,
    });
    if (result) {
      resizeImage(
        result.image,
        result.width / 5,
        result.height / 5,
        'JPEG',
        100,
      )
        .then(async uri => {
          const data = await ChatService.uploadSingleFile(uri, item.id);
          if (data) {
            this.setState({
              content: data,
            });
            this.onPressSend();
          }
        })
        .catch(e => console.log(e));
    }
  };

  onPressLibs = async () => {
    const {item} = this.props;
    const result = await openLibSingleImage();
    this.setState({
      showImagePicker: false,
    });
    if (result) {
      console.log(result);
      resizeImage(
        result.image,
        result.width / 5,
        result.height / 5,
        'JPEG',
        100,
      )
        .then(async uri => {
          const data = await ChatService.uploadSingleFile(uri, item.id);
          if (data) {
            this.setState({
              content: data,
            });
            this.onPressSend();
          }
        })
        .catch(e => console.log(e));
    }
    // results.map(item => {
    //   resizeImage(item.image, item.width, item.height, 'JPEG', 50)
    //     .then(uri => {
    //       arrImage.push(uri);
    //       console.log(arrImage);
    //     })
    //     .catch(err => {
    //       console.log(err);
    //     });
    // });
  };

  onPressCamera = () => {
    this.setState({
      showImagePicker: true,
    });
  };

  onPressCancel = () => {
    this.setState({
      showImagePicker: false,
    });
  };

  render() {
    const {item, index, ShopHomeStore} = this.props;
    const {content, data, showImagePicker} = this.state;
    const isImage = content.search('.jpg') > 0 ? true : false;
    return (
      <View style={{flex: 1}}>
        <RoomDetail
          title={item.nameRoom}
          item={item}
          index={index}
          componentId={this.props.componentId}
          content={isImage ? '' : content}
          data={data}
          onChangeText={value => this.setState({content: value})}
          onPressSend={this.onPressSend}
          username={ShopHomeStore.username}
          onPressBack={this.onPressBack}
          onPressCamera={this.onPressCamera}
          onLoadMore={this.onLoadMore}
        />
        {showImagePicker && (
          <OptionBase
            title="Chọn ảnh"
            textBtn1="Chụp ảnh"
            textBtn2="Chọn ảnh từ thư viện"
            onPress1={this.onPressTakePic}
            onPress2={this.onPressLibs}
            onPressCancel={this.onPressCancel}
          />
        )}
      </View>
    );
  }
}

export default RoomDetailShop;
