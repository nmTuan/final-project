import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity, ActivityIndicator } from 'react-native';
import { color, values } from '../../../constant';
import Icon from 'react-native-vector-icons/FontAwesome';
import { formatCurrency, statusProductShop } from '../../../constant/Format';
import moment from 'moment';
import ChatService from '../../../services/ChatService';
import { pushScreen } from '../../../config/NavigationConfig';
import { UserService } from '../../../services/UserService';
import { Navigation } from 'react-native-navigation';
import { observer } from 'mobx-react-lite';

const _renderListProduct = (item: any) => {
    return (
        <View style={styles.containerListProduct}>
            <Image source={{ uri: `${values.domain}${item.thumbnail}` }} style={styles.productImg} />
            <View style={styles.containerInfoProduct}>
                <View style={styles.viewProductName}>
                    <Text style={styles.productName} numberOfLines={1}>
                        {item.productName}
                    </Text>
                </View>
                <View style={styles.viewPrice}>
                    <View style={{ flexDirection: 'row' }}>
                        {item.oldPrice > 0 && (
                            <Text style={styles.oldPrice}>
                                {formatCurrency(item.oldPrice)}
                            </Text>
                        )}
                        <Text>{formatCurrency(item.maxPrice)}</Text>
                    </View>
                    <Text>{`x${item.quantity}`}</Text>
                </View>
            </View>
        </View>
    );
};

const BillDetailBase = ({ data, componentId }) => {
    const [billDetail, setBillDetail] = useState(null);
    useEffect(() => {
        getData();
    }, []);

    const getData = async () => {
        const res = await UserService.getBillDetail(data.id);
        if (res.errorCode == 200) {
            setBillDetail(res.result);
        }
    }

    const onPressChat = async () => {
        var passProps = {};
        const res = await ChatService.getRoomByCustShop(data.custId, data.shopId);
        if (res.errorCode === 200 && res.result) {
            passProps = {
                item: res.result,
                userId: data.custId,
            }
        } else {
            passProps = {
                userId: data.custId,
            }
        }
        pushScreen(componentId, 'ListRoomShop', passProps);
    }

    const caculatePrice = () => {
        const total = billDetail.maxPrice * billDetail.quantity + billDetail.priceDelivery;
        return total;
    };

    return (
        billDetail ?
        <View style={styles.container}>
            <View style={styles.containerBillStatus}>
                <View style={styles.viewStatus}>
                    <Icon name="file-text" size={20} color={color.agree} />
                    <Text style={styles.txtStatus}>
                        {statusProductShop(billDetail.status)}
                    </Text>
                </View>
                {billDetail.status && billDetail.status !== 0 ? (
                    <Text style={styles.txtDateCreate}>
                        {`Thời gian tạo đơn: ${moment(billDetail.createdDate).format(
                            'DD/MM/yyyy kk:mm:ss',
                        )}`}
                    </Text>
                ) : (
                        <></>
                    )}
                <Text style={styles.txtDateCreate}>
                    {billDetail.status && billDetail.status !== 0
                        ? `${statusProductShop(
                            billDetail.status,
                        )} ngày: ${moment(billDetail.actionDate).format(
                            'DD/MM/yyyy kk:mm:ss',
                        )}`
                        : `Thời gian tạo đơn: ${moment(billDetail.createdDate).format(
                            'DD/MM/yyyy kk:mm:ss',
                        )}`}
                </Text>
            </View>
            <View style={{ ...styles.containerBillStatus, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <Text>{billDetail.custName}</Text>
                <TouchableOpacity onPress={onPressChat}>
                    <Icon name='comment' size={25} color={color.agree} />
                </TouchableOpacity>
            </View>
            <View style={styles.containerBillStatus}>
                <View style={styles.viewStatus}>
                    <Icon name="map-marker" size={20} color={color.agree} />
                    <Text style={styles.txtStatus} numberOfLines={2}>
                        Thông tin nhận hàng
            </Text>
                </View>
                <Text style={styles.txtInfo}>{billDetail.custName}</Text>
                <Text style={styles.txtInfo}>{billDetail.phoneNumber}</Text>
                <Text style={styles.txtInfo}>{billDetail.orderPlace}</Text>
            </View>
            <View style={styles.containerBillStatus}>
                <View style={styles.viewStatus}>
                    <Icon name="credit-card" size={20} color={color.agree} />
                    <Text style={styles.txtStatus} numberOfLines={2}>
                        Phương thức thanh toán
            </Text>
                </View>
                {billDetail.paymentStatus !== 1 ? (
                    <Text style={styles.txtInfo}>Thanh toán khi nhận hàng</Text>
                ) : (
                        <View>
                            <Text style={styles.txtInfo}>Thanh toán qua ngân hàng</Text>
                            <Text style={styles.txtInfo}>{billDetail.bankName}</Text>
                            <Text style={styles.txtInfo}>{'Đã thanh toán'}</Text>
                            {/* <Text style={styles.txtInfo}>{`STK: ${
                                billDetail.creditNumber
                                }`}</Text> */}
                        </View>
                    )}
            </View>
            <View style={styles.containerBillStatus}>
                <View>
                    <Text style={styles.shopName} numberOfLines={2}>
                        {billDetail.shopName}
                    </Text>
                    <View style={styles.viewShopAddress}>
                        <Icon name="map-marker" size={15} />
                        <Text style={styles.shopAddress}>
                            {billDetail.shopAddress}
                        </Text>
                    </View>
                </View>
                {_renderListProduct(billDetail)}
                <View style={styles.containerDelivery}>
                    <Text style={styles.titleDelivery}>Vận chuyển</Text>
                    <View style={styles.viewContentDelivery}>
                        <Text>Phí vận chuyển</Text>
                        <Text>
                            {`${formatCurrency(
                                billDetail.priceDelivery,
                            )} VNĐ`}
                        </Text>
                    </View>
                </View>
                <View style={styles.line} />
                <View style={styles.viewTotalPrice}>
                    <Text>Thành tiền</Text>
                    <Text>{`${formatCurrency(caculatePrice())} VNĐ`}</Text>
                </View>
                <View style={styles.viewNote}>
                    <Text style={styles.txtNote}>
                        {billDetail.note &&
                            billDetail.note !== ''
                            ? billDetail.note
                            : 'Không có ghi chú'}
                    </Text>
                </View>
            </View>
        </View>
        :
        <ActivityIndicator size="large" color={color.agree} />
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: color.lightGrayColor },
    containerBillStatus: {
        width: '100%',
        padding: 10,
        backgroundColor: '#fff',
        marginTop: 5,
    },
    viewStatus: { width: '100%', flexDirection: 'row', alignItems: 'flex-start' },
    txtStatus: {
        marginLeft: 10,
        fontSize: 16,
        fontWeight: '700',
        color: color.agree,
    },
    txtDateCreate: {
        marginTop: 5,
    },
    txtInfo: { fontSize: 16, marginTop: 5 },
    containerListProduct: {
        width: '100%',
        borderWidth: 1,
        borderColor: 'gray',
        paddingHorizontal: 10,
        paddingVertical: 5,
        flexDirection: 'row',
        marginTop: 10,
    },
    productImg: { width: 100, height: 80, resizeMode: 'stretch' },
    containerInfoProduct: { flex: 1, marginHorizontal: 10 },
    viewProductName: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    productName: { fontSize: 17 },
    viewPrice: { flexDirection: 'row', justifyContent: 'space-between' },
    oldPrice: {
        fontSize: 14,
        textDecorationLine: 'line-through',
        color: 'gray',
        marginRight: 10,
    },
    viewVote: { paddingTop: 10, alignItems: 'flex-start' },
    containerItem: {
        backgroundColor: '#fff',
        width: '100%',
        marginVertical: 5,
        paddingBottom: 10,
    },
    shopName: { fontSize: 20, color: '#000' },
    viewShopAddress: { flexDirection: 'row', alignItems: 'center', paddingTop: 5 },
    shopAddress: { marginLeft: 5, color: '#000' },
    containerDelivery: { paddingTop: 10 },
    titleDelivery: { color: color.agree, fontSize: 16 },
    viewContentDelivery: { flexDirection: 'row', justifyContent: 'space-between' },
    line: {
        width: '100%',
        height: 1,
        backgroundColor: '#000',
        marginVertical: 10,
    },
    viewTotalPrice: { flexDirection: 'row', justifyContent: 'space-between' },
    viewNote: {
        width: '100%',
        borderBottomWidth: 1,
        borderColor: 'gray',
        marginTop: 10,
    },
    txtNote: { color: 'gray' },
});

export default BillDetailBase;