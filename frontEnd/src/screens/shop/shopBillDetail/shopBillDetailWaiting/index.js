import React, {Component} from 'react';
import {
  Text,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  View,
  StyleSheet,
} from 'react-native';
import {TopBar, BtnBase} from '../../../../common/base';
import Icon from 'react-native-vector-icons/FontAwesome';
import BillDetailBase from '../BillDetailBase';
import {Navigation} from 'react-native-navigation';

class ShopBillDetailWaiting extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onPressBack = () => {
    Navigation.pop(this.props.componentId);
  };

  render() {
    const {data, componentId, ShopHomeStore} = this.props;
    return (
      <SafeAreaView style={{flex: 1}}>
        <TopBar
          childLeft={
            <TouchableOpacity onPress={this.onPressBack}>
              <Icon name="angle-left" size={40} style={{marginLeft: 10}} />
            </TouchableOpacity>
          }
          childCenter={
            <Text style={{fontSize: 16, textAlign: 'center'}}>
              Đơn chờ xác nhận
            </Text>
          }
        />
        <ScrollView style={{flex: 1}}>
          <BillDetailBase
            data={data}
            componentId={componentId}
            ShopHomeStore={ShopHomeStore}
          />
          <View style={styles.viewBtn}>
            <BtnBase
              styleBtn={styles.btn}
              textBtn="Huỷ đơn"
              backgroundColor="gray"
            />
            <BtnBase styleBtn={styles.btn} textBtn="Xác nhận" />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  viewBtn: {
    width: '100%',
    paddingTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    marginBottom: 20,
  },
  btn: {width: '40%', height: 40},
});

export default ShopBillDetailWaiting;
