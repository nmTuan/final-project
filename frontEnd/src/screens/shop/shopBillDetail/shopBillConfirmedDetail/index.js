import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {TopBar} from '../../../../common/base';
import Icon from 'react-native-vector-icons/FontAwesome';
import BillDetailBase from '../BillDetailBase';
import {Navigation} from 'react-native-navigation';

class ShopBillConfirmedDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onPressBack = () => {
    Navigation.pop(this.props.componentId);
  };

  render() {
    const {data, componentId, ShopHomeStore} = this.props;
    return (
      <SafeAreaView style={{flex: 1}}>
        <TopBar
          childLeft={
            <TouchableOpacity onPress={this.onPressBack}>
              <Icon name="angle-left" size={40} style={{marginLeft: 10}} />
            </TouchableOpacity>
          }
          childCenter={
            <Text style={{fontSize: 16, textAlign: 'center'}}>
              Đơn đã xác nhận
            </Text>
          }
        />
        <ScrollView style={{flex: 1}}>
          <BillDetailBase
            data={data}
            componentId={componentId}
            ShopHomeStore={ShopHomeStore}
          />
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  containerBtn: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
    alignItems: 'center',
  },
  btn: {height: 40, width: '45%'},
  backgroundRating: {
    width: '100%',
    height: '100%',
    backgroundColor: '#00000050',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerRating: {
    width: '80%',
    backgroundColor: '#fff',
    borderRadius: 10,
    padding: 10,
    alignItems: 'center',
  },
  titleRating: {fontSize: 18},
  viewRating: {paddingTop: 10},
  textInputRating: {
    height: 80,
    width: '100%',
    borderWidth: 1,
    borderColor: 'gray',
    textAlignVertical: 'top',
    borderRadius: 6,
    marginTop: 10,
  },
  viewBtnRating: {
    width: '100%',
    flexDirection: 'row',
    paddingTop: 20,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  btnRating: {width: '45%', height: 30},
});

export default ShopBillConfirmedDetail;
