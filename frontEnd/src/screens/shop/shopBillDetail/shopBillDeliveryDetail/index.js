import React, {Component} from 'react';
import {Text, SafeAreaView, TouchableOpacity, ScrollView} from 'react-native';
import {Navigation} from 'react-native-navigation';
import {TopBar} from '../../../../common/base';
import Icon from 'react-native-vector-icons/FontAwesome';
import BillDetailBase from '../BillDetailBase';

class ShopBillDeliveryDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onPressBack = () => {
    Navigation.pop(this.props.componentId);
  };

  render() {
    const {data, componentId, ShopHomeStore} = this.props;
    return (
      <SafeAreaView style={{flex: 1}}>
        <TopBar
          childLeft={
            <TouchableOpacity onPress={this.onPressBack}>
              <Icon name="angle-left" size={40} style={{marginLeft: 10}} />
            </TouchableOpacity>
          }
          childCenter={
            <Text style={{fontSize: 16, textAlign: 'center'}}>Đang giao hàng</Text>
          }
        />
        <ScrollView style={{flex: 1}}>
          <BillDetailBase
            data={data}
            componentId={componentId}
            ShopHomeStore={ShopHomeStore}
          />
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default ShopBillDeliveryDetail;
