import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import BaseBill from '../BaseBill';
import {TouchablePreventDouble, BtnBase} from '../../../../common/base';
import moment from 'moment';
import {pushScreen} from '../../../../config/NavigationConfig';
import {color} from '../../../../constant';

type Props = {
  onDelivery?: (v: value, i: index) => {},
  onAbort?: (v: value, i: index) => {},
};

class ItemBillFail extends React.PureComponent<Props> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onPressBill = () => {
    const {item, componentId, ShopHomeStore} = this.props;
    const passProps = {
      data: item,
      ShopHomeStore: ShopHomeStore,
    };
    pushScreen(componentId, 'ShopBillFailDetail', passProps);
  };

  render() {
    const {item, index, onDelivery, onAbort} = this.props;
    return (
      <TouchablePreventDouble
        style={styles.container}
        key={`${index}`}
        activeOpacity={1}
        onPress={this.onPressBill}>
        <BaseBill item={item} index={index} />
        <View style={styles.viewBottom}>
          <Text style={styles.txtDateCreate}>
            {`Ngày giao hàng: ${moment(item.actionDate).format('DD/MM/YYYY')}`}
          </Text>
          <View style={{flexDirection: 'row'}}>
            <BtnBase
              styleBtn={[styles.btnAbort, {marginRight: 15}]}
              textBtn="Huỷ đơn"
              backgroundColor={color.lightGrayColor}
              textColor="#000"
              onPress={() => onAbort(item, index)}
            />
            <BtnBase
              styleBtn={styles.btnAbort}
              textBtn="Giao lại"
              onPress={() => onDelivery(item, index)}
            />
          </View>
        </View>
      </TouchablePreventDouble>
    );
  }
}

const styles = StyleSheet.create({
  container: {backgroundColor: '#fff', marginTop: 20, paddingBottom: 10},
  viewBottom: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
  },
  txtDateCreate: {color: color.agree},
  btnAbort: {height: 30},
});

export default ItemBillFail;
