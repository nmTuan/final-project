import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  RefreshControl,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import ItemBillSuccess from './ItemBillSuccess';
import {shopService} from '../../../../services/ShopService';
import {values, color} from '../../../../constant';
import {observer} from 'mobx-react';

@observer
class BillSuccess extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      isLoadData: true,
    };
  }

  componentDidMount() {
    this.getData();
  }

  async getData() {
    const {ShopHomeStore} = this.props;
    var bills = [];
    const bill = await shopService.getListBill(
      ShopHomeStore.shopInfo.id,
      values.bill.succes,
    );
    if (
      bill.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      bills = [...bill.result.result];
    }
    ShopHomeStore.setBillSuccess(bills);
    this.setState({
      isLoadData: false,
    });
  }

  onRefresh = () => {
    this.setState({
      refreshing: true,
    });
    this.getData();
    this.setState({
      refreshing: false,
    });
  };

  render() {
    const {componentId, ShopHomeStore} = this.props;
    const {refreshing, isLoadData} = this.state;
    return (
      <View>
        {!isLoadData ? (
          ShopHomeStore.billSuccess.length > 0 ? (
            <FlatList
              data={ShopHomeStore.billSuccess}
              refreshing={refreshing}
              onRefresh={this.onRefresh}
              renderItem={({item, index}) => (
                <ItemBillSuccess
                  item={item}
                  index={index}
                  componentId={componentId}
                  ShopHomeStore={ShopHomeStore}
                />
              )}
            />
          ) : (
            <View style={styles.containerEmpty}>
              <ScrollView
                style={{flex: 1}}
                contentContainerStyle={styles.scrollView}
                refreshControl={
                  <RefreshControl
                    refreshing={refreshing}
                    onRefresh={this.onRefresh}
                  />
                }>
                <Text style={styles.txtNoneProduct}>Không có sản phẩm</Text>
              </ScrollView>
            </View>
          )
        ) : (
          <View style={styles.loading}>
            <ActivityIndicator size="large" color={color.agree} />
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  txtNoneProduct: {textAlign: 'center', marginTop: 10},
  scrollView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerEmpty: {width: '100%', height: '100%'},
  loading: {
    width: '100%',
    height: '100%',
    alignContent: 'center',
    justifyContent: 'center',
  },
});

export default BillSuccess;
