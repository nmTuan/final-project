import React from 'react';
import { Navigation } from 'react-native-navigation';
import { TouchablePreventDouble } from '../../../../common/base';
import BaseBill from '../BaseBill';
import { View, Text, StyleSheet } from 'react-native';
import { color } from '../../../../constant';
import moment from 'moment';
import {statusProductShop} from '../../../../constant/Format';
import { showModal, pushScreen } from '../../../../config/NavigationConfig';

const IteamAbortBill = ({ item, index, componentId, ShopHomeStore }) => {
    const onPressBill = () => {
      const  passProps = {
        data: item,
        ShopHomeStore: ShopHomeStore,
      };
      pushScreen(componentId, 'ShopBillAbortedDetail', passProps);
    };
    return (
        <TouchablePreventDouble
            style={styles.container}
            key={`${index}`}
            activeOpacity={1}
            onPress={onPressBill}>
            <BaseBill item={item} index={index} />
            <View style={styles.viewBottom}>
                <Text style={styles.txtDateCreate}>
                    {`Ngày huỷ đơn: ${moment(item.actionDate).format('DD/MM/YYYY')}`}
                </Text>
                <Text style={styles.txtDateCreate}>{statusProductShop(item.status)}</Text>
            </View>
        </TouchablePreventDouble>
    )
}

const styles = StyleSheet.create({
    container: { backgroundColor: '#fff', marginTop: 20, paddingBottom: 10 },
    viewBottom: {
        width: '100%',
        justifyContent: 'center',
        paddingHorizontal: 10,
    },
    txtDateCreate: { color: color.agree, marginTop: 5 },
});

export default IteamAbortBill;