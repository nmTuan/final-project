import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  ScrollView,
  RefreshControl,
  ActivityIndicator,
} from 'react-native';
import IteamAbortBill from './ItemAbortBill';
import {shopService} from '../../../../services/ShopService';
import {values, color} from '../../../../constant';
import {observer} from 'mobx-react';

@observer
class BillAborted extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      isLoadData: true,
    };
  }

  componentDidMount() {
    this.getData();
  }

  async getData() {
    const {ShopHomeStore} = this.props;
    const id = ShopHomeStore.shopInfo.id;
    const {abort, custAbort} = values.bill;
    var billCust = [];
    var billShop = [];
    const billAbortFromCust = await shopService.getListBill(id, custAbort);
    const billAbortFromShop = await shopService.getListBill(id, abort);
    if (
      billAbortFromCust.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      billCust = [...billAbortFromCust.result.result];
    }
    if (
      billAbortFromShop.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      billShop = [...billAbortFromShop.result.result];
    }
    ShopHomeStore.setBillAbort([...billCust, ...billShop]);
    this.setState({
      isLoadData: false,
    });
  }

  onRefresh = () => {
    this.setState({
      refreshing: true,
    });
    this.getData();
    this.setState({
      refreshing: false,
    });
  };

  render() {
    const {ShopHomeStore, componentId} = this.props;
    const {refreshing, isLoadData} = this.state;
    return (
      <View>
        {!isLoadData ? (
          ShopHomeStore.billAbort.length > 0 ? (
            <FlatList
              data={ShopHomeStore.billAbort}
              refreshing={refreshing}
              onRefresh={this.onRefresh}
              renderItem={({item, index}) => (
                <IteamAbortBill
                  item={item}
                  index={index}
                  componentId={componentId}
                  ShopHomeStore={ShopHomeStore}
                />
              )}
            />
          ) : (
            <View style={styles.containerEmpty}>
              <ScrollView
                style={{flex: 1}}
                contentContainerStyle={styles.scrollView}
                refreshControl={
                  <RefreshControl
                    refreshing={refreshing}
                    onRefresh={this.onRefresh}
                  />
                }>
                <Text style={styles.txtNoneProduct}>Không có sản phẩm</Text>
              </ScrollView>
            </View>
          )
        ) : (
          <View style={styles.loading}>
            <ActivityIndicator size="large" color={color.agree} />
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  txtNoneProduct: {textAlign: 'center', marginTop: 10},
  scrollView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerEmpty: {width: '100%', height: '100%'},
  loading: {
    width: '100%',
    height: '100%',
    alignContent: 'center',
    justifyContent: 'center',
  },
});

export default BillAborted;
