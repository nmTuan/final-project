import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { TouchablePreventDouble, BtnBase } from '../../../../common/base';
import BaseBill from '../BaseBill';
import { color } from '../../../../constant';
import { Navigation } from 'react-native-navigation';
import moment from 'moment';
import { showModal, pushScreen } from '../../../../config/NavigationConfig';

const ItemBillWaiting = ({ item, index, componentId, abortBill, confirmBill, ShopHomeStore }) => {
    const onPressBill = () => {
        const passProps = {
            data: item,
            ShopHomeStore: ShopHomeStore,
        };
        pushScreen(componentId, 'ShopBillDetailWaiting', passProps);
    }

    return (
        <TouchablePreventDouble
            key={`${index}`}
            style={styles.container}
            activeOpacity={1}
            onPress={onPressBill}>
            <BaseBill item={item} index={index} />
            <View style={styles.viewBottom}>
                <Text style={styles.txtDateCreate}>
                    {`Ngày tạo đơn: ${moment(item.createdDate).format('DD/MM/YYYY')}`}
                </Text>
                <View style={{ flexDirection: 'row' }}>
                    <BtnBase
                        styleBtn={styles.btnAbort}
                        textBtn="Huỷ đơn hàng"
                        backgroundColor={color.lightGrayColor}
                        textColor="#000"
                        onPress={() => abortBill(item, index)}
                    />
                    <BtnBase
                        styleBtn={styles.btnAbort}
                        textBtn="Xác nhận"
                        onPress={() => confirmBill(item, index)}
                    />
                </View>

            </View>
        </TouchablePreventDouble>
    )
}

const styles = StyleSheet.create({
    container: { backgroundColor: '#fff', marginTop: 20, paddingBottom: 10 },
    viewBottom: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
    },
    txtDateCreate: { color: color.agree },
    btnAbort: { height: 30, marginLeft: 10 },
});

export default ItemBillWaiting;