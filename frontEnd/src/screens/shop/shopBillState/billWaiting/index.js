import React, {Component} from 'react';
import {
  View,
  Text,
  Modal,
  StyleSheet,
  FlatList,
  ScrollView,
  RefreshControl,
  ActivityIndicator,
} from 'react-native';
import ItemBillWaiting from './ItemBillWaiting';
import Reason from '../Reason';
import {shopService} from '../../../../services/ShopService';
import {values, color} from '../../../../constant';
import {notifyMessage} from '../../../../config/Func';
import {observer} from 'mobx-react';

@observer
class BillWaiting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showReason: false,
      item: null,
      index: -1,
      refreshing: false,
      isLoadData: true,
    };
  }

  componentDidMount() {
    this.getData();
  }

  async getData() {
    const {ShopHomeStore} = this.props;
    var bills = [];
    const bill = await shopService.getListBill(
      ShopHomeStore.shopInfo.id,
      values.bill.waiting,
    );
    if (
      bill.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      bills = [...bill.result.result];
    }
    ShopHomeStore.setBillWaiting(bills);
    this.setState({
      isLoadData: false,
    });
  }
  onAbortBill = (item, index) => {
    this.setState({
      showReason: true,
      item,
      index,
    });
  };

  onPressSend = async value => {
    const {ShopHomeStore} = this.props;
    const {item, index} = this.state;
    const res = await shopService.changeSatausBill(
      item.id,
      values.bill.abort,
      value,
    );
    if (
      res.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      ShopHomeStore.billWaiting.splice(index, 1);
      item.statsu = values.bill.abort;
      ShopHomeStore.billAbort.unshift(item);
    } else {
      notifyMessage(res.messge);
    }
    this.setState({
      showReason: false,
    });
  };

  onPressCancel = () => {
    this.setState({
      showReason: false,
    });
  };

  onRefresh = () => {
    this.setState({
      refreshing: true,
    });
    this.getData();
    this.setState({
      refreshing: false,
    });
  };

  onConfirmBill = async (item, index) => {
    const {ShopHomeStore} = this.props;
    const res = await shopService.changeSatausBill(
      item.id,
      values.bill.confirmed,
      '',
    );
    if (
      res.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      ShopHomeStore.billWaiting.splice(index, 1);
      item.status = values.bill.confirmed;
      ShopHomeStore.billConfirm.unshift(item);
    } else {
      notifyMessage(res.messge);
    }
  };

  render() {
    const {componentId, ShopHomeStore} = this.props;
    const {showReason, refreshing, isLoadData} = this.state;
    return (
      <View>
        {!isLoadData ? (
          ShopHomeStore.billWaiting.length > 0 ? (
            <FlatList
              data={ShopHomeStore.billWaiting}
              refreshing={refreshing}
              onRefresh={this.onRefresh}
              renderItem={({item, index}) => (
                <ItemBillWaiting
                  item={item}
                  index={index}
                  componentId={componentId}
                  ShopHomeStore={ShopHomeStore}
                  abortBill={this.onAbortBill}
                  confirmBill={this.onConfirmBill}
                />
              )}
            />
          ) : (
            <View style={styles.containerEmpty}>
              <ScrollView
                style={{flex: 1}}
                contentContainerStyle={styles.scrollView}
                refreshControl={
                  <RefreshControl
                    refreshing={refreshing}
                    onRefresh={this.onRefresh}
                  />
                }>
                <Text style={styles.txtNoneProduct}>Không có sản phẩm</Text>
              </ScrollView>
            </View>
          )
        ) : (
          <View style={styles.loading}>
            <ActivityIndicator size="large" color={color.agree} />
          </View>
        )}
        <Modal visible={showReason} transparent>
          <Reason
            onPressCancel={this.onPressCancel}
            onPressSend={this.onPressSend}
          />
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  txtNoneProduct: {textAlign: 'center', marginTop: 10},
  scrollView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerEmpty: {width: '100%', height: '100%'},
  loading: {
    width: '100%',
    height: '100%',
    alignContent: 'center',
    justifyContent: 'center',
  },
});

export default BillWaiting;
