import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import BaseBill from '../BaseBill';
import {TouchablePreventDouble, BtnBase} from '../../../../common/base';
import moment from 'moment';
import {pushScreen} from '../../../../config/NavigationConfig';
import {color} from '../../../../constant';

type Props = {
  onFail?: (v: value, i: index) => {},
  onSuccess?: (v: value, i: index) => {},
};

class ItemBillDelivering extends React.PureComponent<Props> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onPressBill = () => {
    const {item, componentId, ShopHomeStore} = this.props;
    const passProps = {
      data: item,
      ShopHomeStore: ShopHomeStore,
    };
    pushScreen(componentId, 'ShopBillDeliveryDetail', passProps);
  };

  render() {
    const {item, index, onFail, onSuccess} = this.props;
    return (
      <TouchablePreventDouble
        style={styles.container}
        key={`${index}`}
        activeOpacity={1}
        onPress={this.onPressBill}>
        <BaseBill item={item} index={index} />
        <View style={styles.viewBottom}>
          <Text style={styles.txtDateCreate}>
            {`Ngày Xác nhận đơn: ${moment(item.actionDate).format(
              'DD/MM/YYYY',
            )}`}
          </Text>
          <View style={{flexDirection: 'row'}}>
            <BtnBase
              styleBtn={[styles.btnAbort, {marginRight: 5}]}
              textBtn="Thất bại"
              backgroundColor={color.lightGrayColor}
              textColor="#000"
              onPress={() => onFail(item, index)}
            />
            <BtnBase
              styleBtn={styles.btnAbort}
              textBtn="Thành công"
              onPress={() => onSuccess(item, index)}
            />
          </View>
        </View>
      </TouchablePreventDouble>
    );
  }
}

const styles = StyleSheet.create({
  container: {backgroundColor: '#fff', marginTop: 20, paddingBottom: 10},
  viewBottom: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
  },
  txtDateCreate: {color: color.agree},
  btnAbort: {height: 30},
});

export default ItemBillDelivering;
