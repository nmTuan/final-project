import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  RefreshControl,
  FlatList,
  ActivityIndicator,
  Modal,
} from 'react-native';
import ItemBillDelivering from './ItemBillDelivering';
import {shopService} from '../../../../services/ShopService';
import {values, color} from '../../../../constant';
import {observer} from 'mobx-react';
import ReasonDelivery from '../ReasonDelivery';
import { notifyMessage } from '../../../../config/Func';

@observer
class BillDelivering extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      isLoadData: true,
      showReason: false,
      item: null,
      index: 0,
    };
  }

  componentDidMount() {
    this.getData();
  }

  async getData() {
    const {ShopHomeStore} = this.props;
    var bills = [];
    const bill = await shopService.getListBill(
      ShopHomeStore.shopInfo.id,
      values.bill.delivering,
    );
    if (
      bill.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      bills = [...bill.result.result];
    }
    ShopHomeStore.setBillDelivering(bills);
    this.setState({
      isLoadData: false,
    });
  }

  onRefresh = () => {
    this.setState({
      refreshing: true,
    });
    this.getData();
    this.setState({
      refreshing: false,
    });
  };

  onFail = (item, index) => {
    this.setState({
      showReason: true,
      item,
      index,
    });
  };

  onSuccess = async (item, index) => {
    const {ShopHomeStore} = this.props;
    const res = await shopService.changeSatausBill(
      item.id,
      values.bill.succes,
      '',
    );
    if (
      res.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      ShopHomeStore.billDelivering.splice(index, 1);
      item.status = values.bill.succes;
      ShopHomeStore.billSuccess.unshift(item);
    } else {
      notifyMessage(res.message);
    }
  };

  dissMissModal = () => {
    this.setState({
      showReason: false,
    });
  };

  onPressSend = async value => {
    const {item, index} = this.state;
    const {ShopHomeStore} = this.props;
    const res = await shopService.changeSatausBill(
      item.id,
      values.bill.fail,
      value,
    );
    if (
      res.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      ShopHomeStore.billDelivering.splice(index, 1);
      item.status = values.bill.fail;
      ShopHomeStore.billFail.unshift(item);
    } else {
      notifyMessage(res.message);
    }
    this.setState({
      showReason: false,
    });
  };

  render() {
    const {componentId, ShopHomeStore} = this.props;
    const {refreshing, isLoadData, showReason} = this.state;
    return (
      <View>
        {!isLoadData ? (
          ShopHomeStore.billDelivering.length > 0 ? (
            <FlatList
              data={ShopHomeStore.billDelivering}
              refreshing={refreshing}
              onRefresh={this.onRefresh}
              renderItem={({item, index}) => (
                <ItemBillDelivering
                  item={item}
                  index={index}
                  componentId={componentId}
                  ShopHomeStore={ShopHomeStore}
                  onFail={this.onFail}
                  onSuccess={this.onSuccess}
                />
              )}
            />
          ) : (
            <View style={styles.containerEmpty}>
              <ScrollView
                style={{flex: 1}}
                contentContainerStyle={styles.scrollView}
                refreshControl={
                  <RefreshControl
                    refreshing={refreshing}
                    onRefresh={this.onRefresh}
                  />
                }>
                <Text style={styles.txtNoneProduct}>Không có sản phẩm</Text>
              </ScrollView>
            </View>
          )
        ) : (
          <View style={styles.loading}>
            <ActivityIndicator size="large" color={color.agree} />
          </View>
        )}
        <Modal visible={showReason} transparent animationType="slide">
          <ReasonDelivery
            onPressCancel={this.dissMissModal}
            onPressConfirm={this.onPressSend}
          />
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  txtNoneProduct: {textAlign: 'center', marginTop: 10},
  scrollView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerEmpty: {width: '100%', height: '100%'},
  loading: {
    width: '100%',
    height: '100%',
    alignContent: 'center',
    justifyContent: 'center',
  },
});

export default BillDelivering;
