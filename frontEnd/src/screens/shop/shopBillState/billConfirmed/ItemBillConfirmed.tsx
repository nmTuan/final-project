import React from 'react';
import TouchablePreventDouble from '../../../../common/base/TouchablePreventDouble';
import BaseBill from '../BaseBill';
import { View, Text, StyleSheet } from 'react-native';
import { color } from '../../../../constant';
import { Navigation } from 'react-native-navigation';
import moment from 'moment';
import { showModal, pushScreen } from '../../../../config/NavigationConfig';
import { BtnBase } from '../../../../common/base';

const ItemBillConfirmed = ({ item, index, componentId, abortBill, delevery, ShopHomeStore }) => {
  const onPressBill = () => {
    const passProps = {
      data: item,
      ShopHomeStore: ShopHomeStore,
    };
    pushScreen(componentId, 'ShopBillConfirmedDetail', passProps);
  };

  return (
    <TouchablePreventDouble
      style={styles.container}
      key={`${index}`}
      activeOpacity={1}
      onPress={onPressBill}>
      <BaseBill item={item} index={index} />
      <View style={styles.viewBottom}>
        <Text style={styles.txtDateCreate}>
          {`Ngày Xác nhận đơn: ${moment(item.actionDate).format('DD/MM/YYYY')}`}
        </Text>
        <View style={{ flexDirection: 'row'}}>
          <BtnBase
            styleBtn={[styles.btnAbort,{marginRight: 5}]}
            textBtn="Huỷ đơn hàng"
            backgroundColor={color.lightGrayColor}
            textColor="#000"
            onPress={() => abortBill(item, index)}
          />
          <BtnBase
            styleBtn={styles.btnAbort}
            textBtn="Vận chuyển"
            onPress={() => delevery(item, index)}
          />
        </View>
      </View>
    </TouchablePreventDouble>
  )
}

const styles = StyleSheet.create({
  container: { backgroundColor: '#fff', marginTop: 20, paddingBottom: 10 },
  viewBottom: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
  },
  txtDateCreate: { color: color.agree },
  btnAbort: { height: 30 },
});

export default ItemBillConfirmed;