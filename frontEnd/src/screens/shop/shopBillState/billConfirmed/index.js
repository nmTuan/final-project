import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  RefreshControl,
  FlatList,
  ActivityIndicator,
  Modal,
} from 'react-native';
import ItemBillConfirmed from './ItemBillConfirmed';
import {shopService} from '../../../../services/ShopService';
import {values, color} from '../../../../constant';
import {observer} from 'mobx-react';
import Reason from '../Reason';
import {notifyMessage} from '../../../../config/Func';

@observer
class BillConfirmed extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      isLoadData: true,
      showReason: false,
      item: null,
      index: 0,
    };
  }

  componentDidMount() {
    this.getData();
  }

  async getData() {
    const {ShopHomeStore} = this.props;
    var bills = [];
    const bill = await shopService.getListBill(
      ShopHomeStore.shopInfo.id,
      values.bill.confirmed,
    );
    if (
      bill.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      bills = [...bill.result.result];
    }
    ShopHomeStore.setBillConfirm(bills);
    this.setState({
      isLoadData: false,
    });
  }

  onRefresh = () => {
    this.setState({
      refreshing: true,
    });
    this.getData();
    this.setState({
      refreshing: false,
    });
  };

  onPressSend = async value => {
    const {ShopHomeStore} = this.props;
    const {item, index} = this.state;
    const res = await shopService.changeSatausBill(
      item.id,
      values.bill.abort,
      value,
    );
    if (
      res.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      ShopHomeStore.billConfirm.splice(index, 1);
      item.status = values.bill.abort;
      ShopHomeStore.billAbort.unshift(item);
    } else {
      notifyMessage(res.messge);
    }
    this.setState({
      showReason: false,
    });
  };

  onPressCancel = () => {
    this.setState({
      showReason: false,
    });
  };

  onAbortBill = (item, index) => {
    this.setState({
      showReason: true,
      item,
      index,
    });
  };

  onDelivery = async (item, index) => {
    const {ShopHomeStore} = this.props;
    const res = await shopService.changeSatausBill(
      item.id,
      values.bill.delivering,
      '',
    );
    if (
      res.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      ShopHomeStore.billConfirm.splice(index, 1);
      item.status = values.bill.delivering;
      ShopHomeStore.billDelivering.unshift(item);
    } else {
      notifyMessage(res.message);
    }
  };

  render() {
    const {componentId, ShopHomeStore} = this.props;
    const {refreshing, isLoadData, showReason} = this.state;
    return (
      <View>
        {!isLoadData ? (
          ShopHomeStore.billConfirm.length > 0 ? (
            <FlatList
              data={ShopHomeStore.billConfirm}
              refreshing={refreshing}
              onRefresh={this.onRefresh}
              renderItem={({item, index}) => (
                <ItemBillConfirmed
                  item={item}
                  index={index}
                  componentId={componentId}
                  ShopHomeStore={ShopHomeStore}
                  abortBill={this.onAbortBill}
                  delevery={this.onDelivery}
                />
              )}
            />
          ) : (
            <View style={styles.containerEmpty}>
              <ScrollView
                style={{flex: 1}}
                contentContainerStyle={styles.scrollView}
                refreshControl={
                  <RefreshControl
                    refreshing={refreshing}
                    onRefresh={this.onRefresh}
                  />
                }>
                <Text style={styles.txtNoneProduct}>Không có sản phẩm</Text>
              </ScrollView>
            </View>
          )
        ) : (
          <View style={styles.loading}>
            <ActivityIndicator size="large" color={color.agree} />
          </View>
        )}
        <Modal visible={showReason} transparent>
          <Reason
            onPressCancel={this.onPressCancel}
            onPressSend={this.onPressSend}
          />
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  txtNoneProduct: {textAlign: 'center', marginTop: 10},
  scrollView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerEmpty: {width: '100%', height: '100%'},
  loading: {
    width: '100%',
    height: '100%',
    alignContent: 'center',
    justifyContent: 'center',
  },
});

export default BillConfirmed;
