import React, { useState } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { color } from '../../../constant';
import { CheckBox } from 'react-native-elements';
import {notifyMessage} from '../../../config/Func';



const Reason = ({onPressCancel, onPressSend}) => {
    const [indexCheck, setIndexChecked] = useState(-1);
    const [reasons, setReasons] = useState([
        { id: 1, name: 'Sản phẩm này đã hết' },
        { id: 2, name: 'Không đủ số lượng sản phẩm' },
    ])
    const onPressConfirm = () => {
        if (indexCheck >= 0) {
            onPressSend(reasons[indexCheck].name);
        } else {
            notifyMessage('Bạn cần phải chọn lý do huỷ hàng');
        }
    }

    const renderItem = (item, index ) => {
        const onPressCheck = check => {
            setIndexChecked(check)
        };
        return (
            <View style={styles.containerItem}>
                <CheckBox
                    title={item.name}
                    checkedIcon="dot-circle-o"
                    uncheckedIcon="circle-o"
                    checked={indexCheck === index ? true : false}
                    onPress={() => onPressCheck(index)}
                    containerStyle={styles.containerCheckBox}
                />
            </View>
        );
    };

    return (
        <View style={styles.background}>
            <View style={styles.container}>
                <View style={{ padding: 10 }}>
                    <Text style={styles.title}>Lý do bạn muốn huỷ đơn hàng</Text>
                    {reasons.map((item, index) => renderItem(item, index))}
                </View>
                <View style={styles.containerBtn}>
                    <TouchableOpacity
                        style={styles.btnCancel}
                        onPress={onPressCancel}>
                        <Text style={styles.txtCancel}>Huỷ</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnSend} onPress={onPressConfirm}>
                        <Text>Gửi</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    background: {
        width: '100%',
        height: '100%',
        backgroundColor: '#00000066',
        alignItems: 'center',
        justifyContent: 'center',
    },
    container: {
        width: ' 70%',
        borderRadius: 8,
        backgroundColor: '#fff',
    },
    title: { fontSize: 18, color: 'gray' },
    containerItem: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 10,
    },
    containerCheckBox: {
        width: '100%',
        borderColor: 'transparent',
        padding: 0,
        backgroundColor: 'transparent',
    },
    containerBtn: {
        width: '100%',
        flexDirection: 'row',
        borderTopWidth: 1,
        borderColor: color.lightGrayColor,
        height: 30,
    },
    btnCancel: {
        flex: 1,
        borderBottomLeftRadius: 8,
        backgroundColor: color.agree,
        alignItems: 'center',
        justifyContent: 'center',
    },
    txtCancel: { color: '#fff' },
    btnSend: {
        flex: 1,
        borderBottomRightRadius: 8,
        alignItems: 'center',
        justifyContent: 'center',
    },
});

export default Reason;