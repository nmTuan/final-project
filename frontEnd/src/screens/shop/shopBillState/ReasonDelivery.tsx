import React, { useState } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity } from "react-native";
import { color, values } from '../../../constant';
import { notifyMessage } from '../../../config/Func';

const ReasonDelivery = ({ onPressCancel, onPressConfirm = (value: String) => {} }) => {
    const [value, setValue] = useState('');

    const onPressSend = () => {
        if (value !== '') {
            onPressConfirm(value);
        } else {
            notifyMessage('Lý do không được bỏ trống');
        }
    }
    return (
        <View style={styles.background}>
            <View style={styles.container}>
                <View style={styles.inner}>
                    <Text style={styles.text}>Lý do</Text>
                    <TextInput
                        placeholder="Nhập lý do..."
                        style={styles.textInput}
                        textAlignVertical="top"
                        multiline
                        value={value}
                        onChangeText={value => setValue(value)}
                    />
                </View>
                <View style={styles.containerBtn}>
                    <TouchableOpacity
                        style={styles.btnCancel}
                        onPress={onPressCancel}>
                        <Text style={styles.txtCancel}>Huỷ</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnSend} onPress={onPressSend}>
                        <Text>Gửi</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    background: {
        width: '100%',
        height: '100%',
        backgroundColor: '#00000066',
        alignItems: 'center',
        justifyContent: 'center',
    },
    container: {
        width: '80%',
        backgroundColor: '#fff',
        borderRadius: 6,
    },
    inner: {
        padding: 10,
    },
    text: {
        fontSize: 16,
    },
    textInput: {
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 8,
        fontSize: 13,
        height: 90,
        marginTop: 10,
    },
    containerBtn: {
        width: '100%',
        flexDirection: 'row',
        borderTopWidth: 1,
        borderColor: color.lightGrayColor,
        height: 40,
    },
    btnCancel: {
        flex: 1,
        borderBottomLeftRadius: 6,
        backgroundColor: 'gray',
        alignItems: 'center',
        justifyContent: 'center',
    },
    txtCancel: { color: '#fff' },
    btnSend: {
        flex: 1,
        borderBottomRightRadius: 8,
        alignItems: 'center',
        justifyContent: 'center',
    },
})

export default ReasonDelivery;