import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  Modal,
  ActivityIndicator,
  KeyboardAvoidingView,
  Platform,
  Keyboard,
  SafeAreaView,
} from 'react-native';
import {TxtError, TouchablePreventDouble} from '../../common/base';
import BtnBase from '../../common/base/BtnBase';
import ForgotPassword from '../forgotPassword';
import SignUpForm from '../signUpForm';
import {MainApp, MainShop} from '../../navigation/Navigation';
import {AccountService} from '../../services/AccountService';
import {UserService} from '../../services/UserService';
import {color, values} from '../../constant';
import {inject, observer} from 'mobx-react';
import {notifyMessage} from '../../config/Func';
import AsyncStorage from '@react-native-community/async-storage';
import {CheckBox} from 'react-native-elements';
import {shopService} from '../../services/ShopService';
import {Navigation} from 'react-native-navigation';
import {pushScreen} from '../../config/NavigationConfig';

@inject('UserStore', 'SignUpStore', 'AddressStore', 'ShopHomeStore')
@observer
class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showForgotPass: false,
      showModal: false,
      username: '',
      password: '',
      error: '',
      isLoading: false,
      isShop: false,
    };
  }

  onPressBack = () => {
    Navigation.pop(this.props.componentId);
  };

  onPressSignUp = () => {
    Keyboard.dismiss();
    pushScreen(this.props.componentId, 'SignUpForm');
  };

  onPressForgotPass = () => {
    this.setState({
      showModal: true,
      showForgotPass: true,
    });
  };

  onPressBackForgotPass = () => {
    this.setState({
      showForgotPass: false,
      showModal: false,
    });
  };

  cancelModalSignUp = () => {
    this.setState({
      showLoginForm: false,
      showModal: false,
    });
  };

  onChangeUsername = value => {
    this.setState({
      username: value,
    });
  };

  onChangePassword = value => {
    this.setState({
      password: value,
    });
  };

  onPressLogin = async () => {
    const {username, password, isShop} = this.state;
    if (username !== '') {
      if (password !== '') {
        this.setState({
          isLoading: true,
        });
        const res = await AccountService.login(username, password);
        if (res.userId) {
          await AsyncStorage.setItem(values.TOKEN, `${res.accessToken}`);
          if (isShop) {
            this.loginShop(res);
          } else {
            this.loginCustomer(res);
          }
        } else {
          this.setState({
            error: res.message,
            isLoading: false,
          });
        }
      } else {
        this.setState({
          error: 'Password không được để trống',
        });
      }
    } else {
      this.setState({
        error: 'Username không được để trống',
      });
    }
  };

  loginShop = async res => {
    const {ShopHomeStore} = this.props;
    const {username} = this.state;
    const shopInfo = await shopService.getShopInfoByAcc(res.userId);
    if (shopInfo.errorCode === 200) {
      Keyboard.dismiss();
      await ShopHomeStore.setShopInfo(shopInfo.result);
      ShopHomeStore.setUsername(username);
      this.setState({
        isLoading: false,
      });
      MainShop();
    } else {
      this.setState({
        isLoading: false,
      });
      notifyMessage(shopInfo.message);
    }
  };

  loginCustomer = async res => {
    const {UserStore, firstLogin} = this.props;
    const {username} = this.state;
    const userInfo = await UserService.getUserInfo(res.userId);
    UserStore.setUsername(username);
    if (userInfo.errorCode === 404) {
      Keyboard.dismiss();
      UserStore.isNoneInfo = true;
      UserStore.signupInfo.accId = res.userId;
      this.setState({
        showModal: true,
        isLoading: false,
      });
    } else {
      if (userInfo.result) {
        await UserStore.setUserInfo(userInfo.result, res.userId);
        this.setState({
          isLoading: false,
        });
        if (firstLogin) {
          Navigation.pop(this.props.componentId);
        } else {
          MainApp(3);
        }
      } else {
        this.setState({
          isLoading: false,
        });
        notifyMessage(res.message);
      }
    }
  };

  onPressLoginShop = () => {
    this.setState({
      isShop: !this.state.isShop,
    });
  };

  render() {
    const {showForgotPass, showModal, error, isLoading, isShop} = this.state;
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <SafeAreaView>
          <View style={styles.cotainer}>
            <Image
              source={require('@assets/logo/logo1.png')}
              style={styles.logo}
            />
            <View style={styles.containerTextInput}>
              <Text>Username</Text>
              <TextInput
                style={styles.textInput}
                placeholder="Nhập tên tài khoản"
                onChangeText={this.onChangeUsername}
              />
            </View>
            <View style={styles.containerTextInput}>
              <Text>Password</Text>
              <TextInput
                style={styles.textInput}
                placeholder="Nhập mật khẩu"
                secureTextEntry
                onChangeText={this.onChangePassword}
              />
            </View>
            {error !== '' && <TxtError text={error} />}
            <View style={{width: '100%', alignItems: 'flex-start'}}>
              <CheckBox
                containerStyle={styles.checkBox}
                title="Đăng nhập quyền shop"
                checked={isShop}
                onPress={this.onPressLoginShop}
              />
            </View>
            <View style={styles.viewBack}>
              <TouchablePreventDouble onPress={this.onPressBack}>
                <Text style={styles.txtBack}>Quay lại</Text>
              </TouchablePreventDouble>
              <TouchablePreventDouble onPress={this.onPressForgotPass}>
                <Text style={styles.txtForgotPass}>Quên mật khẩu</Text>
              </TouchablePreventDouble>
            </View>
            <BtnBase
              styleBtn={styles.btnLogin}
              textBtn="Đăng nhập"
              onPress={this.onPressLogin}
            />
            <View style={styles.viewSignUp}>
              <Text>Chưa có tài khoản?</Text>
              <TouchablePreventDouble onPress={this.onPressSignUp}>
                <Text style={styles.txtForgotPass}> Đăng ký</Text>
              </TouchablePreventDouble>
            </View>
          </View>

          <Modal visible={showModal} transparent animationType="slide">
            {showModal && showForgotPass ? (
              <View style={styles.viewModal}>
                <ForgotPassword
                  onPressBackForgotPass={this.onPressBackForgotPass}
                />
              </View>
            ) : (
              <></>
            )}
          </Modal>
          {isLoading && <ActivityIndicator size="large" color={color.agree} />}
        </SafeAreaView>
      </KeyboardAvoidingView>
    );
  }
}
const styles = StyleSheet.create({
  cotainer: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
    backgroundColor: '#fff',
  },
  logo: {width: 150, height: 150, resizeMode: 'contain'},
  containerTextInput: {width: '100%', paddingTop: 10},
  textInput: {
    borderBottomWidth: 1,
    borderColor: 'gray',
    height: 40,
    width: '100%',
  },
  viewBack: {
    width: '100%',
    paddingTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  txtBack: {textDecorationLine: 'underline', color: '#2c829e'},
  txtForgotPass: {textDecorationLine: 'underline', color: '#18b4db'},
  btnLogin: {height: 40, width: '90%', marginTop: 30},
  viewSignUp: {
    width: '100%',
    position: 'absolute',
    bottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewModal: {
    width: '100%',
    height: '100%',
    backgroundColor: '#fff',
  },
  checkBox: {
    padding: 0,
    marginLeft: 0,
    backgroundColor: 'transparent',
    borderColor: 'transparent',
  },
});

export default LoginForm;
