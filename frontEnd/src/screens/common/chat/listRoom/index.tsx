import React, {useState} from 'react';
import {View, Text, FlatList} from 'react-native';
import {TopBarTitle, ViewSafeArea} from '../../../../common/base';
import {Navigation} from 'react-native-navigation';
import RenderItem from './RenderItem';

const ListRoom = ({
  componentId,
  data,
  onPressItem,
  onLoadMore,
  onRefeshing,
  refreshing,
  keyStatus,
}) => {
  const onPressBack = () => {
    Navigation.popToRoot(componentId);
  };

  return (
    <ViewSafeArea>
      <TopBarTitle title="Chat" onPressBack={onPressBack} childRight={<></>} />
      <FlatList
        data={data}
        keyExtractor={({item, index}) => `${index}`}
        onEndReachedThreshold={16}
        onEndReached={onLoadMore}
        refreshing={refreshing}
        onRefresh={onRefeshing}
        renderItem={({item, index}) => (
          <RenderItem 
            item={item} 
            index={index} 
            onPressItem={onPressItem} 
            keyStatus={keyStatus} 
          />
        )}
      />
    </ViewSafeArea>
  );
};

export default ListRoom;
