/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import {TouchablePreventDouble} from '../../../../common/base';
import moment from 'moment';
import {values} from '../../../../constant';

class RenderItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {item, index, keyStatus} = this.props;
    return (
      <View>
        <TouchablePreventDouble
          style={[
            styles.containerNofity,
            {
              backgroundColor:
                item[`${keyStatus}`] === 0 ? '#3ac95710' : '#fff',
            },
          ]}
          onPress={() => this.props.onPressItem(item, index)}>
          <View style={styles.containerAva}>
            <View style={styles.viewAvatar}>
              <Image
                source={{uri: `${values.domain}${item.shopImage}`}}
                style={styles.image}
              />
            </View>
          </View>
          <View style={styles.containerContent}>
            <Text style={styles.txtTitle} numberOfLines={1}>
              {item.nameRoom}
            </Text>
            <Text style={styles.txtContent} numberOfLines={2}>
              {item.lastContent}
            </Text>
            {/* <Text style={styles.txtDate}>
              {moment(item).format('DD/MM/YYYY')}
            </Text> */}
          </View>
        </TouchablePreventDouble>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerNofity: {
    width: '100%',
    flexDirection: 'row',
    paddingVertical: 10,
    paddingHorizontal: 5,
  },
  containerAva: {flex: 1},
  containerContent: {flex: 5},
  viewAvatar: {
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: 'gray',
    overflow: 'hidden',
  },
  txtTitle: {fontWeight: '700'},
  txtContent: {marginVertical: 3},
  txtDate: {color: 'gray'},
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
  },
});

export default RenderItem;
