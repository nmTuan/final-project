import React from 'react';
import {View, FlatList, TextInput, StyleSheet} from 'react-native';
import {
  TopBarTitle,
  ViewSafeArea,
  TouchablePreventDouble,
  ShowImage,
} from '../../../../common/base';
import {Navigation} from 'react-native-navigation';
import RenderItemChat from './RenderItemChat';
import {color, values} from '../../../../constant';
import Icon from 'react-native-vector-icons/FontAwesome';
import {ScreenConfig} from '../../../../config';
import {observer} from 'mobx-react';

type Props = {
  data?: Array,
  title?: '',
  onChangeText?: (v: value) => {},
  onPressSend?: Function,
  content?: '',
  username?: '',
  onPressBack?: () => {},
  onPressCamera?: () => {},
  onLoadMore?: () => {},
};
@observer
class RoomDetail extends React.PureComponent<Props> {
  constructor(props) {
    super(props);
    this.state = {
      content: '',
      heightInput: 0,
      showImage: false,
      arrImage: [],
    };
  }

  onPressBack = () => {
    Navigation.dismissModal(this.props.componentId);
    this.props.onPressBack();
  };

  onContentSizeChange = e => {
    this.setState({
      heightInput: e.nativeEvent.contentSize.height,
    });
  };

  onPressSend = () => {
    this.props.onPressSend();
  };

  onPressImage = imageSelected => {
    this.setState({
      showImage: true,
      arrImage: [imageSelected],
    });
  };

  onPressCancel = () => {
    this.setState({
      showImage: false,
    });
  };

  render() {
    const {heightInput, showImage, arrImage} = this.state;
    const {
      data,
      content,
      title,
      onChangeText,
      username,
      onLoadMore,
    } = this.props;
    return (
      <ViewSafeArea>
        {!showImage && (
          <TopBarTitle
            title={title}
            onPressBack={this.onPressBack}
            childRight={<></>}
          />
        )}
        <FlatList
          style={{flex: 1}}
          contentContainerStyle={{paddingTop: 50, paddingBottom: 10}}
          data={data}
          keyExtractor={(item, index) => `${index}`}
          inverted
          scrollEventThrottle={16}
          onEndReached={onLoadMore}
          renderItem={({item, index}) => (
            <RenderItemChat
              itemChat={item}
              indexChat={index}
              data={data}
              username={username}
              onPressImage={this.onPressImage}
            />
          )}
        />

        <TextInput
          placeholder="Nhập văn bản..."
          value={content}
          onChangeText={value => onChangeText(value)}
          multiline
          onContentSizeChange={this.onContentSizeChange}
          style={[styles.textInput, {height: Math.max(35, heightInput)}]}
        />
        <View style={styles.btn}>
          <TouchablePreventDouble onPress={this.onPressSend}>
            <Icon name="send" size={25} color={color.agree} />
          </TouchablePreventDouble>
          <TouchablePreventDouble
            style={styles.btnCamera}
            onPress={this.props.onPressCamera}>
            <Icon name="camera" size={25} color={color.agree} />
          </TouchablePreventDouble>
        </View>
        {showImage && (
          <ShowImage onPressCancel={this.onPressCancel} arrImage={arrImage} />
        )}
      </ViewSafeArea>
    );
  }
}

const styles = StyleSheet.create({
  viewListMessage: {flex: 1, paddingBottom: 50},
  textInput: {
    width: '100%',
    maxHeight: 50,
    borderTopWidth: 1,
    borderColor: color.lightGrayColor,
    paddingTop: 5,
    backgroundColor: '#fff',
    paddingRight: 95,
    position: 'absolute',
    bottom: ScreenConfig.bottomIphoneX,
    paddingLeft: 10,
  },
  viewInput: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'red',
    flexDirection: 'row',
    overflow: 'visible',
  },
  btn: {
    position: 'absolute',
    bottom: ScreenConfig.bottomIphoneX + 5,
    right: 10,
    flexDirection: 'row',
  },
  btnCamera: {
    marginLeft: 15,
  },
});

export default RoomDetail;
