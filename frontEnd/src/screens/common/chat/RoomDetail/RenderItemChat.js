/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import {values, color} from '../../../../constant';

type RenderItemChatProps = {
  onPressImage?: (v: value) => {},
};

class RenderItemChat extends React.PureComponent<RenderItemChatProps> {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
    };
  }

  componentDidMount() {
    this.setAvaPosition();
  }

  UNSAFE_componentWillUpdate() {
    this.setState({
      index: 0,
    });
  }

  componentDidUpdate() {
    this.setAvaPosition();
  }

  setAvaPosition() {
    const {indexChat, data} = this.props;
    const preIndex = indexChat === 0 ? 0 : indexChat - 1;
    if (
      data[preIndex].sender.toLocaleLowerCase() !==
      data[indexChat].sender.toLocaleLowerCase()
    ) {
      this.setState({
        index: indexChat,
      });
    }
  }

  render() {
    const {index} = this.state;
    const {itemChat, indexChat, username} = this.props;
    const isLoop = index === indexChat ? true : false;
    const isImage = itemChat.content.search('.jpg') > 0 ? true : false;
    return itemChat.sender.toLocaleLowerCase() ===
      username.toLocaleLowerCase() ? (
      <View style={styles.containerReceive}>
        <View
          style={{
            ...styles.viewContent,
            marginRight: isLoop ? 10 : 40,
            marginTop: isLoop ? 2 : 3,
          }}>
          {isImage ? (
            <TouchableWithoutFeedback
              onPress={() => this.props.onPressImage(itemChat.content)}>
              <Image
                source={{uri: `${values.domain}${itemChat.content}`}}
                style={{width: 100, height: 90}}
              />
            </TouchableWithoutFeedback>
          ) : (
            <Text>{itemChat.content}</Text>
          )}
          {/* <View style={styles.viewImage}>
            {this.state.arr.map(item => {
              return (
                <TouchableWithoutFeedback onPress={this.props.onPressImage}>
                  <Image
                    source={{uri: `${values.domain}images/1591608849040.jpg`}}
                    style={{width: 100, height: 90}}
                  />
                </TouchableWithoutFeedback>
              );
            })}
          </View> */}
        </View>
        {isLoop && (
          <View style={styles.viewAva}>
            <Image
              source={{uri: `${values.domain}${itemChat.senderAva}`}}
              style={styles.image}
            />
          </View>
        )}
      </View>
    ) : (
      <View style={styles.containerSender}>
        {isLoop && (
          <View style={styles.viewAva}>
            <Image
              source={{uri: `${values.domain}${itemChat.senderAva}`}}
              style={styles.image}
            />
          </View>
        )}
        <View
          style={{
            ...styles.viewContent,
            marginLeft: isLoop ? 10 : 40,
            marginTop: isLoop ? 2 : 3,
          }}>
          {isImage ? (
            <TouchableWithoutFeedback
              onPress={() => this.props.onPressImage(itemChat.content)}>
              <Image
                source={{uri: `${values.domain}${itemChat.content}`}}
                style={{width: 100, height: 90}}
              />
            </TouchableWithoutFeedback>
          ) : (
            <Text>{itemChat.content}</Text>
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerSender: {
    width: '100%',
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  containerReceive: {
    width: '100%',
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  viewAva: {
    width: 30,
    height: 30,
    borderRadius: 15,
    backgroundColor: color.agree,
    overflow: 'hidden',
  },
  viewContent: {
    maxWidth: '80%',
    paddingHorizontal: 8,
    paddingVertical: 5,
    backgroundColor: '#3df57a40',
    borderRadius: 6,
  },
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'stretch',
  },
  viewImage: {
    flex: 1,
    maxWidth: 200,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-end',
  },
});

export default RenderItemChat;
