import React, { useState, useRef } from 'react';
import { View, Text, TextInput, StyleSheet } from "react-native";
import { observer } from 'mobx-react-lite';
import { color } from '../../../constant';
import { BtnBase, ScreenSms, ScreenOTP } from '../../../common/base';
import { AccountService } from '../../../services/AccountService';
import { notifyMessage } from '../../../config/Func';

var timeoutSms = null;
const BankInformation = observer(({ BankStore, onPressGetOtp }) => {
    const bankInfo = BankStore.bankInfo;

    return (
        <View style={styles.container}>
            <View style={styles.inner}>
                <Text style={styles.title}>
                    Ngân hàng liên kết
            </Text>
                <Text style={styles.value}>
                    {bankInfo.bankName}
                </Text>
            </View>

            <View style={styles.inner}>
                <Text>Họ và tên(Không dấu)</Text>
                <TextInput
                    style={{ borderWidth: 1, borderColor: 'gray', borderRadius: 6, height: 35 }}
                    value={bankInfo.creditName}
                    autoCapitalize="characters"
                    onChangeText={value => bankInfo.creditName = value}
                />
            </View>
            <View style={styles.inner}>
                <Text>Số thẻ</Text>
                <TextInput
                    style={{ borderWidth: 1, borderColor: 'gray', borderRadius: 6, height: 35 }}
                    value={bankInfo.creditNumber}
                    onChangeText={value => bankInfo.creditNumber = value}
                    keyboardType="numeric"
                />
            </View>
            <View style={styles.inner}>
                <Text>Ngày cấp</Text>
                <View style={styles.viewCreditDue}>
                    <TextInput
                        placeholder="Tháng"
                        style={{ borderWidth: 1, borderColor: 'gray', borderRadius: 6, height: 35, width: 60 }}
                        value={bankInfo.month}
                        onChangeText={value => bankInfo.month = value}
                        keyboardType="numeric"
                    />
                    <View style={styles.space} />
                    <TextInput
                        placeholder="Năm"
                        style={{ borderWidth: 1, borderColor: 'gray', borderRadius: 6, height: 35, width: 60 }}
                        value={bankInfo.year}
                        onChangeText={value => bankInfo.year = value}
                        keyboardType="numeric"
                    />
                </View>
            </View>
            <View style={styles.btnNext} >
                <BtnBase textBtn="Tiếp theo" onPress={onPressGetOtp} />
            </View>
        </View>
    )
})

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    viewCreditDue: {
        flexDirection: 'row',
    },
    space: {
        width: 20,
    },
    inner: {
        paddingVertical: 5,
        paddingHorizontal: 20,
    },
    title: {
        fontSize: 15
    },
    value: {
        fontSize: 16,
        color: color.agree
    },
    btnNext: {
        marginTop: 10,
        height: 40,
        paddingHorizontal: 20,
    },
    
})

export default BankInformation;