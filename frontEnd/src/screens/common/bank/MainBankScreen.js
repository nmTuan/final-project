import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StyleSheet, SafeAreaView} from 'react-native';
import {inject, observer} from 'mobx-react';
import SearchBank from './SearchBank';
import {TopBar, ScreenSms, ScreenOTP} from '../../../common/base';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Navigation} from 'react-native-navigation';
import BankInformation from './BankInformation';
import {notifyMessage} from '../../../config/Func';
import {AccountService} from '../../../services/AccountService';
var timeoutSms = null;

@inject('BankStore')
@observer
class MainBankScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showOtp: false,
      otpSms: '',
      inputOtp: '',
      errorOtp: '',
    };
  }

  onPressBack = () => {
    const {BankStore} = this.props;
    if (BankStore.step === 0) {
      this.props.onPressBack();
    } else {
      BankStore.setStep(BankStore.step - 1);
    }
  };

  onPressGetOtp = async () => {
    const otpSms = await AccountService.getOtpWithToken('0962486601');
    this.setState({
      showOtp: true,
      otpSms,
    });
    timeoutSms = setTimeout(() => {
      this.sms.slideDown();
    }, 1000);
  };

  onPressDissmissOtp = () => {
    this.setState({
      showOtp: false,
    });
  };

  onPressReSendOtp = async () => {
    this.setState({
      errorOtp: '',
    });
    this.sms.slideUp();
    clearTimeout(timeoutSms);
    this.onPressGetOtp();
  };

  onPressSend = () => {
    const {inputOtp, otpSms} = this.state;
    const {onPressNext, BankStore} = this.props;
    if (inputOtp == otpSms) {
      onPressNext(BankStore.bankInfo);
      BankStore.resetData();
    } else {
      this.setState({
        errorOtp: 'Sai otp',
      });
    }
  };

  render() {
    const {BankStore} = this.props;
    const {showOtp, otpSms, errorOtp} = this.state;
    return (
      <SafeAreaView style={{flex: 1}}>
        <View style={styles.viewTopBar}>
          <TopBar
            childLeft={
              <TouchableOpacity onPress={this.onPressBack}>
                <Icon name="angle-left" size={40} style={styles.iconBack} />
              </TouchableOpacity>
            }
            childCenter={<Text style={styles.title}>Thông tin ngân hàng</Text>}
          />
        </View>
        {BankStore.step === 0 && <SearchBank BankStore={BankStore} />}
        {BankStore.step === 1 && (
          <BankInformation
            BankStore={BankStore}
            onPressGetOtp={this.onPressGetOtp}
          />
        )}
        {showOtp ? (
          <View style={styles.containerOtp}>
            <ScreenOTP
              cancleModal={this.onPressDissmissOtp}
              reSend={this.onPressReSendOtp}
              send={this.onPressSend}
              textError={errorOtp}
              onChangeText={value => this.setState({inputOtp: value})}
            />
          </View>
        ) : (
          <></>
        )}
        <ScreenSms
          ref={ref => (this.sms = ref)}
          message={`Mã OTP liên kết ngân hàng là: ${otpSms}`}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  viewTopBar: {
    backgroundColor: '#fff',
  },
  iconBack: {marginLeft: 10},
  title: {fontSize: 16, textAlign: 'center'},
  containerOtp: {position: 'absolute', width: '100%', height: '100%'},
});

export default MainBankScreen;
