import React from 'react';
import {View, Text, FlatList, StyleSheet} from 'react-native';
import {SearchBar} from 'react-native-elements';
import {color} from '../../../constant';
import {TouchablePreventDouble} from '../../../common/base';
import BankService from '../../../services/BankService';

class SearchBank extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      valueSearch: '',
      data: [],
    };
  }

  componentDidMount() {
    this.getListBank();
  }

  async getListBank(name = '') {
    const data = await BankService.getListBank(name);
    this.setState({
      data,
    });
  }

  updateSearch = async value => {
    this.setState({
      valueSearch: value,
    });
    this.getListBank(value);
  };

  onEndReached = () => {};

  onPressItem = item => {
    const {BankStore} = this.props;
    BankStore.bankInfo.bankName = item.bankName;
    BankStore.setStep(BankStore.step + 1);
  };

  renderItem = ({item, index}) => {
    return (
      <TouchablePreventDouble
        style={styles.viewItem}
        onPress={() => this.onPressItem(item)}>
        <Text>{item.bankName}</Text>
      </TouchablePreventDouble>
    );
  };

  render() {
    const {valueSearch, data} = this.state;
    return (
      <View style={{flex: 1}}>
        <View style={styles.viewSearchBar}>
          <SearchBar
            containerStyle={styles.styleContainerSearch}
            lightTheme
            inputContainerStyle={styles.styleInputContainer}
            inputStyle={styles.styleInputSearch}
            placeholder="Tìm kiếm..."
            onChangeText={this.updateSearch}
            value={valueSearch}
          />
        </View>
        <FlatList
          style={styles.flatList}
          data={data}
          keyExtractor={(item, index) => `${index}`}
          renderItem={this.renderItem}
          onEndReached={this.onEndReached}
          onEndReachedThreshold={0.1}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  styleContainerSearch: {
    backgroundColor: 'transparent',
    width: '100%',
    borderBottomColor: 'transparent',
    borderTopColor: 'transparent',
  },
  styleInputContainer: {
    backgroundColor: color.lightGrayColor,
    borderRadius: 20,
    height: 30,
    borderColor: 'transparent',
  },
  styleInputSearch: {fontSize: 15, color: '#000'},
  viewItem: {
    width: '100%',
    borderBottomWidth: 1,
    borderColor: 'gray',
    paddingBottom: 5,
    paddingTop: 10,
    paddingHorizontal: 10,
  },
  viewSearchBar: {
    width: '100%',
    paddingHorizontal: 10,
    backgroundColor: '#fff',
  },
  flatList: {
    flex: 1,
    width: '100%',
    backgroundColor: '#fff',
  },
});

export default SearchBank;
