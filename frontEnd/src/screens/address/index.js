/* eslint-disable react/no-did-mount-set-state */
import React, {Component} from 'react';
import {Modal} from 'react-native';
import {SelectedScrollView} from '../../common/base';
import {values} from '../../constant';
import {UserService} from '../../services/UserService';
import {observer} from 'mobx-react';
import {getCityAddress} from '../../constant/Format';

@observer
class Address extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      valueSeach: '',
      page: 1,
    };
  }

  UNSAFE_componentWillMount() {
    const {type} = this.props;
    const address = values.address;
    // console.log('tuype ', type);
    switch (type) {
      case address.city:
        this.setState({
          title: 'Tỉnh/Thành phố',
        });
        break;
      case address.district:
        this.setState({
          title: 'Quận/Huyện',
        });
        break;
      case address.village:
        this.setState({
          title: 'Xã/Phường',
        });
        break;
      default:
        break;
    }
  }

  async componentDidMount() {
    const {type, AddressStore} = this.props;
    const data = await this.getData(type, '', 0);
    AddressStore.setData([], data);
  }

  getData = async (type, value, page) => {
    const {AddressStore} = this.props;
    var data = [];
    const address = values.address;
    switch (type) {
      case address.city:
        data = await UserService.getCity(value, page, 30);
        break;
      case address.district:
        var code = 1;
        if (AddressStore.citySelected) {
          code = AddressStore.citySelected.code;
        }
        data = await UserService.getDistrict(code, value, page, 30);
        break;
      case address.village:
        var code = 1;
        if (AddressStore.districtSelected) {
          code = AddressStore.districtSelected.code;
        }
        data = await UserService.getVillage(code, value, page, 30);
        break;
      default:
        break;
    }
    return data;
  };

  handleSearch = async value => {
    const {type, AddressStore} = this.props;
    this.setState({
      valueSeach: value,
    });
    const data = await this.getData(type, value, 0);
    AddressStore.setData([], data);
  };

  onScrollEnd = async () => {
    const {type, AddressStore} = this.props;
    const {valueSeach, page} = this.state;
    const newData = await this.getData(type, valueSeach, page);
    if (newData.length > 0) {
      AddressStore.setData(AddressStore.data, newData);
      this.setState({
        page: page + 1,
      });
    }
  };

  onPressItem = item => {
    const {type, AddressStore} = this.props;
    const address = values.address;
    const itemSelected = {
      type: -1,
      name: '',
    };
    switch (type) {
      case address.city:
        AddressStore.setCity(item);
        itemSelected.type = address.city;
        itemSelected.name = getCityAddress(item.name);
        break;
      case address.district:
        AddressStore.setDistrict(item);
        itemSelected.type = address.district;
        itemSelected.name = getCityAddress(item.name);
        break;
      case address.village:
        AddressStore.setVillage(item);
        itemSelected.type = address.village;
        itemSelected.name = getCityAddress(item.name);
        break;
      default:
        break;
    }
    this.props.onPressItem(itemSelected);
  };

  render() {
    const {title, valueSeach} = this.state;
    const {showModalAddress, onPressBack, AddressStore} = this.props;
    return (
      <Modal visible={showModalAddress} transparent animationType="slide">
        {
          <SelectedScrollView
            data={AddressStore.data}
            keyItem={'name'}
            title={title}
            valueSearch={valueSeach}
            onChagneTextSearch={this.handleSearch}
            onPressBack={onPressBack}
            onScrollEnd={this.onScrollEnd}
            onPressItem={this.onPressItem}
          />
        }
      </Modal>
    );
  }
}

export default Address;
