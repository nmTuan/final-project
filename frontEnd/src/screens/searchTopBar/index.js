import React, {Component} from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {SearchBar} from 'react-native-elements';
import {color} from '../../constant';
import {TopBarChatCart} from '../../common/base';
import TopBar from '../../common/base/TopBar';

type Props = {
  onChangeText?: (v: value) => void,
  search?: String,
  onFocus?: Function,
  searchOnly: Boolean,
  cancel?: Function,
  showCart?: Boolean,
};

class index extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      height: 0,
    };
    // this.props.animationSearch = new Animated.Value(0);
  }

  static defaultProps = {
    search: '',
    searchOnly: false,
    showCart: true,
  };

  _onPressCart = () => {
    this.props.onPressCart();
  };

  _onPressChat = () => {
    this.props.onPressChat();
  };

  blur = () => {
    this.search.blur();
  };

  renderLeft() {
    const {search, onChangeText, onFocus, searchOnly} = this.props;
    return (
      <SearchBar
        ref={search => (this.search = search)}
        containerStyle={styles.styleContainerSearch}
        lightTheme
        selectionColor="#000"
        inputContainerStyle={styles.styleInputContainer}
        inputStyle={styles.styleInputSearch}
        placeholder="Tìm kiếm sản phẩm..."
        onChangeText={onChangeText}
        onFocus={onFocus}
        autoFocus={searchOnly}
        value={search}
      />
    );
  }

  render() {
    const {searchOnly, cancel, showCart, componentId} = this.props;
    return !searchOnly ? (
      <View style={{flex: 1}}>
        <TopBarChatCart
          childLeft={this.renderLeft()}
          onPressCart={this._onPressCart}
          onPressChat={this._onPressChat}
          componentId={componentId}
          showCart={showCart}
        />
      </View>
    ) : (
      <View style={styles.containerSearch}>
        <View style={styles.viewSearch}>{this.renderLeft()}</View>
        <TouchableOpacity onPress={cancel}>
          <Text>Huỷ</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  styleContainerSearch: {
    backgroundColor: 'transparent',
    height: '100%',
    borderBottomColor: 'transparent',
    borderTopColor: 'transparent',
    // backgroundColor:'red',
  },
  styleInputContainer: {
    backgroundColor: color.lightGrayColor,
    borderRadius: 20,
    height: 30,
    borderColor: 'transparent',
  },
  styleInputSearch: {fontSize: 15, color: '#000'},
  containerSearch: {
    width: '100%',
    height: 50,
    paddingHorizontal: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  viewSearch: {flex: 1, paddingRight: 10},
});

export default index;
