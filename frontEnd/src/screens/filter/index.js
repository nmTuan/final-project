import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {ScreenConfig} from '../../config';
import {color, values} from '../../constant';
import {Navigation} from 'react-native-navigation';
import * as Animatable from 'react-native-animatable';
import RenderItem from './RenderItem';
import {inject, observer} from 'mobx-react';
import HomeService from '../../services/HomeService';
@inject('FilterStore', 'HomeStore')
@observer
class Filter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      navBottomHeight: 0,
    };
  }

  async componentDidMount() {
    this.filter.slideInRight();
    await Navigation.constants().then(result => {
      this.setState({
        navBottomHeight: result.bottomTabsHeight + ScreenConfig.bottomIphoneX,
      });
    });
  }

  resetFilter = async () => {
    const {FilterStore, HomeStore} = this.props;
    FilterStore.reset();
    const dataProduct = await HomeService.getProduct(
      FilterStore.sort,
      0,
      values.sizePage,
    );
    if (dataProduct) {
      HomeStore.setProduct([], dataProduct.result);
    }
  };

  onPressAccept = () => {
    this.onPressCancel();
    this.props.onPressAccept();
  };

  onPressCancel = () => {
    const {FilterStore} = this.props;
    this.filter.transitionTo({scale: 0});
    setTimeout(() => {
      FilterStore.isShowFilter = false;
    }, 100);
  };

  render() {
    const {typeFilter} = values;
    const {FilterStore} = this.props;
    return (
      <View style={[styles.container]}>
        <TouchableOpacity
          style={{flex: 1}}
          activeOpacity={1}
          onPress={this.onPressCancel}
        />
        <Animatable.View
          style={styles.viewChild}
          duration={values.duaration}
          useNativeDriver
          ref={ref => (this.filter = ref)}>
          <ScrollView
            style={{flex: 1}}
            contentContainerStyle={{position: 'absolute'}}>
            <View style={styles.viewHeader}>
              <Text style={styles.txtHeader}> Bộ lọc tìm kiếm </Text>
            </View>
            <RenderItem
              title="Danh mục"
              data={FilterStore.categories}
              type={typeFilter.category}
              typeSelected={FilterStore.categorySelected}
              filterStore={FilterStore}
              nameValue="name"
            />
            <RenderItem
              title="Giới tính"
              data={FilterStore.gender}
              type={typeFilter.gender}
              typeSelected={FilterStore.genderSelected}
              filterStore={FilterStore}
              nameValue="gender"
            />
            <RenderItem
              title="Độ tuổi"
              data={FilterStore.age}
              type={typeFilter.age}
              typeSelected={FilterStore.ageSelected}
              filterStore={FilterStore}
              nameValue="age"
            />
            <RenderItem
              title="Đánh giá"
              data={FilterStore.vote}
              type={typeFilter.vote}
              typeSelected={FilterStore.voteSelected}
              filterStore={FilterStore}
            />
            <View
              style={[
                styles.viewBtn,
                {paddingBottom: this.state.navBottomHeight + 10},
              ]}>
              <TouchableOpacity
                style={[styles.btnCategory]}
                onPress={this.resetFilter}>
                <Text>Thiết lập lại</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.btnCategory, {backgroundColor: color.agree}]}
                onPress={this.onPressAccept}>
                <Text style={{color: '#fff'}}>Ap dung</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </Animatable.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    width: '100%',
    height: ScreenConfig.deviceHeight,
    backgroundColor: '#00000080',
  },
  viewChild: {
    paddingTop: ScreenConfig.marginTopScreen,
    position: 'absolute',
    height: '100%',
    width: '70%',
    backgroundColor: '#fff',
    right: 0,
  },
  viewHeader: {padding: 10, backgroundColor: color.lightGrayColor},
  txtHeader: {fontSize: 18, fontWeight: 'bold'},
  viewBtn: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingRight: 10,
  },
  btnCategory: {
    padding: 10,
    marginRight: 10,
    marginBottom: 10,
    maxWidth: 150,
    borderWidth: 1,
    borderColor: color.lightGrayColor,
    borderRadius: 4,
  },
});
export default Filter;
