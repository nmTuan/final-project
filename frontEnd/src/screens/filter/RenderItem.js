import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {values, color} from '../../constant';
import {Rating} from 'react-native-elements';
import {observer} from 'mobx-react';

@observer
class RenderItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  _onPressItem = (type, index) => {
    const {filterStore} = this.props;
    filterStore.setValueSelected(type, index);
  };

  render() {
    const {title, data, type, typeSelected, nameValue} = this.props;
    const {typeFilter} = values;
    return type === typeFilter.vote ? (
      <View style={styles.containerCategory}>
        <Text style={{fontWeight: 'bold'}}>{title}</Text>
        <View style={styles.viewItemCategory}>
          {data.map((item, index) => {
            return (
              <TouchableOpacity
                key={index.toString()}
                activeOpacity={0.6}
                style={[
                  styles.btnCategory,
                  typeSelected === index && styles.voteSelected,
                ]}
                onPress={() => this._onPressItem(type, index)}>
                <Rating
                  imageSize={15}
                  startingValue={item}
                  type="custom"
                  readonly
                />
              </TouchableOpacity>
            );
          })}
        </View>
      </View>
    ) : (
      <View style={styles.containerCategory}>
        <Text style={{fontWeight: 'bold'}}>{title}</Text>
        <View style={styles.viewItemCategory}>
          {data.map((item, index) => {
            return (
              <TouchableOpacity
                key={index.toString()}
                activeOpacity={0.6}
                style={[
                  styles.btnCategory,
                  typeSelected === index && styles.itemSelected,
                ]}
                onPress={() => this._onPressItem(type, index)}>
                <Text numberOfLines={2} style={styles.txtCategory}>
                  {nameValue ? item[`${nameValue}`] : item}
                </Text>
              </TouchableOpacity>
            );
          })}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerCategory: {padding: 10},
  viewItemCategory: {flexDirection: 'row', flexWrap: 'wrap', paddingTop: 10},
  btnCategory: {
    padding: 10,
    marginRight: 10,
    marginBottom: 10,
    maxWidth: 120,
    borderWidth: 1,
    borderColor: color.lightGrayColor,
    borderRadius: 4,
  },
  txtCategory: {textAlign: 'center'},
  itemSelected: {backgroundColor: color.lightGrayColor},
  voteSelected: {borderWidth: 2, borderColor: 'red'},
});

export default RenderItem;
