import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  ActivityIndicator,
  Platform,
} from 'react-native';
import {ViewSafeArea, TopBarChatCart} from '../../common/base';
import {pushScreen} from '../../config/NavigationConfig';
import {inject, observer} from 'mobx-react';
import {UserService} from '../../services/UserService';
import RenderItem from './RenderItem';
import {values, color} from '../../constant';
import {notifyMessage} from '../../config/Func';
import {Navigation} from 'react-native-navigation';

@inject('UserStore')
@observer
class Notification extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      isLoadding: false,
      page: 0,
      countNotifi: 0,
    };
  }

  componentDidMount() {
    this.initData();
    this.getBadgeNotifi();
  }

  async initData() {
    const {UserStore} = this.props;
    const data = await UserService.getListNotification(UserStore.userInfo.id);
    if (data) {
      UserStore.setNotification([], data.result.notificationCustDTOs);
    }
  }

  async getBadgeNotifi() {
    const {UserStore} = this.props;
    const id = UserStore.userInfo.id;
    const res = await UserService.countNotification(id, 0);
    if (res.result && res.result > 0) {
      this.setState({
        countNotifi: res.result,
      });
      setTimeout(() => {
        Navigation.mergeOptions(this.props.componentId, {
          bottomTab: {
            badge: `${res.result}`,
          },
        });
      }, 500);
    }
  }

  onPressItem = async (item, index) => {
    const {countNotifi} = this.state;
    const {UserStore} = this.props;
    const custId = UserStore.userInfo.id;
    var badge =
      countNotifi > 0 ? countNotifi : Platform.OS === 'ios' ? null : '';
    const bill = item.title.search('đơn');
    if (item.status === 0) {
      item.status = 1;
      const count = countNotifi - 1;
      badge = count > 0 ? count : Platform.OS === 'ios' ? null : '';
      this.setState({
        countNotifi: count,
      });
      await UserService.changeStatusNotification(item.id, custId);
    }
    Navigation.mergeOptions(this.props.componentId, {
      bottomTab: {
        badge: `${badge}`,
      },
    });
    if (bill >= 0) {
      this.redirectToBill(item);
    }
  };

  redirectToBill(item) {
    const {UserStore} = this.props;
    const {abortBill, confirmBill} = values.typeNotificationCust;
    setTimeout(() => {
      switch (item.title.toLocaleLowerCase().trim()) {
        case abortBill.toLocaleLowerCase().trim():
          UserStore.setTabBillSelected(values.bill.abort);
          break;
        case confirmBill.toLocaleLowerCase().trim():
          UserStore.setTabBillSelected(values.bill.confirmed);
          break;
        default:
          break;
      }
    }, 200);
    Navigation.mergeOptions(this.props.componentId, {
      bottomTabs: {
        currentTabIndex: 1,
      },
    });
  }

  onPressCart = () => {
    pushScreen(this.props.componentId, 'Cart');
  };

  onPressChat = () => {
    pushScreen(this.props.componentId, 'ListRoomCust');
  };

  onRefresh = () => {
    this.setState({
      refreshing: true,
    });
    this.initData();
    this.setState({
      refreshing: false,
    });
  };

  onLoadMore = async () => {
    const {page} = this.state;
    const {UserStore} = this.props;
    this.setState({
      isLoadding: true,
    });
    const currentPage = page + 1;
    const id = UserStore.userInfo.id;
    const data = await UserService.getListNotification(id, currentPage, 10);
    if (
      data.message.toLocaleLowerCase() ===
      values.messageSuccess.toLocaleLowerCase()
    ) {
      if (data.result.notificationCustDTOs.length > 0) {
        this.setState({
          page: currentPage,
        });
        UserStore.setNotification(
          UserStore.notificationCust,
          data.result.notificationShopDTOs,
        );
      }
      this.setState({
        isLoadding: false,
      });
    } else {
      this.setState({
        isLoadding: false,
      });
      notifyMessage(data.message);
    }
  };

  render() {
    const {refreshing, isLoadding} = this.state;
    const {UserStore} = this.props;
    return (
      <ViewSafeArea>
        <TopBarChatCart
          childLeft={<Text style={styles.txtTopBar}>Thông báo</Text>}
          onPressCart={this.onPressCart}
          onPressChat={this.onPressChat}
        />
        <FlatList
          data={UserStore.notificationCust}
          refreshing={refreshing}
          onRefresh={this.onRefresh}
          onEndReached={this.onLoadMore}
          keyExtractor={(item, index) => `${index}`}
          renderItem={({item, index}) => (
            <RenderItem
              item={item}
              index={index}
              onPressItem={this.onPressItem}
            />
          )}
        />
        {isLoadding && (
          <View style={styles.loading}>
            <ActivityIndicator size="large" color={color.agree} />
          </View>
        )}
      </ViewSafeArea>
    );
  }
}

const styles = StyleSheet.create({
  txtTopBar: {marginLeft: 10, fontSize: 18, fontWeight: 'bold'},
  loading: {
    width: '100%',
    position: 'absolute',
    bottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Notification;
