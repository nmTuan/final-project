import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Keyboard,
} from 'react-native';
import {TouchablePreventDouble, BtnBase, TxtError} from '../../common/base';
import {AccountService} from '../../services/AccountService';

class AccountInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      repassword: '',
      error: '',
    };
  }

  onChangeUsername = value => {
    this.setState({
      username: value,
    });
  };

  onChangePassword = value => {
    this.setState({
      password: value,
    });
  };

  onChangeRepassword = value => {
    this.setState({
      repassword: value,
    });
  };

  onPressNext = async () => {
    const {SignUpStore, UserStore} = this.props;
    const {username, password, repassword} = this.state;
    if (username !== '' && password !== '' && repassword !== '') {
      if (password === repassword) {
        const res = await AccountService.signup(username, password);
        if (res.errorCode === 202) {
          this.setState({
            error: res.message,
          });
        } else {
          Keyboard.dismiss();
          UserStore.signupInfo.accId = res.result.id;
          SignUpStore.viewPager.setPage(1);
        }
      } else {
        this.setState({
          error: 'Mật khẩu không trùng nhau',
        });
      }
    } else {
      this.setState({
        error: 'Thiếu trường thông tin',
      });
    }
  };

  render() {
    const {error} = this.state;
    return (
      <TouchableOpacity style={styles.container} onPress={Keyboard.dismiss}>
        <Image source={require('@assets/logo/logo1.png')} style={styles.logo} />
        <View style={styles.containerTextInput}>
          <Text>Username</Text>
          <TextInput
            style={styles.textInput}
            placeholder="Nhập username"
            onChangeText={this.onChangeUsername}
          />
        </View>
        <View style={styles.containerTextInput}>
          <Text>Mật khẩu</Text>
          <TextInput
            style={styles.textInput}
            placeholder="Nhập mật khẩu"
            onChangeText={this.onChangePassword}
          />
        </View>
        <View style={styles.containerTextInput}>
          <Text>Nhập lại mật khẩu</Text>
          <TextInput
            style={styles.textInput}
            placeholder="Nhập lại mật khẩu"
            onChangeText={this.onChangeRepassword}
          />
        </View>
        {error !== '' && <TxtError text={error} />}
        <View style={styles.viewBack}>
          <TouchablePreventDouble onPress={this.props.onPressBack}>
            <Text style={styles.txtBack}>Quay lại</Text>
          </TouchablePreventDouble>
        </View>
        <BtnBase
          styleBtn={styles.btn}
          textBtn="Tiếp theo"
          onPress={this.onPressNext}
        />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    paddingTop: 60,
    paddingHorizontal: 30,
    width: '100%',
  },
  logo: {width: 90, height: 90, resizeMode: 'contain'},
  containerTextInput: {width: '100%', paddingTop: 10},
  textInput: {
    borderBottomWidth: 1,
    borderColor: 'gray',
    height: 40,
    width: '100%',
  },
  viewBack: {
    width: '100%',
    paddingTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  txtBack: {textDecorationLine: 'underline', color: '#2c829e'},
  btn: {height: 40, width: '90%', marginTop: 30},
});

export default AccountInfo;
