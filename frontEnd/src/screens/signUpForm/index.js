import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
} from 'react-native';
import CheckPhoneNumber from './CheckPhoneNumber';
import ViewPager from '@react-native-community/viewpager';
import AccountInfo from './AccountInfo';
import PersonalInfo from './PersonalInfo';
import {inject, observer} from 'mobx-react';
import { Navigation } from 'react-native-navigation';

@inject('SignUpStore', 'UserStore', 'AddressStore')
@observer
class SignUpForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 0,
    };
  }

  UNSAFE_componentWillMount() {
    const {UserStore} = this.props;
    if (UserStore.isNoneInfo) {
      this.setState({
        currentPage: 1,
      });
    }
  }

  onPressBack = () => {
    Navigation.pop(this.props.componentId);
  }

  componentDidMount() {
    this.props.SignUpStore.viewPager = this.viewPager;
  }

  onPageSelected = event => {
    this.setState({
      currentPage: event.nativeEvent.position,
    });
  };

  render() {
    const {SignUpStore, UserStore, AddressStore} = this.props;
    const {currentPage} = this.state;
    return (
      <KeyboardAvoidingView
        style={{flex: 1, backgroundColor: '#fff'}}
        behavior={Platform.OS === 'ios' ? 'padding' : 'null'}>
        <ViewPager
          ref={ref => (this.viewPager = ref)}
          style={{flex: 1}}
          initialPage={currentPage}
          scrollEnabled={false}
          onPageSelected={this.onPageSelected}>
          <SafeAreaView style={styles.inner}>
            <AccountInfo
              onPressBack={this.onPressBack}
              SignUpStore={SignUpStore}
              UserStore={UserStore}
            />
            <View style={{flex: 1}} />
          </SafeAreaView>
          <SafeAreaView style={styles.inner}>
            <CheckPhoneNumber
              SignUpStore={SignUpStore}
              UserStore={UserStore}
              onPressBack={this.onPressBack}
            />
          </SafeAreaView>
          {currentPage >= 1 && (
            <SafeAreaView style={styles.inner}>
              <PersonalInfo
                SignUpStore={SignUpStore}
                UserStore={UserStore}
                AddressStore={AddressStore}
              />
              <View style={{flex: 1}} />
            </SafeAreaView>
          )}
        </ViewPager>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  inner: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  header: {
    fontSize: 36,
    marginBottom: 48,
  },
  input: {
    height: 40,
    borderColor: '#000000',
    borderBottomWidth: 1,
    marginBottom: 36,
  },
  btnContainer: {
    backgroundColor: 'white',
    marginTop: 12,
  },
});

export default SignUpForm;
