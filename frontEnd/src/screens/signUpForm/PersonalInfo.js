import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  Modal,
  Platform,
  TouchableOpacity,
  Keyboard,
} from 'react-native';
import {
  TouchablePreventDouble,
  BtnBase,
  TxtError,
  PickerScrollView,
  DatePicker,
} from '../../common/base';
import Icon from 'react-native-vector-icons/FontAwesome';
// import DateTimePicker from '@react-native-community/datetimepicker';
import {values} from '../../constant';
import moment from 'moment';
import Address from '../address';
import {observer} from 'mobx-react';
import { UserService } from '../../services/UserService';
import { MainApp } from '../../navigation/Navigation';

@observer
class PersonalInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gender: ['Nam', 'Nữ', 'khác'],
      genderSelected: 0,
      showModalGender: false,
      showModalAddress: false,
      addressSelected: values.address.city,
      showDatepicker: false,
      dateIos: new Date(),
      error: '',
    };
  }

  onPressAddress = type => {
    const address = values.address;
    switch (type) {
      case address.city:
        this.setState({
          addressSelected: address.city,
        });
        break;
      case address.district:
        this.setState({
          addressSelected: address.district,
        });
        break;
      case address.village:
        this.setState({
          addressSelected: address.village,
        });
        break;
      default:
        break;
    }
    this.setState({
      showModalAddress: true,
    });
  };

  onSelecteAddress = item => {
    const address = values.address;
    const personalInfo = this.props.UserStore.signupInfo;
    switch (item.type) {
      case address.city:
        personalInfo.city = item.name;
        personalInfo.district = '';
        personalInfo.village = '';
        personalInfo.address = '';
        break;
      case address.district:
        personalInfo.district = item.name;
        personalInfo.village = '';
        personalInfo.address = '';
        break;
      case address.village:
        personalInfo.village = item.name;
        personalInfo.address = '';
        break;
      default:
        break;
    }
    this.setState({
      personalInfo,
      showModalAddress: false,
    });
  };

  dissmissAddress = () => {
    this.setState({
      showModalAddress: false,
    });
  };

  onPressBack = () => {
    const {SignUpStore} = this.props;
    SignUpStore.viewPager.setPage(1);
  };

  handleValueChange = index => {
    this.setState({
      genderSelected: index,
    });
  };

  onChangeName = value => {
    const {UserStore} = this.props;
    UserStore.signupInfo.fullName = value;
  };

  onChangeAddress = value => {
    const {UserStore} = this.props;
    UserStore.signupInfo.address = value;
  };

  dissmissGender = () => {
    this.setState({
      showModalGender: false,
    });
  };

  onPressGender = () => {
    this.setState({
      showModalGender: true,
    });
  };

  onPressDate = () => {
    this.setState({
      showDatepicker: true,
    });
  };

  onChageDate = (event, dateSelected) => {
    if (Platform.OS === 'ios') {
      this.props.UserStore.signupInfo.dob = event.nativeEvent.timestamp;
    } else {
      if (event.type === 'set') {
        this.props.UserStore.signupInfo.dob = event.nativeEvent.timestamp;
      }
    }
  };

  onCloseDatePicker = () => {
    if (Platform.OS === 'ios') {
      this.props.UserStore.signupInfo.dob = this.state.dateIos;
    }
    this.setState({
      showDatepicker: false,
    });
  };

  onConfirmDate = () => {
    this.setState({
      dateIos: this.props.UserStore.signupInfo.dob,
      showDatepicker: false,
    });
  };

  onPressNext = async () => {
    const {UserStore} = this.props;
    const userInfo = UserStore.signupInfo;
    if (
      userInfo.fullName !== '' &&
      userInfo.dob !== null &&
      userInfo.gender !== '' &&
      userInfo.phoneNumber !== '' &&
      userInfo.city !== '' &&
      userInfo.district !== '' &&
      userInfo.village !== '' &&
      userInfo.address !== ''
    ) {
      const res = await UserService.createInfo(userInfo);
      if (res.message === 'success') {
        UserStore.setUserInfo(res.result);
        MainApp();
      } else {
        this.setState({
          error: res.message,
        });
      }
    } else {
      this.setState({
        error: 'Thiếu thông tin',
      });
    }
  };

  renderInfor = () => {
    const {UserStore} = this.props;
    const userInfo = UserStore.signupInfo;
    const {address} = values;
    const {error} = this.state;
    return (
      <View style={{width: '100%'}}>
        <View style={styles.containerTextInput}>
          <Text>Họ và tên</Text>
          <TextInput
            style={styles.textInput}
            placeholder="Nhập tên đầy đủ"
            value={userInfo.fullName}
            onChangeText={this.onChangeName}
          />
        </View>
        <View style={styles.viewPicker}>
          <Text>Ngày sinh</Text>
          <TouchablePreventDouble
            style={styles.viewSelected}
            onPress={this.onPressDate}>
            <Text>{moment(userInfo.dob).format('DD/MM/YYYY')}</Text>
            <View style={styles.viewIconSelected}>
              <Icon name="calendar" size={12} style={styles.iconSelected} />
            </View>
          </TouchablePreventDouble>
        </View>
        <View style={styles.viewPicker}>
          <Text>Giới tính</Text>
          <TouchablePreventDouble
            style={styles.viewSelected}
            onPress={this.onPressGender}>
            <Text>{userInfo.gender}</Text>
            <View style={styles.viewIconSelected}>
              <Icon name="sort-down" size={20} style={styles.iconSelected} />
            </View>
          </TouchablePreventDouble>
        </View>
        <View style={styles.containerTextInput}>
          <Text>Số điện thoại</Text>
          <TextInput
            style={styles.textInput}
            placeholder="vd: 0962486601"
            value={userInfo ? userInfo.phoneNumber : ''}
            editable={false}
          />
        </View>
        <View style={styles.viewPicker}>
          <Text>Tỉnh/Thành phố</Text>
          <TouchablePreventDouble
            style={styles.viewSelected}
            onPress={() => this.onPressAddress(address.city)}>
            <View style={{flex: 1}}>
              <Text numberOfLines={1}>
                {userInfo && userInfo.city !== ''
                  ? userInfo.city
                  : 'Chọn Tỉnh/Thành'}
              </Text>
            </View>

            <View style={styles.viewIconSelected}>
              <Icon name="sort-down" size={20} style={styles.iconSelected} />
            </View>
          </TouchablePreventDouble>
        </View>
        <View style={styles.viewPicker}>
          <Text>Quận/Huyện</Text>
          <TouchablePreventDouble
            style={styles.viewSelected}
            onPress={() => this.onPressAddress(address.district)}>
            <View style={{flex: 1}}>
              <Text numberOfLines={1}>
                {userInfo && userInfo.district !== ''
                  ? userInfo.district
                  : 'Chọn Quận/Huyện'}
              </Text>
            </View>

            <View style={styles.viewIconSelected}>
              <Icon name="sort-down" size={20} style={styles.iconSelected} />
            </View>
          </TouchablePreventDouble>
        </View>
        <View style={styles.viewPicker}>
          <Text>Xã/Phường</Text>
          <TouchablePreventDouble
            style={styles.viewSelected}
            onPress={() => this.onPressAddress(address.village)}>
            <View style={{flex: 1}}>
              <Text numberOfLines={1}>
                {userInfo && userInfo.village !== ''
                  ? userInfo.village
                  : 'Chọn Xã/Phường'}
              </Text>
            </View>

            <View style={styles.viewIconSelected}>
              <Icon name="sort-down" size={20} style={styles.iconSelected} />
            </View>
          </TouchablePreventDouble>
        </View>
        <View style={styles.containerTextInput}>
          <Text>Nhập địa chỉ chi tiết</Text>
          <TextInput
            style={styles.textInput}
            placeholder="Địa chỉ chi tiết"
            value={userInfo.address}
            onChangeText={this.onChangeAddress}
          />
        </View>
        {error !== '' && <TxtError text={error} />}
      </View>
    );
  };

  render() {
    const {AddressStore, UserStore} = this.props;
    const {
      showModalAddress,
      gender,
      genderSelected,
      showModalGender,
      showDatepicker,
      addressSelected,
    } = this.state;
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={Keyboard.dismiss}
        activeOpacity={1}>
        <Image source={require('@assets/logo/logo1.png')} style={styles.logo} />
        {this.renderInfor()}
        <View style={styles.viewBack}>
          <TouchablePreventDouble onPress={this.onPressBack}>
            <Text style={styles.txtBack}>Quay lại</Text>
          </TouchablePreventDouble>
        </View>
        <BtnBase
          styleBtn={styles.btn}
          textBtn="Hoàn thành"
          onPress={this.onPressNext}
        />
        {showModalAddress && (
          <Address
            type={addressSelected}
            showModalAddress={showModalAddress}
            onPressBack={this.dissmissAddress}
            AddressStore={AddressStore}
            onPressItem={this.onSelecteAddress}
          />
        )}
        <Modal visible={showModalGender} transparent animationType="slide">
          {showModalGender && (
            <View style={styles.backgroundModalGender}>
              <TouchablePreventDouble
                style={{flex: 1}}
                onPress={this.dissmissGender}
              />
              <View style={styles.viewChildGender}>
                <View>
                  <Text style={styles.txtTitleGender}>Giới tính</Text>
                  <TouchablePreventDouble
                    style={styles.btnSelecteGender}
                    onPress={this.dissmissGender}>
                    <Text style={styles.txtSelected}>Chọn</Text>
                  </TouchablePreventDouble>
                </View>
                <View style={styles.line} />
                <PickerScrollView
                  ref={ref => (this.pickerScroll = ref)}
                  dataSource={gender}
                  selectedIndex={genderSelected}
                  itemHeight={105 / 3}
                  wrapperHeight={105}
                  getSelected={this.handleValueChange}
                />
              </View>
            </View>
          )}
        </Modal>
        <Modal visible={showDatepicker} transparent animationType="slide">
          {showDatepicker && (
            <DatePicker
              date={UserStore.signupInfo.dob}
              onChange={this.onChageDate}
              onClose={this.onCloseDatePicker}
              onConfirm={this.onConfirmDate}
            />
          )}
        </Modal>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    paddingTop: 60,
    paddingHorizontal: 30,
    width: '100%',
  },
  logo: {width: 90, height: 90, resizeMode: 'contain'},
  containerTextInput: {width: '100%', paddingTop: 10},
  textInput: {
    borderBottomWidth: 1,
    borderColor: 'gray',
    height: 40,
    width: '100%',
  },
  viewBack: {
    width: '100%',
    paddingTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  txtBack: {textDecorationLine: 'underline', color: '#2c829e'},
  btn: {height: 40, width: '90%', marginTop: 30},
  viewPicker: {
    width: '100%',
    height: 40,
    paddingVertical: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  viewSelected: {
    height: '100%',
    width: 130,
    borderWidth: 1,
    borderColor: 'gray',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingLeft: 5,
  },
  viewIconSelected: {
    height: '100%',
    width: 25,
    paddingHorizontal: 6,
    borderLeftWidth: 1,
    borderColor: 'gray',
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconSelected: {marginBottom: 5},
  backgroundModalGender: {flex: 1, backgroundColor: '#00000060'},
  viewChildGender: {
    width: '100%',
    padding: 10,
    backgroundColor: '#fff',
    position: 'absolute',
    bottom: 0,
  },
  txtTitleGender: {fontSize: 18, textAlign: 'center'},
  line: {width: '100%', height: 1, backgroundColor: 'gray', marginVertical: 10},
  btnSelecteGender: {
    position: 'absolute',
    right: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtSelected: {color: 'blue'},
});

export default PersonalInfo;
