import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  Modal,
  Keyboard,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native';
import {
  TouchablePreventDouble,
  BtnBase,
  ScreenOTP,
  TxtError,
  ScreenSms,
} from '../../common/base';
import {UserService} from '../../services/UserService';
import {AccountService} from '../../services/AccountService';

var timeoutSms = null;
class CheckPhoneNumber extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showOtp: false,
      phoneNumber: '',
      error: '',
      otp: '',
      otpInput: '',
    };
  }

  onPressBack = () => {
    const {SignUpStore, UserStore} = this.props;
    if (!UserStore.isNoneInfo) {
      SignUpStore.viewPager.setPage(0);
    } else {
      this.props.onPressBack();
      UserStore.isNoneInfo = false;
    }
  };

  onChangePhoneNumber = value => {
    this.setState({
      phoneNumber: value,
    });
  };

  onPressGetOtp = async () => {
    const {phoneNumber} = this.state;
    if (phoneNumber !== '') {
      const res = await UserService.checkPhoneNumber(phoneNumber, '');
      if (res) {
        if (res.errorCode === 202) {
          this.setState({
            error: res.message,
          });
        } else {
          Keyboard.dismiss();
          this.getOtp(phoneNumber);
          this.setState({
            showOtp: true,
            error: '',
          });
          this.sms.slideDown();
        }
      }
    } else {
      this.setState({
        error: 'Số điện thoại không được để trống',
      });
    }
  };

  getOtp = async phoneNumber => {
    const otpSms = await AccountService.getOtp(phoneNumber);
    this.sms.slideUp();
    clearTimeout(timeoutSms);
    this.setState({
      showOtp: true,
    });
    timeoutSms = setTimeout(() => {
      this.setState({
        otp: otpSms,
      });
      this.sms.slideDown();
    }, 1000);
  };

  cancleModal = () => {
    this.setState({
      showOtp: false,
    });
  };

  handleOTP = value => {
    this.setState({
      otpInput: value,
      error: '',
    });
  };

  sendOtp = async () => {
    const {SignUpStore, UserStore} = this.props;
    const {phoneNumber, otpInput} = this.state;
    if (otpInput !== '') {
      const res = await UserService.checkPhoneNumber(phoneNumber, otpInput);
      if (res) {
        if (res.message === 'success') {
          this.setState({
            showOtp: false,
          });
          SignUpStore.viewPager.setPage(2);
          UserStore.signupInfo.phoneNumber = phoneNumber;
        } else {
          this.setState({
            error: res.message,
          });
        }
      }
    } else {
      this.setState({
        error: 'OTP chưa được nhập',
      });
    }
  };

  render() {
    const {showOtp, error, otp} = this.state;
    return (
      <TouchableOpacity
        style={{flex: 1}}
        onPress={Keyboard.dismiss}
        activeOpacity={1}>
        <View style={styles.viewInputPhone}>
          <Image
            source={require('@assets/logo/logo1.png')}
            style={styles.logo}
          />
          <View style={styles.containerTextInput}>
            <Text>Số điện thoại</Text>
            <TextInput
              style={styles.textInput}
              placeholder="vd: 0962486601"
              onChangeText={this.onChangePhoneNumber}
            />
          </View>
          {error !== '' && <TxtError text={error} />}
          <View style={styles.viewBack}>
            <TouchablePreventDouble onPress={this.onPressBack}>
              <Text style={styles.txtBack}>Quay lại</Text>
            </TouchablePreventDouble>
          </View>
          <BtnBase
            styleBtn={styles.btn}
            textBtn="Gửi OTP"
            onPress={this.onPressGetOtp}
          />
        </View>
        {showOtp && (
          <View style={styles.viewInputOtp}>
            <ScreenOTP
              onChangeText={this.handleOTP}
              reSend={this.onPressGetOtp}
              send={this.sendOtp}
              cancleModal={this.cancleModal}
              textError={error}
            />
          </View>
        )}
        <ScreenSms ref={ref => (this.sms = ref)} message={otp} />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  cotainer: {
    width: '100%',
    height: '100%',
    paddingHorizontal: 20,
  },
  viewInputPhone: {
    alignItems: 'center',
    paddingTop: 60,
    paddingHorizontal: 30,
    width: '100%',
  },
  logo: {width: 150, height: 150, resizeMode: 'contain'},
  btn: {height: 40, width: '90%', marginTop: 30},
  viewModal: {
    width: '100%',
    height: '98%',
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    borderWidth: 1,
    borderColor: '#000',
    position: 'absolute',
    bottom: 0,
    backgroundColor: '#fff',
  },
  containerTextInput: {width: '100%', paddingTop: 10},
  textInput: {
    borderBottomWidth: 1,
    borderColor: 'gray',
    height: 40,
    width: '100%',
  },
  viewBack: {
    width: '100%',
    paddingTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  txtBack: {textDecorationLine: 'underline', color: '#2c829e'},
  viewInputOtp: {width: '100%', height: '100%', position: 'absolute'},
});

export default CheckPhoneNumber;
