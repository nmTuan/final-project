import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  StyleProp,
  ViewStyle,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import ItemProduct from '../delivery/ItemProduct';
import {color} from '../../constant';

type Props = {
  otherPlace?: String,
  isShowAll?: Boolean,
  editable?: Boolean,
  styleTextInput?: StyleProp<ViewStyle>,
  onPressEditAddress?: Function,
  onPressEditPayment?: Function,
};

class InformationBill extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      otherPlace: '',
      deliveryName: [],
      totalPrice: 0,
    };
  }

  static defaultProps = {
    otherPlace: '',
    isShowAll: true,
    editable: true,
  };

  onChangeText = value => {
    this.props.onChangeText(value);
  };

  render() {
    const {
      OrderStore,
      otherPlace,
      isShowAll,
      editable,
      styleTextInput,
      onPressEditAddress,
      onPressEditPayment,
      UserStore,
    } = this.props;
    const {deliveryName} = this.state;
    const userInfo = UserStore.userInfo;
    return (
      <View style={{flex: 1}}>
        <View style={styles.containerInfor}>
          <View style={styles.containerTitleInfor}>
            <View style={styles.viewTextTitle}>
              <Icon name="map-marker" size={15} />
              <Text style={styles.titleAddress}>Địa chỉ nhận hàng</Text>
            </View>
            {!isShowAll && (
              <TouchableOpacity onPress={onPressEditAddress}>
                {editable ? (
                  <Text style={{color: 'blue'}}>Lưu</Text>
                ) : (
                  <Icon name="edit" size={20} />
                )}
              </TouchableOpacity>
            )}
          </View>
          <Text style={styles.txtInfor}>
            {userInfo.fullName.toLocaleUpperCase()}
          </Text>
          <Text style={styles.txtInfor}>{userInfo.phoneNumber}</Text>
          {isShowAll ? (
            <View>
              <Text style={styles.txtInfor} numberOfLines={2}>
                {userInfo.address}
              </Text>
              <TextInput
                style={styles.textInputAddress}
                placeholder="Nhập địa chỉ khác"
                value={otherPlace}
                onChangeText={this.onChangeText}
              />
              <Text style={styles.txtAttention}>
                * Địa chỉ khác sẽ là địa chỉ nhận hàng (Mặc định là địa chỉ nhà)
              </Text>
            </View>
          ) : editable ? (
            <TextInput
              style={styleTextInput}
              placeholder="Nhập địa chỉ nhận hàng"
              value={otherPlace}
              multiline
              onContentSizeChange={this.props.onContentSizeChange}
              editable={editable}
              onChangeText={this.onChangeText}
            />
          ) : (
            <Text style={styles.txtInfor} numberOfLines={2}>
              {otherPlace}
            </Text>
          )}
        </View>
        {!isShowAll && (
          <View style={{paddingTop: 10}}>
            <View style={styles.ViewPayment}>
              <Text style={styles.txtTitlePayment}>Phương thức thanh toán</Text>
              <View style={styles.viewValuePayment}>
                {OrderStore.bill.paymentId == 1 ? (
                  <Text style={styles.txtInfor}>Thanh toán khi nhận hàng</Text>
                ) : (
                  <View>
                    <Text style={styles.txtInfor}>
                      Thanh toán qua ngân hàng
                    </Text>
                    <Text style={styles.txtInfor}>
                      {OrderStore.bill.bankName}
                    </Text>
                    <Text style={styles.txtInfor}>{`STK: ${
                      OrderStore.bill.creditNumber
                    }`}</Text>
                  </View>
                )}
                <TouchableOpacity onPress={onPressEditPayment}>
                  <Icon name="edit" size={20} />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        )}
        {OrderStore.bill.productBillDTOs.map((item, index) => (
          <ItemProduct
            ref={ref => (this.itemProduct = ref)}
            item={item}
            index={index}
            deliveryName={deliveryName}
            OrderStore={OrderStore}
          />
        ))}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  txtInfor: {fontSize: 15, flexWrap: 'wrap'},
  textInputAddress: {
    width: '100%',
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    height: 40,
    marginTop: 10,
  },
  txtAttention: {fontSize: 13, color: 'gray'},
  containerInfor: {
    width: '100%',
    padding: 10,
    backgroundColor: '#fff',
    marginTop: 10,
  },
  containerTitleInfor: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  viewTextTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    // color: color.agree,
  },
  line: {
    width: '100%',
    height: 1,
    backgroundColor: 'gray',
    marginVertical: 10,
  },
  titleAddress: {fontSize: 16, marginLeft: 10, color: color.agree},
  btnNext: {height: 40, marginTop: 10},
  ViewPayment: {
    backgroundColor: '#fff',
    width: '100%',
    justifyContent: 'center',
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: 'gray',
  },
  viewValuePayment: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  txtTitlePayment: {
    fontSize: 16,
    marginBottom: 5,
    color: color.agree,
  },
});

export default InformationBill;
