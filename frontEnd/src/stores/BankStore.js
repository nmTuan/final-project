import {observable, action} from 'mobx';

class BankStore {
  @observable step = 0;

  @observable bankInfo = {
    bankName: '',
    creditName: '',
    creditNumber: '',
    month: '',
    year: '',
  };

  @action
  resetData() {
    this.bankInfo = {
      bankName: '',
      creditName: '',
      creditNumber: '',
      month: '',
      year: '',
    };
    this.step = 0;
  }

  @action
  setStep(step) {
    this.step = step;
  }
}

export default new BankStore();
