/* eslint-disable prettier/prettier */
import { observable, action } from 'mobx';

class HomeStore {
    @observable listProducts = [];

    @action
    setProduct(oldData, products){
        this.listProducts = [...oldData, ...products];
    }
}

export default new HomeStore();
