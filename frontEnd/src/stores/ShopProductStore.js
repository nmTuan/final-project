import {observable, action} from 'mobx';

class ShopProductStore {
  @observable productInfo = null;

  @action
  setSizeColors(arr) {
    this.productInfo.sizeColors = arr;
  }

  @action
  setProduct(product) {
    this.productInfo = product;
  }

  @action
  checkSizeColor() {
    const productInfo = this.productInfo;
    var boo = false;
    if (productInfo.isRent) {
      for (let i = 0; i < productInfo.sizeColors.length; i++) {
        const item = productInfo.sizeColors[i];
        const baseCondition =
          item.size !== '' &&
          item.color !== '' &&
          item.quantity > 0 &&
          item.price > 0
            ? true
            : false;
        if (baseCondition && item.rentPrice > 0 && item.deposit > 0) {
          boo = true;
        } else {
          boo = false;
          break;
        }
      }
    } else {
      for (let i = 0; i < productInfo.sizeColors.length; i++) {
        const item = productInfo.sizeColors[i];
        if (
          item.size !== '' &&
          item.color !== '' &&
          item.quantity > 0 &&
          item.price > 0
        ) {
          boo = true;
        } else {
          boo = false;
          break;
        }
      }
    }
    return boo;
  }
}

export default new ShopProductStore();
