import { observable } from "mobx";

class SignUpStore {
  @observable viewPager = null;
}

export default new SignUpStore();
