import {observable, action} from 'mobx';

class UserChatStore {
  @observable listRoomChat = [];

  @observable listMessage = [];

  @action
  setListRoomChat(list) {
    this.listRoomChat = list;
  }

  @action
  setListMess(list) {
    this.listMessage = list;
  }
}

export default new UserChatStore();
