import {observable, action} from 'mobx';
import AsyncStorage from '@react-native-community/async-storage';
import {values} from '../constant';

class ShopHomeStore {
  @observable shopInfo = null;

  @observable username = '';

  @observable productDetail = null;

  @observable listProduct = [];

  @observable scrollRef = null;

  @observable billWaiting = [];

  @observable billConfirm = [];

  @observable billDelivering = [];

  @observable billSuccess = [];

  @observable billFail = [];

  @observable billAbort = [];

  @observable notification = [];

  @observable quantityChat = 0;

  @observable tabBillSelected = values.bill.waiting;

  @observable listBank = [];

  @action
  async setShopInfo(shopInfo) {
    const key = values.keyAsynStorage;
    await AsyncStorage.setItem(key.userInfo, JSON.stringify(shopInfo));
    this.shopInfo = shopInfo;
  }

  @action
  resetData() {
    this.shopInfo = null;
    this.username = '';
    this.quantityChat = 0;
  }

  @action
  async setUsername(username) {
    const key = values.keyAsynStorage;
    await AsyncStorage.setItem(key.username, username);
    this.username = username;
  }

  @action
  setListProduct(oldList, newList) {
    this.listProduct = [...oldList, ...newList];
  }

  @action
  setProduct(product) {
    this.productDetail = product;
  }

  @action
  setBillWaiting(bill) {
    this.billWaiting = bill;
  }

  @action
  setBillConfirm(bill) {
    this.billConfirm = bill;
  }

  @action
  setBillDelivering(bill) {
    this.billDelivering = bill;
  }

  @action
  setBillSuccess(bill) {
    this.billSuccess = bill;
  }

  @action
  setBillFail(bill) {
    this.billFail = bill;
  }

  @action
  setBillAbort(bill) {
    this.billAbort = bill;
  }

  @action
  setNotification(oldData, newData) {
    this.notification = [...oldData, ...newData];
  }

  @action
  setTabBillSelected(selected) {
    this.tabBillSelected = selected;
  }

  @action
  setListBank(list) {
    this.listBank = list;
  }
}

export default new ShopHomeStore();
