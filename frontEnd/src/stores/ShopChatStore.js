import {observable, action} from 'mobx';

class ShopChatStore {
  @observable listRoomChat = [];

  @observable listMessage = [];

  @action
  setListRoomChat(list) {
    this.listRoomChat = list;
  }

  @action
  setListMess(list) {
    // console.log('List ', JSON.stringify(list));
    this.listMessage = list;
  }
}

export default new ShopChatStore();
