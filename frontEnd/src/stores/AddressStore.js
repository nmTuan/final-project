import {observable, action} from 'mobx';

class AddressStore {
  @observable data = [];

  @observable citySelected = null;

  @observable districtSelected = null;

  @observable villageSelected = null;

  @action
  setData(oldData, newData) {
    this.data = [...oldData, ...newData];
  }

  @action
  setCity(city) {
    this.citySelected = city;
    this.districtSelected = null;
    this.villageSelected = null;
  }

  @action
  setDistrict(district) {
    this.districtSelected = district;
    this.villageSelected = null;
  }

  @action
  setVillage(village) {
    this.villageSelected = village;
  }

  @action
  resetData() {
    this.data = [];
    this.citySelected = null;
    this.districtSelected = null;
    this.villageSelected = null;
  }
}

export default new AddressStore();
