import {observable, action} from 'mobx';
import AsyncStorage from '@react-native-community/async-storage';
import {values} from '../constant';

class UserStore {
  @observable isNoneInfo = false;

  @observable userInfo = null;

  @observable username = '';

  @observable billWaiting = [];

  @observable billConfirm = [];

  @observable billDelivering = [];

  @observable billSuccess = [];

  @observable billFail = [];

  @observable billAbort = [];

  @observable productInBill = [];

  @observable notificationCust = [];

  @observable listBank = [];

  @observable tabBillSelected = values.bill.waiting;

  @observable quantityChat = 0;

  @observable signupInfo = {
    accId: 0,
    fullName: '',
    dob: 50321332484,
    gender: 'nam',
    avatar: '',
    coverava: '',
    phoneNumber: '',
    city: '',
    district: '',
    village: '',
    address: '',
  };

  @observable shopInfo = {
    accId: '',
    shopName: '',
    description: '',
    avatar: '',
    coverava: '',
    phoneNumber: '',
    address: '',
  };

  @action
  async setUserInfo(userInfo, accountId) {
    const key = values.keyAsynStorage;
    await AsyncStorage.setItem(key.accountId, accountId.toString());
    await AsyncStorage.setItem(key.userInfo, JSON.stringify(userInfo));
    this.userInfo = userInfo;
  }

  @action
  async setUsername(username) {
    const key = values.keyAsynStorage;
    await AsyncStorage.setItem(key.username, username);
    this.username = username;
  }

  @action
  reset() {
    this.userInfo = null;
    this.username = '';
    this.quantityChat = 0;
  }

  @action
  setBillWaiting(bill) {
    this.billWaiting = bill;
  }

  @action
  setBillConfirm(bill) {
    this.billConfirm = bill;
  }

  @action
  setBillDelivering(bill) {
    this.billDelivering = bill;
  }

  @action
  setBillSuccess(bill) {
    this.billSuccess = bill;
  }

  @action
  setBillFail(bill) {
    this.billFail = bill;
  }

  @action
  setBillAbort(bill) {
    this.billAbort = bill;
  }

  @action
  setProductBill(data) {
    const product = {
      shopId: data.shopId,
      shopName: data.shopName,
      shopAddress: data.shopAddress,
      listProduct: [
        {
          sizeId: data.sizeId,
          productImg: data.thumbnail,
          productName: data.productName,
          quantity: data.quantity,
          oldPrice: data.oldPrice,
          price: data.maxPrice,
          vote: data.vote,
          rent: data.isRent,
          rentDue: data.rentDue,
          rentPrice: data.rentPrice,
          deposit: data.deposit,
        },
      ],
    };
    this.productInBill.push(product);
  }

  @action
  setNotification(oldData, newData) {
    this.notificationCust = [...oldData, ...newData];
  }

  @action
  setTabBillSelected(position) {
    this.tabBillSelected = position;
  }

  @action
  setListBank(list) {
    this.listBank = list;
  }
}

export default new UserStore();
