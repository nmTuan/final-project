import {observable, action} from 'mobx';

class OrderStore {
  @observable quantity = 0;

  @observable rentDue = 0;

  @observable pageSelected = 0;

  @observable scrollViewRef = null;

  @observable stepIndicatorRef = null;

  @observable productInCart = {
    shopId: 0,
    shopName: '',
    shopAddress: '',
    listProduct: [
      {
        productImg: '',
        productName: '',
        quantity: 0,
        oldPrice: 0,
        price: 0,
        vote: 0,
        isRent: -1,
        rentDue: 0,
        rentPrice: 0,
        deposit: 0,
      },
    ],
  };

  @observable listProductInCart = [];

  @observable bill = {
    custId: 0,
    orderPlace: '',
    paymentId: 0,
    bankId: '',
    bankName: '',
    creditNumber: '',
    totalBill: 0,
    productBillDTOs: [],
  };

  @action
  decreaseQuantity() {
    this.quantity -= 1;
  }

  @action
  increaseQuantity() {
    this.quantity += 1;
  }

  @action
  decreaseRentDue() {
    this.rentDue -= 1;
  }

  @action
  increaseRentDue() {
    this.rentDue += 1;
  }

  @action
  resetQuantity() {
    this.quantity = 0;
  }

  @action
  resetRentDue() {
    this.rentDue = 0;
  }

  @action
  setBill(totalBill) {
    this.bill.totalBill = totalBill;
  }

  @action
  resetBill() {
    this.bill = {
      custId: 0,
      orderPlace: '',
      paymentId: 0,
      totalBill: 0,
      productBillDTOs: [],
    };
  }

  @action
  setProductInBill(data) {
    this.bill.productBillDTOs = data;
  }
}

export default new OrderStore();
