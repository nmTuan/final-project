import {observable, action} from 'mobx';
import {values} from '../constant';

class FilterStore {
  @observable currentTypeSelected = values.selected.all;
  @observable currentStateSort = values.sortState.normal;
  @observable isShowFilter = false;
  @observable categories = [];
  @observable gender = [];
  @observable age = [];
  @observable vote = [1, 2, 3, 4, 5];
  @observable categorySelected = -1;
  @observable genderSelected = -1;
  @observable ageSelected = -1;
  @observable voteSelected = -1;
  @observable sort = {
    productName: '',
    category_id: '',
    gender_id: '',
    age_id: '',
    sold: '',
    vote: '',
    isRent: '',
    listSort: [],
  };

  @action
  setValueSelected(type, index) {
    // console.log("type ", type, " ", value);
    const {typeFilter} = values;
    switch (type) {
      case typeFilter.category:
        this.categorySelected = index;
        this.sort.category_id = this.categories[index].id;
        break;
      case typeFilter.gender:
        this.genderSelected = index;
        this.sort.gender_id = this.gender[index].id;
        break;
      case typeFilter.age:
        this.ageSelected = index;
        this.sort.age_id = this.age[index].id;
        break;
      case typeFilter.vote:
        this.voteSelected = index;
        this.sort.vote = this.vote[index];
        break;
      default:
        break;
    }
  }

  @action
  reset() {
    this.categorySelected = -1;
    this.genderSelected = -1;
    this.ageSelected = -1;
    this.voteSelected = -1;
    this.sort = {
      productName: '',
      category_id: '',
      gender_id: '',
      age_id: '',
      sold: '',
      vote: '',
      isRent: '',
      listSort: [],
      page: 0,
      size: 10,
    };
  }

  @action
  onPressSelectedType(type) {
    const {selected} = values;
    switch (type) {
      case selected.all:
        this.currentTypeSelected = selected.all;
        this.reset();
        break;
      case selected.mostSell:
        this.currentTypeSelected = selected.mostSell;
        break;
      case selected.rent:
        this.currentTypeSelected = selected.rent;
        break;
      default:
        break;
    }
  }

  @action
  priceSort(currentSortState) {
    const sortState = values.sortState;
    if (
      currentSortState === sortState.up ||
      currentSortState === sortState.normal
    ) {
      this.currentStateSort = sortState.down;
    } else {
      this.currentStateSort = sortState.up;
    }
  }
}

export default new FilterStore();
