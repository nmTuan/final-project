/* eslint-disable prettier/prettier */
import HomeStore from './HomeStore';
import FilterStore from './FilterStore';
import OrderStore from './OrderStore';
import SignUpStore from './SignUpStore';
import UserStore from './UserStore';
import AddressStore from './AddressStore';
import ShopHomeStore from './ShopHomeStore';
import ShopProductStore from './ShopProductStore';
import ShopChatStore from './ShopChatStore';
import UserChatStore from './UserChatStore';
import BankStore from './BankStore';

export default {
    HomeStore,
    FilterStore,
    OrderStore,
    SignUpStore,
    UserStore,
    AddressStore,
    ShopHomeStore,
    ShopProductStore,
    ShopChatStore,
    UserChatStore,
    BankStore,
};
