import {Navigation} from 'react-native-navigation';
import {color} from '../constant';

export const pushScreen = (componentId, screenName, passProps) => {
  Navigation.push(componentId, {
    component: {
      passProps,
      name: screenName,
      options: {
        topBar: {
          visible: false,
          height: 0,
        },
        bottomTabs: {
          visible: false,
        },
        bottomTab: {
          textColor: color.lightGrayColor,
          selectedTextColor: color.primary,
          iconColor: color.lightGrayColor,
          selectedIconColor: color.primary,
        },
        animations: {
          push: {
            waitForRender: true,
          },
        },
      },
    },
  });
};

export const showModal = (screenName, passProps) => {
  Navigation.showModal({
    component: {
      passProps,
      name: screenName,
      options: {
        topBar: {
          height: 0,
          visible: false,
        },
      },
    },
  });
};
