import Axios from 'axios';
import {values} from '../constant';
import AsyncStorage from '@react-native-community/async-storage';
import Values from '../constant/Values';

export const request = {
  async header() {
    return await Axios.create({
      baseURL: Values.domain,
      timeout: values.requestTimeout,
      headers: {
        'Content-Type': 'application/json',
      },
    });
  },

  async uploadFile(url, image, roomId = '') {
    let data = new FormData();
    data.append('file', {
      uri: image,
      type: 'image/jpeg',
      name: `${new Date().getTime()}.jpg`,
    });
    if (roomId !== '') {
      data.append('roomId', roomId);
    }
    console.log(JSON.stringify(data));
    var instance = Axios.create({
      baseURL: Values.domain,
      timeout: values.requestTimeout,
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
    const res = await instance.post(url, data);
    try {
      return res.data;
    } catch (e) {
      console.log(e);
    }
  },

  async headerWithToken() {
    const token = await AsyncStorage.getItem(values.TOKEN);
    return await Axios.create({
      baseURL: Values.domain,
      timeout: values.requestTimeout,
      headers: {
        'Content-Type': 'application/json',
        layout: 'app',
        Authorization: token ? 'Bearer ' + token : '',
      },
    });
  },

  async post(url, body) {
    const api = await this.header();
    const res = await api.post(url, body);
    return res.data;
  },

  async postWithToken(url, body) {
    const api = await this.headerWithToken();
    const res = await api.post(url, body);
    return res.data;
  },

  async get(url) {
    const api = await this.header();
    const res = await api.get(url);
    return res.data;
  },

  async getWithToken(url) {
    const api = await this.headerWithToken();
    const res = await api.get(url);
    return res.data;
  },

  // async postFile(url, body) {
  //   const api = await this.uploadFile();
  //   const res = await api.post(url, body);
  //   console.log(res);
  //   return res.data;
  // },
};
