import {ToastAndroid, Platform, Alert} from 'react-native';

export function isCloseToBottom({
  layoutMeasurement,
  contentOffset,
  contentSize,
}) {
  const paddingToBottom = 20;
  return (
    layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom
  );
}

export function notifyMessage(msg: String) {
  if (Platform.OS === 'android') {
    ToastAndroid.show(msg, ToastAndroid.SHORT);
  } else {
    Alert.alert(msg);
  }
}
