import {Platform, Dimensions, StatusBar} from 'react-native';
import {Navigation} from 'react-native-navigation';

/**
 * Common
 */
const activeOpacity = 0.7;
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const aspectRatio = deviceHeight / deviceWidth;
const deviceIsIphone = aspectRatio > 1.6;
const platform = Platform.OS;
const platformOS = platform === 'android' ? 1 : 0;
const deviceHeightBig = deviceHeight >= 812;
// const LATITUDE = 21.028667;
// const LONGITUDE = 105.852148;
const LATITUDE_DELTA = 0.005;
const LONGITUDE_DELTA = LATITUDE_DELTA * aspectRatio;
// const POSITION_DEFAULT = {
//   latitude: LATITUDE,
//   longitude: LONGITUDE,
//   latitudeDelta: LATITUDE_DELTA,
//   longitudeDelta: LONGITUDE_DELTA,
// };

/**
 * IOS
 */
const isIphoneX =
  platform === 'ios' && deviceHeight === 812 && deviceWidth === 375;
// aspect ratio iphone XS max == iphone XR
const isIphoneXR =
  platform === 'ios' && deviceHeight === 896 && deviceWidth === 414;
const isIphoneBasic = platform === 'ios' && !isIphoneX && !isIphoneXR;
const statusBarHeightIos = 20;
const statusBarHeightX = 45;
const marginTopIphone = isIphoneBasic ? statusBarHeightIos : statusBarHeightX;
const bottomIphoneX = isIphoneBasic || platform === 'android' ? 0 : 16;
/**
 * Android
 */
const isAPILest21 = platform === 'android' && Platform.Version < 21;
const statusBarHeightAndroid = StatusBar.currentHeight;
const marginTopScreen = platform === 'android' ? 0 : marginTopIphone;
const heightContentHeader = 48;
const heightContentSearchHeader = 48;
const heightContainerHeader = heightContentHeader + (marginTopScreen || 0);

const HEIGHT_PAYMENT = deviceHeight - 100;

export default {
  activeOpacity,
  aspectRatio,
  bottomIphoneX,
  deviceHeight,
  deviceIsIphone,
  deviceWidth,
  heightContainerHeader,
  heightContentHeader,
  heightContentSearchHeader,
  isAPILest21,
  marginTopIphone,
  marginTopScreen,
  platform,
  statusBarHeightAndroid,
  statusBarHeightIos,
  statusBarHeightX,
  platformOS,
  deviceHeightBig,
  LATITUDE_DELTA,
  LONGITUDE_DELTA,
  HEIGHT_PAYMENT,
};
