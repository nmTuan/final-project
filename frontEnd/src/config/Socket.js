import io from 'socket.io-client';
import {values} from '../constant';

var socket = null;
export function openSocket() {
  socket = io(`http://${values.ip}:3000`);
}

export function getSocket() {
  return socket;
}
