import {Alert, Linking, Platform} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import ImageResizer from 'react-native-image-resizer';
import {FileService} from '../services/FileService';

export function alertWaring(content) {
  Alert.alert(
    '',
    content,
    [
      {
        text: 'Huỷ bỏ',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {text: 'Cài đặt', onPress: () => Linking.openSettings()},
    ],
    {cancelable: false},
  );
}

export async function openCamera() {
  try {
    const image = await ImagePicker.openCamera({
      mediaType: 'any',
    });
    const result = {
      name: image.mime,
      image: image.path,
      height: image.height,
      width: image.width,
    };
    return result;
  } catch (e) {
    if (e.code === 'E_PERMISSION_MISSING') {
      const text = 'Bạn chưa cấp quyền truy cập máy ảnh cho ứng dụng';
      alertWaring(text);
    }
  }
}

export async function openLibMultiImage(length = 0) {
  const maxFile = 6 - length;
  var images = [];
  try {
    images = await ImagePicker.openPicker({
      multiple: true,
      includeBase64: true,
      includeExif: true,
      maxFiles: maxFile,
    });
    if (images.length > maxFile) {
      alert(`Không chọn quá ${maxFile} ảnh`);
    } else {
      const listImages = [];
      images.map(item => {
        const image = {
          name: item.mime,
          image: item.path,
          height: item.height,
          width: item.width,
        };
        listImages.push(image);
      });
      return listImages;
    }
  } catch (e) {
    if (e.code === 'E_PERMISSION_MISSING') {
      const text = 'Bạn chưa cấp quyền truy cập thư viện ảnh cho ứng dụng';
      alertWaring(text);
    }
  }
}

export async function openLibSingleImage() {
  try {
    const image = await ImagePicker.openPicker({
      includeBase64: true,
      includeExif: true,
    });
    const result = {
      name: image.mime,
      image: image.path,
      height: image.height,
      width: image.width,
    };
    return result;
  } catch (e) {
    if (e.code === 'E_PERMISSION_MISSING') {
      const text = 'Bạn chưa cấp quyền truy cập thư viện ảnh cho ứng dụng';
      alertWaring(text);
    }
  }
}

export async function resizeImage(image, width, height, type, quality) {
  return new Promise(function(resolve, reject) {
    ImageResizer.createResizedImage(image, width, height, type, quality)
      .then(response => {
        const uri =
          Platform.OS === 'ios'
            ? response.uri.replace('file://', '')
            : response.uri;
        resolve(uri);
      })
      .catch(e => {
        reject(e);
      });
  });
}
