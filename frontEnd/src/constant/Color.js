export default {
  backgroundPopup: '#6e6a6a80',
  lightGrayColor:'#b6b9bf70',
  white: '#fff',
  cancel: '#94b5a0',
  agree: '#14c451',
  background: '#ebebeb',
  primary: '#61f280',
  red: '#ff3700',
};
