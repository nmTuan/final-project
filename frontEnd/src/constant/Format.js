import {values} from '.';

export function formatCurrency(number) {
  if (number) {
    return number
      .toFixed(2)
      .replace(/\d(?=(\d{3})+\.)/g, '$&,')
      .toString()
      .replace('.00', '');
  }
  return 0;
}

export function statusProduct(status) {
  const {
    waiting,
    confirmed,
    delivering,
    succes,
    fail,
    abort,
    custAbort,
  } = values.bill;
  switch (status) {
    case waiting:
      return 'Chờ xác nhận';
    case confirmed:
      return 'Chờ giao hàng';
    case delivering:
      return 'Đang giao hàng';
    case succes:
      return 'Giao hàng thành công';
    case fail:
      return 'Giao hàng thất bại';
    case abort:
      return 'Shop huỷ đơn hàng';
    case custAbort:
      return 'Bạn huỷ đơn hàng';
    default:
      return 'undifined';
  }
}

export function statusProductShop(status) {
  const {
    waiting,
    confirmed,
    delivering,
    succes,
    fail,
    abort,
    custAbort,
  } = values.bill;
  switch (status) {
    case waiting:
      return 'Chờ xác nhận';
    case confirmed:
      return 'Chờ giao hàng';
    case delivering:
      return 'Đang giao hàng';
    case succes:
      return 'Giao hàng thành công';
    case fail:
      return 'Giao hàng thất bại';
    case abort:
      return 'Shop huỷ đơn hàng';
    case custAbort:
      return 'Khách huỷ đơn hàng';
    default:
      return 'undifined';
  }
}

export function getCityAddress(name) {
  if (name.search('Thành phố') >= 0) {
    return name.replace('Thành phố', '');
  } else if (name.search('Tỉnh') >= 0) {
    return name.replace('Tỉnh', '');
  } else if (name.search('Quận') >= 0) {
    return name.replace('Quận', '');
  } else if (name.search('Huyện') >= 0) {
    return name.replace('Huyện', '');
  } else if (name.search('Thị xã') >= 0) {
    return name.replace('Thị xã', '');
  } else if (name.search('Phường') >= 0) {
    return name.replace('Phường', '');
  } else if (name.search('Thị trấn') >= 0) {
    return name.replace('Thị trấn', '');
  } else if (name.search('Xã') >= 0) {
    return name.replace('Xã', '');
  } else {
    return name;
  }
}
