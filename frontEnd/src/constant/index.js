import Color from './Color';
import Values from './Values';

export const color = Color;
export const values = Values;
