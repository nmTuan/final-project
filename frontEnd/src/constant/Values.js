const ip = '192.168.10.100';
const domain = `http://${ip}:8080/`;
const keyAsynStorage = {
  userInfo: 'userInfo',
  username: 'username',
  accountId: 'accountId',
};
const requestTimeout = 10000;
const duaration = 500;
const shadowOffset = {
  width: 0,
  height: 1,
};
const messageSuccess = 'success';
const sizePage = 10;
const topBarHeight = 50;
const shadowOpacity = 0.22;
const shadowRadius = 2.22;
const elevation = 3;
const shadowColor = '#000';
const labels = ['Delivery', 'payment', 'Check'];
const addToCart = 0;
const buy = 1;
const sortState = {
  normal: 0,
  up: 1,
  down: 2,
};
const selected = {
  all: 0,
  mostSell: 1,
  rent: 2,
};
const bill = {
  waiting: 0,
  confirmed: 1,
  delivering: 2,
  succes: 3,
  fail: 4,
  abort: 5,
  custAbort: 6,
};

const typeFilter = {
  category: 0,
  age: 1,
  gender: 2,
  vote: 3,
};

const address = {
  city: 0,
  district: 1,
  village: 2,
};

const typeNotificationShop = {
  abortBill: 'Huỷ đơn hàng',
  waitingBill: 'Yêu cầu xác nhận đơn hàng',
};

const typeNotificationCust = {
  abortBill: 'Huỷ đơn hàng',
  confirmBill: 'Xác nhận đơn',
};

const TOKEN = 'token';

const socket = {
  login: 'login',
  customer: 0,
  shop: 1,
};

export default {
  duaration,
  labels,
  shadowOffset,
  shadowOpacity,
  elevation,
  shadowRadius,
  shadowColor,
  sortState,
  selected,
  typeFilter,
  topBarHeight,
  bill,
  address,
  requestTimeout,
  sizePage,
  addToCart,
  buy,
  messageSuccess,
  TOKEN,
  domain,
  keyAsynStorage,
  typeNotificationShop,
  typeNotificationCust,
  ip,
  socket,
};
