import {Dimensions} from 'react-native';
import ScreenConfig from '../config/ScreenConfig';

export default {
  screenWidth:
    ScreenConfig.deviceWidth < ScreenConfig.deviceHeight
      ? ScreenConfig.deviceWidth
      : ScreenConfig.deviceHeight,
  screenHeight:
    ScreenConfig.deviceWidth < ScreenConfig.deviceHeight
      ? ScreenConfig.deviceHeight
      : ScreenConfig.deviceWidth,
  navBarHeight: ScreenConfig.platform === 'ios' ? 64 : 54,
  buttonRadius: 4,
  heightScreen: Dimensions.get('window').height,
  witdhScreen: Dimensions.get('window').width,
};
