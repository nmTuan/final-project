/* eslint-disable prettier/prettier */
import React from 'react';
import { Navigation } from 'react-native-navigation';
import { Provider } from 'mobx-react';
import _ from 'lodash';
import * as screens from '../screens';
import stores from '../stores';

function wrapperComponent(Component) {
    return class App extends React.Component {
        render() {
         return (
          <Provider {...stores}>
            <Component {...this.props} />
          </Provider>
        );
       }
    };
}

export function registerScreen() {
    _.map(screens, (v, k) => {
        Navigation.registerComponent(`${k}`, () => wrapperComponent(v));
    });
}
