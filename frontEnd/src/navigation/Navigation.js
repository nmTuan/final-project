import {Navigation} from 'react-native-navigation';
import {registerScreen} from './RegisterScreen';
import {color} from '../constant';

registerScreen();

export function SplashScreen() {
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: 'App',
              options: {
                topBar: {
                  visible: false,
                  height: 0,
                },
              },
            },
          },
        ],
      },
    },
  });
}

export function MainApp(currentTabIndex) {
  Navigation.setRoot({
    root: {
      bottomTabs: {
        options: {
          bottomTabs: {
            titleDisplayMode: 'alwaysShow',
            currentTabIndex: currentTabIndex ? currentTabIndex : 0,
          },
        },
        children: [
          {
            stack: {
              children: [
                {
                  component: {
                    name: 'Home',
                    options: {
                      bottomTab: {
                        icon: require('@assets/icons/home.png'),
                        text: 'Trang chủ',
                        textColor: color.lightGrayColor,
                        selectedTextColor: color.primary,
                        iconColor: color.lightGrayColor,
                        selectedIconColor: color.primary,
                      },
                      topBar: {
                        visible: false,
                        height: 0,
                      },
                    },
                    animations: {
                      push: {
                        waitForRender: true,
                        content: {
                          alpha: {
                            from: 0,
                            to: 1,
                            duration: 300,
                            startDelay: 0,
                            interpolation: 'accelerate',
                          },
                        },
                      },
                    },
                  },
                },
              ],
            },
          },
          {
            stack: {
              children: [
                {
                  component: {
                    name: 'Bill',
                    options: {
                      bottomTab: {
                        icon: require('@assets/icons/bill.png'),
                        text: 'Đơn hàng',
                        textColor: color.lightGrayColor,
                        selectedTextColor: color.primary,
                        iconColor: color.lightGrayColor,
                        selectedIconColor: color.primary,
                      },
                      topBar: {
                        visible: false,
                        height: 0,
                      },
                    },
                    animations: {
                      push: {
                        waitForRender: true,
                        content: {
                          alpha: {
                            from: 0,
                            to: 1,
                            duration: 300,
                            startDelay: 0,
                            interpolation: 'accelerate',
                          },
                        },
                      },
                    },
                  },
                },
              ],
            },
          },
          {
            stack: {
              children: [
                {
                  component: {
                    name: 'Notification',
                    options: {
                      bottomTab: {
                        icon: require('@assets/icons/notification.png'),
                        text: 'Thông báo',
                        textColor: color.lightGrayColor,
                        selectedTextColor: color.primary,
                        iconColor: color.lightGrayColor,
                        selectedIconColor: color.primary,
                      },
                      topBar: {
                        visible: false,
                        height: 0,
                      },
                    },
                    animations: {
                      push: {
                        waitForRender: true,
                        content: {
                          alpha: {
                            from: 0,
                            to: 1,
                            duration: 300,
                            startDelay: 0,
                            interpolation: 'accelerate',
                          },
                        },
                      },
                    },
                  },
                },
              ],
            },
          },
          {
            stack: {
              children: [
                {
                  component: {
                    name: 'Personal',
                    options: {
                      bottomTab: {
                        icon: require('@assets/icons/user.png'),
                        text: 'Cá nhân',
                        textColor: color.lightGrayColor,
                        selectedTextColor: color.primary,
                        iconColor: color.lightGrayColor,
                        selectedIconColor: color.primary,
                      },
                      topBar: {
                        visible: false,
                        height: 0,
                      },
                    },
                    animations: {
                      push: {
                        waitForRender: true,
                        content: {
                          alpha: {
                            from: 0,
                            to: 1,
                            duration: 300,
                            startDelay: 0,
                            interpolation: 'accelerate',
                          },
                        },
                      },
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    },
  });
}

export function MainShop(currentTabIndex) {
  Navigation.setRoot({
    root: {
      bottomTabs: {
        options: {
          bottomTabs: {
            titleDisplayMode: 'alwaysShow',
            currentTabIndex: currentTabIndex ? currentTabIndex : 0,
          },
        },
        children: [
          {
            stack: {
              children: [
                {
                  component: {
                    name: 'ShopHome',
                    options: {
                      bottomTab: {
                        icon: require('@assets/icons/home.png'),
                        text: 'Trang chủ',
                        textColor: color.lightGrayColor,
                        selectedTextColor: color.primary,
                        iconColor: color.lightGrayColor,
                        selectedIconColor: color.primary,
                      },
                      topBar: {
                        visible: false,
                        height: 0,
                      },
                    },
                    animations: {
                      push: {
                        waitForRender: true,
                        content: {
                          alpha: {
                            from: 0,
                            to: 1,
                            duration: 300,
                            startDelay: 0,
                            interpolation: 'accelerate',
                          },
                        },
                      },
                    },
                  },
                },
              ],
            },
          },
          {
            stack: {
              children: [
                {
                  component: {
                    name: 'Product',
                    options: {
                      bottomTab: {
                        icon: require('@assets/icons/product.png'),
                        text: 'Sản phẩm',
                        textColor: color.lightGrayColor,
                        selectedTextColor: color.primary,
                        iconColor: color.lightGrayColor,
                        selectedIconColor: color.primary,
                      },
                      topBar: {
                        visible: false,
                        height: 0,
                      },
                    },
                    animations: {
                      push: {
                        waitForRender: true,
                        content: {
                          alpha: {
                            from: 0,
                            to: 1,
                            duration: 300,
                            startDelay: 0,
                            interpolation: 'accelerate',
                          },
                        },
                      },
                    },
                  },
                },
              ],
            },
          },
          {
            stack: {
              children: [
                {
                  component: {
                    name: 'BillShop',
                    options: {
                      bottomTab: {
                        icon: require('@assets/icons/bill.png'),
                        text: 'Đơn hàng',
                        textColor: color.lightGrayColor,
                        selectedTextColor: color.primary,
                        iconColor: color.lightGrayColor,
                        selectedIconColor: color.primary,
                      },
                      topBar: {
                        visible: false,
                        height: 0,
                      },
                    },
                    animations: {
                      push: {
                        waitForRender: true,
                        content: {
                          alpha: {
                            from: 0,
                            to: 1,
                            duration: 300,
                            startDelay: 0,
                            interpolation: 'accelerate',
                          },
                        },
                      },
                    },
                  },
                },
              ],
            },
          },
          {
            stack: {
              children: [
                {
                  component: {
                    name: 'NotificationShop',
                    options: {
                      bottomTab: {
                        icon: require('@assets/icons/notification.png'),
                        text: 'Thông báo',
                        textColor: color.lightGrayColor,
                        selectedTextColor: color.primary,
                        iconColor: color.lightGrayColor,
                        selectedIconColor: color.primary,
                      },
                      topBar: {
                        visible: false,
                        height: 0,
                      },
                    },
                    animations: {
                      push: {
                        waitForRender: true,
                        content: {
                          alpha: {
                            from: 0,
                            to: 1,
                            duration: 300,
                            startDelay: 0,
                            interpolation: 'accelerate',
                          },
                        },
                      },
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    },
  });
}

export function NotLogin() {
  Navigation.setRoot({
    root: {
      bottomTabs: {
        options: {
          bottomTabs: {
            titleDisplayMode: 'alwaysShow',
          },
        },
        children: [
          {
            stack: {
              children: [
                {
                  component: {
                    name: 'Home',
                    options: {
                      bottomTab: {
                        icon: require('@assets/icons/home.png'),
                        text: 'Trang chủ',
                        textColor: color.lightGrayColor,
                        selectedTextColor: color.primary,
                        iconColor: color.lightGrayColor,
                        selectedIconColor: color.primary,
                      },
                      topBar: {
                        visible: false,
                        height: 0,
                      },
                    },
                    animations: {
                      push: {
                        waitForRender: true,
                        content: {
                          alpha: {
                            from: 0,
                            to: 1,
                            duration: 300,
                            startDelay: 0,
                            interpolation: 'accelerate',
                          },
                        },
                      },
                    },
                  },
                },
              ],
            },
          },
          {
            stack: {
              children: [
                {
                  component: {
                    name: 'NotLogged',
                    options: {
                      bottomTab: {
                        icon: require('@assets/icons/user.png'),
                        text: 'Cá nhân',
                        textColor: color.lightGrayColor,
                        selectedTextColor: color.primary,
                        iconColor: color.lightGrayColor,
                        selectedIconColor: color.primary,
                      },
                      topBar: {
                        visible: false,
                        height: 0,
                      },
                    },
                    animations: {
                      push: {
                        waitForRender: true,
                        content: {
                          alpha: {
                            from: 0,
                            to: 1,
                            duration: 300,
                            startDelay: 0,
                            interpolation: 'accelerate',
                          },
                        },
                      },
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    },
  });
}
