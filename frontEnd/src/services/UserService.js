import {request} from '../config/Request';
import {api} from '../constant/Api';
import {shopService} from './ShopService';
import {values} from '../constant';

export const UserService = {
  async getUserInfo(userId) {
    const body = {
      id: userId,
      username: '',
    };
    const res = await request.post(api.getUserInfoById, body);
    if (res) {
      return res;
    }
  },

  async createInfo(userInfo) {
    const body = userInfo;
    const res = await request.post(api.createInfo, body);
    if (res) {
      return res;
    }
  },

  async checkPhoneNumber(phoneNumber, otp) {
    const body = {
      phoneNumber,
      otp,
    };
    const res = await request.post(api.checkPhoneNumber, body);
    if (res) {
      return res;
    }
  },
  async getCart(userId) {
    const body = {
      userId,
    };
    const res = await request.post(api.getListProductCart, body);
    if (res.errorCode === 200) {
      const data = res.result.result;
      const dataCart = [];
      for (let i = 0; i < data.length; i++) {
        const shopInfo = await shopService.getShopInfo(data[i].shopId);
        const productOrder = {
          shopId: shopInfo.id,
          shopName: shopInfo.shopName,
          shopAddress: shopInfo.address,
          listProduct: [],
        };
        const product = data[i];
        if (dataCart.length === 0) {
          productOrder.listProduct.push(product);
          dataCart.push(productOrder);
        } else {
          var indexShop = -1;
          dataCart.map((item, index) => {
            if (item.shopId === data[i].shopId) {
              indexShop = index;
            }
          });
          if (indexShop >= 0) {
            dataCart[indexShop].listProduct.push(product);
          } else {
            productOrder.listProduct.push(product);
            dataCart.push(productOrder);
          }
        }
      }
      return dataCart;
    }
  },

  async addToCart(userId, sizeId, quantity, rent, rentDue) {
    const body = {
      userId,
      sizeId,
      quantity,
      rent,
      rentDue,
    };
    const res = await request.post(api.addToCart, body);
    if (res.errorCode === 200) {
      return res;
    }
  },

  async removeProductFromCart(listProductId) {
    const body = {
      ids: listProductId,
    };
    const res = await request.post(api.removeProductFromCart, body);
    if (res) {
      return res;
    }
  },

  async getCity(name, page = 0, size = 10) {
    const res = await request.get(
      `${api.city}?name=${name}&page=${page}&size=${size}`,
    );
    if (res.errorCode === 200 && res.result) {
      return res.result.result;
    }
  },
  async getDistrict(cityCode, name, page = 0, size = 10) {
    const res = await request.get(
      `${
        api.district
      }?cityCode=${cityCode}&name=${name}&page=${page}&size=${size}`,
    );
    if (res.errorCode === 200 && res.result) {
      return res.result.result;
    }
  },
  async getVillage(districtCode, name, page = 0, size = 10) {
    const res = await request.get(
      `${
        api.village
      }?districtCode=${districtCode}&name=${name}&page=${page}&size=${size}`,
    );
    if (res.errorCode === 200 && res.result) {
      return res.result.result;
    }
  },

  async updateUserInfo(
    userId,
    dob,
    gender,
    avatar,
    coverava,
    city,
    district,
    village,
    address,
  ) {
    const body = {
      id: userId,
      dob,
      gender,
      avatar,
      coverava,
      city,
      district,
      village,
      address,
    };
    const res = await request.post(api.updateUserInfo, body);
    if (res) {
      return res;
    }
  },

  async updatePhoneNumber(userId, phoneNumber, otp) {
    const body = {
      id: userId,
      phoneNumber,
      otp,
    };
    const res = request.post(api.updatePhoneNumber, body);
    if (res) {
      return res;
    }
  },

  async createBill(data) {
    const res = await request.post(api.createBill, data);
    if (res) {
      return res;
    }
  },

  async getBill(custId, status) {
    const body = {
      custId,
      status,
    };
    const res = await request.post(api.getBillCust, body);
    if (res) {
      return res;
    }
  },

  async getBillDetail(billId) {
    const body = {
      id: billId,
    };
    const res = await request.post(api.getBillDetail, body);
    return res;
  },
  async abortBill(idBill, status, reason) {
    const body = {
      id: idBill,
      status: status,
      reason: reason,
    };
    const res = await request.post(api.changeStatusBillCust, body);
    if (res) {
      return res;
    }
  },

  async getListNotification(custId, page = 0, size = 10) {
    const url = `${
      api.getListNotificationCust
    }?custId=${custId}&page=${page}&size=${size}`;
    const res = await request.get(url);
    if (res) {
      return res;
    }
  },
  async countNotification(custId, status) {
    const url = `${
      api.countNotificationCust
    }?custId=${custId}&status=${status}`;
    const res = await request.get(url);
    if (res) {
      return res;
    }
  },

  async changeStatusNotification(idNotifi, custId) {
    const body = {
      id: idNotifi,
      custId,
    };
    await request.post(api.changeStatusNotifyCust, body);
  },

  async getListRoomChat(custId, page = 0, size = 10) {
    const url = `${
      api.getListRoomChatCust
    }?custId=${custId}&page=${page}&size=${size}`;
    const res = await request.get(url);
    if (res) {
      return res;
    }
  },

  async getQuantityChat(custId, status) {
    const url = `${
      api.getQuantityChatByCustStatus
    }?custId=${custId}&status=${status}`;
    const count = await request.get(url);
    if (count) {
      return count.result;
    }
    return 0;
  },

  async updateStatusRoomChat(roomId, status) {
    const url = `${
      api.updateStatusRoomChatCust
    }?roomId=${roomId}&status=${status}`;
    await request.get(url);
  },

  async getListPaymentBySizeProduct(sizeId) {
    const url = `${api.getListPaymentBySizeProduct}?sizeId=${sizeId}`;
    const res = request.get(url);
    return res;
  },

  async getListBankByCustId(custId) {
    const url = `${api.getListBankByCustId}?custId=${custId}`;
    const res = request.get(url);
    return res;
  },

  async addBank(body) {
    const res = request.post('bank/setBankForCust', body);
    return res;
  },

  async delteteBank(bankId) {
    const url = `${api.deleteBankCust}?bankId=${bankId}`;
    const res = await request.get(url);
    return res;
  },

  async sendFeedback(body) {
    const res = await request.post(api.sendFeedback, body);
    return res;
  },

  async getListFeedback(detailId, vote, page = 0, size = 10) {
    const url = `${
      api.getListFeedback
    }?detailId=${detailId}&vote=${vote}&page=${page}&size=${size}`;
    const res = await request.get(url);
    return res;
  },
};
