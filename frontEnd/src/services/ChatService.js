import {request} from '../config/Request';
import {api} from '../constant/Api';

const ChatService = {
  async getListMessByRoom(roomId, page = 0, size = 20) {
    const url = `${
      api.getListContent
    }?roomId=${roomId}&page=${page}&size=${size}`;
    const res = await request.get(url);
    if (res) {
      return res;
    }
  },
  async getRoomByCustShop(custId, shopId) {
    const url = `${api.getRoomByCustShop}?custId=${custId}&shopId=${shopId}`;
    const res = await request.get(url);
    if (res) {
      return res;
    }
  },

  async uploadSingleFile(uri, roomId) {
    const image = await request.uploadFile(
      'upload/singleInRoomChat',
      uri,
      roomId,
    );
    return image;
  },
};

export default ChatService;
