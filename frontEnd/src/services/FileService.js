import {request} from '../config/Request';

export const FileService = {
  async getImage() {
    const res = await request.get('uploads/logo_transparent.png');
    if (res) {
      return res;
    }
  },
  async uploadFile(data) {
    const res = await request.uploadFile('upload/single', data);
    if (res) {
      return res;
    }
  },
};
