import HomeService from './HomeService';
import AccountService from './AccountService';
import ShopService from './ShopService';
import UserService from './UserService';

export default {
  HomeService,
  AccountService,
  ShopService,
  UserService,
};
