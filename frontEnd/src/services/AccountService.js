import {request} from '../config/Request';
import {api} from '../constant/Api';

export const AccountService = {
  async signup(username, password) {
    const body = {
      username,
      password,
    };
    const res = await request.post(api.signup, body);
    if (res) {
      return res;
    }
  },

  async login(username, password) {
    const body = {
      username,
      password,
    };
    const res = await request.post(api.login, body);
    if (res) {
      return res;
    }
  },

  async getOtpWithToken(phoneNumber) {
    const url = `${api.getOtpWithToken}?phonenumber=${phoneNumber}`;
    const res = await request.getWithToken(url);
    if (res) {
      return res;
    }
  },

  async getOtp(phoneNumber) {
    const url = `${api.getOtp}?phonenumber=${phoneNumber}`;
    const res = await request.get(url);
    if (res) {
      return res;
    }
  },

  async changePass(userId, oldPassword, newPass) {
    const body = {
      id: userId,
      oldPassword,
      password: newPass,
    };
    const res = await request.postWithToken(api.changePass, body);
    if (res) {
      return res;
    }
  },
};
