import {request} from '../config/Request';
import {api} from '../constant/Api';

const ProductShopService = {
  async addNewProduct(body) {
    console.log(JSON.stringify(body));
    const res = await request.postWithToken(api.addNewProduct, body);
    if (res.status === 401) {
      return 401;
    } else {
      return res;
    }
  },
  async getDetailProduct(id) {
    const body = {
      id,
    };
    const res = await request.post(api.getDetailById, body);
    if (res) {
      return res;
    }
  },
  async updateProductInfo(productId, infoProduct) {
    const body = {
      id: productId,
      ...infoProduct,
    };
    const res = await request.post(api.updateProductInfo, body);
    if (res) {
      return res;
    }
  },
  async deleteProduct(productId) {
    const body = {
      id: productId,
    };
    const res = await request.post(api.deleteProduct, body);
    if (res) {
      return res;
    }
  },
  async updateImage(imageId, image, name) {
    const body = {
      id: imageId,
      image,
      name,
    };
    const res = await request.post(api.updateImage, body);
    if (res) {
      return res;
    }
  },
  async addNewImage(productId, images) {
    const body = {
      id: productId,
      images,
    };
    const res = await request.post(api.addNewImage, body);
    if (res) {
      return res;
    }
  },
  async updateSizeColor(sizeColorId, sizeColor) {
    const body = {
      id: sizeColorId,
      ...sizeColor,
    };
    const res = await request.post(api.updateSizeColor, body);
    if (res) {
      return res;
    }
  },
  async addNewSizeColor(productId, sizeColors) {
    const body = {
      id: productId,
      sizeColors,
    };
    const res = await request.post(api.addNewSizeColor, body);
    if (res) {
      return res;
    }
  },
  async deleteSizeColor(sizeColorId) {
    const body = {
      id: sizeColorId,
    };
    const res = await request.post(api.deleteSizeColor, body);
    if (res) {
      return res;
    }
  },
};

export default ProductShopService;
