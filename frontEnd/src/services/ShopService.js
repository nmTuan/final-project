import {request} from '../config/Request';
import {api} from '../constant/Api';

export const shopService = {
  async getShopInfoByAcc(accId) {
    const body = {
      accId,
    };
    const res = await request.post(api.shopInfoByAccId, body);
    if (res) {
      return res;
    }
  },
  async getShopInfo(shopId) {
    const body = {
      id: shopId,
    };
    const res = await request.post(api.shopInfoById, body);
    if (res.errorCode === 200) {
      return res.result;
    }
  },
  async updateInfoShop(
    shopId,
    shopName,
    description,
    avatar,
    coverave,
    phoneNumber,
    address,
  ) {
    const body = {
      id: shopId,
      shopName,
      description,
      avatar,
      coverave,
      phoneNumber,
      address,
    };
    const res = await request.post(api.updateInfoShop, body);
    if (res.errorCode === 200) {
      return res;
    }
  },

  async checkPhoneNumber(phoneNumber, otp) {
    const body = {
      phoneNumber,
      otp,
    };
    const res = await request.post(api.checkPhoneNumberShop, body);
    if (res) {
      return res;
    }
  },

  async getProductByShopId(shopId, page = 0, size = 10) {
    const body = {
      shopId,
      page,
      size,
    };
    const res = await request.post(api.getListProductByShopId, body);
    if (res) {
      return res;
    }
  },

  async getListBill(shopId, status) {
    const body = {
      shopId,
      status,
    };
    const res = await request.post(api.getListBillShop, body);
    if (res) {
      return res;
    }
  },

  async changeSatausBill(billId, status, reason, isFail = false) {
    const body = {
      id: billId,
      status,
      reason,
      isFail,
    };
    const res = await request.post(api.changeSatusShop, body);
    if (res) {
      return res;
    }
  },

  async getListNotifiShop(shopId, page = 0, size = 10) {
    const url = `${
      api.getListNotificationShop
    }?shopId=${shopId}&page=${page}&size=${size}`;
    const res = await request.get(url);
    if (res) {
      return res;
    }
  },

  async countNotification(shopId, status) {
    const url = `${
      api.countNotificationShop
    }?shopId=${shopId}&status=${status}`;
    const res = await request.get(url);
    if (res) {
      return res;
    }
  },

  async changeStatusNotify(idNotifi, idShop) {
    const body = {
      id: idNotifi,
      shopId: idShop,
    };
    await request.post(api.changeStatusNotifyShop, body);
  },

  async getListRoom(shopId, page = 0, size = 10) {
    const url = `${
      api.getListRoomChatShop
    }?shopId=${shopId}&page=${page}&size=${size}`;
    const res = await request.get(url);
    if (res) {
      return res;
    }
  },

  async countChatByShopStatus(shopId, status) {
    const url = `${
      api.countChatByShopStatus
    }?shopId=${shopId}&status=${status}`;
    const count = await request.get(url);
    if (count) {
      return count.result;
    }
    return 0;
  },

  async updateStatusRoomChat(roomId, status) {
    const url = `${
      api.updateStatusRoomChatShop
    }?roomId=${roomId}&status=${status}`;
    const res = await request.get(url);
    console.log(JSON.stringify(res));
  },

  async addBank(body) {
    const res = request.post('bank/setBankForShop', body);
    return res;
  },

  async getListBankByShopId(shopId) {
    const url = `${api.getListBankByShopId}?shopId=${shopId}`;
    const res = request.get(url);
    return res;
  },

  async deleteBank(bankId) {
    const url = `${api.deleteBankShop}?bankId=${bankId}`;
    const res = await request.get(url);
    return res;
  },
};
