import {request} from '../config/Request';

const BankService = {
  async getListBank(name = '') {
    const url = `bank/getListBank?bank=${name}`;
    var data = [];
    const res = await request.get(url);
    if (res) {
      data = res.result.result;
    }
    return data;
  },
};

export default BankService;
