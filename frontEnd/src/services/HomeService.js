import {request} from '../config/Request';
import { api } from '../constant/Api';

class HomeService {
  async getProduct(sort, page = 0, size = 10) {
    const body = {...sort, ...{page, size}};
    const res = await request.post(api.getlistProduct, body);
    if (res.errorCode === 200) {
      return res.result;
    }
  }

  async searchProductByName(productName, page = 0, size = 10) {
    const body = {
      productName,
      category_id: '',
      gender_id: '',
      age_id: '',
      sold: '',
      vote: '',
      isRent: '',
      listSort: [],
      page,
      size,
    };
    const res = await request.post(api.getlistProduct, body);
    if (res.errorCode === 200) {
      return res.result.result;
    }
  }

  async getListCategory() {
    const res = await request.get(api.getListCategory);
    var data = [];
    if (res.errorCode === 200) {
      data = res.result.result;
    }
    return data;
  }

  async getListAge() {
    const res = await request.get(api.getListAge);
    var data = [];
    if (res.errorCode === 200) {
      data = res.result.result;
    }
    return data;
  }

  async getListGender() {
    const res = await request.get(api.getListGender);
    var data = [];
    if (res.errorCode === 200) {
      data = res.result.result;
    }
    return data;
  }

  async detailProduct(id) {
    const body = {
      id,
    };
    const res = await request.post(api.productDetail, body);
    if (res.errorCode === 200) {
      return res.result;
    }
  }

  async getListPayment() {
    const res = await request.get(api.listPayment);
    var data = [];
    if (res.errorCode === 200) {
      data = res.result.result;
      // res.result.result.map(item => {
      //   item = {...item, check: false};
      //   data.push(item);
      // });
    }
    return data;
  }
}

export default new HomeService();
