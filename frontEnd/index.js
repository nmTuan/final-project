/**
 * @format
 */

import {Navigation} from 'react-native-navigation';
import {SplashScreen} from './src/navigation/Navigation';

Navigation.events().registerAppLaunchedListener(() => SplashScreen());

console.disableYellowBox = true;
